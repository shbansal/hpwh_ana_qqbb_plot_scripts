#!/bin/bash

#Submit script for gbb Tuple Ana
echo "Submit jobs for MVA Analysis......"

#OUTPATH="/afs/cern.ch/work/s/shbansal/chargedHiggs_Ana/MockOutput"
LOG_FOLDER="/cephfs/user/s6subans/ChargedHiggsLog/"

echo "Logs in: ${LOG_FOLDER}"
echo "Output in: ${OUTPATH}"

#OUTDIR="/cephfs/user/s6subans/ChargedHiggs_V2/"
#USE_BATCH_MODE=1


export EOS_MGM_URL=root://eosuser.cern.ch
echo "reading files !!! "
Tree=(
"MVATree_bbqq"
#"ChargedHiggsNtups/HpWh_Ntup_2021/"
#"/afs/cern.ch/work/s/shbansal/chargedHiggs_Ana/chargedHiggs_MC16a/"
)

File=(
#hp_3MP_UB10.root 
#hp_3MP_UB12.root
#hp17MP_wdRcond.root
#hp_17MP_MC16a.root
#hp3MP_Sugg_77p_4.root
#hp400_AFII.root_77p_4.root
#hp800_AFII.root_77p_4.root
#hp1600_AFII.root_77p_4.root
#hp700_AFII.root_77p_4.root
#hp1600_AFII.root_77p_4.root
#hp3MP_77p_4.root
#hp_17MP.root
#hp17MP_77p_4.root
#hp17MP_77p_4_wocosth.root
#hp17MP.root
hp17MP_77p_9.root
#hp3MP_100Tree.root 
#hp3MP_800Tree.root
#hp17MP_V2.root
#hp250_AFII.root_77p_4.root
#hp300_AFII.root_77p_4.root  
#hp350_AFII.root_77p_4.root 
#hp500_AFII.root_77p_4.root 
#hp600_AFII.root_77p_4.root
#hp800_AFII.root_77p_4.root
#hp900_AFII.root_77p_4.root
#hp1000_AFII.root_77p_4.root
#hp1200_AFII.root_77p_4.root
#hp1400_AFII.root_77p_4.root
#hp1800_AFII.root_77p_4.root
#hp2000_AFII.root_77p_4.root
#hp2500_AFII.root_77p_4.root
#hp3000_AFII.root_77p_4.root
#hp800_AFII.root_77p_4.root
#hp700_AFII.root_77p_4.root
#hp1600_AFII.root_77p_4.root
#hp17MP.root
)


#rm -rf cluster_pack.tar.gz
#tar -czf cluster_pack.tar.gz Makefile_batch  main_RunMVATraining.C dataset/ main/ TH1Fs/ utilis/ LatexOutput/ python/ style/

echo "sucessfuly opened tar files"
for path in "${File[@]}"
do
   for tree in "${Tree[@]}"
   do    
   condor_submit TREE="${tree}" INFILE="${path}" OUTFILE="${path}_MVA.root" /cephfs/user/s6subans/ChargedHiggsAna_V2L2/Code/run_MVA_V4.sub
   done
done
echo "all done !!! "
