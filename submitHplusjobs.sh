#!/bin/bash

#Submit script for gbb Tuple Ana
echo "Submit jobs for Charged Higgs Analysis......"

#OUTPATH="/afs/cern.ch/work/s/shbansal/chargedHiggs_Ana/MockOutput"
LOG_FOLDER="/cephfs/user/s6subans/ChargedHiggsLog/"

echo "Logs in: ${LOG_FOLDER}"
echo "Output in: ${OUTPATH}"

OUTDIR="/cephfs/user/s6subans/ChargedHiggs_V2/"
#USE_BATCH_MODE=1

TOPCON=(
#"250"
"225"
#"250"
#"275"
#"300"
#"325"
#"350"   
)

WP=(
#"77p"
"77p"
#"85p"
#"60p"
)

export EOS_MGM_URL=root://eosuser.cern.ch
echo "reading files !!! "
Paths=(  
"ChargedHiggsNtups/HpWh_Ntup_2021/MC16a/"
"ChargedHiggsNtups/HpWh_Ntup_2021/MC16d/"
"ChargedHiggsNtups/HpWh_Ntup_2021/MC16e/"
##"ChargedHiggsNtups/HpWh_Ntup_2021/MC16e/"
##"ChargedHiggsNtups/HpWh_Ntup_2021/"
#"ChargedHiggsNtups/HpWh_Ntup_2021/data_V2/"
#"/afs/cern.ch/work/s/shbansal/chargedHiggs_Ana/chargedHiggs_MC16a/"
)

File=(
#zjets.root   
#st_sc.root   
#st_tc.root   
#tH_AFII.root     
#tt_PP8.root
#tWZ.root         
#tt_PP8filtered.root
tZ.root          
#ttll.root
#ttH_PP8.root         
#wjets.root
#ttW.root             
#db.root    
#Wt.root   
#hp250_AFII.root
#hp300_AFII.root
#hp350_AFII.root
#hp400_AFII.root
#hp500_AFII.root
#hp600_AFII.root
#hp700_AFII.root
#hp800_AFII.root
#hp900_AFII.root
#hp1000_AFII.root
#hp1200_AFII.root
#hp1400_AFII.root
#hp1600_AFII.root
#hp1800_AFII.root
#hp2000_AFII.root
#hp2500_AFII.root
#hp3000_AFII.root
#tt_4FS.root
#tt_H7.root
#tt_NLO.root
#data_2015.root
#data_2016.root
#data_2017.root
#data_2018.root
)

#rm -rf cluster_pack.tar.gz
#tar -czf cluster_pack.tar.gz Makefile_batch  main_RunMVATraining.C dataset/ main/ TH1Fs/ utilis/ LatexOutput/ python/ style/

echo "sucessfuly opened tar files"
for topc in "${TOPCON[@]}"
do
   for path in "${Paths[@]}"
   do
      for file in "${File[@]}"
      do
      ##./execute $path $file $wp $OUTDIR $USE_BATCH_MODE
      condor_submit IN_PATH="${path}" FILE="${file}" WP="${WP}" OUTDIR="${OUTDIR}" TOPCON="${topc}" /cephfs/user/s6subans/ChargedHiggsAna_V2L2/Code/run_Hplus.sub
      done
   done
done

echo "all done !!! "