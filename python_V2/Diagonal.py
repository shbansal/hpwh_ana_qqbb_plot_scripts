# import sympy 
import numpy as np
from scipy.linalg import eig, inv
#A = np.matrix([[8,10,10],
              #[0,3, 0],
              #[-5,-10,-7]])
#Error Matrix for 5 jet region:              
#A = np.matrix([[5.128e-03, -9.581e+05, -5.154e-03,  5.381e-03, -7.052e-02, -8.238e-05],
               #[-9.581e+05,  1.185e+15,  6.271e+06, -1.148e+06,  3.192e+07,  2.962e+04],
               #[-5.154e-03,  6.271e+06,  3.321e-02, -6.170e-03,  1.714e-01,  1.591e-04],
               #[5.381e-03, -1.148e+06, -6.170e-03,  5.674e-03, -7.831e-02, -8.980e-05],
               #[-7.052e-02,  3.192e+07,  1.714e-01, -7.831e-02,  2.054e+00,  2.017e-03],
               #[-8.238e-05,  2.962e+04,  1.591e-04, -8.980e-05,  2.017e-03,  2.062e-06]])   

#Error Matrix for 6 jet region:
#A = np.matrix([[9.740e-05,  1.506e+05,  1.355e-03,  3.278e-04, -7.945e-03, -7.033e-06],
               #[1.506e+05,  3.679e+14,  3.240e+06,  3.559e+05, -9.219e+06, -7.627e+03],
               #[1.355e-03,  3.240e+06,  2.856e-02,  3.246e-03, -8.397e-02, -6.966e-05],
               #[3.278e-04,  3.559e+05,  3.246e-03,  3.366e-03, -3.575e-02, -4.151e-05],
               #[-7.945e-03, -9.219e+06, -8.397e-02, -3.575e-02,  8.486e-01,  7.926e-04],
               #[-7.033e-06, -7.627e+03, -6.966e-05, -4.151e-05,  7.926e-04,  7.875e-07]])

#Error Matrix for 7 jet region:     
#A = np.matrix([[5.677e-04,  4.080e+03,  5.510e-03,  1.059e-03, -9.706e-02, -8.101e-05],
               #[4.080e+03,  3.359e+10,  4.492e+04,  6.818e+03, -5.973e+05, -4.959e+02],
               #[5.510e-03,  4.492e+04,  6.012e-02,  9.275e-03, -8.152e-01, -6.771e-04],
               #[1.059e-03,  6.818e+03,  9.275e-03,  3.349e-03, -2.561e-01, -2.240e-04],
               #[-9.706e-02, -5.973e+05, -8.152e-01, -2.561e-01,  2.566e+01,  2.192e-02],
               #[-8.101e-05, -4.959e+02, -6.771e-04, -2.240e-04,  2.192e-02,  1.883e-05]])

#A = np.matrix([[1.466e-03,  7.907e+02,  1.028e-02,  8.904e-04, -1.059e-01, -7.204e-05],
               #[7.907e+02,  4.692e+08,  6.052e+03,  4.438e+02, -4.501e+04, -3.025e+01],
               #[1.028e-02,  6.052e+03,  7.813e-02,  5.810e-03, -5.975e-01, -4.021e-04],
               #[8.904e-04,  4.438e+02,  5.810e-03,  1.505e-03, -1.752e-01, -1.389e-04],
               #[-1.059e-01, -4.501e+04, -5.975e-01, -1.752e-01,  4.975e+01,  3.786e-02],
               #[-7.204e-05, -3.025e+01, -4.021e-04, -1.389e-04,  3.786e-02,  2.916e-05]]) 

A = np.matrix([[0.0039642, -2.3661e+05,  -0.0023371,   0.0041069,   -0.056743, -6.7634e-05],
               [-2.3661e+05,  1.9705e+14,  1.8879e+06, -2.9115e+05,  9.8185e+06, 8943.9],
               [-0.0023371,  1.8879e+06,    0.018104,  -0.0028685,    0.096595,   8.809e-05],
               [0.0041069, -2.9115e+05,  -0.0028685,   0.0042718,   -0.062007, -7.2595e-05],
               [-0.056743,  9.8185e+06,    0.096595,   -0.062007,    1.9234, 0.0018894],
               [-6.7634e-05,      8943.9,   8.809e-05, -7.2595e-05,   0.0018894,  1.9362e-06]]) 

#C = np.matrix([[0.00051734,       66686,   0.0035105,   0.0015244,    -0.04146, -3.5999e-05],
               #[66686,  1.4654e+13,  7.4453e+05,  1.4899e+05, -3.9596e+06,     -3296.9],
               #[0.0035105, 7.4453e+05,    0.037913,   0.0079917,    -0.21309, -0.00017795],
               #[0.0015244, 1.4899e+05,   0.0079917,   0.0074668,   -0.15012, -0.00014454],
               #[-0.04146, -3.9596e+06,    -0.21309,    -0.15012,      4.0799,   0.0036621],
               #[-3.5999e-05,     -3296.9, -0.00017795, -0.00014454,   0.0036621,  3.3627e-06]])   

#C = np.matrix([[0.00056217,      4034.1,   0.0054494,   0.0010499,   -0.096249, -8.0336e-05],
               #[4034.1,  3.3213e+10,       44418,      6741.5, -5.9059e+05,     -490.37],
               #[0.0054494,       44418,    0.059449,   0.0091735,    -0.80623, -0.00066964],
               #[0.0010499,      6741.5,   0.0091735,   0.0033334,    -0.25469, -0.00022286],
               #[-0.096249, -5.9059e+05,    -0.80623,    -0.25469,      25.536,    0.021821],
               #[-8.0336e-05,     -490.37, -0.00066964, -0.00022286,    0.021821,   1.875e-05]]) 


#C = np.matrix([[0.00055574,      3721.6,    0.005393,   0.0010235,   -0.096587,  -8.057e-05],
               #[3721.6,  2.8548e+10,       40960,      6135.7, -5.5302e+05,     -458.99],
               #[0.005393,       40960,    0.058814,   0.0089565,     -0.8099,  -0.0006724],
               #[0.0010235,      6135.7,   0.0089565,   0.0032359,    -0.25459, -0.00022262],
               #[-0.096587, -5.5302e+05,     -0.8099,    -0.25459,      26.401,    0.022549],
               #[-8.057e-05,     -458.99,  -0.0006724, -0.00022262,    0.022549,  1.9363e-05]])    

#####Adrian's Matrix#########
#C = np.matrix([[5.74196700e-04,  4.32870010e+03,  6.21478576e-03,  7.12753762e-04,   -6.49876076e-02, -5.15226820e-05],
               #[4.32870010e+03,  3.61586865e+10,  5.15477443e+04,  4.94897453e+03,   -4.24056986e+05, -3.33782137e+02],
               #[6.21478576e-03,  5.15477443e+04,  7.35273674e-02,  7.13968983e-03,  -6.14175193e-01, -4.83618635e-04],
               #[7.12753762e-04,  4.94897453e+03,  7.13968983e-03,  1.92051646e-03,  -1.23450690e-01, -1.07397929e-04],
               #[-6.49876076e-02, -4.24056986e+05, -6.14175193e-01, -1.23450690e-01,   1.35337398e+01,  1.12301716e-02],
               #[-5.15226820e-05, -3.33782137e+02, -4.83618635e-04, -1.07397929e-04,   1.12301716e-02,  9.42774994e-06]])      
                          
######Covariance Matrix for the 7j region with first 2 bin comnined from Hyp+Sigmoid function################
C = np.matrix([[0.00039487, 612.17, 0.0035272, 0.00052899, -0.079725, -6.5629e-05],
               [612.17,  1.0918e+09, 6219.9, 728.34, -1.0272e+05, -84.3],
               [0.0035272, 6219.9, 0.035472, 0.0042342, -0.6003, -0.00049278],
               [0.00052899, 728.34, 0.0042342, 0.0017232, -0.21363, -0.00018597],
               [-0.079725, -1.0272e+05, -0.6003, -0.21363, 41.14, 0.034928],
               [-6.5629e-05, -84.3, -0.00049278, -0.00018597, 0.034928, 2.979e-05]])

######Covariance Matrix for the 7j region with first 2 bin comnined from Exp+Sigmoid function######
#C = np.matrix([[0.0055487,  -0.047526, 5.66e-05, 0.0076796, -0.19528, -0.00017585],
               #[-0.047526, 1.1756, -0.001247, -0.083438, 2.6754, 0.0022018],
               #[5.66e-05, -0.001247,  1.3589e-06,  9.7784e-05,  -0.0031403, -2.5956e-06],
               #[0.0076796,   -0.083438,  9.7784e-05,    0.011205,    -0.30845, -0.00027057],
               #[-0.19528, 2.6754,  -0.0031403, -0.30845, 9.8639, 0.0083911],
               #[-0.00017585,  0.0022018, -2.5956e-06, -0.00027057, 0.0083911, 7.214e-06]])               

B = np.matrix([[0.00010378,  1.6318e+05,   0.0014952,  0.00034217,  -0.0083461, -7.3627e-06],
                  [1.6318e+05,  3.9055e+14,  3.5091e+06,  3.8502e+05, -1.0004e+07,     -8274.8],
                  [0.0014952,  3.5091e+06,    0.031563,  0.003571,  -0.092686, -7.6848e-05],
                  [0.00034217,  3.8502e+05,    0.003571,   0.0033923,   -0.036691, -4.2269e-05],
                  [-0.0083461, -1.0004e+07,   -0.092686,   -0.036691,     0.87578,  0.00081514],
                  [-7.3627e-06,     -8274.8, -7.6848e-05, -4.2269e-05,  0.00081514,  8.0629e-07]])

#D = np.matrix([[0.0021872, 1212.8, 0.015724, 0.0013525, -0.18124, -0.00012616],
                  #[1212.8,  7.1556e+08, 9228.2, 711.8, -88651, -61.532],
                  #[0.015724, 9228.2, 0.11907, 0.0092661, -1.1605, -0.00080562],
                  #[0.0013525, 711.8, 0.0092661, 0.0018269, -0.22867, -0.00017759],
                  #[-0.18124, -88651, -1.1605, -0.22867, 59.512, 0.044982],
                  #[ -0.00012616, -61.532, -0.00080562, -0.00017759, 0.044982, 3.4345e-05]])  

###############Matrix from the bin change for ge8j from Hyperbola+Sig###################
D = np.matrix([[0.0010371, 564.01, 0.0070589,  0.00067979,  -0.095328, -6.6481e-05],
                  [564.01,  3.5094e+08, 4340.4, 332.52, -40535, -28.124],
                  [0.0070589, 4340.4, 0.053742, 0.0041988, -0.51813, -0.00035961],
                  [0.00067979, 332.52, 0.0041988, 0.0014391, -0.17681, -0.00014153],
                  [-0.095328, -40535, -0.51813, -0.17681, 51.383, 0.039235],
                  [-6.6481e-05,  -28.124, -0.00035961, -0.00014153,  0.039235, 3.0277e-05]])                    
#T = np.matrix([[6,-1],
               #[2,3]])                                                                 

eVals_5j,eVecs_5j = eig(A)
#eVals_5j = np.matrix ([2.85480000e+10, 1.56893680e+01, 7.43208089e-04, 5.77132320e-05, 2.59356833e-06, 3.33791971e-08])
#eVecs_5j = np.matrix ([[-1.30362898e-07,  1.56123943e-03,  1.70733707e-02,  7.31965607e-01, 6.80547731e-01, -2.80570565e-02],
                      #[-1.00000000e+00, -1.93743986e-05, -7.14894873e-08, -1.03513317e-06, 9.67624059e-07, -9.73952847e-09],
                      #[-1.43477652e-06,  1.04788846e-03,  1.58964823e-02,  6.80772760e-01, -7.32277240e-01,  8.04342924e-03],
                      #[-2.14925739e-07,  8.65194832e-03,  9.99659732e-01, -2.32224755e-02, 2.13541845e-04,  8.13845391e-03],
                      #[1.93715847e-05, -9.99960424e-01,  8.69943551e-03,  1.64200976e-03, 2.75253085e-04, -8.35134918e-04],
                      #[1.60778338e-08, -8.70540213e-04, -7.78082859e-03,  1.52584308e-02, 2.49941636e-02,  9.99540481e-01]])
print(eVals_5j)
print(eVecs_5j)
Evariations = np.matrix([[0.000,  0.000,  0.000,  0.000, 0.000, 0.000],
                  [0.000,  0.000,  0.000,  0.000, 0.000, 0.000],
                  [0.000,  0.000,  0.000,  0.000, 0.000, 0.000],
                  [0.000,  0.000,  0.000,  0.000, 0.000, 0.000],
                  [0.000,  0.000,  0.000,  0.000, 0.000, 0.000],
                  [0.000,  0.000,  0.000,  0.000, 0.000, 0.000]])
par = np.matrix([0.616, 19204385.15, 3.317, -0.312, -7.923, -0.006]) #5j
#par = np.matrix([0.928, 20059296.7903, 3.202, 0.360, 6.313, 0.005]) #6j  
#par = np.matrix([0.841, 31098.02, 1.933, 0.190, 13.816, 0.011]) #7j from Hyp+Sig (Comb Bin)
#par = np.matrix([0.615, 3.382,  -0.0070, -0.315, -6.630, -0.005]) #7j from Exp+Sig  (Comb Bin)
#par = np.matrix([0.851, 13963.31, 1.718, 0.140, 15.249, 0.011]) #ge8j from Hyp+Sig  (Comb Bin)
#par = np.matrix([0.909, 3492431.23, 2.76, 0.312, 6.433, 0.005]) #7j_fine
#par = np.matrix([0.862, 123499.27, 2.17, 0.218, 10.79, 0.008]) #7j_caorse (DELETE ONE POINT)
#par = np.matrix([8.61973930e-01, 1.23651723e+05, 2.17576267e+00, 2.17815448e-01,  1.07833958e+01, 8.50814879e-03]) #7j_caorse (DELETE ONE POINT) ADRIAN's
#par = np.matrix([0.863, 132525.12, 2.19, 0.22, 10.64, 0.008]) #7j_caorse
#par = np.matrix([0.850, 13399.846, 1.712, 0.140, 15.310, 0.011])  #ge8j
par_mod_0 = np.matrix([0, 0, 0, 0, 0, 0])
par_mod_1 = np.matrix([0, 0, 0, 0, 0, 0])
par_mod_2 = np.matrix([0, 0, 0, 0, 0, 0])
par_mod_3 = np.matrix([0, 0, 0, 0, 0, 0])
par_mod_4 = np.matrix([0, 0, 0, 0, 0, 0])
par_mod_5 = np.matrix([0, 0, 0, 0, 0, 0])
eVecs_tp = np.transpose(eVecs_5j)
#print(eVecs_tp)
n = 0
#print(par+(np.transpose(eVecs_5j[0]).dot(np.sqrt(eVals_5j[0]))))
for val,vec in zip(eVals_5j,eVecs_tp):
    aux = vec.dot(np.sqrt(val))
    #print(aux)
    Evariations[n]=aux
    n=n+1
    #Evariations.append(aux)

par_mod_0 = par + Evariations[0]
par_mod_1 = par + Evariations[1]
par_mod_2 = par + Evariations[2]
par_mod_3 = par + Evariations[3]
par_mod_4 = par + Evariations[4]
par_mod_5 = par + Evariations[5]
print(par_mod_0)
print(par_mod_1)
print(par_mod_2)
print(par_mod_3)
print(par_mod_4)
print(par_mod_5)


#eVals_6j,eVecs_6j = eig(B)
#print(eVals_6j)
#print(eVecs_6j)

#eVals_7j,eVecs_7j = eig(C)
#print(eVals_7j)
#print(eVecs_7j)

#eVals_8j,eVecs_8j = eig(D)
#print(eVals_8j)
#print(eVecs_8j)

#from sympy import * M = Matrix([[3, -2,  4, -2],
                                #[5,  3, -3, -2],
                                #[5, -2,  2, -2],
                                #[5, -2, -3,  3]])
  
#print("Matrix : {} ".format(M))
   
## Use sympy.diagonalize() method 
#P, D = M.diagonalize()  
      
#print("Diagonal of a matrix : {}".format(D)) 
