# -*- coding: utf-8 -*-
#python
import sys
import glob
import math
import re
from ROOT import *
from array import *

gROOT.SetBatch(True)
gStyle.SetOptStat(0)
gStyle.SetPalette(1)
gROOT.LoadMacro("../style/AtlasStyle.C")
gROOT.LoadMacro("../style/AtlasUtils.C")
SetAtlasStyle()

y_axis_label="Event fraction"

c_blue   = TColor.GetColor("#3366ff")
c_red    = TColor.GetColor("#ee3311")
c_orange = TColor.GetColor("#ff9900")

def NormalizeHisto(histo):
     n_events=histo.Integral(-1,histo.GetNbinsX()+1)
     if n_events == 0:
         return
     print n_events, histo.Integral(histo.GetNbinsX(),histo.GetNbinsX()+1)
     histo.Scale(1./n_events)
     histo.SetLineWidth(2)
     histo.SetStats(0)
     histo.SetFillStyle(3001)
     histo.SetMarkerColor(histo.GetLineColor())
     histo.SetMarkerSize(0.0)
     histo.GetXaxis().SetTitleOffset(1.2)
     histo.GetYaxis().SetTitleOffset(1.52)
     histo.GetXaxis().SetLabelSize(0.05)
     histo.GetYaxis().SetLabelSize(0.05)
     histo.GetXaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetNdivisions(504)
     histo.GetXaxis().SetNdivisions(504)

c1 = TCanvas("ShapePlots","",720,720)

#for HistoName in ["nBTags"]:
for HistoName in ["mVH","pTWplus","pTH","mWplus","mH","DeltaPhi_HW","DeltaEta_HW","pTH_over_mVH","pTW_over_mVH","maxMVAResponse"]:
#for HistoName in ["mVH"]:
## for bTagStrategy in ["Incl","FourPlusTags","ThreeTags","TwoTags"]:
 for Region in ["Resolved_SR"]:
  for btagStrategy in ["FourPlusTags","ThreeTags"]:
  #for btagStrategy in ["FourPlusTags"]:        
      ReBin = False
      YAxisScale = 1.4

      if "pTWplus" in HistoName:
          Xaxis_label="Transverse Momentum Of W Boson [GeV]"
      if "pTH" in HistoName:
          Xaxis_label="Transverse Momentum of Higgs [GeV]"
      if "mVH" in HistoName:
          Xaxis_label="Mass of Charged Higgs [GeV]"
      if "mH" in HistoName:
          Xaxis_label="Mass of Higgs [GeV]"    
      if "mWplus" in HistoName:
          Xaxis_label="Mass of W Boson [GeV]"
      if "DeltaPhi_HW" in HistoName:
          Xaxis_label="#Delta #Phi_{HW}"
      if "DeltaEta_HW" in HistoName:
          Xaxis_label="#Delta #eta_{HW}"  
      if "pTH_over_mVH" in HistoName:
          Xaxis_label="p_{T,H}/m_{WH}"
      if "pTW_over_mVH" in HistoName:
          Xaxis_label="p_{T,W}/m_{WH}"      
      if "maxMVAResponse" in HistoName:
          Xaxis_label="BDT Score (Signal Reconstruction)"         
      #Xaxis_label=""

      file600      = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_SR_2022/hp600_AFII.root_77p_225.root","READ")
      dir600        = file600.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m600 = dir600.Get("hp600_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m600.SetLineColor(kBlue)
      h_sig_Hplus_m600.SetLineStyle(7) #7
      if ReBin == True:
          h_sig_Hplus_m600.Rebin(2)
       
      file1400	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_SR_2022/hp1400_AFII.root_77p_225.root","READ")
      dir1400        = file1400.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m1400 = dir1400.Get("hp1400_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m1400.SetLineColor(kBlue)
      h_sig_Hplus_m1400.SetLineStyle(7) #7
      if ReBin == True:
          h_sig_Hplus_m1400.Rebin(2)

      file_tt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_SR_2022/tt_PP8.root_77p_225.root","READ")
      dir_tt_1b   = file_tt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1b")
      h_ttbar_background_1b = dir_tt_1b.Get("tt_PP8_1B_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      dir_tt_1c   = file_tt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1c")
      h_ttbar_background_1c = dir_tt_1c.Get("tt_PP8_1C_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      dir_tt_1l   = file_tt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1l")
      h_ttbar_background_1l = dir_tt_1l.Get("tt_PP8_1L_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      
      file_tt_filt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_SR_2022/tt_PP8filtered.root_77p_225.root","READ")
      dir_tt_1b_filt   = file_tt_filt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1b")
      h_ttbar_background_1b_filt = dir_tt_1b_filt.Get("tt_PP8filtered_1B_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      dir_tt_1c_filt   = file_tt_filt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1c")
      h_ttbar_background_1c_filt = dir_tt_1c_filt.Get("tt_PP8filtered_1C_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      dir_tt_1l_filt   = file_tt_filt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1l")
      h_ttbar_background_1l_filt = dir_tt_1l_filt.Get("tt_PP8filtered_1L_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

    
      h_ttbar_background_1l = h_ttbar_background_1l + h_ttbar_background_1l_filt
      h_ttbar_background_1c = h_ttbar_background_1c + h_ttbar_background_1c_filt
      h_ttbar_background_1b = h_ttbar_background_1b + h_ttbar_background_1b_filt

      #h_ttbar_background_1c.Scale(1.22)
      #h_ttbar_background_1b.Scale(1.40)
      #h_ttbar_background_1l.Scale(0.93)
      h_ttbar_background = h_ttbar_background_1b + h_ttbar_background_1c + h_ttbar_background_1l     

      h_ttbar_background.SetLineColor(kGreen)
      h_ttbar_background.SetLineStyle(7) #3
      if ReBin == True:
          h_ttbar_background.Rebin(2)

      file3   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_SR_2022/wjets_77p_225.root","READ")
      dir3    = file3.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_W_background = dir3.Get("wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_W_background.SetLineWidth(8)
      h_W_background.SetLineColor(kMagenta)
      h_W_background.SetLineStyle(7) #3
      if ReBin == True:
          h_W_background.Rebin(2)
      
      #file4   = TFile.Open("../PlotFiles/ForOptimisation/diboson_70p.root","READ")
      file4   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_SR_2022/db.root_77p_225.root","READ")
      dir4    = file4.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      #dir4    = file4.GetDirectory("Nominal").GetDirectory(HistoName)
      h_diboson_background = dir4.Get("db_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_diboson_background.SetLineWidth(8)
      h_diboson_background.SetLineColor(kMagenta)
      h_diboson_background.SetLineStyle(7) #3
      if ReBin == True:
          h_diboson_background.Rebin(2)

      #file5   = TFile.Open("../PlotFiles/ForOptimisation/singleTop_70p.root","READ")
      file5   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_SR_2022/st_sc.root_77p_225.root","READ")
      dir5    = file5.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      #dir5    = file5.GetDirectory("Nominal").GetDirectory(HistoName)
      h_singleTop_background_sc = dir5.Get("st_sc_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_singleTop_background.SetLineWidth(8)

      ##h_singleTop_background_sc.SetLineColor(kMagenta)
      ##h_singleTop_background_sc.SetLineStyle(3)  #3
      ##if ReBin == True:
          ##h_singleTop_background_sc.Rebin(2)

      #file5   = TFile.Open("../PlotFiles/ForOptimisation/singleTop_70p.root","READ")
      file9   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_SR_2022/st_tc.root_77p_225.root","READ")
      dir9   = file9.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      #dir5    = file5.GetDirectory("Nominal").GetDirectory(HistoName)
      h_singleTop_background_tc = dir9.Get("st_tc_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_singleTop_background.SetLineWidth(8)

      file_Wt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_SR_2022/Wt.root_77p_225.root","READ")
      dir_Wt   = file_Wt.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_Wt = dir_Wt.Get("Wt_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      file_tH   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_SR_2022/tH_AFII.root_77p_225.root","READ")
      dir_tH   = file_tH.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_tH = dir_tH.Get("tH_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      file_tWZ   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_SR_2022/tWZ.root_77p_225.root","READ")
      dir_tWZ   = file_tWZ.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_tWZ = dir_tWZ.Get("tWZ_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      file_tZ   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_SR_2022/tZ.root_77p_225.root","READ")
      dir_tZ   = file_tZ.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_tZ = dir_tZ.Get("tZ_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      h_singleTop_background = h_singleTop_background_tc + h_singleTop_background_sc + h_Wt + h_tH + h_tWZ + h_tZ
      h_singleTop_background.SetLineColor(kMagenta)
      h_singleTop_background.SetLineStyle(7)  #3
      if ReBin == True:
          h_singleTop_background.Rebin(2) 

      file_ttW   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_SR_2022/ttW.root_77p_225.root","READ")
      #file_tt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_290121/ttbar_70p.root","READ")
      dir_ttW    = file_ttW.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_ttW_background = dir_ttW.Get("ttW_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttW_background.SetLineColor(kMagenta)
      h_ttW_background.SetLineStyle(7) #3
      if ReBin == True:
          h_ttW_background.Rebin(2)

      file_ttH   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_SR_2022/ttH.root_77p_225.root","READ")
      #file_tt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_290121/ttbar_70p.root","READ")
      dir_ttH    = file_ttH.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_ttH_background = dir_ttH.Get("ttH_PP8_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")    
      h_ttH_background.SetLineColor(kMagenta)
      h_ttH_background.SetLineStyle(7) #3
      if ReBin == True:
          h_ttH_background.Rebin(2)

      #file8   = TFile.Open("../PlotFiles/ForOptimisation/Zjets_70p.root","READ")
      file8   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_SR_2022/zjets_77p_225.root","READ")
      dir8    = file8.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      #dir8    = file8.GetDirectory("Nominal").GetDirectory(HistoName)
      h_Z_background = dir8.Get("zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_Z_background.SetLineWidth(8)
      h_Z_background.SetLineColor(kMagenta)
      h_Z_background.SetLineStyle(7)  #3
      if ReBin == True:
          h_Z_background.Rebin(2)  

      #file8   = TFile.Open("../PlotFiles/ForOptimisation/Zjets_70p.root","READ")
      file_ttZ   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_SR_2022/ttll.root_77p_225.root","READ")
      dir_ttZ    = file_ttZ.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      #dir8    = file8.GetDirectory("Nominal").GetDirectory(HistoName)
      h_ttZ_background = dir_ttZ.Get("ttll_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_Z_background.SetLineWidth(8)
      h_ttZ_background.SetLineColor(kMagenta)
      h_ttZ_background.SetLineStyle(7)  #3
      if ReBin == True:
          h_ttZ_background.Rebin(2)          
      
      h_other_background = h_diboson_background + h_W_background + h_Z_background + h_singleTop_background + h_ttH_background + h_ttW_background + h_ttZ_background
      h_total_background = h_other_background + h_ttbar_background
      h_other_background.SetLineColor(kRed)
      h_other_background.SetLineStyle(7)  #3
      if ReBin == True:
          h_other_background.Rebin(2)

      nbins=20
      ymax=0
      #text_file_tot = open("./txt/h_total_background_"+HistoName+"_"+btagStrategy+".txt","w")
      #text_file_tot.write("Bin Yields\n")
      #nbins_tot = h_total_background.GetNbinsX()
      #for i in range(1, nbins_tot+1):
         #text_file_tot.write(str(i)+" ")
         #text_file_tot.write(str(h_total_background.Integral(i-1,i))+"\n")    
      
      text_file_sig = open("./txt/h_signif_"+HistoName+"_"+btagStrategy+"_600GeV.txt","w")
      text_file_sig.write("Bin Sig\n")
      nbins_sig = h_sig_Hplus_m600.GetNbinsX()
      for i in range(1, nbins_sig+1):
         text_file_sig.write(str(i)+"&")
         if(h_total_background.Integral(i-1,i)==0):   
           text_file_sig.write("Not Defined ")  
         else:
           text_file_sig.write(str(h_sig_Hplus_m600.Integral(i-1,i)/h_total_background.Integral(i-1,i)))
         text_file_sig.write("\\\ \hline"+"\n")

      NormalizeHisto(h_total_background)
      if ymax<h_total_background.GetMaximum():
          ymax=h_total_background.GetMaximum()
      NormalizeHisto(h_sig_Hplus_m600)
      if ymax<h_sig_Hplus_m600.GetMaximum():
          ymax=h_sig_Hplus_m600.GetMaximum()

      h_total_background.Draw("HIST")
      #h_ttbar_background.Draw("HISTSAME")
      #h_sig_Hplus_m400.Draw("HISTSAME")
      #h_sig_Hplus_m800.Draw("HISTSAME")
      #h_sig_Hplus_m2000.Draw("HISTSAME")
      #h_sig_Hplus_m2500.Draw("HISTSAME")
      #h_sig_Hplus_m3000.Draw("HISTSAME")
      h_sig_Hplus_m600.Draw("HISTSAME")
      #h_sig_Hplus_m2000.Draw("HISTSAME")
      #h_sig_Hplus_m1200.Draw("HISTSAME")

      if HistoName in "maxMVAResponse":
         leg = TLegend(0.25,0.55,0.825,0.755)
      else:
         leg = TLegend(0.60,0.60,0.955,0.875)
      #ATLAS_LABEL(0.50,0.875,"Simulation",1,0.19);
      ATLAS_LABEL(0.20,0.875,"Internal",1,0.19)
      leg.SetShadowColor(kWhite)
      leg.SetFillColor(kWhite)
      leg.SetLineColor(kWhite)
      
      #leg.AddEntry(h_sig_Hplus_m400,    "H^{+}#rightarrow W^{+}h(m_{H^{+}} = 400GeV)","L")
      #leg.AddEntry(h_sig_Hplus_m800,    "H^{+}#rightarrow W^{+}h(m_{H^{+}} = 800GeV)","L")
      #leg.AddEntry(h_sig_Hplus_m2500,    "H^{+}#rightarrow W^{+}h(m_{H^{+}} = 3000 GeV)","L")
      leg.AddEntry(h_sig_Hplus_m600,   "H^{+}#rightarrow W^{+}h(m_{H^{+}} = 600GeV)","L")
      #leg.AddEntry(h_sig_Hplus_m2000,   "H^{+}#rightarrow W^{+}h(m_{H^{+}} = 2000GeV)","L")
      #leg.AddEntry(h_sig_Hplus_m1200,   "H^{+}#rightarrow W^{+}h(m_{H^{+}} = 1200GeV)","L")
      #leg.AddEntry(h_ttbar_background,  "t#bar{t}","L")
      #leg.AddEntry(h_other_background,  "other backgrounds","L")
      leg.AddEntry(h_total_background,  "total backgrounds","L")
      leg.SetTextSize(0.0250)
      leg.Draw()
      h_total_background.GetXaxis().SetTitle(Xaxis_label)
      h_total_background.GetYaxis().SetTitle("Normalised Entries")
      h_total_background.GetYaxis().SetRangeUser(0.001,round(ymax*1.25,3)+0.001)
      #h_sig_Hplus_m400.GetXaxis().SetTitle(Xaxis_label)
      #h_sig_Hplus_m400.GetYaxis().SetRangeUser(0.001,round(ymax*1.25,3)+0.001)
      #pad1.cd()
      #if "ThreeTags" in HistoName:
      myText(0.20,0.825,1,"at least 5 jets, 3 b-tags, SR")
      #if "FourPlusTags" in HistoName:
      #myText(0.20,0.825,1,"at least 5 jets, at least 4 b-tags, SR")   
      c1.RedrawAxis()
      c1.Update()
      c1.RedrawAxis()
      c1.SaveAs("../Plots/ShapePlot_%s_qqbb_Kinem_600Sig_Blind.pdf" % (HistoName+"_"+btagStrategy))
     