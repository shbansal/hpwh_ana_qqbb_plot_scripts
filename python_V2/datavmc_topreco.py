# -*- coding: utf-8 -*-
#python
import ROOT
import sys
import glob
import math
import re
from ROOT import *
from array import *

#from ROOT import rootpy.plotting

import numpy as np
from numpy import ndarray
import array
#from rootpy.plotting import Hist, HistStack, Legend, Canvas
#from rootpy.plotting.style import get_style, set_style
#from rootpy.plotting.utils import draw
#from rootpy.interactive import wait
#import rootpy.plotting.root2matplotlib as rplt
from ROOT import TCanvas, TFile, TPad, THStack, TLine,TGraphAsymmErrors, TNtuple, TH1F, TH2F
#from ROOT import TGraphAsymmErrors
from array import *
#from ROOT import TLatex
#import ROOT
#import array

gROOT.SetBatch(True)
gStyle.SetOptStat(0)
gStyle.SetPalette(1)
gROOT.LoadMacro("../style/AtlasStyle.C")
gROOT.LoadMacro("../style/AtlasUtils.C")
SetAtlasStyle()

y_axis_label="Event fraction"

c_blue   = TColor.GetColor("#3366ff")
c_red    = TColor.GetColor("#ee3311")
c_orange = TColor.GetColor("#ff9900")

def NormalizeHisto(histo):
     n_events=histo.Integral(-1,histo.GetNbinsX()+1)
     if n_events == 0:
         return
     print n_events, histo.Integral(histo.GetNbinsX(),histo.GetNbinsX()+1)
     histo.Scale(1./n_events)
     histo.SetLineWidth(2)
     histo.SetStats(0)
     histo.SetFillStyle(3001)
     histo.SetMarkerColor(histo.GetLineColor())
     histo.SetMarkerSize(0.0)
     histo.GetXaxis().SetTitleOffset(1.2)
     histo.GetYaxis().SetTitleOffset(1.52)
     histo.GetXaxis().SetLabelSize(0.05)
     histo.GetYaxis().SetLabelSize(0.05)
     histo.GetXaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetNdivisions(504)
     histo.GetXaxis().SetNdivisions(504)

c1 = TCanvas("DatavMC-qqbb","",700,500)
pad1 = TPad("pad1", "pad1", 0.0, 0.35, 1,1)
#pad1.SetLogy()
pad2 = TPad("pad2", "pad2", 0, 0.0, 1,0.35)
pad1.SetLeftMargin(0.15)
pad1.SetTopMargin(0.1)
pad1.SetBottomMargin(0.0105)
pad1.SetRightMargin(0.08)
pad1.Draw()
pad1.SetTicks()

pad2.SetLeftMargin(0.15)
pad2.SetTopMargin(0.04)
pad2.SetBottomMargin(0.35)
pad2.SetRightMargin(0.08)
pad2.Draw()
pad2.SetTicks()
HistoNameR = "lep_top_mass"
#HistoNameR_d = "lep_top_mass"
#HistoName_d = "Leptonic_Top_mass"
#for HistoName in ["nBTags"]:
#for HistoName in ["nBTags","nJets","HT","HT_bjets","DeltaPhi_HW","mVH","mH","pTWplus","pTH","maxMVAResponse", "mass_resolution"]: # for resolved, histoname set
#for HistoName in ["nJets","HT","HT_bjets","mVH","mH","pTWplus","pTH","DeltaPhi_HW","pTH_over_mVH","pTW_over_mVH","DeltaEta_HW"]:
#for HistoName in ["mVH","nJet","DeltaPhi_HW","DeltaEta_HW","maxMVAResponse"]:
#for HistoName in ["maxMVAResponse","DeltaEta_HW","DeltaPhi_HW"]: 
#for HistoName in ["nJet"]:
#for HistoName in ["HT","pTWplus","pTH","nJet","mVH"]: 
#for HistoName in ["Leptonic_Top_mass", "Leptonic_Top_pT","Leptonic_Top_mass_minpT"]: 
for HistoName in ["Leptonic_Top_mass"]:        
#for HistoName in ["mVH"]:
#for HistoName in ["nJet","maxMVAResponse","DeltaPhi_HW","pTH_over_mVH","pTW_over_mVH","DeltaEta_HW"]:
#for HistoName in ["DeltaEta_HW"]: 
#for HistoName in ["Eta_j1j2","Eta_j1j3","Eta_j1j4","Eta_j1j5","Eta_j1j6","Eta_j1j7","Eta_j2j3","Eta_j2j4","Eta_j2j5","Eta_j2j6","Eta_j2j7","Eta_j3j4","Eta_j3j5","Eta_j3j6","Eta_j3j7","Eta_j4j5","Eta_j4j6","Eta_j4j7","Eta_j5j6","Eta_j5j7","Eta_j6j7"]:
#for HistoName in ["maxMVAResponse_15","maxMVAResponse_10","maxMVAResponse_19","maxMVAResponse_18"]: 
## for bTagStrategy in ["Incl","FourPlusTags","ThreeTags","TwoTags"]:
 for Region in ["Resolved_SR"]:
  #for btagStrategy in ["FourPlusTags","ThreeTags"]:
  for btagStrategy in ["Inclusive","TwoTags","ThreeTags","FourPlusTags"]:    
     
      ReBin = False
      YAxisScale = 1.4
      
      if "Eta_j1j2" in HistoName:
          Xaxis_label="\eta_{j1j2}"
      if "Eta_j1j3" in HistoName:
          Xaxis_label="\eta_{j1j3}"
      if "Eta_j1j4" in HistoName:
          Xaxis_label="\eta_{j1j4}"
      if "Eta_j1j5" in HistoName:
          Xaxis_label="\eta_{j1j5}"
      if "Eta_j1j6" in HistoName:
          Xaxis_label="\eta_{j1j6}"
      if "Eta_j1j7" in HistoName:
          Xaxis_label="\eta_{j1j7}" 
      if "Eta_j2j3" in HistoName:
          Xaxis_label="#eta_{j2j3}"
      if "Eta_j2j4" in HistoName:
          Xaxis_label="#eta_{j2j4}"
      if "Eta_j2j5" in HistoName:
          Xaxis_label="#eta_{j2j5}"
      if "Eta_j2j6" in HistoName:
          Xaxis_label="#eta_{j2j6}"
      if "Eta_j2j7" in HistoName:
          Xaxis_label="#eta_{j2j7}"
      if "Eta_j3j4" in HistoName:
          Xaxis_label="#eta_{j3j4}"
      if "Eta_j3j5" in HistoName:
          Xaxis_label="#eta_{j3j5}"
      if "Eta_j3j6" in HistoName:
          Xaxis_label="#eta_{j3j6}"
      if "Eta_j3j7" in HistoName:
          Xaxis_label="#eta_{j3j7}" 
      if "Eta_j4j5" in HistoName:
          Xaxis_label="#eta_{j4j5}"
      if "Eta_j4j6" in HistoName:
          Xaxis_label="#eta_{j4j6}"
      if "Eta_j4j7" in HistoName:
          Xaxis_label="#eta_{j4j7}"
      if "Eta_j5j6" in HistoName:
          Xaxis_label="#eta_{j5j6}"
      if "Eta_j5j7" in HistoName:
          Xaxis_label="#eta_{j5j7}"
      if "Eta_j6j7" in HistoName:
          Xaxis_label="#eta_{j6j7}"               

      if "nBTags" in HistoName:
          Xaxis_label="b-tag multiplicity"
      if "nJet" in HistoName:
          Xaxis_label="Jet multiplicity"
      if "DeltaPhi_HW" in HistoName:
          Xaxis_label="#Delta #Phi_{HW}"
      if "DeltaEta_HW" in HistoName:
          Xaxis_label="#Delta #eta_{HW}"
      if  HistoName == "pTH_over_mVH":
          Xaxis_label="pT_{H}/m_{VH}"
      if  HistoName == "pTW_over_mVH":
          Xaxis_label="pT_{W}/m_{VH}"            
      if  HistoName == "pTH":
          Xaxis_label="Transverse momentum of Higgs [GeV]"
      if  HistoName == "pTWplus":
          Xaxis_label="Transverse momentum of W Boson [GeV]"
      if HistoName == "mVH":
          Xaxis_label="Mass of charged Higgs [GeV]"
      if "mH" in HistoName:
          Xaxis_label="Mass of Higgs [GeV]"
      if "mass_resolution" in HistoName:
          Xaxis_label="Mass resolution [GeV]"
      if "HT" in HistoName:
          Xaxis_label="H_{T} (Transverse momentum sum of jets) [GeV]"
      if "HT_bjets" in HistoName:
          Xaxis_label="H_{T_{b-jets}} (Transverse momentum sum of b-jets) [GeV]"
      if "maxMVAResponse" in HistoName:
          Xaxis_label="BDT score (Signal reconstruction)"
      if "maxMVAResponse_15" in HistoName:
          Xaxis_label="BDT score (Signal reco)"
      if "maxMVAResponse_10" in HistoName:
          Xaxis_label="BDT score (Signal reco)" 
      if "Leptonic_Top_mass" in HistoName:
          Xaxis_label="Leptonic Top mass [GeV]" 
      if "Leptonic_Top_pT" in HistoName:
          Xaxis_label="Leptonic Top pT [GeV]"                 
      #Xaxis_label=""
      
      #file_data1518       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_1512/data1518_70p.root","READ")
      #dir_1518        = file_data1518.GetDirectory("Nominal").GetDirectory(HistoName)
      #h_data1518 = dir_1518.Get("data15_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      #file1       = TFile.Open("../PlotFiles/ForOptimisation/sig_Hplus_Wh_m400-0_70p.root","READ")
      #file_data15       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_ResCR2808/data15.root_70p_MC16a.root","READ")
      file_data15       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/BkgDat_PSel_LepTop/data_2015.root_77p_225_.root","READ")
      #file_data15       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_290121/data15.root_70p_.root","READ")
      dir_15        = file_data15.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_data15 = dir_15.Get("data_2015_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_data15.GetYaxis.SetRange(0,10e6)
      #h_sig_Hplus_m40.SetLineColor(kRed)
      #h_sig_Hplus_m400.SetLineStyle(7)
      #if ReBin == True:
          #h_sig_Hplus_m400.Rebin(2)


      file_data16       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/BkgDat_PSel_LepTop/data_2016.root_77p_225_.root","READ")  
      #file_data16       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_290121/data16.root_70p_.root","READ")  
      #file_data16       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_ResCR2808/data16.root_70p_MC16a.root","READ")
      dir_16        = file_data16.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_data16 = dir_16.Get("data_2016_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_data16.GetYaxis.SetRange(0,10e6)

      #file_data17       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/data17.root_70p_MC16d.root","READ")
      #file_data17       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_290121/data17.root_70p_.root","READ")
      file_data17       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/BkgDat_PSel_LepTop/data_2017.root_77p_225_.root","READ")
      dir_17        = file_data17.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_data17 = dir_17.Get("data_2017_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_data17.GetYaxis.SetRangeUser(0,10e6)

      file_data18      = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/BkgDat_PSel_LepTop/data_2018.root_77p_225_.root","READ")
      #file_data18      = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_290121/data18.root_70p_.root","READ")
      dir_18        = file_data18.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_data18 = dir_18.Get("data_2018_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_data18.GetYaxis.SetRangeUser(0,10e6)
      
        
      #file2   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/ttbar_70p.root","READ")
      #file_tt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_1512/ttbar.root_70p_MC16e.root","READ")
      #file_tt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_NewTrain_nobias_leptopreco/tt_PP8.root_70p_250.root","READ")
      file_tt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/BkgDat_PSel_LepTop/tt_PP8.root_77p_225.root","READ")
      #file_tt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_290121/ttbar_70p.root","READ")
      dir_tt_1b   = file_tt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1b")
      h_ttbar_background_1b = dir_tt_1b.Get("tt_PP8_1B_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      dir_tt_1c   = file_tt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1c")
      h_ttbar_background_1c = dir_tt_1c.Get("tt_PP8_1C_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      dir_tt_1l   = file_tt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1l")
      h_ttbar_background_1l = dir_tt_1l.Get("tt_PP8_1L_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_ttbar_background_mc16d = dir_tt.Get("tt_PP8_mc16d_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_ttbar_background_mc16e = dir_tt.Get("tt_PP8_mc16e_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_ttbar_background = h_ttbar_background_mc16a + h_ttbar_background_mc16d + h_ttbar_background_mc16e
      #h_ttbar_background.GetYaxis.SetRangeUser(0,10e6)
      #h_ttbar_background.SetFillStyle(solid)
      #h_ttbar_background.SetFillColor(kGreen)
      h_ttbar_background = h_ttbar_background_1b + h_ttbar_background_1c + h_ttbar_background_1l
      #h_ttbar_background_1l.SetFillColor(kBlue-7)
      h_ttbar_background_1l.SetFillColor(kWhite)
      h_ttbar_background_1l.SetLineColor(kBlack)
      #h_ttbar_background.Scale(139*1000000)
      h_ttbar_background_1l.SetLineWidth(1)
      if ReBin == True:
          h_ttbar_background_1l.Rebin(2)

      h_ttbar_background_1c.SetFillColor(kBlue-10)
      h_ttbar_background_1c.SetLineColor(kBlack)
      #h_ttbar_background.Scale(139*1000000)
      h_ttbar_background_1c.SetLineWidth(1)
      if ReBin == True:
          h_ttbar_background_1c.Rebin(2)

      h_ttbar_background_1b.SetFillColor(kBlue-3)
      h_ttbar_background_1b.SetLineColor(kBlack)
      #h_ttbar_background.Scale(139*1000000)
      h_ttbar_background_1b.SetLineWidth(1)
      if ReBin == True:
          h_ttbar_background_1b.Rebin(2)         

    
      #file_Wjet   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_NewTrain_neglep/wjets_70p_250.root","READ")
      #dir_Wjet    = file_Wjet.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      #h_W_background = dir_Wjet.Get("wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_W_background.SetFillColor(kRed-3)
      #h_W_background.SetLineColor(kBlack)
      #h_W_background.SetLineWidth(1)
      #if ReBin == True:
          #h_W_background.Rebin(2)
      
      #file4   = TFile.Open("../PlotFiles/ForOptimisation/diboson_70p.root","READ")
      file_dib   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/BkgDat_PSel_LepTop/db.root_77p_225.root","READ")
      #file_dib   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_290121/diboson_70p.root","READ")
      #file_dib   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_1512/diboson.root_70p_MC16e.root","READ")
      dir_dib   = file_dib.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_diboson_background = dir_dib.Get("db_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      #file5   = TFile.Open("../PlotFiles/ForOptimisation/singleTop_70p.root","READ")
      file_st_tc   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/BkgDat_PSel_LepTop/st_tc.root_77p_225.root","READ")
      dir_st_tc   = file_st_tc.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_singleTop_background_tc = dir_st_tc.Get("st_tc_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      
      file_Wt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/BkgDat_PSel_LepTop/Wt.root_77p_225.root","READ")
      dir_Wt   = file_Wt.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_Wt = dir_Wt.Get("Wt_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      file_tH   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/BkgDat_PSel_LepTop/tH_AFII.root_77p_225.root","READ")
      dir_tH   = file_tH.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_tH = dir_tH.Get("tH_AFII_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      file_tWZ   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/BkgDat_PSel_LepTop/tWZ.root_77p_225.root","READ")
      dir_tWZ   = file_tWZ.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_tWZ = dir_tWZ.Get("tWZ_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      file_tZ   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/BkgDat_PSel_LepTop/tZ.root_77p_225.root","READ")
      dir_tZ   = file_tZ.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_tZ = dir_tZ.Get("tZ_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      file_st_sc   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/BkgDat_PSel_LepTop/st_sc.root_77p_225.root","READ")
      dir_st_sc   = file_st_sc.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_singleTop_background_sch = dir_st_sc.Get("st_sc_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      file_Zjet   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/BkgDat_PSel_LepTop/zjets_77p_225.root","READ")
      dir_Zjet    = file_Zjet.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_Z_background = dir_Zjet.Get("zjets_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")   

      file_Wjet   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/BkgDat_PSel_LepTop/wjets_77p_225.root","READ")
      dir_Wjet    = file_Wjet.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_W_background = dir_Wjet.Get("wjets_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      
      h_nontt_background = h_singleTop_background_tc + h_singleTop_background_sch + h_Wt + h_tH + h_tWZ + h_tZ + h_Z_background + h_W_background + h_diboson_background
      h_nontt_background.SetFillColor(kYellow)
      h_nontt_background.SetLineColor(kBlack)
      #h_singleTop_background.Scale(139*1000000)
      h_nontt_background.SetLineWidth(1)
      if ReBin == True:
          h_nontt_background.Rebin(2)
      
      file_ttW   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/BkgDat_PSel_LepTop/ttW.root_77p_225.root","READ")     
      dir_ttW    = file_ttW.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_ttW_background = dir_ttW.Get("ttW_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      file_ttZ   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/BkgDat_PSel_LepTop/ttll.root_77p_225.root","READ")     
      dir_ttZ    = file_ttZ.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_ttZ_background = dir_ttZ.Get("ttll_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      file_ttH   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/BkgDat_PSel_LepTop/ttH.root_77p_225.root","READ")
      dir_ttH    = file_ttH.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_ttH_background = dir_ttH.Get("ttH_PP8_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      h_ttV_background = h_ttW_background + h_ttZ_background + h_ttH_background
      h_ttV_background.SetFillColor(kGreen-7)
      h_ttV_background.SetLineColor(kBlack)
      h_ttV_background.SetLineWidth(1)
      if ReBin == True:
          h_ttV_background.Rebin(2)
      

      #stack = HistStack([h_ttbar_background, h_W_background,h_singleTop_background, h_Z_background], drawstyle='HIST E1 X0')
      h_data = h_data15 + h_data16 + h_data17 + h_data18
      #h_data =  h_data1518
      #h_data = h_data15 + h_data16
      #h_data = h_data18
            
      #h_data = h_data17
      h_data.SetMarkerStyle(20)
      h_data.SetMarkerSize(1.0)
      h_data.SetMarkerColor(1)
      stack = THStack()

      #stack.Add(h_sig_Hplus_m400)
      #stack.Add(h_sig_Hplus_m800)
      #stack.Add(h_sig_Hplus_m1600)  

      #stack.Add(h_W_background)
      stack.Add(h_nontt_background)
      stack.Add(h_ttV_background)
      #stack.Add(h_ttW_background)
      #stack.Add(h_ttH_background)
      #stack.Add(h_ttbar_background)
      stack.Add(h_ttbar_background_1b)
      stack.Add(h_ttbar_background_1c)
      stack.Add(h_ttbar_background_1l)
      # plot with ROOT
      #canvas = Canvas(width=700, height=500)
      #draw([stack, h3], xtitle= Xaxis_label, ytitle='Events', pad=canvas)
      #draw(stack, xtitle= Xaxis_label, ytitle='Events', pad=c1)
      # set the number of expected legend entries
      #stack.Scale(139*1000000)
      pad1.cd()
      
      #pad1.SetMaximum(h_data.GetYaxis())
      #y2 = h_ttbar_background.GetMaximum()+(h_ttbar_background.GetMaximum()*0.8)
      if "TwoTags" in btagStrategy:
         stack.SetMaximum(stack.GetMaximum()*1.3)
      if "ThreeTags" in btagStrategy :
         stack.SetMaximum (stack.GetMaximum()*1.5)
      if "FourPlusTags" in btagStrategy :
         stack.SetMaximum (stack.GetMaximum()*1.5)

      stack.Draw("HIST")
      h_data.Draw("EP SAME")
      
      stack.GetYaxis().SetTitleOffset(0.9)
      stack.GetYaxis().SetTitle("Events")
      nbins=20
      ymax=0
      #h_sig_Hplus_m400.Draw("HISTSAME")
      #h_sig_Hplus_m800.Draw("HISTSAME")
      #h_sig_Hplus_m1600.Draw("HISTSAME")
      #yield_H800 = h_sig_Hplus_m800.Integral()
      #yield_H400 = h_sig_Hplus_m400.Integral()
      #yield_H1600 = h_sig_Hplus_m1600.Integral()
      
      #print yield_H400
      #print yield_H800
      #print yield_H1600

      #yield_tt = h_ttbar_background.Integral()
      #str_yield_tt = str(round(yield_tt,1))
      yield_tt_1b = h_ttbar_background_1b.Integral()
      
      str_yield_tt_1b = str(round(yield_tt_1b,1))
      yield_tt_1c = h_ttbar_background_1c.Integral()
      
      str_yield_tt_1c = str(round(yield_tt_1c,1))
      yield_tt_1l = h_ttbar_background_1l.Integral()
      
      str_yield_tt_1l = str(round(yield_tt_1l,1))

      yield_ttV = h_ttV_background.Integral()
      
      str_yield_ttV = str(round(yield_ttV,1))
      yield_nontt = h_nontt_background.Integral()
      
      str_yield_nontt = str(round(yield_nontt,1))
      #yield_ttH = h_ttH_background.Integral()
      
      #str_yield_ttH = str(round(yield_ttH,1))
      #yield_ttW = h_ttW_background.Integral()
      
      #str_yield_ttW = str(round(yield_ttW,1))
      yield_Total = yield_tt_1b+yield_tt_1c+yield_tt_1l+yield_ttV+yield_nontt
      
      str_yield_Total = str(round(yield_Total,1))

      print yield_tt_1l
      print yield_tt_1c
      print yield_tt_1b
      print yield_ttV
      print yield_nontt
      print yield_Total
      yield_Data = h_data.Integral()
      str_yield_Data = str(round(yield_Data,1))
      #if "ThreeTags" or "FourPlusTags" in btagStrategy:
        #leg = TLegend(0.70,0.45,0.825,0.860)
      #else :
        #leg = TLegend(0.65,0.45,0.825,0.855)
      if "maxMVAResponse" in HistoName:  
         leg = TLegend(0.25,0.40,0.425,0.805)  
      elif "DeltaPhi_HW" in HistoName:  
         leg = TLegend(0.25,0.40,0.425,0.805)   
      elif "nJet" in HistoName:  
         leg = TLegend(0.74,0.25,0.875,0.655)   
      else:
         leg = TLegend(0.72,0.25,0.885,0.685) 
         #leg = TLegend(0.65,0.45,0.825,0.855)  
      ATLAS_LABEL(0.45,0.80,"work-in-progress",1,0.09)
      #myText(0.785,0.825,1,"139 fb^{-1}")
      
      leg.SetShadowColor(kWhite)
      leg.SetFillColor(kWhite)
      leg.SetLineColor(kWhite)
      leg.SetNColumns(2)
      leg.AddEntry(h_data,"Data","epl")
      leg.AddEntry(None, str_yield_Data,"")
      leg.AddEntry(h_ttbar_background_1l,"t#bar{t} + light","f")
      leg.AddEntry(None, str_yield_tt_1l, "")
      leg.AddEntry(h_ttbar_background_1c,"t#bar{t} + >=1c","f")
      leg.AddEntry(None, str_yield_tt_1c, "")
      leg.AddEntry(h_ttbar_background_1b,"t#bar{t} + >=1b","f")
      leg.AddEntry(None, str_yield_tt_1b, "")
      leg.AddEntry(h_ttV_background, "t#bar{t} + V","f")
      leg.AddEntry(None, str_yield_ttV, "")
      leg.AddEntry(h_nontt_background,"Non-t#bar{t}","f")
      leg.AddEntry(None, str_yield_nontt, "")
      #leg.AddEntry(h_ttH_background,"t#bar{t}+H","f")
      #leg.AddEntry(None, str_yield_ttH, "")
      #leg.AddEntry(h_ttW_background,"t#bar{t}+W","f")
      #leg.AddEntry(None, str_yield_ttW, "")
      #leg.AddEntry(h_W_background,"W+jet","f")
      #leg.AddEntry(None, str_yield_W, "") 
      leg.AddEntry(None, "Total","")
      leg.AddEntry(None, str_yield_Total, "")
      #leg.AddEntry(h_sig_Hplus_m400, "400 GeV H^{+}","L")
      #leg.AddEntry(h_sig_Hplus_m800, "800 GeV H^{+}","L")
      #leg.AddEntry(h_sig_Hplus_m1600,"1600 GeV H^{+}","L")
      #leg.AddEntry(h_data,"Data","epl")
      leg.SetTextSize(0.0350)
      ##leg2.SetTextSize(0.0350)
      leg.Draw()
      #h_mc = h_W_background + h_diboson_background + h_Z_background + h_ttbar_background + h_singleTop_background + h_ttW_background + h_ttH_background
      h_mc = h_ttV_background + h_ttbar_background + h_nontt_background
      #leg2.Draw()     
      pad2.SetGrid()
      pad2.cd()
      #h_ratio = h_data15 + h_data16
      #h_ratio = h_data.Clone()
      #
      
      h_ratio = h_data.Clone()
      h_ratio.Divide(h_mc)
      if "Inclusive" in btagStrategy:
        h_ratio.GetYaxis().SetRangeUser(0.0,2.0)
      if "TwoTags" in btagStrategy:
        h_ratio.GetYaxis().SetRangeUser(0.0,2.0)  
      if "ThreeTags" in btagStrategy :
        h_ratio.GetYaxis().SetRangeUser(0.0,2.0)
      if "FourPlusTags" in btagStrategy :
        h_ratio.GetYaxis().SetRangeUser(0.0,2.0)    
      h_ratio.SetStats(0)
      h_ratio.SetMarkerStyle(20)
      h_ratio.SetMarkerSize(1.0)
      h_ratio.SetMarkerColor(1)
      h_ratio.SetLabelSize(0.082,"X")
      h_ratio.SetLabelSize(0.082,"Y")
      h_ratio.SetTitleSize(0.082,"X")
      h_ratio.SetTitleSize(0.082,"Y")
      h_ratio.SetTitleOffset(0.48,"Y")
      h_ratio.SetTitleOffset(1.2,"X")
      h_ratio.GetYaxis().SetNdivisions(505)
      h_ratio.SetTickLength(0.005,"X") #0.05
      h_ratio.SetLabelOffset(0.01,"X")
      h_ratio.GetXaxis().SetTitle(Xaxis_label)
      h_ratio.GetYaxis().SetTitle("Data/Pred.")
      h_ratio.Draw("EP")
      

      #stat= TGraphAsymmErrors(nbins,np.array(x_values),np.array(y_values),np.array(x_error_down),np.array(x_error_up),np.array(y_error_down),np.array(y_error_up))
      #print x_values
      

      #leg.AddEntry(totsys,"total uncertainty","f")
      

      
      #h_other_background.Draw("HIST")
      #h_ttbar_background.Draw("HISTSAME")
      

      #if HistoName in "maxMVAResponse":
         #leg = TLegend(0.2,0.65,0.725,0.855)
      #else:
         #leg = TLegend(0.45,0.65,0.925,0.855)
      #ATLAS_LABEL(0.20,0.885," Simulation Internal",1,0.19);
      
      
      #h_other_background.GetYaxis().SetRangeUser(0.001,round(ymax*1.25,3)+0.001)

      #legend = Legend([h1, h2, h3], leftmargin=0.45, margin=0.3)
      #legend.Draw()
      #canvas.Modified()
      #canvas.Update()
      pad1.cd()
      myText(0.45,0.75,1,"#sqrt{s}=13 TeV, 139 fb^{-1}")
      if "TwoTags" in btagStrategy:
          myText(0.45,0.70,1,"l^{-}+jets Resolved: at least 5-jet, 2 b-tag")
          #myText(0.20,0.65,1,"Pre-fit")
          #myText(0.20,0.65,1,"SR cuts")
      if "ThreeTags" in btagStrategy :
          myText(0.45,0.70,1,"l^{-}+jets Resolved: at least 5-jet, 3 b-tag")
          #myText(0.20,0.65,1,"CR cuts")
      if "FourPlusTags" in btagStrategy :
          myText(0.45,0.70,1,"l^{-}+jets Resolved: at least 5-jet, at least 4 b-tag")
          #myText(0.20,0.65,1,"CR cuts")
      if "Inclusive" in btagStrategy :  
          myText(0.20,0.70,1,"l^{-}+jets Resolved: at least 5-jet, Inclusive")
          myText(0.20,0.67,1,"")
      c1.RedrawAxis()
      c1.Update()
      c1.RedrawAxis()
      c1.SaveAs("../Plots/datavmc/DPG_2022/DatavMC_%s_qqbb_datavmc_V2Ntup_LepTop_minmass_DPG.pdf" % (HistoName+"_"+btagStrategy))

