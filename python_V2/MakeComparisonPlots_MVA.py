# -*- coding: utf-8 -*-
#python
import sys
import glob
import math
import re
from ROOT import *
from array import *
from ROOT import TCanvas, TFile, TLine, TProfile, TNtuple, TH1F, TH2F
#import ROOT
#import array

gROOT.SetBatch(True)
gStyle.SetOptStat(0)
gStyle.SetPalette(1)
gROOT.LoadMacro("../style/AtlasStyle.C")
gROOT.LoadMacro("../style/AtlasUtils.C")
SetAtlasStyle()

y_axis_label="Event fraction"

c_blue   = TColor.GetColor("#3366ff")
c_red    = TColor.GetColor("#ee3311")
c_orange = TColor.GetColor("#ff9900")

def NormalizeHisto(histo):
     #n_events=histo.Integral(-1,histo.GetNbinsX()+1)
     n_events=histo.Integral()
     if n_events == 0:
         return
     print n_events, histo.Integral(histo.GetNbinsX(),histo.GetNbinsX()+1)
     histo.Scale(1./n_events)
     histo.SetLineWidth(3)
     histo.SetStats(0)
     histo.SetFillStyle(3001)
     histo.SetMarkerColor(histo.GetLineColor())
     histo.SetMarkerSize(0.0)
     histo.GetXaxis().SetTitleOffset(1.2)
     histo.GetYaxis().SetTitleOffset(1.52)
     histo.GetXaxis().SetLabelSize(0.05)
     histo.GetYaxis().SetLabelSize(0.05)
     histo.GetXaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetNdivisions(504)
     histo.GetXaxis().SetNdivisions(504)

c1 = TCanvas("ShapePlots","",500,500)

HistoNameR = "lep_top_mass"
#for HistoName in ["nBTags"]:
#for HistoName in ["nBTags","nJets","HT","HT_bjets","DeltaPhi_HW","mVH","mH","pTWplus","pTH","maxMVAResponse", "mass_resolution"]: # for resolved, histoname set
#for HistoName in ["pTWplus","pTH","mVH","Leptonic_top_mass"]: # for resolved, histoname set
#for HistoName in ["pTWminus"]: 
#for HistoName in ["nBTags","nJets","HT","HT_bjets","DeltaPhi_HW","mVH","mH","pTWplus","pTH","mass_resolution"]:#for boosted, histoname set
#for HistoName in ["mass_resolution"]:
for HistoName in ["mVH"]:
#for HistoName in ["mVH"]:     
#for HistoName in ["maxMVAResponse"]:
## for bTagStrategy in ["Incl","FourPlusTags","ThreeTags","TwoTags"]:
 for Region in ["Resolved_SR"]:
  for btagStrategy in ["FourPlusTags"]:
   #for btagStrategy in ["Inclusive","FourPlusTags","ThreeTags","TwoTags"]:
     
      ReBin = False
      YAxisScale = 1.4

      if "nBTags" in HistoName:
          Xaxis_label="b-tag multiplicity"
      if "nJet" in HistoName:
          Xaxis_label="Jet Multiplicity"
      if "DeltaPhi_HW" in HistoName:
          Xaxis_label="DeltaPhi_HW"
      if "pTH" in HistoName:
          Xaxis_label="Transverse Momentum of Higgs [GeV]"
      if "pTWplus" in HistoName:
          Xaxis_label="Transverse Momentum of W Boson [GeV]"
      if "mVH" in HistoName:
          Xaxis_label="Mass of Charged Higgs [GeV]"
      if "mH" in HistoName:
          Xaxis_label="Mass of Higgs [GeV]"
      if "pTWplus" in HistoName:
          Xaxis_label="Transverse Momentum Of Wplus [GeV]"
      if "pTH" in HistoName:
          Xaxis_label="Transverse Momentum Of Higgs [GeV]"
      if "mass_resolution" in HistoName:
          Xaxis_label="Mass Resolution"
      if "HT" in HistoName:
          Xaxis_label="H_{T} (Scalar Transverse Momentum Sum of jets) [GeV]"
      if "HT_bjets" in HistoName:
          Xaxis_label="H_{T_{b-jet}} (Scalar Transverse Momentum Sum of b-jets) [GeV]"
      if "maxMVAResponse" in HistoName:
          Xaxis_label="BDT Score (Signal Reconstruction)"
      if "mass_resolution" in HistoName:
          Xaxis_label="Leptonic Top (GeV)"   
      if "Leptonic_Top_mass" in HistoName:
          Xaxis_label="Leptonic top mass (GeV)"   
      #Xaxis_label=""

      file500       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_qqbb_CR_2910/hp500_AFII.root_77p_225.root","READ")
      dir500_10       = file500.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_07")
      h_sig_Hplus_m500_10 = dir500_10.Get("hp500_AFII_"+HistoName+"_07_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m500_10.SetLineColor(kBlack)
      h_sig_Hplus_m500_10.SetLineStyle(1) #7

      file600       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_qqbb_CR_2910/hp600_AFII.root_77p_225.root","READ")
      dir600_10       = file600.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_07")
      h_sig_Hplus_m600_10 = dir600_10.Get("hp600_AFII_"+HistoName+"_07_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m600_10.SetLineColor(kMagenta)
      h_sig_Hplus_m600_10.SetLineStyle(1) #7

      file700       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_qqbb_CR_2910/hp700_AFII.root_77p_225.root","READ")
      dir700_10       = file700.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_07")
      h_sig_Hplus_m700_10 = dir700_10.Get("hp700_AFII_"+HistoName+"_07_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m700_10.SetLineColor(kOrange)
      h_sig_Hplus_m700_10.SetLineStyle(1) #7

      file1000       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_qqbb_CR_2910/hp1000_AFII.root_77p_225.root","READ")
      dir1000_10       = file1000.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_07")
      h_sig_Hplus_m1000_10 = dir1000_10.Get("hp1000_AFII_"+HistoName+"_07_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m1000_10.SetLineColor(kRed)
      h_sig_Hplus_m1000_10.SetLineStyle(1) #7

      file1200       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_qqbb_CR_2910/hp1200_AFII.root_77p_225.root","READ")
      dir1200_10       = file1200.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_07")
      h_sig_Hplus_m1200_10 = dir1200_10.Get("hp1200_AFII_"+HistoName+"_07_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m1200_10.SetLineColor(kViolet)
      h_sig_Hplus_m1200_10.SetLineStyle(1) #7

      file1400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_qqbb_CR_2910/hp1400_AFII.root_77p_225.root","READ")
      dir1400_10       = file1400.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_07")
      h_sig_Hplus_m1400_10 = dir1400_10.Get("hp1400_AFII_"+HistoName+"_07_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m1400_10.SetLineColor(kBlue)
      h_sig_Hplus_m1400_10.SetLineStyle(1) #7

      file1600       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_qqbb_CR_2910/hp1600_AFII.root_77p_225.root","READ")
      dir1600_10       = file1600.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_07")
      h_sig_Hplus_m1600_10 = dir1600_10.Get("hp1600_AFII_"+HistoName+"_07_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m1600_10.SetLineColor(kGreen)
      h_sig_Hplus_m1600_10.SetLineStyle(1) #7

      file1800       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_qqbb_CR_2910/hp1800_AFII.root_77p_225.root","READ")
      dir1800_10       = file1800.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_07")
      h_sig_Hplus_m1800_10 = dir1800_10.Get("hp1800_AFII_"+HistoName+"_07_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m1800_10.SetLineColor(kYellow)
      h_sig_Hplus_m1800_10.SetLineStyle(1) #7

      #print "Passed the histogram initialisation"   
      nbins=20
      ymax=0
      NormalizeHisto(h_sig_Hplus_m500_10)
      if ymax<h_sig_Hplus_m500_10.GetMaximum():
          ymax=h_sig_Hplus_m500_10.GetMaximum()
      NormalizeHisto(h_sig_Hplus_m600_10)
      if ymax<h_sig_Hplus_m600_10.GetMaximum():
          ymax=h_sig_Hplus_m600_10.GetMaximum()
      NormalizeHisto(h_sig_Hplus_m700_10)
      if ymax<h_sig_Hplus_m700_10.GetMaximum():
          ymax=h_sig_Hplus_m700_10.GetMaximum()
      NormalizeHisto(h_sig_Hplus_m1000_10)
      if ymax<h_sig_Hplus_m1000_10.GetMaximum():
          ymax=h_sig_Hplus_m1000_10.GetMaximum()
      NormalizeHisto(h_sig_Hplus_m1200_10)
      if ymax<h_sig_Hplus_m1200_10.GetMaximum():
          ymax=h_sig_Hplus_m1200_10.GetMaximum()
      NormalizeHisto(h_sig_Hplus_m1400_10)
      if ymax<h_sig_Hplus_m1400_10.GetMaximum():
          ymax=h_sig_Hplus_m1400_10.GetMaximum()
      NormalizeHisto(h_sig_Hplus_m1600_10)
      if ymax<h_sig_Hplus_m1600_10.GetMaximum():
          ymax=h_sig_Hplus_m1600_10.GetMaximum()
      NormalizeHisto(h_sig_Hplus_m1800_10)
      if ymax<h_sig_Hplus_m1800_10.GetMaximum():
          ymax=h_sig_Hplus_m1800_10.GetMaximum()
      
      h_sig_Hplus_m500_10.Draw("HIST")
      h_sig_Hplus_m600_10.Draw("HISTSAME")
      h_sig_Hplus_m700_10.Draw("HISTSAME")
      h_sig_Hplus_m1000_10.Draw("HISTSAME")
      h_sig_Hplus_m1200_10.Draw("HISTSAME")
      h_sig_Hplus_m1400_10.Draw("HISTSAME")
      h_sig_Hplus_m1600_10.Draw("HISTSAME")
      h_sig_Hplus_m1800_10.Draw("HISTSAME")
      #h_sig_Hplus_m900_10.Draw("HISTSAME")

      if HistoName in "maxMVAResponse":
         leg = TLegend(0.25,0.55,0.825,0.755)
      else:
         leg = TLegend(0.65,0.30,0.85,0.75)
      ATLAS_LABEL(0.20,0.90,"Internal",1,0.19); 
      myText(0.20,0.85,1,"at least 5 jet, at least 4 b-tags") 
      myText(0.20,0.80,1,"0.0 <= maxMVA < 0.7") 
      #myText(0.20,0.80,1,"maxMVA >= 0.9") 
      #ATLAS_LABEL(0.20,0.875,"Simulation",1,0.19);
      #ATLAS_LABEL(0.20,0.80,"Simulation",1,0.09)
      leg.SetShadowColor(kWhite)
      leg.SetFillColor(kWhite)
      leg.SetLineColor(kWhite)
      
      leg.AddEntry(h_sig_Hplus_m500_10,    "m_{H^{+}} = 500 GeV","L")
      leg.AddEntry(h_sig_Hplus_m600_10,    "m_{H^{+}} = 600 GeV","L")
      leg.AddEntry(h_sig_Hplus_m700_10,    "m_{H^{+}} = 700 GeV","L")
      leg.AddEntry(h_sig_Hplus_m1000_10,    "m_{H^{+}} = 1000 GeV","L")
      leg.AddEntry(h_sig_Hplus_m1200_10,    "m_{H^{+}} = 1200 GeV","L")
      leg.AddEntry(h_sig_Hplus_m1400_10,    "m_{H^{+}} = 1400 GeV","L")
      leg.AddEntry(h_sig_Hplus_m1600_10,    "m_{H^{+}} = 1600 GeV","L")
      leg.AddEntry(h_sig_Hplus_m1800_10,    "m_{H^{+}} = 1800 GeV","L")
      #leg.AddEntry(h_sig_Hplus_m900_10,    "m_{H^{+}} = 900 GeV","L")
      
      leg.SetTextSize(0.0250)
      leg.Draw()
      h_sig_Hplus_m500_10.GetXaxis().SetTitle(Xaxis_label)
      h_sig_Hplus_m500_10.GetYaxis().SetRangeUser(0.001,round(ymax*1.25,3)+0.001)
      h_sig_Hplus_m500_10.GetYaxis().SetTitle("Normalised Entries")
      #h_sig_Hplus_m400.GetXaxis().SetTitle(Xaxis_label)
      #h_sig_Hplus_m400.GetYaxis().SetRangeUser(0.001,round(ymax*1.25,3)+0.001)
      #pad1.cd()
      #myText(0.20,0.825,1,"work-in-progress")
      #myText(0.20,0.90,1,"Simulation")
      c1.RedrawAxis()
      c1.Update()
      c1.RedrawAxis()
      c1.SaveAs("../Plots/ShapePlot_%s_qqbb_SR_Resolved_CR_MVACut07_500GeV_1800GeV.pdf" % (HistoName+"_"+btagStrategy))

