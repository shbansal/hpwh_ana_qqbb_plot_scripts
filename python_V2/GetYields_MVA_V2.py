# -*- coding: utf-8 -*-
#python
import ROOT
import sys
import glob
import math
import re
from ROOT import *
from array import *

#from ROOT import rootpy.plotting

import numpy as np
from numpy import ndarray
import array
#from rootpy.plotting import Hist, HistStack, Legend, Canvas
#from rootpy.plotting.style import get_style, set_style
#from rootpy.plotting.utils import draw
#from rootpy.interactive import wait
#import rootpy.plotting.root2matplotlib as rplt
from ROOT import TCanvas, TFile, TPad, THStack, TLine,TGraphAsymmErrors, TNtuple, TH1F, TH2F
#from ROOT import TGraphAsymmErrors
from array import *
#from ROOT import TLatex
#import ROOT
#import array

gROOT.SetBatch(True)
gStyle.SetOptStat(0)
gStyle.SetPalette(1)
gROOT.LoadMacro("../style/AtlasStyle.C")
gROOT.LoadMacro("../style/AtlasUtils.C")
SetAtlasStyle()

y_axis_label="Event fraction"

c_blue   = TColor.GetColor("#3366ff")
c_red    = TColor.GetColor("#ee3311")
c_orange = TColor.GetColor("#ff9900")

def NormalizeHisto(histo):
     n_events=histo.Integral(-1,histo.GetNbinsX()+1)
     if n_events == 0:
         return
     print n_events, histo.Integral(histo.GetNbinsX(),histo.GetNbinsX()+1)
     histo.Scale(1./n_events)
     histo.SetLineWidth(2)
     histo.SetStats(0)
     histo.SetFillStyle(3001)
     histo.SetMarkerColor(histo.GetLineColor())
     histo.SetMarkerSize(0.0)
     histo.GetXaxis().SetTitleOffset(1.2)
     histo.GetYaxis().SetTitleOffset(1.52)
     histo.GetXaxis().SetLabelSize(0.05)
     histo.GetYaxis().SetLabelSize(0.05)
     histo.GetXaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetNdivisions(504)
     histo.GetXaxis().SetNdivisions(504)

c1 = TCanvas("DatavMC-qqbb","",700,500)
pad1 = TPad("pad1", "pad1", 0.0, 0.35, 1,1)
#pad1.SetLogy()
pad2 = TPad("pad2", "pad2", 0, 0.0, 1,0.35)
pad1.SetLeftMargin(0.15)
pad1.SetTopMargin(0.1)
pad1.SetBottomMargin(0.0105)
pad1.SetRightMargin(0.08)
pad1.Draw()
pad1.SetTicks()

pad2.SetLeftMargin(0.15)
pad2.SetTopMargin(0.04)
pad2.SetBottomMargin(0.35)
pad2.SetRightMargin(0.08)
pad2.Draw()
pad2.SetTicks()
HistoNameR ="evw_MVAaccu"
#HistoNameR ="evw_trueM_eone"
#HistoNameR ="evw"
#HistoNameR ="evw_Whad"
#for HistoName in ["nBTags"]:
#for HistoName in ["nBTags","nJets","HT","HT_bjets","DeltaPhi_HW","mVH","mH","pTWplus","pTH","maxMVAResponse", "mass_resolution"]: # for resolved, histoname set
#for HistoName in ["nJets","HT","HT_bjets","mVH","mH","pTWplus","pTH","DeltaPhi_HW","pTH_over_mVH","pTW_over_mVH","DeltaEta_HW"]:
#for HistoName in ["mVH","nJet","DeltaPhi_HW","DeltaEta_HW","maxMVAResponse"]:
#for HistoName in ["maxMVAResponse","DeltaEta_HW","DeltaPhi_HW"]: 
#for HistoName in ["nJet"]:
#for HistoName in ["HT","pTWplus","pTH"]:       
#for HistoName in ["Event_Weight_Wlep"]:
#for HistoName in ["Event_Weight_Whad"]:
for HistoName in ["Event_Weight_MVAccu"]:
#for HistoName in ["Event_Weight"]:   
#for HistoName in ["nJet","maxMVAResponse","DeltaPhi_HW","pTH_over_mVH","pTW_over_mVH","DeltaEta_HW"]:
#for HistoName in ["DeltaEta_HW"]: 
#for HistoName in ["Eta_j1j2","Eta_j1j3","Eta_j1j4","Eta_j1j5","Eta_j1j6","Eta_j1j7","Eta_j2j3","Eta_j2j4","Eta_j2j5","Eta_j2j6","Eta_j2j7","Eta_j3j4","Eta_j3j5","Eta_j3j6","Eta_j3j7","Eta_j4j5","Eta_j4j6","Eta_j4j7","Eta_j5j6","Eta_j5j7","Eta_j6j7"]:
#for HistoName in ["maxMVAResponse_15","maxMVAResponse_10","maxMVAResponse_19","maxMVAResponse_18"]: 
## for bTagStrategy in ["Incl","FourPlusTags","ThreeTags","TwoTags"]:
 for Region in ["Resolved_top"]:
  #for btagStrategy in ["FourPlusTags","ThreeTags"]:
  for btagStrategy in ["Inclusive"]:    
     
      ReBin = False
      YAxisScale = 1.4
  
      #file_800       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_qqbb_Corrtthbb_Mod_nolsl_17MPAcc/hp800_AFII.root_77p_225.root","READ")
      #file_800       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_qqbbEvent_3MP_mod_nolsl_AccuV3/hp800_AFII.root_77p_225.root","READ")
      file_800       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/HpFiles_Acc_wocth_600T/hp800_AFII.root_77p_225.root","READ")
      #file_800       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_FullEvent_Corrtthbb_Mod_nolsl_17MPAcc/hp800_AFII.root_77p_225.root","READ")
      #file_400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_290121/sig_Hplus_Wh_m400-0_70p.root","READ")
      #file_400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_1512/sig_Hplus_Wh_m400-0.root_70p_MC16d.root","READ")
      dir_800        = file_800.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m800 = dir_800.Get("hp800_AFII_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      #file7	  = TFile.Open("../PlotFiles/ForOptimisation/sig_Hplus_Wh_m1600-0_70p.root","READ")
      #file_1600	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_qqbb_Corrtthbb_Mod_nolsl_17MPAcc/hp1600_AFII.root_77p_225.root","READ")
      #file_1600	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_qqbbEvent_3MP_mod_nolsl_AccuV3/hp1600_AFII.root_77p_225.root","READ")
      file_1600	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/HpFiles_Acc_wocth_600T/hp1600_AFII.root_77p_225.root","READ")
      #file_1600	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_FullEvent_Corrtthbb_Mod_nolsl_17MPAcc/hp1600_AFII.root_77p_225.root","READ")
      #file_1600	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_290121/sig_Hplus_Wh_m1600-0_70p.root","READ")
      #file_1600	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_1512/sig_Hplus_Wh_m1600-0.root_70p_MC16e.root","READ")
      dir_1600        = file_1600.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m1600 = dir_1600.Get("hp1600_AFII_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      #file_400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_qqbb_Corrtthbb_Mod_nolsl_17MPAcc/hp400_AFII.root_77p_225.root","READ")
      #file_400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_qqbbEvent_3MP_mod_nolsl_AccuV3/hp400_AFII.root_77p_225.root","READ")
      file_400	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/HpFiles_Acc_wocth_600T/hp400_AFII.root_77p_225.root","READ")
      #file_400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_FullEvent_Corrtthbb_Mod_nolsl_17MPAcc/hp400_AFII.root_77p_225.root","READ")
      dir_400       = file_400.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m400 = dir_400.Get("hp400_AFII_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
 
      yield_H400 = h_sig_Hplus_m400.Integral(11,20)-h_sig_Hplus_m400.Integral(1,10)
      yield_H800 = h_sig_Hplus_m800.Integral(11,20)-h_sig_Hplus_m800.Integral(1,10)
      yield_H1600 = h_sig_Hplus_m1600.Integral(11,20)-h_sig_Hplus_m1600.Integral(1,10) 
    
      print "yield_H400 is", yield_H400
      print "yield_H800 is", yield_H800
      print "yield_H1600 is", yield_H1600
