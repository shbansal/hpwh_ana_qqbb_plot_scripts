# -*- coding: utf-8 -*-
#python
import sys
import glob
import math
import re
from ROOT import *
from array import *
from ROOT import TCanvas, TFile, TLine, TProfile, TNtuple, TH1F, TH2F
#import ROOT
#import array

gROOT.SetBatch(True)
gStyle.SetOptStat(0)
gStyle.SetPalette(1)
gROOT.LoadMacro("../style/AtlasStyle.C")
gROOT.LoadMacro("../style/AtlasUtils.C")
SetAtlasStyle()

y_axis_label="Event fraction"

c_blue   = TColor.GetColor("#3366ff")
c_red    = TColor.GetColor("#ee3311")
c_orange = TColor.GetColor("#ff9900")

def NormalizeHisto(histo): #To normalise the histogram for shape plotting
     n_events=histo.Integral()
     if n_events == 0:
         return
     histo.Scale(1./n_events)
     histo.SetLineWidth(3)
     histo.SetStats(0)
     histo.SetFillStyle(3001)
     histo.SetMarkerColor(histo.GetLineColor())
     histo.SetMarkerSize(0.0)
     histo.GetXaxis().SetTitleOffset(1.2)
     histo.GetYaxis().SetTitleOffset(1.52)
     histo.GetXaxis().SetLabelSize(0.05)
     histo.GetYaxis().SetLabelSize(0.05)
     histo.GetXaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetNdivisions(504)
     histo.GetXaxis().SetNdivisions(504)

c1 = TCanvas("ShapePlots","",500,500)

for HistoName in ["pTH"]: #Add name of histograms
 for Region in ["Resolved_SR"]: #Change this, may be Merged_... something in your case
  for btagStrategy in ["Inclusive"]: #Including all b-tag categories
        
      ReBin = False
      YAxisScale = 1.4
      
      #Add the x axis labels for the histograms..
      if "pTH" in HistoName:
          Xaxis_label="Transverse Momentum of Higgs [GeV]"   

      file1       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_qqbb_CR_V2/hp800_AFII.root_77p_225.root","READ") # add your signal file name here
      dir1        = file1.GetDirectory("nominal_Loose").GetDirectory(HistoName) #Get the directory where histograms are stored
      h_sig_Hplus_m800 = dir1.Get("hp800_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m800.SetLineColor(kRed)
      h_sig_Hplus_m800.SetLineStyle(7) #7
      if ReBin == True:
          h_sig_Hplus_m800.Rebin(2)    

      
      #file_tt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_qqbb_CR_V2/tt_PP8.root_77p_225.root","READ") # add your background file name here
      #dir_tt   = file_tt.GetDirectory("nominal_Loose").GetDirectory(HistoName) #Get the directory where histograms are stored
      #h_ttbar_background = dir_tt.Get("tt_PP8"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_ttbar_background.SetLineColor(kGreen)
      #h_ttbar_background.SetLineStyle(7) #3
      #if ReBin == True:
          #h_ttbar_background.Rebin(2)

      nbins=20
      ymax=0
      #Normalise the histogram, i.e the integral of the histograms is unity
      NormalizeHisto(h_sig_Hplus_m800) 
      if ymax<h_sig_Hplus_m800.GetMaximum():
          ymax=h_sig_Hplus_m800.GetMaximum()
      #NormalizeHisto(h_ttbar_background)
      #if ymax<h_ttbar_background.GetMaximum():
          #ymax=h_ttbar_background.GetMaximum()   


      h_sig_Hplus_m800.Draw("HIST")
      #h_ttbar_background.Draw("HISTSAME")
           
      leg = TLegend(0.55,0.65,0.955,0.855)
      ATLAS_LABEL(0.20,0.875,"Simulation",1,0.19)
      leg.SetShadowColor(kWhite)
      leg.SetFillColor(kWhite)
      leg.SetLineColor(kWhite)
      
    
      leg.AddEntry(h_sig_Hplus_m800,   "H^{+}#rightarrow W^{+}h(m_{H^{+}} = 800GeV)","L")
      #leg.AddEntry(h_ttbar_background,  "t#bar{t}","L")
      leg.SetTextSize(0.0250)
      leg.Draw()
      
      h_sig_Hplus_m800.GetXaxis().SetTitle(Xaxis_label)
      h_sig_Hplus_m800.GetYaxis().SetRangeUser(0.001,round(ymax*1.25,3)+0.001) #adjust accordingly
      #pad1.cd()
      myText(0.20,0.825,1,"Internal")
      c1.RedrawAxis()
      c1.Update()
      c1.RedrawAxis()
      c1.SaveAs("../Plots/ShapePlot_%s_qqbb_Kinem.pdf" % (HistoName+"_"+btagStrategy))

