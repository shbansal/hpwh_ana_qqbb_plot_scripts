import sys
import glob
import math
import re
from ROOT import *
from array import *
gROOT.SetBatch(True)
gStyle.SetOptStat(0)
gStyle.SetPalette(1)
gROOT.LoadMacro("../style/AtlasStyle.C")
gROOT.LoadMacro("../style/AtlasUtils.C")
SetAtlasStyle()
y_axis_label="S/#sqrt(B)"
#############################
#btagStrategy = "TwoTags"
#btagStrategy = "ThreeTags"
#btagStrategy = "FourPlusTags"
btagStrategy = "Inclusive"
#btagStrategy = "TwoTags"
Region       = "Resolved_SR"
############################

def SignificanceFunction2(s,b,rel_unc):
    n=s+b
    sigma=n*rel_unc
    print n,b,s,sigma
    #print n*math.log((n*(b+(sigma**2)))/((b**2)+n*(sigma**2)))
    #print(b**2)/(sigma**2)*math.log(1+((sigma**2)*(n-b))/(b*(b+(sigma**2))))
    if s<=0 or b<=0 :
        print("ERROR signal or background events is Zero!")
        return -1
    else:
        if b>=s:
            try:
                Z=math.sqrt(2*(n*math.log((n*(b+(sigma**2)))/((b**2)+n*(sigma**2)))-(b**2)/(sigma**2)*math.log(1+(((sigma**2)*(n-b))/(b*(b+(sigma**2)))))))
            except:
                Z=0
        else:
            Z=-1
            print "this is at n<b"
        return Z
        

def Loop():
    
    #for HistoName in ["MET","maxMVAResponse","pTH","METSig","nJets","Lepton_Pt","nBtagCategory","ntagsOutside","mH","DeltaPhi_HW","nBTags"]:#,"pTH_over_mVH","pTW_over_mVH","Lepton_Pt"
    #for HistoName in ["pTH","pTWplus","nJet","mH","mWplus","nBTags", "maxMVAResponse"]:
    for HistoName in ["pTWplus_1200Res","pTH_1200Res","maxMVAResponse_1200Res"]:
    #for HistoName in ["maxMVAResponse"]:
    #for HistoName in ["nJets"]:
    #HistoName="MET"
    #for HistoName in ["mVH"]:
    #for HistoName in ["DeltaPhi_HW"]:
        
        c1=TCanvas("c1","",1200,900)  
        

        if "nBTags" in HistoName:
          Xaxis_label="b-tag multiplicity"
        if "nJets" in HistoName:
          Xaxis_label="Jet Multiplicity"
        if "DeltaPhi_HW" in HistoName:
          Xaxis_label="DeltaPhi_HW"
        if "pTH" in HistoName:
          Xaxis_label="Transverse Momentum of Higgs [GeV]"
        if "pTWplus" in HistoName:
          Xaxis_label="Transverse Momentum of W Boson [GeV]"
        if "mVH" in HistoName:
          Xaxis_label="Mass of Charged Higgs [GeV]"
        if "mH" in HistoName:
          Xaxis_label="Mass of Higgs [GeV]"
        if "pTWplus" in HistoName:
          Xaxis_label="Transverse Momentum Of Wplus [GeV]"
        if "pTH" in HistoName:
          Xaxis_label="Transverse Momentum Of Higgs [GeV]"
        if "mass_resolution" in HistoName:
          Xaxis_label="Mass Resolution"
        if "HT" in HistoName:
          Xaxis_label="H_{T} (Scalar Transverse Momentum Sum of jets) [GeV]"
        if "HT_bjets" in HistoName:
          Xaxis_label="H_{T_{b-jet}} (Scalar Transverse Momentum Sum of b-jets) [GeV]"
        if "maxMVAResponse" in HistoName:
          Xaxis_label="BDT Score (Signal Reconstruction)"  

        pad1=TPad()
        pad2=TPad()
        pad1.SetCanvas(c1)
        pad2.SetCanvas(c1)
        pad1.SetCanvasSize(1200,900)
        pad2.SetCanvasSize(1200,900)
        pad1.SetLeftMargin(0.15)
        pad2.SetLeftMargin(0.15)
        pad2.SetRightMargin(0.13)
        pad1.SetRightMargin(0.13)
        
        pad2.SetFillStyle(4000)
        pad2.SetFrameFillStyle(0)
        bin_breite=0
    
        #file1       = TFile.Open("../PlotFiles/ForOptimisation/sig_Hplus_Wh_m400-0_70p.root","READ")
        file1       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_qqbb_CR_V2/hp400_AFII.root_77p_225.root","READ")
        dir1        = file1.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      #dir1        = file1.GetDirectory("Nominal").GetDirectory(HistoName)
        h_signalHisto_400 = dir1.Get("hp400_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")   

      #file6	  = TFile.Open("../PlotFiles/ForOptimisation/sig_Hplus_Wh_m800-0_70p.root","READ")
        file6	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_qqbb_CR_V2/hp800_AFII.root_77p_225.root","READ")
        dir6        = file6.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      #dir6        = file6.GetDirectory("Nominal").GetDirectory(HistoName)
        h_signalHisto_800 = dir6.Get("hp800_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      #file7	  = TFile.Open("../PlotFiles/ForOptimisation/sig_Hplus_Wh_m1600-0_70p.root","READ")
        file7	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_qqbb_CR_V2/hp1600_AFII.root_77p_225.root","READ")
        dir7        = file7.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      #dir7        = file7.GetDirectory("Nominal").GetDirectory(HistoName)
        h_signalHisto_1600 = dir7.Get("hp1600_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

        file25  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_qqbb_CR_V2/hp250_AFII.root_77p_225.root","READ")
        dir25        = file25.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      #dir7        = file7.GetDirectory("Nominal").GetDirectory(HistoName)
        h_signalHisto_250 = dir25.Get("hp250_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

        file20  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_qqbb_CR_V2/hp2000_AFII.root_77p_225.root","READ")
        dir20        = file20.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      #dir7        = file7.GetDirectory("Nominal").GetDirectory(HistoName)
        h_signalHisto_2000 = dir20.Get("hp2000_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

        file12  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_qqbb_CR_V2/hp1200_AFII.root_77p_225.root","READ")
        dir12        = file12.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      #dir7        = file7.GetDirectory("Nominal").GetDirectory(HistoName)
        h_signalHisto_1200 = dir12.Get("hp1200_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")


        
        file2   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_qqbb_CR_V2/tt_PP8.root_77p_225.root","READ")
        dir_ttb    = file2.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1b")
        h_ttbar_background_1b = dir_ttb.Get("tt_PP8_1B_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
        dir_ttc    = file2.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1c")
        h_ttbar_background_1c = dir_ttc.Get("tt_PP8_1C_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
        dir_ttl    = file2.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1l")
        h_ttbar_background_1l = dir_ttl.Get("tt_PP8_1L_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

        file2_filt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_qqbb_CR_V2/tt_PP8filtered.root_77p_225.root","READ")
        dir_ttb_filt    = file2_filt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1b")
        h_ttbar_background_1b_filt = dir_ttb_filt.Get("tt_PP8filtered_1B_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
        dir_ttc_filt    = file2_filt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1c")
        h_ttbar_background_1c_filt = dir_ttc_filt.Get("tt_PP8filtered_1C_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
        dir_ttl_filt    = file2_filt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1l")
        h_ttbar_background_1l_filt = dir_ttl_filt.Get("tt_PP8filtered_1L_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")


        h_ttbar_background = h_ttbar_background_1b+h_ttbar_background_1c+h_ttbar_background_1l+h_ttbar_background_1b_filt+h_ttbar_background_1c_filt+h_ttbar_background_1l_filt


      #file3   = TFile.Open("../PlotFiles/ForOptimisation/Wjets_70p.root","READ")
        file3   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_qqbb_CR_V2/wjets_77p_225.root","READ")
        dir3    = file3.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      #dir3    = file3.GetDirectory("Nominal").GetDirectory(HistoName)
        h_W_background = dir3.Get("wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      
      #file4   = TFile.Open("../PlotFiles/ForOptimisation/diboson_70p.root","READ")
        file4   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_qqbb_CR_V2/db.root_77p_225.root","READ")
        dir4    = file4.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      #dir4    = file4.GetDirectory("Nominal").GetDirectory(HistoName)
        h_diboson_background = dir4.Get("db_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      #file5   = TFile.Open("../PlotFiles/ForOptimisation/singleTop_70p.root","READ")
        file5   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_qqbb_CR_V2/st_sc.root_77p_225.root","READ")
        dir5    = file5.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      #dir5    = file5.GetDirectory("Nominal").GetDirectory(HistoName)
        h_singleTop_background_sc = dir5.Get("st_sc_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      
     #file5   = TFile.Open("../PlotFiles/ForOptimisation/singleTop_70p.root","READ")
        file9   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_qqbb_CR_V2/st_tc.root_77p_225.root","READ")
        dir9   = file9.GetDirectory("nominal_Loose").GetDirectory(HistoName)
        h_singleTop_background_tc = dir9.Get("st_tc_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
        

        file_Wt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_qqbb_CR_V2/Wt.root_77p_225.root","READ")
        dir_Wt   = file_Wt.GetDirectory("nominal_Loose").GetDirectory(HistoName)
        h_Wt = dir_Wt.Get("Wt_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

        file_tH   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_qqbb_CR_V2/tH_AFII.root_77p_225.root","READ")
        dir_tH   = file_tH.GetDirectory("nominal_Loose").GetDirectory(HistoName)
        h_tH = dir_tH.Get("tH_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

        file_tWZ   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_qqbb_CR_V2/tWZ.root_77p_225.root","READ")
        dir_tWZ   = file_tWZ.GetDirectory("nominal_Loose").GetDirectory(HistoName)
        h_tWZ = dir_tWZ.Get("tWZ_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

        file_tZ   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_qqbb_CR_V2/tZ.root_77p_225.root","READ")
        dir_tZ   = file_tZ.GetDirectory("nominal_Loose").GetDirectory(HistoName)
        h_tZ = dir_tZ.Get("tZ_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

        h_singleTop_background = h_singleTop_background_tc + h_singleTop_background_sc + h_Wt + h_tH + h_tWZ + h_tZ

        file_ttW   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_qqbb_CR_V2/ttW.root_77p_225.root","READ")
        #file_tt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_290121/ttbar_70p.root","READ")
        dir_ttW    = file_ttW.GetDirectory("nominal_Loose").GetDirectory(HistoName)
        h_ttW_background = dir_ttW.Get("ttW_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

        file_ttH   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_qqbb_CR_V2/ttH.root_77p_225.root","READ")
        #file_tt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_290121/ttbar_70p.root","READ")
        dir_ttH    = file_ttH.GetDirectory("nominal_Loose").GetDirectory(HistoName)
        h_ttH_background = dir_ttH.Get("ttH_PP8_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")    

        #file8   = TFile.Open("../PlotFiles/ForOptimisation/Zjets_70p.root","READ")
        file8   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_qqbb_CR_V2/zjets_77p_225.root","READ")
        dir8    = file8.GetDirectory("nominal_Loose").GetDirectory(HistoName)
        #dir8    = file8.GetDirectory("Nominal").GetDirectory(HistoName)
        h_Z_background = dir8.Get("zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose") 

        #file8   = TFile.Open("../PlotFiles/ForOptimisation/Zjets_70p.root","READ")
        file_ttZ   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_qqbb_CR_V2/ttll.root_77p_225.root","READ")
        dir_ttZ    = file_ttZ.GetDirectory("nominal_Loose").GetDirectory(HistoName)
        #dir8    = file8.GetDirectory("Nominal").GetDirectory(HistoName)
        h_ttZ_background = dir_ttZ.Get("ttll_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")      

        h_other_background = h_diboson_background + h_W_background + h_Z_background + h_singleTop_background + h_ttH_background + h_ttW_background + h_ttZ_background
        #h_backgHisto_single = h_singleTop_background_sc + h_singleTop_background_tc +  h_Wt + h_tH + h_tWZ + h_tZ  
        
        #h_backgHisto=h_backgHisto_t
        #h_backgHisto_diboson+h_backgHisto_Z+h_ttW_background+h_ttH_background
        #h_backgHisto=h_backgHisto_t+h_backgHisto_single+h_backgHisto_diboson+h_ttW_background+h_ttH_background
        h_backgHisto=h_other_background+h_ttbar_background
        print h_signalHisto_1200.Integral()/math.sqrt(h_backgHisto.Integral())
        #print h_signalHisto_400.Integral()/math.sqrt(h_backgHisto.Integral())
        #print h_sig_Hplus_m400.Integral()/math.sqrt(h_backgHisto.Integral())

        x=array('d')
        y_800=array('d')
        y_400=array('d')
        y_1600=array('d')
        y_backg=array('d')
        z=array('d')
        z_800=array('d')
        z_400=array('d')
        z_400_5=array('d')
        z_400_25=array('d')
        z_400_10=array('d')
        z_1600=array('d')
        y=array('d')
        z_5=array('d')
        z_25=array('d')
        z_10=array('d')
        ##x_bin_size=h_signalHisto_400.GetXaxis().GetXmax()/h_signalHisto_400.GetNbinsX()
        #x_bin_size=h_signalHisto_400.GetBinWidth(2)
        x_bin_size=h_signalHisto_1200.GetBinWidth(2)
        
        for i in range(1,h_signalHisto_1200.GetNbinsX()+1):
        #for i in range(1,h_signalHisto_400.GetNbinsX()+1):

            h_signalHisto=h_signalHisto_1200
            #h_signalHisto=h_signalHisto_400
            bin_breite=h_signalHisto.GetBinWidth(2)
            print (bin_breite-x_bin_size)
            
            n_sig=GetNumberOfEvents(i,h_signalHisto)
            n_backg=GetNumberOfEvents(i,h_backgHisto)
            if n_backg==0:
                continue
            x.append(h_signalHisto.GetBinCenter(i))
           # n_sig_800=GetNumberOfEvents(i,h_signalHisto_800)
           # n_sig_400=GetNumberOfEvents(i,h_signalHisto_1600)
           # n_sig_1600=GetNumberOfEvents(i,h_signalHisto_1600)
            
            #y_800.append((GetNumberOfEvents(i,h_signalHisto_1600)/math.sqrt(n_backg)))
            #y_400.append((GetNumberOfEvents(i,h_signalHisto_1600)/math.sqrt(n_backg)))
            #y_1600.append((GetNumberOfEvents(i,h_signalHisto_1600)/math.sqrt(n_backg)))
            #z_400.append(SignificanceFunction2(n_sig_800,n_backg,0.05))
            #z_400_5.append(SignificanceFunction2(n_sig_1600,n_backg,0.05))
            #z_400_25.append(SignificanceFunction2(n_sig_1600,n_backg,0.025))
            #z_400_10.append(SignificanceFunction2(n_sig_1600,n_backg,0.1))
            y.append(((n_sig)/math.sqrt(n_backg)))
            z_5.append(SignificanceFunction2(n_sig,n_backg,0.05))
            z_25.append(SignificanceFunction2(n_sig,n_backg,0.025))
            z_10.append(SignificanceFunction2(n_sig,n_backg,0.1))
            
            #z_800.append(SignificanceFunction2(n_sig_800,n_backg,0.05))
            #z_1600.append(SignificanceFunction2(n_sig_1600,n_backg,0.05))
            
          #  z.append(SignificanceFunction2(n_sig,n_backg,0.05))
         #   x_single.append(n_sig/math.sqrt(GetNumberOfEvents(i,h_backgHisto_single)))
          #  x_di.append(n_sig/math.sqrt(GetNumberOfEvents(i,h_backgHisto_diboson)))
           # x_W.append(n_sig/math.sqrt(GetNumberOfEvents(i,h_backgHisto_W)))
           # z_single.append(SignificanceFunction2(n_sig,GetNumberOfEvents(i,h_backgHisto_single),0.05))
            #z_W.append(SignificanceFunction2(n_sig,GetNumberOfEvents(i,h_backgHisto_W),0.05))
            #z_di.append(SignificanceFunction2(n_sig,GetNumberOfEvents(i,h_backgHisto_diboson),0.05))
        
        ##graph_800_z=TGraph(len(x),x,z_800)
        ##graph_400_z=TGraph(len(x),x,z_400)
        ##graph_1600_z=TGraph(len(x),x,z_1600)
        ##graph_800_sq=TGraph(len(x),x,y_800)
        #graph_400_sq=TGraph(len(x),x,y)
        ##graph_1600_sq=TGraph(len(x),x,y_1600)
        #graph_400_25_z=TGraph(len(x),x,z_25)
        #graph_400_5_z=TGraph(len(x),x,z_5)
        #graph_400_10_z=TGraph(len(x),x,z_10)

        graph_400_sq=TGraph(len(x),x,y)
        #graph_1600_sq=TGraph(len(x),x,y_1600)
        graph_400_25_z=TGraph(len(x),x,z_25)
        graph_400_5_z=TGraph(len(x),x,z_5)
        graph_400_10_z=TGraph(len(x),x,z_10)
        #graph_W=TGraph(len(x),x,z_W)
        #graph_single=TGraph(len(x),x,z_single)
        #graph_di=TGraph(len(x),x,z_di)
          #  print "GetMaximun:" 
           # print h_signalHisto.GetXaxis().GetXmax()
       # graph_z=TGraph(len(x),x,z_5)
        #graph_sq=TGraph(len(x),x,y)

           
        #c1.Divide(1,2,0,0)
        #c1.cd(1)
       # graph_1600_z.GetXaxis().SetTitle(HistoName)
       # graph_1600_z.GetYaxis().SetTitle("Z")
       # graph_1600_z.SetMarkerColor(kBlue)
       # graph_800_z.SetMarkerColor(kRed)
        #graph_400_z.SetMarkerColor(kGreen)

        #graph_1600_sq.SetMarkerColor(kBlue)
        #graph_800_sq.SetMarkerColor(kRed)
        graph_400_sq.SetMarkerColor(kBlack)
        #graph_800_sq.GetXaxis().SetTitle("Lowerbound on:"+HistoName)
        graph_400_sq.GetXaxis().SetTitle(HistoName)
        graph_400_sq.GetYaxis().SetTitle("s/#sqrt{b}")
        graph_400_sq.GetYaxis().CenterTitle()
        graph_400_sq.GetYaxis().SetRange(0,50)
       
        graph_400_25_z.SetMarkerStyle(4)
        graph_400_25_z.GetYaxis().SetTitle("z")
        graph_400_25_z.GetYaxis().CenterTitle()
        graph_400_25_z.GetYaxis().SetLimits(-0.05,1)
        graph_400_5_z.SetMarkerStyle(25)
        graph_400_10_z.SetMarkerStyle(26)
        graph_400_sq.SetMarkerStyle(3)
        graph_400_5_z.SetMarkerColor(kBlue)
        graph_400_10_z.SetMarkerColor(kRed)
        graph_400_25_z.SetMarkerColor(kGreen)
    
        
       # graph_1600_z.Draw("AP")
       # graph_400_z.Draw("P")
       # graph_800_z.Draw("P")
        #c1.cd(2)
        
        pad1.Draw()
        pad1.cd()
        graph_400_sq.Draw("AP")
        #graph_800_sq.Draw("P")
        #graph_1600_sq.Draw("P")
        
        pad2.Draw()
        pad2.cd()
        #graph_400_25_z.Draw("APY+")
        #graph_400_10_z.Draw("P")
        #graph_400_5_z.Draw("P")

        if (HistoName == "maxMVAResponse" or HistoName == "nBTags"):
            leg=TLegend(0.175,0.7,0.35,0.95)
        else :   
            leg=TLegend(0.675,0.7,0.85,0.95)
        if (HistoName == "maxMVAResponse" or HistoName == "nBTags"):
            ATLAS_LABEL(0.20,0.885,"Simulation",1,0.16);   
            myText(0.20,0.835,1,"Internal")  
        else :   
            ATLAS_LABEL(0.45,0.885,"Simulation",1,0.16);  
            myText(0.45,0.835,1,"Internal")     
        #ATLAS_LABEL(0.50,0.885,"Simulation",1,0.16);    
        ###leg.SetHeader("m=400 GeV","C")
        leg.SetTextAlign(12)
        #leg.AddEntry(graph_400_25_z,"z_{ 2.5%}","P")
        #leg.AddEntry(graph_400_5_z,"z_{5%}","P")
        #leg.AddEntry(graph_400_10_z,"z_{10%}","P")
        ###leg.AddEntry(graph_400_sq,"S/#sqrt{B}","P")
        
        #leg.Draw()

        print x 
            

           # graphZ.Draw("ACP")
        #myText(0.50,0.835,1,"work-in-progress")   
        c1.Update()
        c1.SaveAs("../Plots/Sig_Plots/m1200/m1200_%s_newBDT_inclusive_1310_1200Res_lowerbound.pdf"% (HistoName))
        pad1.Close()
        pad2.Close()
        c1.Close()
        
        
        max_sq=GetMaximumX(x,y)
        max_5=GetMaximumX(x,z_5)
        max_25=GetMaximumX(x,z_25)
        max_10=GetMaximumX(x,z_10)
        f=open("../max_z/max_m1200.txt","a")
        f.write(HistoName+";"+str(max_sq)+";"+str(max_25)+";"+str(max_5)+";"+str(max_10)+";"+str(bin_breite)+"\n")
        f.close()

       
        
def GetNumberOfEvents(lowerBound,histo):
  #  x_lowerbound=histo.FindBin(lowerBound)
    x_lowerbound=lowerBound
    x_upperbound=histo.GetNbinsX()
    if x_lowerbound<1:
        return;
    n=histo.Integral(x_lowerbound,x_upperbound)
    return n

def GetNumberOfEventsRevers(upperBound,histo):
    x_lowerbound=1
    x_upperbound=upperBound
    if x_lowerbound<1:
        return;
    n=histo.Integral(x_lowerbound,x_upperbound)
    return n

def GetMaximumX(x,y):
    maximum=0
    x_maximum=0
    for i in range(0,len(x)):
        if y[i]>maximum:
            maximum=y[i]
            x_maximum=x[i]
    return x_maximum


Loop()
 
        



