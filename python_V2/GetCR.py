# -*- coding: utf-8 -*-
#python
import sys
import glob
import math
import re
from ROOT import *
from array import *
#from ROOT import TCanvas, TFile, TLine, TProfile, TNtuple, TH1F, TH2F
#import ROOT
#import array

gROOT.SetBatch(True)
gStyle.SetOptStat(0)
gStyle.SetPalette(1)
gROOT.LoadMacro("../style/AtlasStyle.C")
gROOT.LoadMacro("../style/AtlasUtils.C")
SetAtlasStyle()

y_axis_label="Event fraction"

c_blue   = TColor.GetColor("#3366ff")
c_red    = TColor.GetColor("#ee3311")
c_orange = TColor.GetColor("#ff9900")

def NormalizeHisto(histo):
     #n_events=histo.Integral(-1,histo.GetNbinsX()+1)
     n_events=histo.Integral()
     if n_events == 0:
         return
     print n_events, histo.Integral(histo.GetNbinsX(),histo.GetNbinsX()+1)
     histo.Scale(1./n_events)
     histo.SetLineWidth(2)
     histo.SetStats(0)
     histo.SetFillStyle(3001)
     histo.SetMarkerColor(histo.GetLineColor())
     histo.SetMarkerSize(0.0)
     histo.GetXaxis().SetTitleOffset(1.2)
     histo.GetYaxis().SetTitleOffset(1.52)
     histo.GetXaxis().SetLabelSize(0.05)
     histo.GetYaxis().SetLabelSize(0.05)
     histo.GetXaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetNdivisions(504)
     histo.GetXaxis().SetNdivisions(504)

c1 = TCanvas("CRPlot","",500,500)

#for HistoName in ["nBTags"]:
#for HistoName in ["nBTags","nJets","HT","HT_bjets","DeltaPhi_HW","mVH","mH","pTWplus","pTH","maxMVAResponse", "mass_resolution"]: # for resolved, histoname set
for HistoName in ["mVH"]: #for boosted, histoname set
## for bTagStrategy in ["Incl","FourPlusTags","ThreeTags","TwoTags"]:
 for Region in ["Resolved_SR"]:
  for btagStrategy in ["Inclusive"]:
     
      ReBin = False
      YAxisScale = 1.4

      
      if "mVH" in HistoName:
          Xaxis_label="Mass of Charged Higgs [GeV]"
         
      #Xaxis_label=""

      #file1       = TFile.Open("../PlotFiles/ForOptimisation/sig_Hplus_Wh_m400-0_70p.root","READ")
      file_sig400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR2308/sig_Hplus_Wh_m400-0_70p.root","READ")
      dir_sig400_06       = file_sig400.GetDirectory("Nominal").GetDirectory(HistoName+"_6")
      dir_sig400       = file_sig400.GetDirectory("Nominal").GetDirectory(HistoName)
      h_sig_Hplus_m400_06 = dir_sig400_06.Get("sig_Hplus_Wh_m400-0_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_sig_Hplus_m400 = dir_sig400.Get("sig_Hplus_Wh_m400-0_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_sig_Hplus_m400_06.SetLineColor(kRed)
      h_sig_Hplus_m400_06.SetLineStyle(7)
      if ReBin == True:
          h_sig_Hplus_m400_06.Rebin(2)

      #file6	  = TFile.Open("../PlotFiles/ForOptimisation/sig_Hplus_Wh_m800-0_70p.root","READ")
      file_sig800	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR2308/sig_Hplus_Wh_m800-0_70p.root","READ")
      dir_sig800_06        = file_sig800.GetDirectory("Nominal").GetDirectory(HistoName+"_6")
      dir_sig800        = file_sig800.GetDirectory("Nominal").GetDirectory(HistoName)
      h_sig_Hplus_m800_06 = dir_sig800_06.Get("sig_Hplus_Wh_m800-0_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_sig_Hplus_m800 = dir_sig800.Get("sig_Hplus_Wh_m800-0_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_sig_Hplus_m800_06.SetLineColor(kBlack)
      h_sig_Hplus_m800_06.SetLineStyle(7)
      if ReBin == True:
          h_sig_Hplus_m800_06.Rebin(2)

      #file7	  = TFile.Open("../PlotFiles/ForOptimisation/sig_Hplus_Wh_m1600-0_70p.root","READ")
      file_sig1600	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR2308/sig_Hplus_Wh_m1600-0_70p.root","READ")
      dir_sig1600_06        = file_sig1600.GetDirectory("Nominal").GetDirectory(HistoName+"_6")
      dir_sig1600        = file_sig1600.GetDirectory("Nominal").GetDirectory(HistoName)
      h_sig_Hplus_m1600_06 = dir_sig1600_06.Get("sig_Hplus_Wh_m1600-0_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_sig_Hplus_m1600 = dir_sig1600.Get("sig_Hplus_Wh_m1600-0_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_sig_Hplus_m1600_06.SetLineColor(kBlue)
      h_sig_Hplus_m1600_06.SetLineStyle(7)
      if ReBin == True:
          h_sig_Hplus_m1600_06.Rebin(2)
        
     # #file2   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/ttbar_70p.root","READ")

      file_nom   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR2308/Comb_70p.root","READ")
      dir_nom    = file_nom.GetDirectory("Nominal").GetDirectory(HistoName)
      h_tt_nom = dir_nom.Get("ttbar_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_t_nom = dir_nom.Get("singleTop_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Zjets_nom = dir_nom.Get("Zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Wjets_nom = dir_nom.Get("Wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_diboson_nom = dir_nom.Get("Zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_nom= h_tt_nom+h_t_nom+h_Zjets_nom+h_Wjets_nom+h_diboson_nom
      h_nom.SetLineColor(kGreen)
      h_nom.SetLineStyle(3)
      if ReBin == True:
          h_nom.Rebin(2)

 
      file_04   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR2308/Comb_70p.root","READ")
      dir_04    = file_04.GetDirectory("Nominal").GetDirectory(HistoName+"_04")
      h_tt_04 = dir_04.Get("ttbar_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_t_04 = dir_04.Get("singleTop_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Zjets_04 = dir_04.Get("Zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Wjets_04 = dir_04.Get("Wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_diboson_04 = dir_04.Get("diboson_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_04= h_tt_04+h_t_04+h_Zjets_04+h_Wjets_04+h_diboson_04
      h_04.SetLineColor(kGreen)
      h_04.SetLineStyle(3)
      if ReBin == True:
          h_04.Rebin(2)

      file_24   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR2308/Comb_70p.root","READ")
      dir_24    = file_24.GetDirectory("Nominal").GetDirectory(HistoName+"_24")
      h_tt_24 = dir_24.Get("ttbar_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_t_24 = dir_24.Get("singleTop_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Zjets_24 = dir_24.Get("Zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Wjets_24 = dir_24.Get("Wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_diboson_24 = dir_24.Get("diboson_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_24= h_tt_24+h_t_24+h_Zjets_24+h_Wjets_24+h_diboson_24
      h_24.SetLineColor(kCyan)
      h_24.SetLineStyle(3)
      if ReBin == True:
          h_24.Rebin(2) 
      
      file_44   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR2308/Comb_70p.root","READ")
      dir_44    = file_44.GetDirectory("Nominal").GetDirectory(HistoName+"_44")
      h_tt_44 = dir_44.Get("ttbar_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_t_44 = dir_44.Get("singleTop_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Zjets_44 = dir_44.Get("Zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Wjets_44 = dir_44.Get("Wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_diboson_44 = dir_44.Get("diboson_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_44= h_tt_44+h_t_44+h_Zjets_44+h_Wjets_44+h_diboson_44
      h_44.SetLineColor(kTeal)
      h_44.SetLineStyle(3)
      if ReBin == True:
          h_44.Rebin(2)  

      file_03   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR2308/Comb_70p.root","READ")
      dir_03    = file_03.GetDirectory("Nominal").GetDirectory(HistoName+"_03")
      h_tt_03 = dir_03.Get("ttbar_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_t_03 = dir_03.Get("singleTop_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Zjets_03 = dir_03.Get("Zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Wjets_03 = dir_03.Get("Wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_diboson_03 = dir_03.Get("diboson_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_03= h_tt_03+h_t_03+h_Zjets_03+h_Wjets_03+h_diboson_03
      h_03.SetLineColor(kGreen+4)
      h_03.SetLineStyle(3)
      if ReBin == True:
          h_03.Rebin(2) 

      file_23   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR2308/Comb_70p.root","READ")
      dir_23    = file_23.GetDirectory("Nominal").GetDirectory(HistoName+"_23")
      h_tt_23 = dir_23.Get("ttbar_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_t_23 = dir_23.Get("singleTop_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Zjets_23 = dir_23.Get("Zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Wjets_23 = dir_23.Get("Wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_diboson_23 = dir_23.Get("diboson_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_23= h_tt_23+h_t_23+h_Zjets_23+h_Wjets_23+h_diboson_23
      h_23.SetLineColor(kSpring)
      h_23.SetLineStyle(3)
      if ReBin == True:
          h_23.Rebin(2) 

      file_43   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR2308/Comb_70p.root","READ")
      dir_43    = file_43.GetDirectory("Nominal").GetDirectory(HistoName+"_43")
      h_tt_43 = dir_43.Get("ttbar_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_t_43 = dir_43.Get("singleTop_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Zjets_43 = dir_43.Get("Zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Wjets_43 = dir_43.Get("Wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_diboson_43 = dir_43.Get("diboson_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_43= h_tt_43+h_t_43+h_Zjets_43+h_Wjets_43+h_diboson_43
      h_43.SetLineColor(kYellow)
      h_43.SetLineStyle(3)
      if ReBin == True:
          h_43.Rebin(2)

      file_05   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR2308/Comb_70p.root","READ")
      dir_05    = file_05.GetDirectory("Nominal").GetDirectory(HistoName+"_05")
      h_tt_05 = dir_05.Get("ttbar_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_t_05 = dir_05.Get("singleTop_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Zjets_05 = dir_05.Get("Zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Wjets_05 = dir_05.Get("Wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_diboson_05 = dir_05.Get("diboson_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_05= h_tt_05+h_t_05+h_Zjets_05+h_Wjets_05+h_diboson_05
      h_05= h_tt_05 +h_t_05
      h_05.SetLineColor(kOrange)
      h_05.SetLineStyle(3)
      if ReBin == True:
          h_05.Rebin(2) 

      file_25   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR2308/Comb_70p.root","READ")
      dir_25    = file_25.GetDirectory("Nominal").GetDirectory(HistoName+"_25")
      h_tt_25 = dir_25.Get("ttbar_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_t_25 = dir_25.Get("singleTop_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Zjets_25 = dir_25.Get("Zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Wjets_25 = dir_25.Get("Wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_diboson_25 = dir_25.Get("diboson_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_25= h_tt_25+h_t_25+h_Zjets_25+h_Wjets_25+h_diboson_25
      h_25.SetLineColor(kPink)
      h_25.SetLineStyle(3)
      if ReBin == True:
          h_25.Rebin(2) 

      file_45   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR2308/Comb_70p.root","READ")
      dir_45    = file_45.GetDirectory("Nominal").GetDirectory(HistoName+"_45")
      h_tt_45 = dir_45.Get("ttbar_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_t_45 = dir_45.Get("singleTop_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Zjets_45 = dir_45.Get("Zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Wjets_45 = dir_45.Get("Wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_diboson_45 = dir_45.Get("diboson_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_45= h_tt_45+h_t_45+h_Zjets_45+h_Wjets_45+h_diboson_45
      h_45.SetLineColor(kMagenta)
      h_45.SetLineStyle(3)
      if ReBin == True:
          h_45.Rebin(2)

      file_10   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR2308/Comb_70p.root","READ")
      dir_10    = file_10.GetDirectory("Nominal").GetDirectory(HistoName+"_10")
      h_tt_10 = dir_10.Get("ttbar_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_t_10 = dir_10.Get("singleTop_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Zjets_10 = dir_10.Get("Zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Wjets_10 = dir_10.Get("Wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_diboson_10 = dir_10.Get("diboson_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_10= h_tt_10+h_t_10+h_Zjets_10+h_Wjets_10+h_diboson_10 
      h_10.SetLineColor(kTeal+3)
      h_10.SetLineStyle(3)
      if ReBin == True:
          h_10.Rebin(2) 

      file_50   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR2308/Comb_70p.root","READ")
      dir_50    = file_50.GetDirectory("Nominal").GetDirectory(HistoName+"_50")
      h_tt_50 = dir_50.Get("ttbar_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_t_50 = dir_50.Get("singleTop_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Zjets_50 = dir_50.Get("Zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Wjets_50 = dir_50.Get("Wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_diboson_50 = dir_50.Get("diboson_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_50= h_tt_50+h_t_50+h_Zjets_50+h_Wjets_50+h_diboson_50 
      h_50.SetLineColor(kBlue+4)
      h_50.SetLineStyle(3)
      if ReBin == True:
          h_50.Rebin(2)


      file_53   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR2308/Comb_70p.root","READ")
      dir_53    = file_53.GetDirectory("Nominal").GetDirectory(HistoName+"_53")
      h_tt_53 = dir_53.Get("ttbar_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_t_53 = dir_53.Get("singleTop_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Zjets_53 = dir_53.Get("Zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Wjets_53 = dir_53.Get("Wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_diboson_53 = dir_53.Get("diboson_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_53= h_tt_53+h_t_53+h_Zjets_53+h_Wjets_53+h_diboson_53 
      h_53.SetLineColor(kBlue+5)
      h_53.SetLineStyle(3)
      if ReBin == True:
          h_53.Rebin(2)

      file_54   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR2308/Comb_70p.root","READ")
      dir_54    = file_54.GetDirectory("Nominal").GetDirectory(HistoName+"_54")
      h_tt_54 = dir_54.Get("ttbar_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_t_54 = dir_54.Get("singleTop_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Zjets_54 = dir_54.Get("Zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Wjets_54 = dir_54.Get("Wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_diboson_54 = dir_54.Get("diboson_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_54= h_tt_54+h_t_54+h_Zjets_54+h_Wjets_54+h_diboson_54 
      h_54.SetLineColor(kMagenta)
      h_54.SetLineStyle(3)
      if ReBin == True:
          h_54.Rebin(2)


      file_52   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR2308/Comb_70p.root","READ")
      dir_52    = file_52.GetDirectory("Nominal").GetDirectory(HistoName+"_52")
      h_tt_52 = dir_52.Get("ttbar_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_t_52 = dir_52.Get("singleTop_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Zjets_52 = dir_52.Get("Zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Wjets_52 = dir_52.Get("Wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_diboson_52 = dir_52.Get("diboson_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_52= h_tt_52+h_t_52+h_Zjets_52+h_Wjets_52+h_diboson_52 
      h_52.SetLineColor(kGreen+6)
      h_52.SetLineStyle(3)
      if ReBin == True:
          h_52.Rebin(2)


      file_42   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR2308/Comb_70p.root","READ")
      dir_42    = file_42.GetDirectory("Nominal").GetDirectory(HistoName+"_42")
      h_tt_42 = dir_42.Get("ttbar_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_t_42 = dir_42.Get("singleTop_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Zjets_42 = dir_42.Get("Zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Wjets_42 = dir_42.Get("Wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_diboson_42 = dir_42.Get("diboson_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_42= h_tt_42+h_t_42+h_Zjets_42+h_Wjets_42+h_diboson_42 
      h_42.SetLineColor(kBlue+6)
      h_42.SetLineStyle(3)
      if ReBin == True:
          h_42.Rebin(2)    

      file_06   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR2308/Comb_70p.root","READ")
      dir_06    = file_06.GetDirectory("Nominal").GetDirectory(HistoName+"_6")
      h_tt_06 = dir_06.Get("ttbar_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_t_06 = dir_06.Get("singleTop_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Zjets_06 = dir_06.Get("Zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Wjets_06 = dir_06.Get("Wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_diboson_06 = dir_06.Get("diboson_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_06= h_tt_06+h_t_06+h_Zjets_06+h_Wjets_06+h_diboson_06
      h_06.SetLineColor(kRed)
      h_06.SetLineStyle(1)
      if ReBin == True:
          h_06.Rebin(2)

      file_5   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR2308/Comb_70p.root","READ")
      dir_5    = file_5.GetDirectory("Nominal").GetDirectory(HistoName+"_5")
      h_tt_5 = dir_5.Get("ttbar_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_t_5 = dir_5.Get("singleTop_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Zjets_5 = dir_5.Get("Zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Wjets_5 = dir_5.Get("Wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_diboson_5 = dir_5.Get("diboson_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_5= h_tt_5+h_t_5+h_Zjets_5+h_Wjets_5+h_diboson_5
      h_5.SetLineColor(kRed)
      h_5.SetLineStyle(1)
      if ReBin == True:
          h_5.Rebin(2)

      file_6   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR2308/Comb_70p.root","READ")
      dir_6    = file_6.GetDirectory("Nominal").GetDirectory(HistoName+"_6")
      h_tt_6 = dir_6.Get("ttbar_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_t_6 = dir_6.Get("singleTop_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Zjets_6 = dir_6.Get("Zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Wjets_6 = dir_6.Get("Wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_diboson_6 = dir_6.Get("diboson_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_6= h_tt_6+h_t_6+h_Zjets_6+h_Wjets_6+h_diboson_6
      h_6.SetLineColor(kRed)
      h_6.SetLineStyle(1)
      if ReBin == True:
          h_6.Rebin(2)    

     # h_other_background = h_singleTop_background + h_diboson_background + h_W_background + h_Z_background     
      
      print("h_05 contribution is", h_05.Integral()/h_nom.Integral())
      print("h_25 contribution is", h_25.Integral()/h_nom.Integral())
      print("h_45 contribution is", h_45.Integral()/h_nom.Integral())
      print("h_04 contribution is", h_04.Integral()/h_nom.Integral())
      print("h_24 contribution is", h_24.Integral()/h_nom.Integral())
      print("h_44 contribution is", h_44.Integral()/h_nom.Integral())
      print("h_03 contribution is", h_03.Integral()/h_nom.Integral())
      print("h_23 contribution is", h_23.Integral()/h_nom.Integral())
      print("h_43 contribution is", h_43.Integral()/h_nom.Integral())
      print("h_10 contribution is", h_10.Integral()/h_nom.Integral())
      print("h_50 contribution is", h_50.Integral()/h_nom.Integral())
      print("h_06 contribution is", h_06.Integral()/h_nom.Integral())
      print("h_53 contribution is", h_53.Integral()/h_nom.Integral())
      print("h_54 contribution is", h_54.Integral()/h_nom.Integral())
      print("h_52 contribution is", h_52.Integral()/h_nom.Integral())
      print("h_42 contribution is", h_42.Integral()/h_nom.Integral())
      print("h_5 contribution is", h_5.Integral()/h_nom.Integral())


      NormalizeHisto(h_05)
      NormalizeHisto(h_25)
      NormalizeHisto(h_45)
      NormalizeHisto(h_04)
      NormalizeHisto(h_24)
      NormalizeHisto(h_44)
      NormalizeHisto(h_03)
      NormalizeHisto(h_23)
      NormalizeHisto(h_43)
      NormalizeHisto(h_10)
      NormalizeHisto(h_50)
      NormalizeHisto(h_06)
      NormalizeHisto(h_53)
      NormalizeHisto(h_54)
      NormalizeHisto(h_52)
      NormalizeHisto(h_42)
      NormalizeHisto(h_5)
      
      nbins=20
      ymax=0
      
      if ymax<h_24.GetMaximum():
          ymax=h_24.GetMaximum()
      
      if ymax<h_10.GetMaximum():
          ymax=h_10.GetMaximum()
      
      if ymax<h_50.GetMaximum():
          ymax=h_50.GetMaximum()
      
      if ymax<h_54.GetMaximum():
          ymax=h_54.GetMaximum()
      
      if ymax<h_23.GetMaximum():
          ymax=h_23.GetMaximum()
      
      if ymax<h_5.GetMaximum():
          ymax=h_5.GetMaximum()


      h_24.Draw("HIST")
      h_05.Draw("HISTSAME")
      h_25.Draw("HISTSAME")
      h_04.Draw("HISTSAME")
      h_10.Draw("HISTSAME")
      h_50.Draw("HISTSAME")
      h_54.Draw("HISTSAME")
      h_23.Draw("HISTSAME")
      h_5.Draw("HISTSAME")

      if HistoName in "maxMVAResponse":
         leg = TLegend(0.2,0.65,0.725,0.855)
      else:
         leg = TLegend(0.45,0.65,0.925,0.855)
      ATLAS_LABEL(0.20,0.885," Simulation Internal",1,0.19);
      leg.SetShadowColor(kWhite)
      leg.SetFillColor(kWhite)
      leg.SetLineColor(kWhite)
      
      leg.AddEntry(h_24,    " MVA Score =(-0.2, 0.4)","L")
      leg.AddEntry(h_10,    " MVA Score =(-1.0, 0.0)","L")
      leg.AddEntry(h_50,    " MVA Score =(-0.5, 0.0)","L")
      leg.AddEntry(h_54,    " MVA Score =(-0.5, 0.4)","L")
      leg.AddEntry(h_23,    " MVA Score =(-0.2, 0.3)","L")
      leg.AddEntry(h_5,    " MVA Score =(0.5, 1.0)","L")
      
      leg.SetTextSize(0.0250)
      leg.Draw()
      h_24.GetXaxis().SetTitle(Xaxis_label)
      h_24.GetYaxis().SetRangeUser(0.001,round(ymax*1.25,3)+0.001)
      h_24.GetYaxis().SetTitle("Normalised Entries")
      
      c1.RedrawAxis()
      c1.Update()
      c1.RedrawAxis()
      c1.SaveAs("../Plots/mWH_CR_5_HDBS.pdf")

base= "/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR2308/"
for FileName in ["sig_Hplus_Wh_m400-0_", "sig_Hplus_Wh_m800-0_", "sig_Hplus_Wh_m1600-0_"]: #for boosted, histoname set
## for bTagStrategy in ["Incl","FourPlusTags","ThreeTags","TwoTags"]:
   for CR in ["_05", "_25", "_45", "_04", "_24", "_44", "_03", "_23", "_43", "_10", "_50", "_54", "_53", "_52", "_42"]:


                 file       = TFile.Open(base+FileName+"70p.root","READ")
                 dir       = file.GetDirectory("Nominal").GetDirectory("mVH"+CR)
                 dir_nom = file.GetDirectory("Nominal").GetDirectory("mVH")
                 h = dir.Get(FileName+"mVH_Resolved_SR_Inclusive_Nominal")
                 h_nom =  dir_nom.Get(FileName+"mVH_Resolved_SR_Inclusive_Nominal")
                 #h.Divide(h_nom)
                 #NormalizeHisto(h)
                 print(FileName+CR+"contribution is", h.Integral()/h_nom.Integral())


































      
