# -*- coding: utf-8 -*-
#python
import ROOT
import sys
import glob
import math
import re
from ROOT import *
from array import *
from ROOT import TF1

gROOT.SetBatch(True)
gStyle.SetOptStat(0)
gStyle.SetPalette(1)
gROOT.LoadMacro("../style/AtlasStyle.C")
gROOT.LoadMacro("../style/AtlasUtils.C")
SetAtlasStyle()
#from ROOT import rootpy.plotting

import numpy as np
from numpy import ndarray
import array
#from rootpy.plotting import Hist, HistStack, Legend, Canvas
#from rootpy.plotting.style import get_style, set_style
#from rootpy.plotting.utils import draw
#from rootpy.interactive import wait
#import rootpy.plotting.root2matplotlib as rplt
from ROOT import TCanvas, TFile, TPad, THStack, TLine,TGraphAsymmErrors, TNtuple, TH1F, TH2F
#from ROOT import TGraphAsymmErrors
from array import *
#from ROOT import TLatex
#import ROOT
#import array

y_axis_label="Event fraction"

c_blue   = TColor.GetColor("#3366ff")
c_red    = TColor.GetColor("#ee3311")
c_orange = TColor.GetColor("#ff9900")

c1 = TCanvas("WeightnJet-qqbb","",700,500)
#pad1 = TPad("pad1", "pad1", 0.0, 0.05, 1,1)

#pad1.SetLeftMargin(0.15)
#pad1.SetTopMargin(0.1)
##pad1.SetBottomMargin(0.0105)
#pad1.SetRightMargin(0.08)
#pad1.Draw()
#pad1.SetTicks()

#pad2.SetLeftMargin(0.15)
#pad2.SetTopMargin(0.04)
#pad2.SetBottomMargin(0.35)
#pad2.SetRightMargin(0.08)
#pad2.Draw()
#pad2.SetTicks()

for HistoName in ["nJet"]:       
 for Region in ["Resolved_SR"]:
  for btagStrategy in ["TwoTags"]:    
     
      ReBin = False
      YAxisScale = 1.4               
      #if "nJet" in HistoName:
          #Xaxis_label="Jet multiplicity"           
      
      file_data15       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/data_leptop_NewTrain_ttRew/data_2015.root_77p_225_.root","READ")
      dir_15        = file_data15.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_data15 = dir_15.Get("data_2015_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      
      file_data16       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/data_leptop_NewTrain_ttRew/data_2016.root_77p_225_.root","READ")  
      dir_16        = file_data16.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_data16 = dir_16.Get("data_2016_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      file_data17       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/data_leptop_NewTrain_ttRew/data_2017.root_77p_225_.root","READ")
      dir_17        = file_data17.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_data17 = dir_17.Get("data_2017_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      file_data18      = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/data_leptop_NewTrain_ttRew/data_2018.root_77p_225_.root","READ")
      dir_18        = file_data18.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_data18 = dir_18.Get("data_2018_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      
      file_tt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew/tt_PP8.root_77p_225.root","READ")
      dir_tt_1b   = file_tt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1b")
      h_ttbar_background_1b = dir_tt_1b.Get("tt_PP8_1B_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      dir_tt_1c   = file_tt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1c")
      h_ttbar_background_1c = dir_tt_1c.Get("tt_PP8_1C_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      dir_tt_1l   = file_tt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1l")
      h_ttbar_background_1l = dir_tt_1l.Get("tt_PP8_1L_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      file_tt_filt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew/tt_PP8filtered.root_77p_225.root","READ")
      dir_tt_1b_filt   = file_tt_filt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1b")
      h_ttbar_background_1b_filt = dir_tt_1b_filt.Get("tt_PP8filtered_1B_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      dir_tt_1c_filt   = file_tt_filt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1c")
      h_ttbar_background_1c_filt = dir_tt_1c_filt.Get("tt_PP8filtered_1C_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      dir_tt_1l_filt   = file_tt_filt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1l")
      h_ttbar_background_1l_filt = dir_tt_1l_filt.Get("tt_PP8filtered_1L_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      h_ttbar_background = h_ttbar_background_1b + h_ttbar_background_1c + h_ttbar_background_1l + h_ttbar_background_1b_filt + h_ttbar_background_1c_filt + h_ttbar_background_1l_filt
      #h_ttbar_background = h_ttbar_background_1b + h_ttbar_background_1c + h_ttbar_background_1l
               
      file_st_tc   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew/st_tc.root_77p_225.root","READ")
      dir_st_tc   = file_st_tc.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_singleTop_background_tc = dir_st_tc.Get("st_tc_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      file_st_sc   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew/st_sc.root_77p_225.root","READ")
      dir_st_sc   = file_st_sc.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_singleTop_background_sch = dir_st_sc.Get("st_sc_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      
      file_Wt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew/Wt.root_77p_25.root","READ")
      dir_Wt   = file_Wt.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_Wt = dir_Wt.Get("Wt_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      file_tH   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew/tH_AFII.root_77p_225.root","READ")
      dir_tH   = file_tH.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_tH = dir_tH.Get("tH_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      file_tWZ   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew/tWZ.root_77p_225.root","READ")
      dir_tWZ   = file_tWZ.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_tWZ = dir_tWZ.Get("tWZ_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      file_tZ   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew/tZ.root_77p_225.root","READ")
      dir_tZ   = file_tZ.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_tZ = dir_tZ.Get("tZ_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      file_Wjet   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew/wjets_77p_225.root","READ")
      dir_Wjet    = file_Wjet.GetDirectory("nominal_Loose").GetDirectory(HistoName)        
      h_Wjet = dir_Wjet.Get("wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      file_Zjet   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew/zjets_77p_225.root","READ")
      dir_Zjet    = file_Zjet.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_Zjet = dir_Zjet.Get("zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      
      file_ttW   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew/ttW.root_77p_225.root","READ")
      dir_ttW    = file_ttW.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_ttW_background = dir_ttW.Get("ttW_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      file_ttH   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew/ttH_PP8.root_77p_225.root","READ")
      dir_ttH    = file_ttH.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_ttH_background = dir_ttH.Get("ttH_PP8_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      file_ttZ   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew/ttll.root_77p_225.root","READ")
      dir_ttZ    = file_ttZ.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_ttZ_background = dir_ttZ.Get("ttll_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      file_dib   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew/db.root_77p_225.root","READ")
      dir_dib   = file_dib.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_diboson_background = dir_dib.Get("db_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      
      h_data = h_data15 + h_data16 + h_data17 + h_data18
      h_nontt = h_diboson_background + h_singleTop_background_tc + h_singleTop_background_sch + h_Wt + h_tH + h_tWZ + h_tZ + h_Zjet + h_Wjet + h_ttW_background + h_ttH_background + h_ttZ_background
      
      #npars = 6
      h_weight = h_data.Clone()
      h_weight = h_weight - h_nontt
      h_weight.Divide(h_ttbar_background)
      
      Nbins_X = h_weight.GetNbinsX()
      #print Nbins_X
      sum_data = 0
      sum_tt = 0
      sum_nontt = 0
      for i in range (0,14):
           sum_data += h_data.GetBinContent(i)
           sum_tt += h_ttbar_background.GetBinContent(i)
           sum_nontt += h_nontt.GetBinContent(i)

      weight_ge5 = (sum_data - sum_nontt)/sum_tt  
      print weight_ge5

      h_weight.SetMarkerStyle(20)
      h_weight.SetMarkerSize(1.0)
      h_weight.SetMarkerColor(1)
      h_weight.GetXaxis().SetTitle("Number of jets")
      h_weight.GetYaxis().SetTitle("Weight factor")
      h_weight.GetYaxis().SetRangeUser(0.5,1.7)
      h_weight.GetXaxis().SetRangeUser(4.5,12.5)
      leg = TLegend(0.72,0.685,0.885,0.785)
      leg.SetShadowColor(kWhite)
      leg.SetFillColor(kWhite)
      leg.SetLineColor(kWhite)
      h_weight.Draw("EP")  
      leg.AddEntry(h_weight,"Powheg+Pythia8","epl")
      leg.Draw()
      if "TwoTags" in btagStrategy:
          myText(0.35,0.88,1,"l^{-}+jets Resolved: at least 5-jet, 2 b-tag")  
      #c1.RedrawAxis()
      c1.RedrawAxis()
      c1.Update()
      c1.RedrawAxis()
      #c1.SaveAs("../Plots/weight/Weight_%s_ttRew.pdf" % (HistoName+"_"+btagStrategy))
