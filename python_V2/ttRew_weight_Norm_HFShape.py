# -*- coding: utf-8 -*-
#python
import sys
import glob
import math
import re
from ROOT import *
from array import *
from ROOT import TCanvas, TFile, TLine, TProfile, TNtuple, TH1F, TH2F
#import ROOT
#import array

#################----------#################################################
#This macro plots the normalised tt+HF Shape for different b-tag categories
#################----------#################################################

gROOT.SetBatch(True)
gStyle.SetOptStat(0)
gStyle.SetPalette(1)
gROOT.LoadMacro("../style/AtlasStyle.C")
gROOT.LoadMacro("../style/AtlasUtils.C")
SetAtlasStyle()

y_axis_label="Event fraction"

c_blue   = TColor.GetColor("#3366ff")
c_red    = TColor.GetColor("#ee3311")
c_orange = TColor.GetColor("#ff9900")

def NormalizeHisto(histo):
     n_events=histo.Integral(-1,histo.GetNbinsX()+1)
     #n_events=histo.Integral()
     if n_events == 0:
         return
     print n_events, histo.Integral(histo.GetNbinsX(),histo.GetNbinsX()+1)
     histo.Scale(1./n_events)
     histo.SetLineWidth(3)
     histo.SetStats(0)
     histo.SetFillStyle(3001)
     histo.SetMarkerColor(histo.GetLineColor())
     histo.SetMarkerSize(0.0)
     histo.GetXaxis().SetTitleOffset(1.2)
     histo.GetYaxis().SetTitleOffset(1.52)
     histo.GetXaxis().SetLabelSize(0.05)
     histo.GetYaxis().SetLabelSize(0.05)
     histo.GetXaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetNdivisions(504)
     histo.GetXaxis().SetNdivisions(504)

c1 = TCanvas("ShapePlots","",700,500)
pad1 = TPad("pad1", "pad1", 0.0, 0.35, 1,1)
#pad1.SetLogy()
pad2 = TPad("pad2", "pad2", 0, 0.0, 1,0.35)
pad1.SetLeftMargin(0.15)
pad1.SetTopMargin(0.1)
pad1.SetBottomMargin(0.0105)
pad1.SetRightMargin(0.08)
pad1.Draw()
pad1.SetTicks()

pad2.SetLeftMargin(0.15)
pad2.SetTopMargin(0.04)
pad2.SetBottomMargin(0.35)
pad2.SetRightMargin(0.08)
pad2.Draw()
pad2.SetTicks()

HistoNameR = "lep_top_pT"
#for HistoName in ["nBTags"]:
#for HistoName in ["nBTags","nJets","HT","HT_bjets","DeltaPhi_HW","mVH","mH","pTWplus","pTH","maxMVAResponse", "mass_resolution"]: # for resolved, histoname set
#for HistoName in ["Leptonic_Top_pT"]: # for resolved, histoname set
#for HistoName in ["nBTags","nJets","HT","HT_bjets","DeltaPhi_HW","mVH","mH","pTWplus","pTH","mass_resolution"]:#for boosted, histoname set
#for HistoName in ["mass_resolution"]:
for HistoName in ["HT_all_5j","HT_all_6j","HT_all_7j","HT_all_8j"]:     
#for HistoName in ["maxMVAResponse"]:
## for bTagStrategy in ["Incl","FourPlusTags","ThreeTags","TwoTags"]:
 for Region in ["Resolved_SR"]:
  #for btagStrategy in ["FourPlusTags","ThreeTags","TwoTags"]:
  for btagStrategy in ["ThreeTags","FourPlusTags","TwoTags"]:    
   #for btagStrategy in ["Inclusive","FourPlusTags","ThreeTags","TwoTags"]:
     
      ReBin = False
      YAxisScale = 1.4

      if "nBTags" in HistoName:
          Xaxis_label="b-tag multiplicity"
      if "pTWminus" in HistoName:
          Xaxis_label="Transverse Momentum Of Wminus [GeV]"
      if "nJet" in HistoName:
          Xaxis_label="Jet Multiplicity"
      if "mWplus" in HistoName:
          Xaxis_label="Mass of Wplus [GeV]"
      if "Mwt" in HistoName:
          Xaxis_label="Transverse mass of W boson [GeV]"    
      if "mH" in HistoName:
          Xaxis_label="Mass of Higgs [GeV]"
      if "DeltaPhi_HW" in HistoName:
          Xaxis_label="DeltaPhi_HW"
      if "pTH" in HistoName:
          Xaxis_label="Transverse Momentum of Higgs [GeV]"
      if "pTWplus" in HistoName:
          Xaxis_label="Transverse Momentum of W Boson [GeV]"
      if "mVH" in HistoName:
          Xaxis_label="Mass of Charged Higgs [GeV]"
      if "mH" in HistoName:
          Xaxis_label="Mass of Higgs [GeV]"
      if "pTWplus" in HistoName:
          Xaxis_label="Transverse Momentum Of Wplus [GeV]"
      if "pTH" in HistoName:
          Xaxis_label="Transverse Momentum Of Higgs [GeV]"
      if "mass_resolution" in HistoName:
          Xaxis_label="Mass Resolution"
      if "HT" in HistoName:
          Xaxis_label="H_{T} (Scalar Transverse Momentum Sum of jets) [GeV]"
      if "HT_all" in HistoName:
          Xaxis_label="H_{T}_{all} [GeV]"    
      if "HT_bjets" in HistoName:
          Xaxis_label="H_{T_{b-jet}} (Scalar Transverse Momentum Sum of b-jets) [GeV]"
      if "maxMVAResponse" in HistoName:
          Xaxis_label="BDT Score (Signal Reconstruction)"
      if "mass_resolution" in HistoName:
          Xaxis_label="Leptonic Top (GeV)"    
      if "Leptonic_Top_mass" in HistoName:
          Xaxis_label="Leptonic Top mass (GeV)"  
      if "Leptonic_Top_pT" in HistoName:
          Xaxis_label="Leptonic Top pT (GeV)"     
      
      file_tt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_modHT/tt_PP8.root_77p_225.root","READ")
      #file_tt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_imp/tt_PP8.root_77p_225.root","READ")
      dir_tt_1b   = file_tt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1b")
      #h_ttbar_background_1b = dir_tt_1b.Get("tt_PP8_1B_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttbar_background_1b = dir_tt_1b.Get("tt_PP8_"+HistoName+"_1b_"+Region+"_"+btagStrategy+"_nominal_Loose")
      dir_tt_1c   = file_tt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1c")
      #h_ttbar_background_1c = dir_tt_1c.Get("tt_PP8_1C_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttbar_background_1c = dir_tt_1c.Get("tt_PP8_"+HistoName+"_1c_"+Region+"_"+btagStrategy+"_nominal_Loose")
      dir_tt_1l   = file_tt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1l")
      #h_ttbar_background_1l = dir_tt_1l.Get("tt_PP8_1L_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttbar_background_1l = dir_tt_1l.Get("tt_PP8_"+HistoName+"_1l_"+Region+"_"+btagStrategy+"_nominal_Loose")

      #file_tt_filt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_imp/tt_PP8filtered.root_77p_225.root","READ")
      file_tt_filt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_modHT/tt_PP8filtered.root_77p_225.root","READ")
      dir_tt_1b_filt   = file_tt_filt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1b")
      #h_ttbar_background_1b_filt = dir_tt_1b_filt.Get("tt_PP8filtered_1B_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttbar_background_1b_filt = dir_tt_1b_filt.Get("tt_PP8filtered_"+HistoName+"_1b_"+Region+"_"+btagStrategy+"_nominal_Loose")
      dir_tt_1c_filt   = file_tt_filt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1c")
      #h_ttbar_background_1c_filt = dir_tt_1c_filt.Get("tt_PP8filtered_1C_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttbar_background_1c_filt = dir_tt_1c_filt.Get("tt_PP8filtered_"+HistoName+"_1c_"+Region+"_"+btagStrategy+"_nominal_Loose")
      dir_tt_1l_filt   = file_tt_filt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1l")
      #h_ttbar_background_1l_filt = dir_tt_1l_filt.Get("tt_PP8filtered_1L_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttbar_background_1l_filt = dir_tt_1l_filt.Get("tt_PP8filtered_"+HistoName+"_1l_"+Region+"_"+btagStrategy+"_nominal_Loose")

      h_ttbar_background = h_ttbar_background_1b + h_ttbar_background_1c + h_ttbar_background_1l + h_ttbar_background_1b_filt + h_ttbar_background_1c_filt + h_ttbar_background_1l_filt
      h_ttbar_background_1l = h_ttbar_background_1l + h_ttbar_background_1l_filt
      h_ttbar_background_1c = h_ttbar_background_1c + h_ttbar_background_1c_filt
      h_ttbar_background_1b = h_ttbar_background_1b + h_ttbar_background_1b_filt

      #h_ttbar_background = h_ttbar_background_1b + h_ttbar_background_1c + h_ttbar_background_1l 
      h_ttbar_background_tb = h_ttbar_background.Clone()
      h_ttbar_background_1b_tb = h_ttbar_background_1b.Clone()
      h_ttbar_background_1l_tb = h_ttbar_background_1l.Clone()
      h_ttbar_background_1c_tb = h_ttbar_background_1c.Clone()

      #########################Hyperbola+Sigmoid Functions for HT_all distributions without any norm contribution################################
      #if "HT_all_5j" in HistoName:
           #Rew = TF1("Rew","0.598 + 173433239.982/(x**3.598) - (-0.331)/(1+exp(-7.336 - (-0.005)*x))",0,2000)
      #if "HT_all_6j" in HistoName:
           #Rew = TF1("Rew","0.926 + 54110747.299/(x**3.283) - 0.421/(1+exp(6.184 - 0.004*x))",0,2000) 
      #if "HT_all_7j" in HistoName:
           #Rew = TF1("Rew","1.031 + 35793248.291/(x**3.118) - 33.499/(1+exp(6.482 - 0.001*x))",0,2000)   
      #if "HT_all_8j" in HistoName:
           #Rew = TF1("Rew","0.869 + 66112.487/(x**1.942) - 0.160/(1+exp(12.414 - 0.008*x))",0,2000)
      
      #########################Hyperbola+Sigmoid Functions for HT_all_mod distributions without any norm contribution################################
      if "HT_all_5j" in HistoName:
           Rew = TF1("Rew","0.605 + 34834290.982/(x**3.426) - (-0.326)/(1+exp(-7.489 - (-0.006)*x))",0,2000)
      if "HT_all_6j" in HistoName:
           Rew = TF1("Rew","0.928 + 20467135.633/(x**3.205) - 0.360/(1+exp(6.305 - 0.005*x))",0,2000) 
      if "HT_all_7j" in HistoName:
           Rew = TF1("Rew","0.863 + 132525.193/(x**2.188) - 0.219/(1+exp(10.641 - 0.008*x))",0,2000)   
      if "HT_all_8j" in HistoName:
           Rew = TF1("Rew","0.850 + 13399.837/(x**1.712) - 0.140/(1+exp(15.310 - 0.011*x))",0,2000)  

       #########################Hyperbola+Sigmoid Functions for HT_all distributions with norm contribution from H+tb analysis#####################
      #if "HT_all_5j" in HistoName:
           #Rew_tb = TF1("Rew_tb","0.594 + 141186348.143/(x**3.560) - (-0.338)/(1+exp(-7.240 - (-0.005)*x))",0,2000)
      #if "HT_all_6j" in HistoName:
           #Rew_tb = TF1("Rew_tb","0.929 + 47710259.344/(x**3.260) - 0.429/(1+exp(6.113 - 0.004*x))",0,2000)
      #if "HT_all_7j" in HistoName:
           #Rew_tb = TF1("Rew_tb","1.032 + 28848057.308/(x**3.080) - 30.621/(1+exp(6.411 - 0.001*x))",0,2000)
      #if "HT_all_8j" in HistoName:
           #Rew_tb = TF1("Rew_tb","0.867 + 48828.631/(x**1.889) - 0.160/(1+exp(12.555 - 0.008*x))",0,2000)    

      #########################Hyperbola+Sigmoid Functions for HT_all distributions with norm contribution via bcfloat, ttcon#####################
      #if "HT_all_5j" in HistoName:
           #Rew_tb = TF1("Rew_tb","0.556 + 53854543.047/(x**3.389) - (-0.334)/(1+exp(-7.020 - (-0.005)*x))",0,2000)
      #if "HT_all_6j" in HistoName:
           #Rew_tb = TF1("Rew_tb","0.876 + 23473336.614/(x**3.139) - 0.413/(1+exp(5.986 - 0.004*x))",0,2000)
      #if "HT_all_7j" in HistoName:
           #Rew_tb = TF1("Rew_tb","0.961 + 17076047.853/(x**2.995) - 3.263/(1+exp(4.255 - 0.001*x))",0,2000)
      #if "HT_all_8j" in HistoName:
           #Rew_tb = TF1("Rew_tb","0.787 + 44151.703/(x**1.876) - 0.153/(1+exp(11.906 - 0.008*x))",0,2000)
      #######################################################################################################      

      #########################Hyperbola+Sigmoid Functions for HT_all_mod distributions with norm contribution via bcfloat, ttcon#####################
      if "HT_all_5j" in HistoName:
           Rew_tb = TF1("Rew_tb","0.931 + 80580988.124/(x**3.591) - 27973.09/(1+exp(14.52 - 0.002*x))",0,2000)
      if "HT_all_6j" in HistoName:
           Rew_tb = TF1("Rew_tb","0.878 + 9397590.530/(x**3.067) - 0.354/(1+exp(6.101 - 0.005*x))",0,2000)
      if "HT_all_7j" in HistoName:
           Rew_tb = TF1("Rew_tb","0.800 + 86815.502/(x**2.117) - 0.215/(1+exp(10.017 - 0.008*x))",0,2000)
      if "HT_all_8j" in HistoName:
           Rew_tb = TF1("Rew_tb","0.770 + 11112.111/(x**1.682) - 0.135/(1+exp(14.400 - 0.010*x))",0,2000)
      ####################################################################################################### 
      
      nbins = h_ttbar_background.GetNbinsX()
      #weight      = array( 'd', [0.0]*40)
      

      for i in range (0,nbins):         
          scale_bin= Rew.Eval(h_ttbar_background.GetBinCenter(i+1))
          scale_bin_tb= Rew_tb.Eval(h_ttbar_background_tb.GetBinCenter(i+1))
          #h_ttbar_background_tb.SetBinContent(i+1,scale_bin_tb*h_ttbar_background_tb.GetBinContent(i+1))
          #h_ttbar_background.SetBinContent(i+1,scale_bin*h_ttbar_background.GetBinContent(i+1))

          scale_bin_1b= Rew.Eval(h_ttbar_background_1b.GetBinCenter(i+1))
          scale_bin_1b_tb= Rew_tb.Eval(h_ttbar_background_1b_tb.GetBinCenter(i+1))
          #h_ttbar_background_1b.SetBinContent(i+1,scale_bin_1b*h_ttbar_background_1b.GetBinContent(i+1))     
          #h_ttbar_background_1b_tb.SetBinContent(i+1,scale_bin_1b_tb*h_ttbar_background_1b_tb.GetBinContent(i+1))

          scale_bin_1c= Rew.Eval(h_ttbar_background_1c.GetBinCenter(i+1))
          scale_bin_1c_tb= Rew_tb.Eval(h_ttbar_background_1c_tb.GetBinCenter(i+1))
          #h_ttbar_background_1c.SetBinContent(i+1,scale_bin_1c*h_ttbar_background_1c.GetBinContent(i+1)) 
          #h_ttbar_background_1c_tb.SetBinContent(i+1,scale_bin_1c_tb*h_ttbar_background_1c_tb.GetBinContent(i+1))

          scale_bin_1l= Rew.Eval(h_ttbar_background_1l.GetBinCenter(i+1))
          scale_bin_1l_tb= Rew_tb.Eval(h_ttbar_background_1l_tb.GetBinCenter(i+1))
          #h_ttbar_background_1l.SetBinContent(i+1,scale_bin_1l*h_ttbar_background_1l.GetBinContent(i+1))
          #h_ttbar_background_1l_tb.SetBinContent(i+1,scale_bin_1l_tb*h_ttbar_background_1l_tb.GetBinContent(i+1))

      h_ttbar_background_1l.SetLineColor(kBlack)
      h_ttbar_background_1l.SetLineStyle(7) #7
      h_ttbar_background_1c.SetLineColor(kBlue)
      h_ttbar_background_1c.SetLineStyle(7) #7
      h_ttbar_background_1b.SetLineColor(kRed)
      h_ttbar_background_1b.SetLineStyle(7) #7
      #h_ttbar_background_1b_tb.SetLineColor(kRed)
      #h_ttbar_background_1b_tb.SetLineStyle(7) #7

      #h_ttbar_background_1c.SetLineColor(kOrange)
      #h_ttbar_background_1c.SetLineStyle(7) #7
      #h_ttbar_background_1c_tb.SetLineColor(kGreen)
      #h_ttbar_background_1c_tb.SetLineStyle(7) #7

      nbins=20
      ymax=0
      #NormalizeHisto(h_ttbar_background_1b)
      #if ymax<h_ttbar_background_1b.GetMaximum():
          #ymax=h_ttbar_background_1b.GetMaximum()
      #NormalizeHisto(h_ttbar_background_1b_tb)
      #if ymax<h_ttbar_background_1b_tb.GetMaximum():
          #ymax=h_ttbar_background_1b_tb.GetMaximum()
      #NormalizeHisto(h_ttbar_background_1c)
      #if ymax<h_ttbar_background_1c.GetMaximum():
          #ymax=h_ttbar_background_1c.GetMaximum()
      NormalizeHisto(h_ttbar_background_1l)
      if ymax<h_ttbar_background_1l.GetMaximum():
          ymax=h_ttbar_background_1l.GetMaximum()    
      NormalizeHisto(h_ttbar_background_1c)
      if ymax<h_ttbar_background_1c.GetMaximum():
          ymax=h_ttbar_background_1c.GetMaximum()    
      NormalizeHisto(h_ttbar_background_1b)
      if ymax<h_ttbar_background_1b.GetMaximum():
          ymax=h_ttbar_background_1b.GetMaximum()  
          
      pad1.cd()
      h_ttbar_background_1l.Draw("HIST")
      #h_sig_Hplus_m300.Draw("HISTSAME")
      #h_sig_Hplus_m350.Draw("HISTSAME")
      #h_ttbar_background_1b_tb.Draw("HISTSAME")
      h_ttbar_background_1c.Draw("HISTSAME")
      h_ttbar_background_1b.Draw("HISTSAME")
      #h_sig_Hplus_m500.Draw("HISTSAME")
      #h_sig_Hplus_m600.Draw("HISTSAME")
      #h_sig_Hplus_m700.Draw("HISTSAME")
      #h_ttbar_background_1b.Draw("HISTSAME")
      #h_sig_Hplus_m900.Draw("HISTSAME")
      #h_sig_Hplus_m1000.Draw("HISTSAME")
     

      #h_sig_Hplus_m1400.Draw("HISTSAME")
     
      #if HistoName in "maxMVAResponse":
         #leg = TLegend(0.25,0.55,0.825,0.755)
      #else:
         #leg = TLegend(0.55,0.75,0.75,0.90)
      #ATLAS_LABEL(0.20,0.875,"Simulation",1,0.19);
      ATLAS_LABEL(0.55,0.80,"Internal",1,0.09)
      myText(0.55,0.75,1,"#sqrt{s}=13 TeV, 139 fb^{-1}")
      leg = TLegend(0.55,0.40,0.85,0.65)
      leg.SetShadowColor(kWhite)
      leg.SetFillColor(kWhite)
      leg.SetLineColor(kWhite)
      
      #leg.AddEntry(h_ttbar_background_1b,    "t#bar{t} + >=1b (Rew. without tt+HF norm applied)","L")
      #leg.AddEntry(h_ttbar_background_1b_tb,    "t#bar{t} + >=1b (Rew. with tt+HF norm applied) ","L")
      #leg.AddEntry(h_ttbar_background_1c,    "t#bar{t} + >=1c (Rew. without tt+HF norm applied)","L")
      #leg.AddEntry(h_ttbar_background_1c_tb,    "t#bar{t} + >=1c (Rew. with tt+HF norm applied) ","L")
      leg.AddEntry(h_ttbar_background_1b,    "t#bar{t} + >=1b","L")
      leg.AddEntry(h_ttbar_background_1c,    "t#bar{t} + >=1c","L")
      leg.AddEntry(h_ttbar_background_1l,    "t#bar{t} + light","L")
      #leg.AddEntry(h_sig_Hplus_m400IV,    "Inc, m_{t_{l}}<=250 GeV","L")
      #leg.AddEntry(h_sig_Hplus_m400V,    "Inc, m_{t_{l}}<=275 GeV","L")
      #leg.AddEntry(h_sig_Hplus_m400VI,    "Inc, m_{t_{l}}<=300 GeV","L")
      #leg.AddEntry(h_sig_Hplus_m400VII,    "Inc, m_{t_{l}}<=325 GeV","L")
      #leg.AddEntry(h_sig_Hplus_m400VIII,    "Inc, m_{t_{l}}<=350 GeV","L")
      
      leg.SetTextSize(0.0250)
      leg.Draw()

      pad2.SetGrid()
      pad2.cd()
      h_ratio1 = h_ttbar_background_1b.Clone()
      h_ratio2 = h_ttbar_background_1c.Clone()
      h_ratio1.Divide(h_ttbar_background_1l)
      h_ratio2.Divide(h_ttbar_background_1l)
      if "TwoTags" in btagStrategy:
        h_ratio1.GetYaxis().SetRangeUser(0.0,4.0) 
      if "ThreeTags" in btagStrategy :
        h_ratio1.GetYaxis().SetRangeUser(0.0,4.0)
      if "FourPlusTags" in btagStrategy :
        h_ratio1.GetYaxis().SetRangeUser(0.0,4.0)    

      h_ratio1.SetStats(0)
      h_ratio1.SetMarkerStyle(20)
      h_ratio1.SetMarkerSize(1.0)
      h_ratio1.SetMarkerColor(kRed)
      
      h_ratio2.SetStats(0)
      h_ratio2.SetMarkerStyle(20)
      h_ratio2.SetMarkerSize(1.0)
      h_ratio2.SetMarkerColor(kBlue)

      h_ratio1.SetLabelSize(0.082,"X")
      h_ratio1.SetLabelSize(0.082,"Y")
      h_ratio1.SetTitleSize(0.082,"X")
      h_ratio1.SetTitleSize(0.082,"Y")
      h_ratio1.SetTitleOffset(0.48,"Y")
      h_ratio1.SetTitleOffset(1.2,"X")
      h_ratio1.GetYaxis().SetNdivisions(505)
      h_ratio1.SetTickLength(0.005,"X") #0.05
      h_ratio1.SetLabelOffset(0.01,"X")
      h_ratio1.GetXaxis().SetTitle(Xaxis_label)
      h_ratio1.GetYaxis().SetTitle("1 / light template")
      h_ratio1.Draw("EP")
      h_ratio2.Draw("EPSAME")
      
      pad1.cd()
      #myText(0.55,0.60,1,"#sqrt{s}=13 TeV, 139 fb^{-1}")
      if "TwoTags" in btagStrategy:
          #myText(0.45,0.70,1,"l^{-}+jets Resolved: 5 jet, 2 b-tag")
          #myText(0.47,0.70,1,"at least 8 jet, 2 b-tag (Pre-Selection)")
          if "HT_all_5j" in HistoName:
             myText(0.55,0.70,1,"5 jet, 2 b-tag (Pre-Selection)")
          if "HT_all_6j" in HistoName:
             myText(0.55,0.70,1,"6 jet, 2 b-tag (Pre-Selection)") 
          if "HT_all_7j" in HistoName:
             myText(0.55,0.70,1,"7 jet, 2 b-tag (Pre-Selection)")  
          if "HT_all_8j" in HistoName:
             myText(0.55,0.70,1,"at least 8 jet, 2 b-tag (Pre-Selection)")      
          #myText(0.47,0.70,1,"at least 8 jet, 2 b-tag (Pre-Selection)")
          #myText(0.47,0.70,1,"at least 8 jet, 2 b-tag (Pre-Selection)")
          #myText(0.20,0.65,1,"Pre-fit")
          #myText(0.20,0.65,1,"SR cuts")
      if "ThreeTags" in btagStrategy :
          #myText(0.45,0.70,1,"l^{-}+jets Resolved: 5 jet, 3 b-tag")
          #myText(0.47,0.70,1,"at least 8 jet, 3 b-tag (Pre-Selection)")
          if "HT_all_5j" in HistoName:
             myText(0.55,0.70,1,"5 jet, 3 b-tag (Pre-Selection)")
          if "HT_all_6j" in HistoName:
             myText(0.55,0.70,1,"6 jet, 3 b-tag (Pre-Selection)") 
          if "HT_all_7j" in HistoName:
             myText(0.55,0.70,1,"7 jet, 3 b-tag (Pre-Selection)")  
          if "HT_all_8j" in HistoName:
             myText(0.55,0.70,1,"at least 8 jet, 3 b-tag (Pre-Selection)")
          #myText(0.47,0.70,1,"at least 8 jet, 3 b-tag (Pre-Selection)")
          #myText(0.47,0.70,1,"at least 8 jet, 3 b-tag (Pre-Selection)")
          #myText(0.20,0.65,1,"CR cuts")
      if "FourPlusTags" in btagStrategy :
          #myText(0.45,0.70,1,"l^{-}+jets Resolved: 5 jet, at least 4 b-tag")
          #myText(0.47,0.70,1,"at least 8 jet, at least 4 b-tag (Pre-Selection)")
          if "HT_all_5j" in HistoName:
             myText(0.55,0.70,1,"5 jet, at least 4 b-tag (Pre-Selection)")
          if "HT_all_6j" in HistoName:
             myText(0.55,0.70,1,"6 jet, at least 4 b-tag (Pre-Selection)") 
          if "HT_all_7j" in HistoName:
             myText(0.55,0.70,1,"7 jet, at least 4 b-tag (Pre-Selection)")  
          if "HT_all_8j" in HistoName:
             myText(0.55,0.70,1,"at least 8 jet, at least 4 b-tag (Pre-Selection)")
          #myText(0.47,0.70,1,"at least 8 jet, at least 4 b-tag (Pre-Selection)")
          #myText(0.47,0.70,1,"at least 8 jet, at least 4 b-tag (Pre-Selection)")
          #myText(0.20,0.65,1,"CR cuts")
      #h_ttbar_background_1l.GetXaxis().SetTitle(Xaxis_label)
      h_ttbar_background_1l.GetYaxis().SetRangeUser(0.001,round(ymax*1.25,3)+0.001)
      h_ttbar_background_1l.GetYaxis().SetTitle("Normalised to Unity")
      #h_sig_Hplus_m400.GetXaxis().SetTitle(Xaxis_label)
      #h_sig_Hplus_m400.GetYaxis().SetRangeUser(0.001,round(ymax*1.25,3)+0.001)
      #pad1.cd()
      #myText(0.20,0.825,1,"work-in-progress")
      c1.RedrawAxis()
      c1.Update()
      c1.RedrawAxis()
      c1.SaveAs("../Plots/ttShapeHF/ShapePlot_%s_ttHF_comparison_INT.pdf" % (HistoName+"_"+btagStrategy))

