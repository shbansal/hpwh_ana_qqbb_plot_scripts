# -*- coding: utf-8 -*-
#python
import sys
import glob
import math
import re
from ROOT import *
from array import *
import numpy as np

gROOT.SetBatch(True)
gStyle.SetOptStat(0)
gStyle.SetPalette(1)
gROOT.LoadMacro("../style/AtlasStyle.C")
gROOT.LoadMacro("../style/AtlasUtils.C")
SetAtlasStyle()

y_axis_label="Normalised to Unity"

c_blue   = TColor.GetColor("#3366ff")
c_red    = TColor.GetColor("#ee3311")
c_orange = TColor.GetColor("#ff9900")

def NormalizeHisto(histo):
     n_events=histo.Integral(-1,histo.GetNbinsX()+1)
     if n_events == 0:
         return
     print n_events, histo.Integral(histo.GetNbinsX(),histo.GetNbinsX()+1)
     histo.Scale(1./n_events)
     histo.SetLineWidth(2)
     histo.SetStats(0)
     histo.SetFillStyle(3001)
     histo.SetMarkerColor(histo.GetLineColor())
     histo.SetMarkerSize(0.0)
     histo.GetXaxis().SetTitleOffset(1.2)
     histo.GetYaxis().SetTitleOffset(1.52)
     histo.GetXaxis().SetLabelSize(0.04)
     histo.GetYaxis().SetLabelSize(0.04)
     histo.GetXaxis().SetTitleSize(0.035)
     histo.GetYaxis().SetTitleSize(0.035)
     histo.GetXaxis().SetTitleOffset(1.85)
     histo.GetYaxis().SetTitleOffset(2.05)
     histo.GetYaxis().SetNdivisions(504)
     histo.GetXaxis().SetNdivisions(504)

#Bukin's function implementation
def bukin(x, par):

    Xp = par[0]
    sigp = par[1]
    xi = par[2]
    rho1 = par[3]
    rho2 = par[4]
    ap = par[5]
    consts = 2*np.sqrt(2*np.log(2.0))
    r1=0
    r2=0
    r3=0
    r4=0
    r5=0
    hp=0
    x1 = 0
    x2 = 0
    fit_result = 0
    hp=sigp*consts
    r3=np.log(2.)
    r4=np.sqrt(xi*xi+1)
    r1=xi/r4
    if abs(xi) > math.exp(-6.):
        r5=xi/math.log(r4+xi)
    else:
        r5=1
    x1 = Xp + (hp / 2) * (r1-1)
    x2 = Xp + (hp / 2) * (r1+1)
    #--- Left Side
    if x[0] < x1:
        r2=rho1*(x[0] - x1)*(x[0]-x1)/(Xp-x1)/(Xp-x1)-r3+4*r3*(x[0]-x1)/hp*r5*r4/(r4-xi)/(r4-xi)
    #--- Center
    elif x[0] < x2:
        if abs(xi) > np.exp(-6.):
            r2=np.log(1 + 4 * xi * r4 * (x[0] - Xp)/hp)/np.log(1 + 2*xi*(xi - r4))
            r2=-r3*r2*r2
        else:
            r2=-4*r3*(x[0] - Xp)*(x[0] - Xp)/hp/hp
    #--- Right Side
    else:
         r2=rho2*(x[0] - x2)*(x[0] - x2)/(Xp - x2)/(Xp - x2)-r3 - 4 * r3 * (x[0] - x2)/hp * r5 * r4/(r4 + xi)/(r4 + xi)
    if abs(r2) > 100:
        fit_result = 0
    else:
    #---- Normalize the result
        fit_result = np.exp(r2)
    return fit_result *ap     



c1 = TCanvas("ShapePlots","",720,720)
#for fileName in ["hp400_AFII","hp500_AFII","hp600_AFII","hp700_AFII","hp800_AFII","hp900_AFII","hp1000_AFII", "hp1200_AFII","hp1400_AFII","hp1600_AFII","hp1800_AFII","hp2000_AFII"]:
#for fileName in ["hp250_AFII","hp300_AFII","hp350_AFII","hp400_AFII","hp500_AFII","hp600_AFII","hp700_AFII","hp800_AFII","hp900_AFII","hp1000_AFII","hp1200_AFII","hp1400_AFII","hp1600_AFII","hp1800_AFII","hp2000_AFII","hp2500_AFII","hp3000_AFII"]: 
#for fileName in ["hp400_AFII","hp1200_AFII"]:  
for fileName in ["hp1000_AFII"]:     
#for fileName in ["sig_Hplus_Wh_m400"]:
  #### [0]*exp(-0.5*((x-[1])/[2])**2) 
  bukin_tf1 = TF1("bukin_tf1",bukin,-1,1,6)
  
  #suitable parameters for 400, 500 and 600 GeV MP
  #bukin_tf1.SetParameter(0,0.0)    # Xp
  #bukin_tf1.SetParameter(1,0.1)    # sigp
  #bukin_tf1.SetParameter(2,0.1)    # xi      
  #bukin_tf1.SetParameter(3,-0.1)    # rho1
  #bukin_tf1.SetParameter(4,-0.1)    # rho2
  #bukin_tf1.SetParameter(5, 0.4)    # ap
  bukin_tf1.SetParameter(0,0.0)    # Xp
  bukin_tf1.SetParameter(1,0.1)    # sigp
  bukin_tf1.SetParameter(2,0.1)    # xi      
  bukin_tf1.SetParameter(3,-0.1)    # rho1
  bukin_tf1.SetParameter(4, -0.1)    # rho2
  bukin_tf1.SetParameter(5, 0.8)    # ap

  #suitable parameters for 700, 800, 900, 1000, 1200 GeV MP
  #bukin_tf1.SetParameter(0,-1.09171e-02)    # Xp
  #bukin_tf1.SetParameter(1,0.91063e-01)    # sigp
  #bukin_tf1.SetParameter(2,-2.91904e+01)    # xi      
  #bukin_tf1.SetParameter(3,-4.09019e+00)    # rho1
  #bukin_tf1.SetParameter(4,2.74698e-02)    # rho2
  #bukin_tf1.SetParameter(5,1.32821e+02)    # ap
  
  #bukin_tf1.SetParameter(0,-0.89171e-02)    # Xp
  #bukin_tf1.SetParameter(1,1.91063e-01)    # sigp
  #bukin_tf1.SetParameter(2,2.91904e+01)    # xi      
  #bukin_tf1.SetParameter(3,4.09019e+00)    # rho1
  #bukin_tf1.SetParameter(4,-2.74698e-02)    # rho2
  #bukin_tf1.SetParameter(5,-2.32821e+02)    # ap
  
  #suitable parameters for 1400, 1600, 1800, 2000 GeV MP
  #bukin_tf1.SetParameter(0,8.20203e-02)    # Xp
  #bukin_tf1.SetParameter(1,1.40483e-01)    # sigp
  #bukin_tf1.SetParameter(2,-3.49858e-01)    # xi      
  #bukin_tf1.SetParameter(3,2.68504e-01)    # rho1
  #bukin_tf1.SetParameter(4,2.55932e-01)    # rho2
  #bukin_tf1.SetParameter(5,2.10765e+03)    # ap

  #bukin_tf1.SetParLimits(0,-0.3,0.3)    # Xp
  #bukin_tf1.SetParLimits(1,0.1,1e+02)    # sigp
  #bukin_tf1.SetParLimits(2,0,1e+02)    # xi      
  #bukin_tf1.SetParLimits(3,0,1e+02)    # rho1
  #bukin_tf1.SetParLimits(4,0,1e+02)    # rho2
  #bukin_tf1.SetParLimits(5,0,1e+02)    # ap

  sigPars      = array( 'd', [0.0]*6)
  for HistoName in ["mass_resolution"]:
    for Region in ["Resolved_SR"]:
    ##  for bTagStrategy in ["Incl","FourPlusTags","ThreeTags","TwoTags"]: 
      for bTagStrategy in ["Inclusive"]:
         ReBin = False
         YAxisScale = 1.4
         #Xaxis_label = "#frac{m^{reco}_{top_{l}}-m^{truth}_{top_{l}}}{m^{truth}_{top_{l}}}"
         Xaxis_label = "#frac{m^{reco}_{W^{+}h}-m^{truth}_{W^{+}h}}{m^{truth}_{W^{+}h}}"
         #Xaxis_label = "m^{reco}_{W^{+}h}"
         #file1       = TFile.Open("../PlotFiles/MassResolution/"+fileName+".root_70p_250.root","READ")
         file1       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/hp_77pLepTop250/"+fileName+".root_77p_250.root","READ")
         dir1        = file1.GetDirectory("nominal_Loose").GetDirectory(HistoName)
         h_sig_Hplus = dir1.Get(fileName+"_"+HistoName+"_"+Region+"_"+bTagStrategy+"_nominal_Loose")
         h_sig_Hplus.SetLineColor(kBlack)
         h_sig_Hplus.SetLineStyle(1)
              
         nbins=20
         ymax=0
         NormalizeHisto(h_sig_Hplus)
         if ymax<h_sig_Hplus.GetMaximum():
            ymax=h_sig_Hplus.GetMaximum()
         h_sig_Hplus.Draw("HIST")
         MassPoint="" 
         if "400" in fileName:
             MassPoint = "400"     
         if "1000" in fileName:
             MassPoint = "1000" 
         if "250" in fileName:
             MassPoint = "250"     
         if "300" in fileName:
             MassPoint = "300"
       	 if "350" in fileName:
       	     MassPoint = "350"
         if "500" in fileName:
       	     MassPoint = "500"
         if "600" in fileName:
             MassPoint = "600"     
         if "700" in fileName:
             MassPoint = "700"
       	 if "800" in fileName:
       	     MassPoint = "800"
         if "900" in fileName:
       	     MassPoint = "900"  
         if "1200" in fileName:
             MassPoint = "1200"     
         if "1400" in fileName:
             MassPoint = "1400"
       	 if "1800" in fileName:
       	     MassPoint = "1800"
         if "2000" in fileName:
       	     MassPoint = "2000" 
         if "3000" in fileName:
       	     MassPoint = "3000"  
         if "1600" in fileName:
       	     MassPoint = "1600"
         if "2500" in fileName:
       	     MassPoint = "2500"                   

         leg = TLegend(0.7,0.65,0.925,0.855)
         ATLAS_LABEL(0.20,0.885,"Internal",1,0.19)
         #ATLAS_LABEL(0.20,0.885,"work-in-progress",1,0.19)
         #myText(0.785,0.825,1,"139 fb^{-1}")
         #myText(0.20,0.825,1,"at least 5 jet, at least 4 b-tags")
         myText(0.20,0.825,1,"at least 5 jet, at least 2 b-tags")
         myText(0.20,0.765,1,"m_{W^{+}h} = "+MassPoint+" GeV")
         leg.SetShadowColor(kWhite)
         leg.SetFillColor(kWhite)
         leg.SetLineColor(kWhite)
         ##leg.AddEntry(h_sig_Hplus_m800,  "H^{+}#rightarrow hW^{+} (m_{H^{+}} = 800GeV)","L")
         #leg.Draw()

         h_sig_Hplus.GetYaxis().SetTitle(y_axis_label)
         h_sig_Hplus.GetXaxis().SetTitle(Xaxis_label)
         h_sig_Hplus.GetYaxis().SetRangeUser(0.001,round(ymax*1.4,3)+0.001)
         
         fitter = TVirtualFitter.Fitter(h_sig_Hplus)
         fitter.SetMaxIterations(300000)
         fitStatus =  h_sig_Hplus.Fit("bukin_tf1", "S", "", -1, 1)
         myfunc2   =  h_sig_Hplus.GetFunction("bukin_tf1")
         myfunc2.GetParameters(sigPars)
         myfunc2.SetLineColor(kBlue)
         myfunc2.SetLineWidth(3)
         myfunc2.SetLineStyle(7)
         myfunc2.Draw("SAMEL")
         print "1: ",sigPars[0] ,"2:",sigPars[1],"3:", sigPars[2],"4:", sigPars[3],"5:", sigPars[4],"6:", sigPars[5]       
         myText(0.20,0.705,1,"#sigma_{res} = "+ str(round(sigPars[1],2)*100)+"%")
         f=open("../res_qqbb/res_qqbb_bukin_250_Bukin.txt","a")
         f.write(MassPoint+" "+bTagStrategy+" "+str(sigPars[1])+"\n")
         #f.write(str(round(sigPars[2],2))+",")
         f.close()
         c1.RedrawAxis()
         c1.Update()
         c1.RedrawAxis()
         c1.SaveAs("../Plots/MassRes/MassResolution_250_%s_Bukin_Internal.pdf" % (HistoName+"_"+MassPoint+"_"+bTagStrategy))

