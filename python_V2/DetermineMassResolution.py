# -*- coding: utf-8 -*-
#python
import sys
import glob
import math
import re
from ROOT import *
from array import *

gROOT.SetBatch(True)
gStyle.SetOptStat(0)
gStyle.SetPalette(1)
gROOT.LoadMacro("../style/AtlasStyle.C")
gROOT.LoadMacro("../style/AtlasUtils.C")
SetAtlasStyle()

y_axis_label="Events"

c_blue   = TColor.GetColor("#3366ff")
c_red    = TColor.GetColor("#ee3311")
c_orange = TColor.GetColor("#ff9900")

def NormalizeHisto(histo):
     n_events=histo.Integral(-1,histo.GetNbinsX()+1)
     if n_events == 0:
         return
     print n_events, histo.Integral(histo.GetNbinsX(),histo.GetNbinsX()+1)
     ##### histo.Scale(1./n_events)
     histo.SetLineWidth(2)
     histo.SetStats(0)
     histo.SetFillStyle(3001)
     histo.SetMarkerColor(histo.GetLineColor())
     histo.SetMarkerSize(0.0)
     histo.GetXaxis().SetTitleOffset(1.2)
     histo.GetYaxis().SetTitleOffset(1.52)
     histo.GetXaxis().SetLabelSize(0.04)
     histo.GetYaxis().SetLabelSize(0.04)
     histo.GetXaxis().SetTitleSize(0.035)
     histo.GetYaxis().SetTitleSize(0.035)
     histo.GetXaxis().SetTitleOffset(1.85)
     histo.GetYaxis().SetTitleOffset(2.05)
     histo.GetYaxis().SetNdivisions(504)
     histo.GetXaxis().SetNdivisions(504)

c1 = TCanvas("ShapePlots","",720,720)
for fileName in ["hp250_AFII","hp300_AFII","hp350_AFII","hp400_AFII","hp500_AFII","hp600_AFII","hp700_AFII","hp800_AFII","hp900_AFII","hp1000_AFII", "hp1200_AFII","hp1400_AFII","hp1600_AFII","hp1800_AFII","hp2000_AFII","hp2500_AFII","hp3000_AFII"]:
#for fileName in ["hp600_AFII"]:
  #### [0]*exp(-0.5*((x-[1])/[2])**2) 
  gaussian_tf1 = TF1("gaussian_tf1","gaus(0)",-1.0,1.0)
  sigPars      = array( 'd', [0.0]*3)
  for HistoName in ["mass_resolution_MVA095"]:
    for Region in ["Resolved_SR"]:
      #for bTagStrategy in ["Inclusive","FourPlusTags","ThreeTags","TwoTags"]: 
      for bTagStrategy in ["ThreeTags"]:    
      ##for bTagStrategy in ["FourPlusTags"]:
         ReBin = False
         YAxisScale = 1.4
         #Xaxis_label = "#frac{m^{reco}_{top_{l}}-m^{truth}_{top_{l}}}{m^{truth}_{top_{l}}}"
         Xaxis_label = "#frac{m^{reco}_{W^{+}h}-m^{truth}_{W^{+}h}}{m^{truth}_{W^{+}h}}"
         #Xaxis_label = "m^{reco}_{W^{+}h}"
         #file1       = TFile.Open("../PlotFiles/MassResolution/"+fileName+".root_70p_250.root","READ")
         #file1       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_neglep_xc/"+fileName+".root_70p_250.root","READ")
         file1       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_qqbb_massres_V2/"+fileName+".root_77p_225.root","READ")
         dir1        = file1.GetDirectory("nominal_Loose").GetDirectory(HistoName)
         h_sig_Hplus = dir1.Get(fileName+"_"+HistoName+"_"+Region+"_"+bTagStrategy+"_nominal_Loose")
         h_sig_Hplus.SetLineColor(kBlack)
         h_sig_Hplus.SetLineStyle(1)
              
         nbins=20
         ymax=0
         NormalizeHisto(h_sig_Hplus)
         if ymax<h_sig_Hplus.GetMaximum():
            ymax=h_sig_Hplus.GetMaximum()
         h_sig_Hplus.Draw("HIST")
         MassPoint="" 
         if "400" in fileName:
             MassPoint = "400"     
         if "1000" in fileName:
             MassPoint = "1000" 
         if "250" in fileName:
             MassPoint = "250"     
         if "300" in fileName:
             MassPoint = "300"
       	 if "350" in fileName:
       	     MassPoint = "350"
         if "500" in fileName:
       	     MassPoint = "500"
         if "600" in fileName:
             MassPoint = "600"     
         if "700" in fileName:
             MassPoint = "700"
       	 if "800" in fileName:
       	     MassPoint = "800"
         if "900" in fileName:
       	     MassPoint = "900"  
         if "1200" in fileName:
             MassPoint = "1200"     
         if "1400" in fileName:
             MassPoint = "1400"
       	 if "1800" in fileName:
       	     MassPoint = "1800"
         if "2000" in fileName:
       	     MassPoint = "2000" 
         if "3000" in fileName:
       	     MassPoint = "3000"  
         if "1600" in fileName:
       	     MassPoint = "1600"
         if "2500" in fileName:
       	     MassPoint = "2500"                   

         leg = TLegend(0.7,0.65,0.925,0.855)
         ATLAS_LABEL(0.20,0.885," Simulation Internal",1,0.19);
         #myText(0.785,0.825,1,"139 fb^{-1}")
         myText(0.20,0.825,1,"m_{W^{+}h} = "+MassPoint)
         leg.SetShadowColor(kWhite)
         leg.SetFillColor(kWhite)
         leg.SetLineColor(kWhite)
         ##leg.AddEntry(h_sig_Hplus_m800,  "H^{+}#rightarrow hW^{+} (m_{H^{+}} = 800GeV)","L")
         #leg.Draw()

         h_sig_Hplus.GetYaxis().SetTitle(y_axis_label)
         h_sig_Hplus.GetXaxis().SetTitle(Xaxis_label)
         h_sig_Hplus.GetYaxis().SetRangeUser(0.001,round(ymax*1.4,3)+0.001)
      
         fitStatus =  h_sig_Hplus.Fit("gaussian_tf1", "QW", "", -1.0, 1.0)
         myfunc2   =  h_sig_Hplus.GetFunction("gaussian_tf1")
         myfunc2.GetParameters(sigPars);
         myfunc2.SetLineColor(kBlue)
         myfunc2.SetLineWidth(3)
         myfunc2.SetLineStyle(7)
         myfunc2.Draw("SAMEL")
         print "1: ",sigPars[0] ,"2:",sigPars[1],"3:", sigPars[2]         
         myText(0.20,0.765,1,"#sigma_{res} = "+ str(round(sigPars[2],2)*100)+"%")
         f=open("../res_qqbb/res_qqbb_fp_225_3b_MVA095.txt","a")
         f.write(MassPoint+" "+bTagStrategy+" "+str(sigPars[2])+"\n")
         #f.write(str(round(sigPars[2],2))+",")
         #f.close()
         c1.RedrawAxis()
         c1.Update()
         c1.RedrawAxis()
         c1.SaveAs("../Plots/MassResolution_3b_qqbb_gauss_%s_MVA095.pdf" % (HistoName+"_"+MassPoint+"_"+bTagStrategy))
