# -*- coding: utf-8 -*-
#python
import sys
import glob
import math
import re
from ROOT import *
from array import *
from ROOT import TCanvas, TFile, TLine, TProfile, TNtuple, TH1F, TH2F
#import ROOT
#import array

gROOT.SetBatch(True)
gStyle.SetOptStat(0)
gStyle.SetPalette(1)
gROOT.LoadMacro("../style/AtlasStyle.C")
gROOT.LoadMacro("../style/AtlasUtils.C")
SetAtlasStyle()

y_axis_label="Event fraction"

c_blue   = TColor.GetColor("#3366ff")
c_red    = TColor.GetColor("#ee3311")
c_orange = TColor.GetColor("#ff9900")

def NormalizeHisto(histo):
     n_events=histo.Integral(-1,histo.GetNbinsX()+1)
     #n_events=histo.Integral()
     if n_events == 0:
         return
     print n_events, histo.Integral(histo.GetNbinsX(),histo.GetNbinsX()+1)
     histo.Scale(1./n_events)
     histo.SetLineWidth(3)
     histo.SetStats(0)
     histo.SetFillStyle(3001)
     histo.SetMarkerColor(histo.GetLineColor())
     histo.SetMarkerSize(0.0)
     histo.GetXaxis().SetTitleOffset(1.2)
     histo.GetYaxis().SetTitleOffset(1.52)
     histo.GetXaxis().SetLabelSize(0.05)
     histo.GetYaxis().SetLabelSize(0.05)
     histo.GetXaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetNdivisions(504)
     histo.GetXaxis().SetNdivisions(504)

c1 = TCanvas("ShapePlots","",500,500)

HistoNameR = "lep_top_mass"
#for HistoName in ["nBTags"]:
#for HistoName in ["nBTags","nJets","HT","HT_bjets","DeltaPhi_HW","mVH","mH","pTWplus","pTH","maxMVAResponse", "mass_resolution"]: # for resolved, histoname set
#for HistoName in ["pTWplus","pTH","mVH","Leptonic_top_mass"]: # for resolved, histoname set
#for HistoName in ["pTWminus"]: 
#for HistoName in ["nBTags","nJets","HT","HT_bjets","DeltaPhi_HW","mVH","mH","pTWplus","pTH","mass_resolution"]:#for boosted, histoname set
#for HistoName in ["mass_resolution"]:
for HistoName in ["Leptonic_Top_mass"]:
#for HistoName in ["mVH"]:     
#for HistoName in ["maxMVAResponse"]:
## for bTagStrategy in ["Incl","FourPlusTags","ThreeTags","TwoTags"]:
 for Region in ["Resolved_SR"]:
  for btagStrategy in ["Inclusive"]:
   #for btagStrategy in ["Inclusive","FourPlusTags","ThreeTags","TwoTags"]:
     
      ReBin = False
      YAxisScale = 1.4

      if "nBTags" in HistoName:
          Xaxis_label="b-tag multiplicity"
      if "nJet" in HistoName:
          Xaxis_label="Jet Multiplicity"
      if "DeltaPhi_HW" in HistoName:
          Xaxis_label="DeltaPhi_HW"
      if "pTH" in HistoName:
          Xaxis_label="Transverse Momentum of Higgs [GeV]"
      if "pTWplus" in HistoName:
          Xaxis_label="Transverse Momentum of W Boson [GeV]"
      if "mVH" in HistoName:
          Xaxis_label="Mass of Charged Higgs [GeV]"
      if "mH" in HistoName:
          Xaxis_label="Mass of Higgs [GeV]"
      if "pTWplus" in HistoName:
          Xaxis_label="Transverse Momentum Of Wplus [GeV]"
      if "pTH" in HistoName:
          Xaxis_label="Transverse Momentum Of Higgs [GeV]"
      if "mass_resolution" in HistoName:
          Xaxis_label="Mass Resolution"
      if "HT" in HistoName:
          Xaxis_label="H_{T} (Scalar Transverse Momentum Sum of jets) [GeV]"
      if "HT_bjets" in HistoName:
          Xaxis_label="H_{T_{b-jet}} (Scalar Transverse Momentum Sum of b-jets) [GeV]"
      if "maxMVAResponse" in HistoName:
          Xaxis_label="BDT Score (Signal Reconstruction)"
      if "mass_resolution" in HistoName:
          Xaxis_label="Leptonic Top (GeV)"   
      if "Leptonic_Top_mass" in HistoName:
          Xaxis_label="Leptonic top mass (GeV)"   
      #Xaxis_label=""
      
      
      file250       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_poslep/hp250_AFII.root_70p_250.root","READ")
      dir250        = file250.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m250 = dir250.Get("hp250_AFII_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m250.SetLineColor(kRed)
      h_sig_Hplus_m250.SetLineStyle(7) #7
      if ReBin == True:
          h_sig_Hplus_m250.Rebin(2) 


      file300	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_poslep/hp300_AFII.root_70p_250.root","READ")
      dir300       = file300.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m300 = dir300.Get("hp300_AFII_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m300.SetLineColor(kBlack)
      h_sig_Hplus_m300.SetLineStyle(7) #7
      if ReBin == True:
          h_sig_Hplus_m300.Rebin(2)

     
      file350	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_poslep/hp350_AFII.root_70p_250.root","READ")
      dir350        = file350.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m350 = dir350.Get("hp350_AFII_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m350.SetLineColor(kCyan+1)
      h_sig_Hplus_m350.SetLineStyle(7) #7
      if ReBin == True:
          h_sig_Hplus_m350.Rebin(2)
      
      file400      = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_poslep/hp400_AFII.root_70p_250.root","READ")
      dir400        = file400.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m400 = dir400.Get("hp400_AFII_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m400.SetLineColor(kAzure-6)
      h_sig_Hplus_m400.SetLineStyle(7) #7
      if ReBin == True:
          h_sig_Hplus_m400.Rebin(2)
        

      file500	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_poslep/hp500_AFII.root_70p_250.root","READ")
      dir500        = file500.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m500 = dir500.Get("hp500_AFII_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m500.SetLineColor(kViolet)
      h_sig_Hplus_m500.SetLineStyle(7) #7
      if ReBin == True:
          h_sig_Hplus_m500.Rebin(2)
      
      file600	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_poslep/hp600_AFII.root_70p_250.root","READ")
      dir600        = file600.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m600 = dir600.Get("hp600_AFII_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m600.SetLineColor(kViolet-1)
      h_sig_Hplus_m600.SetLineStyle(7) #7
      if ReBin == True:
          h_sig_Hplus_m600.Rebin(2)

     
      file700	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_poslep/hp700_AFII.root_70p_250.root","READ")
      dir700        = file700.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m700 = dir700.Get("hp700_AFII_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m700.SetLineColor(kMagenta)
      h_sig_Hplus_m700.SetLineStyle(7) #7
      if ReBin == True:
          h_sig_Hplus_m700.Rebin(2)

      file800	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_poslep/hp800_AFII.root_70p_250.root","READ")
      dir800        = file800.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m800 = dir800.Get("hp800_AFII_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m800.SetLineColor(kGreen)
      h_sig_Hplus_m800.SetLineStyle(7) #7
      if ReBin == True:
          h_sig_Hplus_m800.Rebin(2) 

      file900	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_poslep/hp900_AFII.root_70p_250.root","READ")
      dir900        = file900.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m900 = dir900.Get("hp900_AFII_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m900.SetLineColor(kYellow)
      h_sig_Hplus_m900.SetLineStyle(7) #7
      if ReBin == True:
          h_sig_Hplus_m900.Rebin(2) 

      file1000	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_poslep/hp1000_AFII.root_70p_250.root","READ")
      dir1000        = file1000.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m1000 = dir1000.Get("hp1000_AFII_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m1000.SetLineColor(kOrange)
      h_sig_Hplus_m1000.SetLineStyle(7) #7
      if ReBin == True:
          h_sig_Hplus_m1000.Rebin(2) 

      file1200	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_poslep/hp1200_AFII.root_70p_250.root","READ")
      dir1200        = file1200.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m1200 = dir1200.Get("hp1200_AFII_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m1200.SetLineColor(kTeal)
      h_sig_Hplus_m1200.SetLineStyle(7) #7
      if ReBin == True:
          h_sig_Hplus_m1200.Rebin(2)

      file1400	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_poslep/hp1400_AFII.root_70p_250.root","READ")
      dir1400        = file1400.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m1400 = dir1400.Get("hp1400_AFII_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m1400.SetLineColor(kYellow-5)
      h_sig_Hplus_m1400.SetLineStyle(7) #7
      if ReBin == True:
          h_sig_Hplus_m1400.Rebin(2)

      file1600	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_poslep/hp1600_AFII.root_70p_250.root","READ")
      dir1600        = file1600.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m1600 = dir1600.Get("hp1600_AFII_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m1600.SetLineColor(kPink+4)
      h_sig_Hplus_m1600.SetLineStyle(7) #7
      if ReBin == True:
          h_sig_Hplus_m1600.Rebin(2)

      file1800	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_poslep/hp1800_AFII.root_70p_250.root","READ")
      dir1800        = file1800.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m1800 = dir1800.Get("hp1800_AFII_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m1800.SetLineColor(kBlue-4)
      h_sig_Hplus_m1800.SetLineStyle(7) #7
      if ReBin == True:
          h_sig_Hplus_m1800.Rebin(2)

      file2000	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_poslep/hp2000_AFII.root_70p_250.root","READ")
      dir2000        = file2000.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m2000 = dir2000.Get("hp2000_AFII_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m2000.SetLineColor(kSpring-9)
      h_sig_Hplus_m2000.SetLineStyle(7) #7
      if ReBin == True:
          h_sig_Hplus_m2000.Rebin(2)

      file2500	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_poslep/hp2500_AFII.root_70p_250.root","READ")
      dir2500        = file2500.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m2500 = dir2500.Get("hp2500_AFII_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m2500.SetLineColor(kGray)
      h_sig_Hplus_m2500.SetLineStyle(7) #7
      if ReBin == True:
          h_sig_Hplus_m2500.Rebin(2)


      file3000	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_poslep/hp3000_AFII.root_70p_250.root","READ")
      dir3000        = file3000.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m3000 = dir3000.Get("hp3000_AFII_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m3000.SetLineColor(kGray)
      h_sig_Hplus_m3000.SetLineStyle(7) #7
      if ReBin == True:
          h_sig_Hplus_m3000.Rebin(2)
        
      nbins=20
      ymax=0
      NormalizeHisto(h_sig_Hplus_m250)
      if ymax<h_sig_Hplus_m250.GetMaximum():
          ymax=h_sig_Hplus_m250.GetMaximum()
      NormalizeHisto(h_sig_Hplus_m300)
      if ymax<h_sig_Hplus_m300.GetMaximum():
          ymax=h_sig_Hplus_m300.GetMaximum()
      NormalizeHisto(h_sig_Hplus_m350)
      if ymax<h_sig_Hplus_m350.GetMaximum():
          ymax=h_sig_Hplus_m350.GetMaximum()
      NormalizeHisto(h_sig_Hplus_m400)
      if ymax<h_sig_Hplus_m400.GetMaximum():
          ymax=h_sig_Hplus_m400.GetMaximum()
      NormalizeHisto(h_sig_Hplus_m500)
      if ymax<h_sig_Hplus_m500.GetMaximum():
          ymax=h_sig_Hplus_m500.GetMaximum()
      NormalizeHisto(h_sig_Hplus_m600)
      if ymax<h_sig_Hplus_m600.GetMaximum():
          ymax=h_sig_Hplus_m600.GetMaximum()
      NormalizeHisto(h_sig_Hplus_m700)
      if ymax<h_sig_Hplus_m700.GetMaximum():
          ymax=h_sig_Hplus_m700.GetMaximum()
      NormalizeHisto(h_sig_Hplus_m800)
      if ymax<h_sig_Hplus_m800.GetMaximum():
          ymax=h_sig_Hplus_m800.GetMaximum()
      NormalizeHisto(h_sig_Hplus_m900)
      if ymax<h_sig_Hplus_m900.GetMaximum():
          ymax=h_sig_Hplus_m900.GetMaximum()
      NormalizeHisto(h_sig_Hplus_m1000)
      if ymax<h_sig_Hplus_m1000.GetMaximum():
          ymax=h_sig_Hplus_m1000.GetMaximum()
      NormalizeHisto(h_sig_Hplus_m1200)
      if ymax<h_sig_Hplus_m1200.GetMaximum():
          ymax=h_sig_Hplus_m1200.GetMaximum()
      NormalizeHisto(h_sig_Hplus_m1400)
      if ymax<h_sig_Hplus_m1400.GetMaximum():
          ymax=h_sig_Hplus_m1400.GetMaximum()
      NormalizeHisto(h_sig_Hplus_m1600)
      if ymax<h_sig_Hplus_m1600.GetMaximum():
          ymax=h_sig_Hplus_m1600.GetMaximum()
      NormalizeHisto(h_sig_Hplus_m1800)
      if ymax<h_sig_Hplus_m1800.GetMaximum():
          ymax=h_sig_Hplus_m1800.GetMaximum()
      NormalizeHisto(h_sig_Hplus_m2000)
      if ymax<h_sig_Hplus_m2000.GetMaximum():
          ymax=h_sig_Hplus_m2000.GetMaximum()
      NormalizeHisto(h_sig_Hplus_m2500)
      if ymax<h_sig_Hplus_m2500.GetMaximum():
          ymax=h_sig_Hplus_m2500.GetMaximum()
      NormalizeHisto(h_sig_Hplus_m3000)
      if ymax<h_sig_Hplus_m3000.GetMaximum():
          ymax=h_sig_Hplus_m3000.GetMaximum()

      h_sig_Hplus_m250.Draw("HIST")
      #h_sig_Hplus_m300.Draw("HISTSAME")
      #h_sig_Hplus_m350.Draw("HISTSAME")
      h_sig_Hplus_m400.Draw("HISTSAME")
      #h_sig_Hplus_m500.Draw("HISTSAME")
      #h_sig_Hplus_m600.Draw("HISTSAME")
      #h_sig_Hplus_m700.Draw("HISTSAME")
      h_sig_Hplus_m800.Draw("HISTSAME")
      #h_sig_Hplus_m900.Draw("HISTSAME")
      #h_sig_Hplus_m1000.Draw("HISTSAME")
      h_sig_Hplus_m1200.Draw("HISTSAME")
      #h_sig_Hplus_m1400.Draw("HISTSAME")
      h_sig_Hplus_m1600.Draw("HISTSAME")
      #h_sig_Hplus_m1800.Draw("HISTSAME")
      h_sig_Hplus_m2000.Draw("HISTSAME")
      #h_sig_Hplus_m2500.Draw("HISTSAME")
      h_sig_Hplus_m3000.Draw("HISTSAME")

      if HistoName in "maxMVAResponse":
         leg = TLegend(0.25,0.55,0.825,0.755)
      else:
         leg = TLegend(0.55,0.50,0.955,0.95)
      #ATLAS_LABEL(0.20,0.875,"Simulation",1,0.19);
      #ATLAS_LABEL(0.20,0.80,"Simulation",1,0.09)
      leg.SetShadowColor(kWhite)
      leg.SetFillColor(kWhite)
      leg.SetLineColor(kWhite)
      
      leg.AddEntry(h_sig_Hplus_m250,    "H^{+}#rightarrow W^{+}h(m_{H^{+}} = 250GeV)","L")
      #leg.AddEntry(h_sig_Hplus_m300,    "H^{+}#rightarrow W^{+}h(m_{H^{+}} = 300GeV)","L")
      #leg.AddEntry(h_sig_Hplus_m350,   "H^{+}#rightarrow W^{+}h(m_{H^{+}} = 350GeV)","L")
      leg.AddEntry(h_sig_Hplus_m400,    "H^{+}#rightarrow W^{+}h(m_{H^{+}} = 400GeV)","L")
      #leg.AddEntry(h_sig_Hplus_m500,    "H^{+}#rightarrow W^{+}h(m_{H^{+}} = 500GeV)","L")
      #leg.AddEntry(h_sig_Hplus_m600,   "H^{+}#rightarrow W^{+}h(m_{H^{+}} = 600GeV)","L")
      #leg.AddEntry(h_sig_Hplus_m700,    "H^{+}#rightarrow W^{+}h(m_{H^{+}} = 700GeV)","L")
      leg.AddEntry(h_sig_Hplus_m800,    "H^{+}#rightarrow W^{+}h(m_{H^{+}} = 800GeV)","L")
      #leg.AddEntry(h_sig_Hplus_m900,   "H^{+}#rightarrow W^{+}h(m_{H^{+}} = 900GeV)","L")
      #leg.AddEntry(h_sig_Hplus_m1000,    "H^{+}#rightarrow W^{+}h(m_{H^{+}} = 1000GeV)","L")
      leg.AddEntry(h_sig_Hplus_m1200,    "H^{+}#rightarrow W^{+}h(m_{H^{+}} = 1200GeV)","L")
      #leg.AddEntry(h_sig_Hplus_m1400,   "H^{+}#rightarrow W^{+}h(m_{H^{+}} = 1400GeV)","L")
      leg.AddEntry(h_sig_Hplus_m1600,    "H^{+}#rightarrow W^{+}h(m_{H^{+}} = 1600GeV)","L")
      #leg.AddEntry(h_sig_Hplus_m1800,    "H^{+}#rightarrow W^{+}h(m_{H^{+}} = 1800GeV)","L")
      leg.AddEntry(h_sig_Hplus_m2000,   "H^{+}#rightarrow W^{+}h(m_{H^{+}} = 2000GeV)","L")
      #leg.AddEntry(h_sig_Hplus_m2500,    "H^{+}#rightarrow W^{+}h(m_{H^{+}} = 2500GeV)","L")
      leg.AddEntry(h_sig_Hplus_m3000,    "H^{+}#rightarrow W^{+}h(m_{H^{+}} = 3000GeV)","L")
      
      
      leg.SetTextSize(0.0250)
      leg.Draw()
      h_sig_Hplus_m250.GetXaxis().SetTitle(Xaxis_label)
      h_sig_Hplus_m250.GetYaxis().SetRangeUser(0.001,round(ymax*1.25,3)+0.001)
      h_sig_Hplus_m250.GetYaxis().SetTitle("Normalised Entries")
      #h_sig_Hplus_m400.GetXaxis().SetTitle(Xaxis_label)
      #h_sig_Hplus_m400.GetYaxis().SetRangeUser(0.001,round(ymax*1.25,3)+0.001)
      #pad1.cd()
      #myText(0.20,0.825,1,"work-in-progress")
      c1.RedrawAxis()
      c1.Update()
      c1.RedrawAxis()
      c1.SaveAs("../Plots/Reco_NegLep_V2/ShapePlot_%s_qqbb_SR_Resolved_LepTop_EventReco_poslep_7MP_250621.pdf" % (HistoName+"_"+btagStrategy))
