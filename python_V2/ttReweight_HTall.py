# -*- coding: utf-8 -*-
#python
import ROOT
import sys
import glob
import math
import re
from ROOT import *
from array import *
from ROOT import TF1

gROOT.SetBatch(True)
gStyle.SetOptStat(0)
gStyle.SetPalette(1)
gROOT.LoadMacro("../style/AtlasStyle.C")
gROOT.LoadMacro("../style/AtlasUtils.C")
SetAtlasStyle()
#from ROOT import rootpy.plotting
#gMinuit.SetMaxIterations(5000)
import numpy as np
from numpy import ndarray
import array
#from rootpy.plotting import Hist, HistStack, Legend, Canvas
#from rootpy.plotting.style import get_style, set_style
#from rootpy.plotting.utils import draw
#from rootpy.interactive import wait
#import rootpy.plotting.root2matplotlib as rplt
from ROOT import TCanvas, TFile, TPad, THStack, TLine,TGraphAsymmErrors, TNtuple, TH1F, TH2F
#from ROOT import TGraphAsymmErrors
from array import *
#from ROOT import TLatex
#import ROOT
#import array
#TVirtualFitter::SetMaxIterations(5000)
y_axis_label="Event fraction"

c_blue   = TColor.GetColor("#3366ff")
c_red    = TColor.GetColor("#ee3311")
c_orange = TColor.GetColor("#ff9900")

c1 = TCanvas("WeightnJet-qqbb","",700,500)
#pad1 = TPad("pad1", "pad1", 0.0, 0.05, 1,1)

#pad1.SetLeftMargin(0.15)
#pad1.SetTopMargin(0.1)
##pad1.SetBottomMargin(0.0105)
#pad1.SetRightMargin(0.08)
#pad1.Draw()
#pad1.SetTicks()

#pad2.SetLeftMargin(0.15)
#pad2.SetTopMargin(0.04)
#pad2.SetBottomMargin(0.35)
#pad2.SetRightMargin(0.08)
#pad2.Draw()
#pad2.SetTicks()
def pyf_tf1_params(x, p):
    return p[0] + p[1] * x[0] + p[2] * x[0] * x[0] + p[3] * x[0] * x[0] * x[0] + p[4] * x[0] * x[0] * x[0] * x[0]

for HistoName in ["HT_all_9j"]:       
 for Region in ["Resolved_SR"]:
  for btagStrategy in ["TwoTags"]:    
     
      ReBin = False
      YAxisScale = 1.4               
      #if "nJet" in HistoName:
          #Xaxis_label="Jet multiplicity"           
      
      file_data15       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/data_leptop_NewTrain_ttRew/data_2015.root_77p_225_.root","READ")
      dir_15        = file_data15.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_data15 = dir_15.Get("data_2015_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      
      file_data16       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/data_leptop_NewTrain_ttRew/data_2016.root_77p_225_.root","READ")  
      dir_16        = file_data16.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_data16 = dir_16.Get("data_2016_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      file_data17       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/data_leptop_NewTrain_ttRew/data_2017.root_77p_225_.root","READ")
      dir_17        = file_data17.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_data17 = dir_17.Get("data_2017_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      file_data18      = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/data_leptop_NewTrain_ttRew/data_2018.root_77p_225_.root","READ")
      dir_18        = file_data18.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_data18 = dir_18.Get("data_2018_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      
      file_tt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew/tt_PP8.root_77p_225.root","READ")
      dir_tt_1b   = file_tt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1b")
      h_ttbar_background_1b = dir_tt_1b.Get("tt_PP8_"+HistoName+"_1b_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_ttbar_background_1b = dir_tt_1b.Get("tt_PP8_1B_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      dir_tt_1c   = file_tt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1c")
      h_ttbar_background_1c = dir_tt_1c.Get("tt_PP8_"+HistoName+"_1c_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_ttbar_background_1c = dir_tt_1c.Get("tt_PP8_1C_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      dir_tt_1l   = file_tt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1l")
      h_ttbar_background_1l = dir_tt_1l.Get("tt_PP8_"+HistoName+"_1l_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_ttbar_background_1l = dir_tt_1l.Get("tt_PP8_1L_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      file_tt_filt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew/tt_PP8filtered.root_77p_225.root","READ")
      dir_tt_1b_filt   = file_tt_filt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1b")
      h_ttbar_background_1b_filt = dir_tt_1b_filt.Get("tt_PP8filtered_"+HistoName+"_1b_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_ttbar_background_1b_filt = dir_tt_1b_filt.Get("tt_PP8filtered_1B_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      dir_tt_1c_filt   = file_tt_filt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1c")
      h_ttbar_background_1c_filt = dir_tt_1c_filt.Get("tt_PP8filtered_"+HistoName+"_1c_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_ttbar_background_1c_filt = dir_tt_1c_filt.Get("tt_PP8filtered_1C_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      dir_tt_1l_filt   = file_tt_filt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1l")
      h_ttbar_background_1l_filt = dir_tt_1l_filt.Get("tt_PP8filtered_"+HistoName+"_1l_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_ttbar_background_1l_filt = dir_tt_1l_filt.Get("tt_PP8filtered_1L_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      h_ttbar_background = h_ttbar_background_1b + h_ttbar_background_1c + h_ttbar_background_1l + h_ttbar_background_1b_filt + h_ttbar_background_1c_filt + h_ttbar_background_1l_filt
      #h_ttbar_background = h_ttbar_background_1b + h_ttbar_background_1c + h_ttbar_background_1l
               
      file_st_tc   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew/st_tc.root_77p_225.root","READ")
      dir_st_tc   = file_st_tc.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_singleTop_background_tc = dir_st_tc.Get("st_tc_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      file_st_sc   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew/st_sc.root_77p_225.root","READ")
      dir_st_sc   = file_st_sc.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_singleTop_background_sch = dir_st_sc.Get("st_sc_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      
      file_Wt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew/Wt.root_77p_25.root","READ")
      dir_Wt   = file_Wt.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_Wt = dir_Wt.Get("Wt_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      file_tH   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew/tH_AFII.root_77p_225.root","READ")
      dir_tH   = file_tH.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_tH = dir_tH.Get("tH_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      file_tWZ   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew/tWZ.root_77p_225.root","READ")
      dir_tWZ   = file_tWZ.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_tWZ = dir_tWZ.Get("tWZ_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      file_tZ   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew/tZ.root_77p_225.root","READ")
      dir_tZ   = file_tZ.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_tZ = dir_tZ.Get("tZ_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      file_Wjet   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew/wjets_77p_225.root","READ")
      dir_Wjet    = file_Wjet.GetDirectory("nominal_Loose").GetDirectory(HistoName)        
      h_Wjet = dir_Wjet.Get("wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      file_Zjet   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew/zjets_77p_225.root","READ")
      dir_Zjet    = file_Zjet.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_Zjet = dir_Zjet.Get("zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      
      file_ttW   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew/ttW.root_77p_225.root","READ")
      dir_ttW    = file_ttW.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_ttW_background = dir_ttW.Get("ttW_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      file_ttH   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew/ttH_PP8.root_77p_225.root","READ")
      dir_ttH    = file_ttH.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_ttH_background = dir_ttH.Get("ttH_PP8_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      file_ttZ   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew/ttll.root_77p_225.root","READ")
      dir_ttZ    = file_ttZ.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_ttZ_background = dir_ttZ.Get("ttll_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      file_dib   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew/db.root_77p_225.root","READ")
      dir_dib   = file_dib.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_diboson_background = dir_dib.Get("db_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      
      h_data = h_data15 + h_data16 + h_data17 + h_data18
      h_nontt = h_diboson_background + h_singleTop_background_tc + h_singleTop_background_sch + h_Wt + h_tH + h_tWZ + h_tZ + h_Zjet + h_Wjet + h_ttW_background + h_ttH_background + h_ttZ_background
      #h_ttbar_background.Scale(0.9964) #correction for 5jet
      #h_ttbar_background.Scale(0.9912) #correction for 6jet
      #h_ttbar_background.Scale(1.0032) #correction for 7jet
      #h_ttbar_background.Scale(1.0285) #correction for 8jet
      h_ttbar_background.Scale(1.0878) #correction for ge9jet
      #h_ttbar_background.Scale(0.99762) # correction for ge5jet

      npars = 6 #6 parameters for hypsigm and expsigm functions
      #npars = 5
      #npars = 7
      h_weight = h_data.Clone()
      h_weight = h_weight - h_nontt
      h_weight.Divide(h_ttbar_background)
      hypsigm_tf1 = TF1("hypsigm_tf1","[0] + [1]/(x**[2]) - [3]/(1+exp([4] - [5]*x))",0,2000)
      #expsigm_tf1 = TF1("expsigm_tf1","[0] + [1]*exp([2]*x) - [3]/(1+exp([4] - [5]*x))",0,2000)
      #poly2_tf1 = TF1("poly2_tf1","[0] + [1]*x + [2]*x*x",0,2000)
      #poly3_tf1 = TF1("poly3_tf1","[0] + [1]*x + [2]*x*x + [3]*x*x*x",0,2000)
      #poly4_tf1 = TF1("poly4_tf1","[0] + [1]*x + [2]*x*x + [3]*x*x*x + [4]*x*x*x*x",0,2000)
      #exppoly4_tf1 = TF1("exppoly4_tf1","[0]*exp([1]*x + [2]*x*x + [3]*x*x*x + [4]*x*x*x*x)",0,2000)
      #poly1exp3_tf1 = TF1("poly1exp3_tf1","[0]+ [1]*x + [2]*exp([3]*x + [4]*x*x + [5]*x*x*x)",0,2000)
      #poly2exp1_tf1 = TF1("poly2exp1_tf1","[0]+ [1]*x + [2]*x*x + [3]*exp([4]*x)",0,2000)
      #poly2exp2_tf1 = TF1("poly2exp2_tf1","[0]+ [1]*x + [2]*x*x + [3]*exp([4]*x + [5]*x*x)",0,2000)
      #poly3exp1_tf1 = TF1("poly3exp1_tf1","[0]+ [1]*x + [2]*x*x + [3]*x*x*x + [4]*exp([5]*x)",0,2000)
      #poly3exp2_tf1 = TF1("poly3exp2_tf1","[0]+ [1]*x + [2]*x*x + [3]*x*x*x + [4]*exp([5]*x + [6]*x*x)",0,2000)

      sigPars      = array( 'd', [0.0]*6)
      chi2         = array ('d', [0.0]*1)
      
      #Nbins_X = h_weight.GetNbinsX()
      #print Nbins_X
      #for i in range (1,Nbins_X):
          #print h_weight.GetBinContent(i)
      
      #kind of optimal setting for function param for hyperbola + sigmoid function
      #hypsigm_tf1.SetParameter(0,0.9)
      #hypsigm_tf1.SetParameter(1,2000)
      #hypsigm_tf1.SetParameter(2,2.0) #1.5 
      #hypsigm_tf1.SetParameter(3,1.0) #0.5
      #hypsigm_tf1.SetParameter(4,10)
      #hypsigm_tf1.SetParameter(5,0.010)  
      
      #Parameters for Hyperbola+Sigmoid functional form
      #hypsigm_tf1.SetParameter(0,0.80)
      #hypsigm_tf1.SetParameter(1,8000)
      #hypsigm_tf1.SetParameter(2,2.0) #1.5 
      #hypsigm_tf1.SetParameter(3,1.0) #0.5
      #hypsigm_tf1.SetParameter(4,6) #2.5
      #hypsigm_tf1.SetParameter(5,0.006) 

      #hypsigm_tf1.SetParameter(0,1.4)
      #hypsigm_tf1.SetParameter(1,6500)
      #hypsigm_tf1.SetParameter(2,3.5) #1.5 
      #hypsigm_tf1.SetParameter(3,1.0) #0.5
      #hypsigm_tf1.SetParameter(4,5) #2.5
      #hypsigm_tf1.SetParameter(5,0.008)
      
      #Parameters for Exponential+Sigmoid functional form
      #expsigm_tf1.SetParameter(0,0.771)
      #expsigm_tf1.SetParameter(1,2.566)
      #expsigm_tf1.SetParameter(2,-0.009) #1.5 
      #expsigm_tf1.SetParameter(3,-0.200) #0.5
      #expsigm_tf1.SetParameter(4,-2.182) #2.5
      #expsigm_tf1.SetParameter(5,-0.003)
      
      #Parameters for Exponential+Sigmoid functional form
      #expsigm_tf1.SetParameter(0,0.461)
      #expsigm_tf1.SetParameter(1,2.458)
      #expsigm_tf1.SetParameter(2,-0.040) #1.5 
      #expsigm_tf1.SetParameter(3,-0.400) #0.5
      #expsigm_tf1.SetParameter(4,-2.282) #2.5
      #expsigm_tf1.SetParameter(5,-0.003)

      
      #Parameters for second order poly functional form
      #poly3_tf1.SetParameter(0,1000)
      #poly3_tf1.SetParameter(1,100)
      #poly3_tf1.SetParameter(2,10)
      #poly3_tf1.SetParameter(3,10)
      
      #second order exponential function
      #exppoly2_tf1.SetParameter(0,0.00001)
      #exppoly2_tf1.SetParameter(1,0.000001)
      #exppoly2_tf1.SetParameter(2,0.0000001)

      #second order exponential function
      #exppoly4_tf1.SetParameter(0,0.000000001)
      #exppoly4_tf1.SetParameter(1,0.00000000001)
      #exppoly4_tf1.SetParameter(2,0.000000000001)
      #exppoly4_tf1.SetParameter(3,0.0000000000001)
      #exppoly4_tf1.SetParameter(4,0.00000000000001)

      #Polynomial 1 exponential  1/1 slightly different parameters 1/2 function
      #poly1exp2_tf1.SetParameter(0,0.00001)
      #poly1exp2_tf1.SetParameter(1,0.000001)
      #poly1exp2_tf1.SetParameter(2,0.0000001)
      #poly1exp2_tf1.SetParameter(3,0.00000001)
      #poly1exp2_tf1.SetParameter(4,0.000000001)
      
      #Polynomial 1 exponential 3
      #poly1exp3_tf1.SetParameter(0,0.000001)
      #poly1exp3_tf1.SetParameter(1,0.0000001)
      #poly1exp3_tf1.SetParameter(2,0.00000001)
      #poly1exp3_tf1.SetParameter(3,0.000000001)
      #poly1exp3_tf1.SetParameter(4,0.0000000001)
      #poly1exp3_tf1.SetParameter(5,0.00000000001)

      #Polynomial 2 exponential 1
      #poly2exp1_tf1.SetParameter(0,0.0001)
      #poly2exp1_tf1.SetParameter(1,0.00001)
      #poly2exp1_tf1.SetParameter(2,0.000001)
      #poly2exp1_tf1.SetParameter(3,0.0000001)
      #poly2exp1_tf1.SetParameter(4,0.00000001)

      #Polynomial 2 exponential 2
      #poly2exp2_tf1.SetParameter(0,0.0001)
      #poly2exp2_tf1.SetParameter(1,0.00001)
      #poly2exp2_tf1.SetParameter(2,0.000001)
      #poly2exp2_tf1.SetParameter(3,0.0000001)
      #poly2exp2_tf1.SetParameter(4,0.00000001)
      #poly2exp2_tf1.SetParameter(5,0.000000001)

      #Polynomial 3 exponential 1 (5jet, 8jet, ge9 jet configuration)
      #poly3exp1_tf1.SetParameter(0,0.00001)
      #poly3exp1_tf1.SetParameter(1,0.000001)
      #poly3exp1_tf1.SetParameter(2,0.0000001)
      #poly3exp1_tf1.SetParameter(3,0.00000001)
      #poly3exp1_tf1.SetParameter(4,0.000000001)
      #poly3exp1_tf1.SetParameter(5,0.0000000001)

      #Polynomial 3 exponential 1 (6jet, 7jet, ge5j configuration)
      #poly3exp1_tf1.SetParameter(0,0.000001)
      #poly3exp1_tf1.SetParameter(1,0.0000001)
      #poly3exp1_tf1.SetParameter(2,0.00000001)
      #poly3exp1_tf1.SetParameter(3,0.000000001)
      #poly3exp1_tf1.SetParameter(4,0.0000000001)
      #poly3exp1_tf1.SetParameter(5,0.00000000001)

      #Polynomial 3 exponential 2 (6jet, 7jet, ge5j configuration)
      #poly3exp2_tf1.SetParameter(0,0.001)
      #poly3exp2_tf1.SetParameter(1,0.0001)
      #poly3exp2_tf1.SetParameter(2,0.00001)
      #poly3exp2_tf1.SetParameter(3,0.000001)
      #poly3exp2_tf1.SetParameter(4,0.0000001)
      #poly3exp2_tf1.SetParameter(5,0.00000001)
      #poly3exp2_tf1.SetParameter(6,0.000000001)


      #optimal parameters:
      # 0.9434
      # 13418.67
      # 2.121
      # 0.2213
      # 7.99
      # 0.0073
       
      fitter = TVirtualFitter.Fitter(h_weight)
      #fitter.SetMaxIterations(100000) #needed for poly3exp1
      fitter.SetMaxIterations(50000)
      fitStatus =  h_weight.Fit("hypsigm_tf1", "V", "", 0, 2000)
      myfunc2   =  h_weight.GetFunction("hypsigm_tf1")
      

      myfunc2.GetParameters(sigPars)
      #myfunc2.GetChisquare()
      myfunc2.SetLineColor(kBlue)
      myfunc2.SetLineWidth(2)
      #myfunc2.SetLineStyle(7)
      #Double chi = myfunc2.GetChisquare()
      Chi2 = myfunc2.GetChisquare()
      NDF = myfunc2.GetNDF()
      RedChi2 = Chi2/NDF
      print "chi2 :", myfunc2.GetChisquare()
      print "NDF :", myfunc2.GetNDF()
      #print "Red chi2 :" myfunc2.GetChisquare()/myfunc2.GetNDF()
      #pad1.cd()
      h_weight.SetMarkerStyle(20)
      h_weight.SetMarkerSize(1.0)
      h_weight.SetMarkerColor(1)
      h_weight.GetXaxis().SetTitle("H_{T}_{all} [GeV]")
      h_weight.GetYaxis().SetTitle("Weight factor")
      h_weight.GetYaxis().SetRangeUser(0.0,5.0)
      #h_weight.GetXaxis().SetRangeUser(4.5,12.5)
      leg = TLegend(0.72,0.45,0.885,0.685)
      leg.SetShadowColor(kWhite)
      leg.SetFillColor(kWhite)
      leg.SetLineColor(kWhite)
      h_weight.Draw("EP")
      myfunc2.Draw("SAMEL")   
      leg.AddEntry(h_weight,"Powheg+Pythia8","epl")
      leg.AddEntry(myfunc2,"Hyp+Sigmoid","l")
      leg.Draw()
      print "1: ",sigPars[0] ,"2:",sigPars[1],"3:", sigPars[2] ,"4:",sigPars[3] ,"5:",sigPars[4] ,"6:",sigPars[5]
      #print "1: ",sigPars[0] ,"2:",sigPars[1],"3:", sigPars[2] ,"4:",sigPars[3] ,"5:",sigPars[4] ,"6:",sigPars[5],"7:",sigPars[6]
      #print "1: ",sigPars[0] ,"2:",sigPars[1],"3:", sigPars[2] ,"4:",sigPars[3] ,"5:",sigPars[4]
      #print "1: ",sigPars[0] ,"2:",sigPars[1],"3:", sigPars[2] , "4:",sigPars[3]
      #print "1: ",sigPars[0] ,"2:",sigPars[1],"3:", sigPars[2], "4:",sigPars[3], "5:",sigPars[4]
      #pad1.cd()
      #myText(0.35,0.78,1,"#sqrt{s}=13 TeV, 139 fb^{-1}")
      if "TwoTags" in btagStrategy:
          myText(0.35,0.78,1,"l^{-}+jets Resolved: at least 9 jet, 2 b-tag")
      myText(0.35,0.73,1,"#chi^{2}/ndf :"+str(round(Chi2,2))+"/"+str(round(NDF,2)))  
      f=open("../ttRew/ttRew_qqbb_HypSigm_HTall.txt","a")
      f.write(HistoName+str(sigPars[0])+";"+str(sigPars[1])+";"+str(sigPars[2])+";"+str(sigPars[3])+";"+str(sigPars[4])+";"+str(sigPars[5])+"\n")
      #f.write(HistoName+str(sigPars[0])+";"+str(sigPars[1])+";"+str(sigPars[2])+";"+str(sigPars[3])+";"+str(sigPars[4])+";"+str(sigPars[5])+";"+str(sigPars[6])+"\n")
      #f.write(HistoName+str(sigPars[0])+";"+str(sigPars[1])+";"+str(sigPars[2])+";"+str(sigPars[3])+";"+str(sigPars[4])+"\n")
      f.close() 
      #c1.RedrawAxis()
      c1.RedrawAxis()
      c1.Update()
      c1.RedrawAxis()
      c1.SaveAs("../Plots/weight/Weight_%s_trial_ttRew_ge9j_HT_all_hypsigm.pdf" % (HistoName+"_"+btagStrategy))
