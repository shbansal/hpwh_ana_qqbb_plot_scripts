# -*- coding: utf-8 -*-
#python
import ROOT
import sys
import glob
import math
import re
from ROOT import *
from array import *
from ROOT import TF1


########----------##############
#This macro is just to keep a cross-check
########----------##############

gROOT.SetBatch(True)
gStyle.SetOptStat(0)
gStyle.SetPalette(1)
gROOT.LoadMacro("../style/AtlasStyle.C")
gROOT.LoadMacro("../style/AtlasUtils.C")
SetAtlasStyle()
#from ROOT import rootpy.plotting
#gMinuit.SetMaxIterations(5000)
import numpy as np
from numpy import ndarray
import array
#from rootpy.plotting import Hist, HistStack, Legend, Canvas
#from rootpy.plotting.style import get_style, set_style
#from rootpy.plotting.utils import draw
#from rootpy.interactive import wait
#import rootpy.plotting.root2matplotlib as rplt
from ROOT import TCanvas, TFile, TPad, THStack, TLine,TGraphAsymmErrors, TNtuple, TH1F, TH2F
#from ROOT import TGraphAsymmErrors
from array import *

y_axis_label="Event fraction"

c_blue   = TColor.GetColor("#3366ff")
c_red    = TColor.GetColor("#ee3311")
c_orange = TColor.GetColor("#ff9900")

def NormalizeHisto(histo):
     n_events=histo.Integral(-1,histo.GetNbinsX()+1)
     if n_events == 0:
         return
     print n_events, histo.Integral(histo.GetNbinsX(),histo.GetNbinsX()+1)
     histo.Scale(1./n_events)
     histo.SetLineWidth(2)
     histo.SetStats(0)
     histo.SetFillStyle(3001)
     histo.SetMarkerColor(histo.GetLineColor())
     histo.SetMarkerSize(0.0)
     histo.GetXaxis().SetTitleOffset(1.2)
     histo.GetYaxis().SetTitleOffset(1.52)
     histo.GetXaxis().SetLabelSize(0.05)
     histo.GetYaxis().SetLabelSize(0.05)
     histo.GetXaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetNdivisions(504)
     histo.GetXaxis().SetNdivisions(504)

c1 = TCanvas("DatavMC-qqbb","",700,500)
pad1 = TPad("pad1", "pad1", 0.0, 0.35, 1,1)
#pad1.SetLogy()
pad2 = TPad("pad2", "pad2", 0, 0.0, 1,0.35)
pad1.SetLeftMargin(0.15)
pad1.SetTopMargin(0.1)
pad1.SetBottomMargin(0.0105)
pad1.SetRightMargin(0.08)
pad1.Draw()
pad1.SetTicks()

pad2.SetLeftMargin(0.15)
pad2.SetTopMargin(0.04)
pad2.SetBottomMargin(0.35)
pad2.SetRightMargin(0.08)
pad2.Draw()
pad2.SetTicks()
HistoNameR = "maxMVAResponse"
#for HistoName in ["pTWplus_5j", "pTH_5j"]:
for HistoName in ["HT_5j"]:
#for HistoName in ["HT_all_5j"]:  
#for HistoName in ["HT_8j"]:     
#for HistoName in ["mVH"]:
## for bTagStrategy in ["Incl","FourPlusTags","ThreeTags","TwoTags"]:
 for Region in ["Resolved_SR"]:
  #for btagStrategy in ["TwoTags","FourPlusTags","ThreeTags"]:
  for btagStrategy in ["TwoTags"]:
  #for btagStrategy in ["Inclusive","TwoTags","FourPlusTags","ThreeTags"]:    
     
      ReBin = False
      YAxisScale = 1.4
      
      if "Eta_j1j2" in HistoName:
          Xaxis_label="\eta_{j1j2}"
      if "Eta_j1j3" in HistoName:
          Xaxis_label="\eta_{j1j3}"
      if "Eta_j1j4" in HistoName:
          Xaxis_label="\eta_{j1j4}"
      if "Eta_j1j5" in HistoName:
          Xaxis_label="\eta_{j1j5}"
      if "Eta_j1j6" in HistoName:
          Xaxis_label="\eta_{j1j6}"
      if "Eta_j1j7" in HistoName:
          Xaxis_label="\eta_{j1j7}" 
      if "Eta_j2j3" in HistoName:
          Xaxis_label="#eta_{j2j3}"
      if "Eta_j2j4" in HistoName:
          Xaxis_label="#eta_{j2j4}"
      if "Eta_j2j5" in HistoName:
          Xaxis_label="#eta_{j2j5}"
      if "Eta_j2j6" in HistoName:
          Xaxis_label="#eta_{j2j6}"
      if "Eta_j2j7" in HistoName:
          Xaxis_label="#eta_{j2j7}"
      if "Eta_j3j4" in HistoName:
          Xaxis_label="#eta_{j3j4}"
      if "Eta_j3j5" in HistoName:
          Xaxis_label="#eta_{j3j5}"
      if "Eta_j3j6" in HistoName:
          Xaxis_label="#eta_{j3j6}"
      if "Eta_j3j7" in HistoName:
          Xaxis_label="#eta_{j3j7}" 
      if "Eta_j4j5" in HistoName:
          Xaxis_label="#eta_{j4j5}"
      if "Eta_j4j6" in HistoName:
          Xaxis_label="#eta_{j4j6}"
      if "Eta_j4j7" in HistoName:
          Xaxis_label="#eta_{j4j7}"
      if "Eta_j5j6" in HistoName:
          Xaxis_label="#eta_{j5j6}"
      if "Eta_j5j7" in HistoName:
          Xaxis_label="#eta_{j5j7}"
      if "Eta_j6j7" in HistoName:
          Xaxis_label="#eta_{j6j7}"               

      if "nBTags" in HistoName:
          Xaxis_label="b-tag multiplicity"
      if "nJet" in HistoName:
          Xaxis_label="Jet multiplicity"
      if "DeltaPhi_HW" in HistoName:
          Xaxis_label="#Delta #Phi_{HW}"
      if "DeltaEta_HW" in HistoName:
          Xaxis_label="#Delta #eta_{HW}"
      if "pTWplus" in HistoName:
          Xaxis_label="Transverse momentum of W Boson [GeV]"
      if "pTH" in HistoName:
          Xaxis_label="Transverse momentum of Higgs [GeV]"    
      if  HistoName == "pTH_over_mVH":
          Xaxis_label="pT_{H}/m_{VH}"
      if  HistoName == "pTW_over_mVH":
          Xaxis_label="pT_{W}/m_{VH}"            
      if  HistoName == "pTH":
          Xaxis_label="Transverse momentum of Higgs [GeV]"
      if  HistoName == "pTWplus":
          Xaxis_label="Transverse momentum of W Boson [GeV]"
      if HistoName == "mVH":
          Xaxis_label="Mass of charged Higgs [GeV]"
      if "mH" in HistoName:
          Xaxis_label="Mass of Higgs [GeV]"
      if "mass_resolution" in HistoName:
          Xaxis_label="Mass resolution [GeV]"
      if "HT" in HistoName:
          Xaxis_label="H_{T} (Transverse momentum sum of jets) [GeV]"
      if "HT_all" in HistoName:
          Xaxis_label="H_{T}_{all} [GeV]"
      if "HT_all_5j" in HistoName:
          Xaxis_label="H_{T}_{all} exc. for 5 jets [GeV]"        
      if "HT_bjets" in HistoName:
          Xaxis_label="H_{T_{b-jets}} (Transverse momentum sum of b-jets) [GeV]"
      if "maxMVAResponse" in HistoName:
          Xaxis_label="BDT score (Signal reconstruction)"
      if "maxMVAResponse_15" in HistoName:
          Xaxis_label="BDT score (Signal reco)"
      if "maxMVAResponse_10" in HistoName:
          Xaxis_label="BDT score (Signal reco)"            
      #Xaxis_label=""
      
      file_data15       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/data_leptop_NewTrain_ttRew_V2/data_2015.root_77p_225_.root","READ")
      #file_data15       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_290121/data15.root_70p_.root","READ")
      dir_15        = file_data15.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_data15 = dir_15.Get("data_2015_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_data15.GetYaxis.SetRange(0,10e6)
    
      file_data16       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/data_leptop_NewTrain_ttRew_V2/data_2016.root_77p_225_.root","READ")  
      #file_data16       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_290121/data16.root_70p_.root","READ")  
      #file_data16       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_ResCR2808/data16.root_70p_MC16a.root","READ")
      dir_16        = file_data16.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_data16 = dir_16.Get("data_2016_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_data16.GetYaxis.SetRange(0,10e6)

      #file_data17       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/data17.root_70p_MC16d.root","READ")
      #file_data17       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_290121/data17.root_70p_.root","READ")
      file_data17       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/data_leptop_NewTrain_ttRew_V2/data_2017.root_77p_225_.root","READ")
      dir_17        = file_data17.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_data17 = dir_17.Get("data_2017_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_data17.GetYaxis.SetRangeUser(0,10e6)

      file_data18      = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/data_leptop_NewTrain_ttRew_V2/data_2018.root_77p_225_.root","READ")
      #file_data18      = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_290121/data18.root_70p_.root","READ")
      dir_18        = file_data18.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_data18 = dir_18.Get("data_2018_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_data18.GetYaxis.SetRangeUser(0,10e6)
      
      #file_400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_Signal_225Con_200721/hp400_AFII.root_77p_225.root","READ")
      #dir_400        = file_400.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      #h_sig_Hplus_m400 = dir_400.Get("hp400_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_sig_Hplus_m400.SetLineColor(kRed)
      #h_sig_Hplus_m400.SetLineStyle(7)
      ##h_sig_Hplus_m400.Scale(139*1000000)
      #if ReBin == True:
          #h_sig_Hplus_m400.Rebin(2)

      
      #file_800       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_Signal_225Con_200721/hp800_AFII.root_77p_225.root","READ")
      #dir_800       = file_800.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      #h_sig_Hplus_m800 = dir_800.Get("hp800_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_sig_Hplus_m800.SetLineColor(kBlack)
      #h_sig_Hplus_m800.SetLineStyle(7)
      ##h_sig_Hplus_m800.Scale(139*1000000)
      #if ReBin == True:
          #h_sig_Hplus_m800.Rebin(2)


      #file_1600	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_Signal_225Con_200721/hp1600_AFII.root_77p_225.root","READ")
      #dir_1600        = file_1600.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      #h_sig_Hplus_m1600 = dir_1600.Get("hp1600_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_sig_Hplus_m1600.SetLineColor(kBlue)
      #h_sig_Hplus_m1600.SetLineStyle(7)
      ##h_sig_Hplus_m1600.Scale(139*1000000)
      #if ReBin == True:
          #h_sig_Hplus_m1600.Rebin(2)
        
      #file2   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/ttbar_70p.root","READ")
      #file_tt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_1512/ttbar.root_70p_MC16e.root","READ")
      file_tt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_V2/tt_PP8.root_77p_225.root","READ")
      #file_tt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_290121/ttbar_70p.root","READ")
      dir_tt_1b   = file_tt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1b")
      #h_ttbar_background_1b = dir_tt_1b.Get("tt_PP8_1B_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttbar_background_1b = dir_tt_1b.Get("tt_PP8_"+HistoName+"_1b_"+Region+"_"+btagStrategy+"_nominal_Loose")
      dir_tt_1c   = file_tt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1c")
      #h_ttbar_background_1c = dir_tt_1c.Get("tt_PP8_1C_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttbar_background_1c = dir_tt_1c.Get("tt_PP8_"+HistoName+"_1c_"+Region+"_"+btagStrategy+"_nominal_Loose")
      dir_tt_1l   = file_tt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1l")
      #h_ttbar_background_1l = dir_tt_1l.Get("tt_PP8_1L_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttbar_background_1l = dir_tt_1l.Get("tt_PP8_"+HistoName+"_1l_"+Region+"_"+btagStrategy+"_nominal_Loose")

      file_tt_filt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_V2/tt_PP8filtered.root_77p_225.root","READ")
      dir_tt_1b_filt   = file_tt_filt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1b")
      #h_ttbar_background_1b_filt = dir_tt_1b_filt.Get("tt_PP8filtered_1B_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttbar_background_1b_filt = dir_tt_1b_filt.Get("tt_PP8filtered_"+HistoName+"_1b_"+Region+"_"+btagStrategy+"_nominal_Loose")
      dir_tt_1c_filt   = file_tt_filt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1c")
      #h_ttbar_background_1c_filt = dir_tt_1c_filt.Get("tt_PP8filtered_1C_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttbar_background_1c_filt = dir_tt_1c_filt.Get("tt_PP8filtered_"+HistoName+"_1c_"+Region+"_"+btagStrategy+"_nominal_Loose")
      dir_tt_1l_filt   = file_tt_filt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1l")
      #h_ttbar_background_1l_filt = dir_tt_1l_filt.Get("tt_PP8filtered_1L_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttbar_background_1l_filt = dir_tt_1l_filt.Get("tt_PP8filtered_"+HistoName+"_1l_"+Region+"_"+btagStrategy+"_nominal_Loose")

    
      h_ttbar_background = h_ttbar_background_1b + h_ttbar_background_1c + h_ttbar_background_1l + h_ttbar_background_1b_filt + h_ttbar_background_1c_filt + h_ttbar_background_1l_filt
      h_ttbar_background_1l = h_ttbar_background_1l + h_ttbar_background_1l_filt
      h_ttbar_background_1c = h_ttbar_background_1c + h_ttbar_background_1c_filt
      h_ttbar_background_1b = h_ttbar_background_1b + h_ttbar_background_1b_filt
      ttbar_area_nrew = h_ttbar_background.Integral()
      h_ttbar_background.Scale(0.9964)
      h_ttbar_background_1b.Scale(0.9964)
      h_ttbar_background_1c.Scale(0.9964)
      h_ttbar_background_1l.Scale(0.9964)
      
      #########################Hyperbola+Sigmoid Functions################################
      Rew = TF1("Rew","0.938 + 1033053.698/(x**2.924) - 0.241/(1+exp(11.994 - 0.011*x))",0,2000)   #5 jet
      #Rew = TF1("Rew","0.937 + 3757191.533/(x**3.011) - 0.325/(1+exp(6.707 - 0.006*x))",0,2000)    #6 jet
      #Rew = TF1("Rew","0.811 + 2118.453/(x**1.506) - 0.255/(1+exp(5.868 - 0.004*x))",0,2000)       #7 jet
      #Rew = TF1("Rew","0.777 + 3208.699/(x**1.505) - 0.233/(1+exp(8.134 - 0.006*x))",0,2000)       #8 jet
      #Rew = TF1("Rew","0.891 + 506302.812/(x**2.328) - 0.126/(1+exp(20.733 - 0.019*x))",0,2000)    #ge9 jet
      #Rew = TF1("Rew","0.966 + 577434.102/(x**2.831) - 0.273/(1+exp(5.833 - 0.005*x))",0,3500)    #ge5 jet
      ####################################################################################

      #########################Exponential+Sigmoid Functions################################
      #Rew = TF1("Rew","0.688 + 3.269*exp(-0.014*x) - (-0.263)/(1+exp(-9.769 - (-0.009)*x))",0,2000)   #5 jet
      #Rew = TF1("Rew","0.592 + 5.441*exp(-0.013*x) - (-0.378)/(1+exp(-5.300 - (-0.005)*x))",0,2000)    #6 jet
      #Rew = TF1("Rew","0.442 + 2.446*exp(-0.008*x) - (-0.574)/(1+exp(-2.710 - (-0.002)*x))",0,2000)       #7 jet
      #Rew = TF1("Rew","0.571 + 2.361*exp(-0.005*x) - (-0.329)/(1+exp(-6.273 - (-0.005)*x))",0,2000)       #8 jet
      #Rew = TF1("Rew","0.786 + 4.902*exp(-0.006*x) - (-0.165)/(1+exp(-17.300 - (-0.016)*x))",0,2000)    #ge9 jet
      ####################################################################################

      #Rew = TF1("Rew","1.0112+ (-0.00011)*x + -5.618e-08*x*x + 2.531*exp(-0.008*x)",0,1000)
      #print 2**2.924
      #weight      = array( 'd', []*6)
      nbins = h_ttbar_background.GetNbinsX()
      weight      = array( 'd', [0.0]*40)
      for i in range (0,nbins):         
          scale_bin= Rew.Eval(h_ttbar_background.GetBinCenter(i+1))
          #weight[i]=scale_bin
          #print weight[i]
          h_ttbar_background.SetBinContent(i+1,scale_bin*h_ttbar_background.GetBinContent(i+1))
          scale_bin_1b= Rew.Eval(h_ttbar_background_1b.GetBinCenter(i+1))
          h_ttbar_background_1b.SetBinContent(i+1,scale_bin_1b*h_ttbar_background_1b.GetBinContent(i+1))
          scale_bin_1c= Rew.Eval(h_ttbar_background_1c.GetBinCenter(i+1))
          h_ttbar_background_1c.SetBinContent(i+1,scale_bin_1c*h_ttbar_background_1c.GetBinContent(i+1))
          scale_bin_1l= Rew.Eval(h_ttbar_background_1l.GetBinCenter(i+1))
          h_ttbar_background_1l.SetBinContent(i+1,scale_bin_1l*h_ttbar_background_1l.GetBinContent(i+1))
      
      ttbar_area_rew = h_ttbar_background.Integral()
      weight_factor = ttbar_area_rew/ttbar_area_nrew
      print weight_factor

      h_ttbar_background_1l.SetFillColor(kWhite)
      h_ttbar_background_1l.SetLineColor(kBlack)
      h_ttbar_background_1l.SetLineWidth(1)

      h_ttbar_background_1c.SetFillColor(kBlue-10)
      h_ttbar_background_1c.SetLineColor(kBlack)
      h_ttbar_background_1c.SetLineWidth(1)

      h_ttbar_background_1b.SetFillColor(kBlue-7)
      h_ttbar_background_1b.SetLineColor(kBlack)
      h_ttbar_background_1b.SetLineWidth(1)
      
      #h_ttbar_background.SetFillColor(kBlue-10)
      #h_ttbar_background.SetLineColor(kBlack)
      #h_ttbar_background.SetLineWidth(1)             
  
      file_dib   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_V2/db.root_77p_225.root","READ")
      dir_dib   = file_dib.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_diboson_background = dir_dib.Get("db_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_diboson_background.SetFillColor(kYellow)
      h_diboson_background.SetLineColor(kBlack)
      #h_diboson_background.Scale(139*1000000)
      h_diboson_background.SetLineWidth(1)
      if ReBin == True:
          h_diboson_background.Rebin(2)

      
      file_st_tc   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_V2/st_tc.root_77p_225.root","READ")
      dir_st_tc   = file_st_tc.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_singleTop_background_tc = dir_st_tc.Get("st_tc_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      
      file_Wt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_V2/Wt.root_77p_225.root","READ")
      dir_Wt   = file_Wt.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_Wt = dir_Wt.Get("Wt_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      file_tH   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_V2/tH_AFII.root_77p_225.root","READ")
      dir_tH   = file_tH.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_tH = dir_tH.Get("tH_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      file_tWZ   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_V2/tWZ.root_77p_225.root","READ")
      dir_tWZ   = file_tWZ.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_tWZ = dir_tWZ.Get("tWZ_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      file_tZ   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_V2/tZ.root_77p_225.root","READ")
      dir_tZ   = file_tZ.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_tZ = dir_tZ.Get("tZ_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      file_st_sc   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_V2/st_sc.root_77p_225.root","READ")
      dir_st_sc   = file_st_sc.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_singleTop_background_sch = dir_st_sc.Get("st_sc_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      h_singleTop_background = h_singleTop_background_tc + h_singleTop_background_sch + h_Wt + h_tH + h_tWZ + h_tZ
      #h_singleTop_background = h_singleTop_background_tc + h_singleTop_background_sch
      h_singleTop_background.SetFillColor(kGreen-7)
      h_singleTop_background.SetLineColor(kBlack)
      #h_singleTop_background.Scale(139*1000000)
      h_singleTop_background.SetLineWidth(1)
      if ReBin == True:
          h_singleTop_background.Rebin(2)

      
      file_Zjet   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_V2/zjets_77p_225.root","READ")
      dir_Zjet    = file_Zjet.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_Z_background = dir_Zjet.Get("zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Z_background.SetFillColor(kRed-1)
      h_Z_background.SetLineColor(kBlack)
      h_Z_background.SetLineWidth(1)
      if ReBin == True:
          h_Z_background.Rebin(2)

      file_Wjet   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_V2/wjets_77p_225.root","READ")
      dir_Wjet    = file_Wjet.GetDirectory("nominal_Loose").GetDirectory(HistoName)        
      h_W_background = dir_Wjet.Get("wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_W_background.SetFillColor(kRed-8)
      h_W_background.SetLineColor(kBlack)
      h_W_background.SetLineWidth(1)
      if ReBin == True:
          h_W_background.Rebin(2)        

      
      file_ttW   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_V2/ttW.root_77p_225.root","READ")
      dir_ttW    = file_ttW.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_ttW_background = dir_ttW.Get("ttW_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttW_background.SetFillColor(kMagenta)
      h_ttW_background.SetLineColor(kBlack)
      h_ttW_background.SetLineWidth(1)
      if ReBin == True:
          h_ttW_background.Rebin(2)

      file_ttH   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_V2/ttH_PP8.root_77p_225.root","READ")
      dir_ttH    = file_ttH.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_ttH_background = dir_ttH.Get("ttH_PP8_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttH_background.SetFillColor(kTeal)
      h_ttH_background.SetLineColor(kBlack)
      h_ttH_background.SetLineWidth(1)
      if ReBin == True:
          h_ttH_background.Rebin(2)

      file_ttZ   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_V2/ttll.root_77p_225.root","READ")
      dir_ttZ    = file_ttZ.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_ttZ_background = dir_ttZ.Get("ttll_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttZ_background.SetFillColor(kOrange+1)
      h_ttZ_background.SetLineColor(kBlack)
      h_ttZ_background.SetLineWidth(1)
      if ReBin == True:
          h_ttZ_background.Rebin(2)    

      h_data = h_data15 + h_data16 + h_data17 + h_data18
      #h_data =  h_data1518
      #h_data = h_data15 + h_data16
      #h_data = h_data18
            
      #h_data = h_data17
      h_data.SetMarkerStyle(20)
      h_data.SetMarkerSize(1.0)
      h_data.SetMarkerColor(1)
      stack = THStack()

      #stack.Add(h_sig_Hplus_m400)
      #stack.Add(h_sig_Hplus_m800)
      #stack.Add(h_sig_Hplus_m1600)  

      #stack.Add(h_W_background)
      
      
      pad1.cd()
      
      #pad1.SetMaximum(h_data.GetYaxis())
      #y2 = h_ttbar_background.GetMaximum()+(h_ttbar_background.GetMaximum()*0.8)
      if "Inclusive" in btagStrategy:
         stack.SetMaximum(stack.GetMaximum())
      if "TwoTags" in btagStrategy:
         stack.SetMaximum(stack.GetMaximum())
      if "ThreeTags" in btagStrategy :
         stack.SetMaximum (stack.GetMaximum()*1.5)
      if "FourPlusTags" in btagStrategy :
         stack.SetMaximum (stack.GetMaximum()*1.7)


      
      #h_sig_Hplus_m400.Draw("HISTSAME")
      #h_sig_Hplus_m800.Draw("HISTSAME")
      #h_sig_Hplus_m1600.Draw("HISTSAME")
      #yield_H800 = h_sig_Hplus_m800.Integral()
      #yield_H400 = h_sig_Hplus_m400.Integral()
      #yield_H1600 = h_sig_Hplus_m1600.Integral()
      
      #print yield_H400
      #print yield_H800
      #print yield_H1600

      #yield_tt = h_ttbar_background.Integral()
      #str_yield_tt = str(round(yield_tt,1))
      yield_tt_1b = h_ttbar_background_1b.Integral()
      
      str_yield_tt_1b = str(round(yield_tt_1b,1))
      yield_tt_1c = h_ttbar_background_1c.Integral()
      
      str_yield_tt_1c = str(round(yield_tt_1c,1))
      yield_tt_1l = h_ttbar_background_1l.Integral()
      
      str_yield_tt_1l = str(round(yield_tt_1l,1))
      yield_tt = h_ttbar_background.Integral()
      
      str_yield_tt = str(round(yield_tt,1))

      yield_Z = h_Z_background.Integral()
      
      str_yield_Z = str(round(yield_Z,1))
      yield_W = h_W_background.Integral()
      
      str_yield_W = str(round(yield_W,1))
      yield_SingleTop = h_singleTop_background.Integral()
      
      str_yield_SingleTop = str(round(yield_SingleTop,1))
      yield_diBoson = h_diboson_background.Integral()
      
      str_yield_diBoson = str(round(yield_diBoson,1))
      yield_ttH = h_ttH_background.Integral()
      
      str_yield_ttH = str(round(yield_ttH,1))
      yield_ttW = h_ttW_background.Integral()
      
      str_yield_ttW = str(round(yield_ttW,1))
      yield_ttZ = h_ttZ_background.Integral()
      
      str_yield_ttZ = str(round(yield_ttZ,1))

      #yield_Total = yield_tt_1b+yield_tt_1c+yield_tt_1l+yield_Z+yield_SingleTop+yield_diBoson+yield_ttH+yield_ttW
      yield_Total = yield_tt_1b+yield_tt_1c+yield_tt_1l+yield_Z+yield_SingleTop+yield_diBoson+yield_W+yield_ttH+yield_ttW+yield_ttZ
      
      str_yield_Total = str(round(yield_Total,1))

      print yield_tt_1l
      print yield_tt_1c
      print yield_tt_1b
      print yield_SingleTop
      print yield_Z
      print yield_W
      print yield_ttH
      print yield_ttW
      print yield_ttZ
      print yield_diBoson
      print yield_Total

      yield_Data = h_data.Integral()
      str_yield_Data = str(round(yield_Data,2))
      #if "ThreeTags" or "FourPlusTags" in btagStrategy:
        #leg = TLegend(0.70,0.45,0.825,0.860)
      #else :
        #leg = TLegend(0.65,0.45,0.825,0.855)
      if "maxMVAResponse" in HistoName:  
         leg = TLegend(0.25,0.40,0.425,0.805)  
      elif "DeltaPhi_HW" in HistoName:  
         leg = TLegend(0.25,0.40,0.425,0.805)   
      elif "nJet" in HistoName:  
         leg = TLegend(0.74,0.25,0.875,0.655)   
      else:
         leg = TLegend(0.72,0.25,0.885,0.685) 
         #leg = TLegend(0.65,0.45,0.825,0.855)  
      ATLAS_LABEL(0.45,0.80,"work-in-progress",1,0.09)
      #myText(0.785,0.825,1,"139 fb^{-1}")
      
      leg.SetShadowColor(kWhite)
      leg.SetFillColor(kWhite)
      leg.SetLineColor(kWhite)
      leg.SetNColumns(2)
      leg.AddEntry(h_data,"Data","epl")
      leg.AddEntry(None, str_yield_Data,"")
      leg.AddEntry(h_ttbar_background_1l,"t#bar{t}+ light","f")
      leg.AddEntry(None, str_yield_tt_1l, "")
      leg.AddEntry(h_ttbar_background_1c,"t#bar{t}+ >=1c","f")
      leg.AddEntry(None, str_yield_tt_1c, "")
      leg.AddEntry(h_ttbar_background_1b,"t#bar{t}+ >=1b","f")
      leg.AddEntry(None, str_yield_tt_1b, "")
      #leg.AddEntry(h_ttbar_background,"t#bar{t}+ jet","f")
      #leg.AddEntry(None, str_yield_tt, "")
      leg.AddEntry(h_singleTop_background, "single Top","f")
      leg.AddEntry(None, str_yield_SingleTop, "")
      
      leg.AddEntry(h_W_background,"W+jet","f")
      leg.AddEntry(None, str_yield_W, "")
      leg.AddEntry(h_Z_background,"Z+jet","f")
      leg.AddEntry(None, str_yield_Z, "")
      leg.AddEntry(h_ttW_background,"t#bar{t}+W","f")
      leg.AddEntry(None, str_yield_ttW, "")
      leg.AddEntry(h_ttH_background,"t#bar{t}+H","f")
      leg.AddEntry(None, str_yield_ttH, "")
      leg.AddEntry(h_diboson_background, "diboson","f")
      leg.AddEntry(None, str_yield_diBoson, "") 
      leg.AddEntry(h_ttZ_background,"t#bar{t}+Z","f")
      leg.AddEntry(None, str_yield_ttZ, "")
      
      #leg.AddEntry(h_W_background,"W+jet","f")
      #leg.AddEntry(None, str_yield_W, "") 
      leg.AddEntry(None, "Total","")
      leg.AddEntry(None, str_yield_Total, "")
      #leg.AddEntry(h_sig_Hplus_m400, "400 GeV H^{+}","L")
      #leg.AddEntry(h_sig_Hplus_m800, "800 GeV H^{+}","L")
      #leg.AddEntry(h_sig_Hplus_m1600,"1600 GeV H^{+}","L")
      #leg.AddEntry(h_data,"Data","epl")
      leg.SetTextSize(0.0350)
      ##leg2.SetTextSize(0.0350)
      leg.Draw()
      #h_mc = h_W_background + h_diboson_background + h_Z_background + h_ttbar_background + h_singleTop_background + h_ttW_background + h_ttH_background
      h_mc = h_diboson_background + h_Z_background + h_ttbar_background + h_singleTop_background + h_W_background + h_ttW_background + h_ttH_background + h_ttZ_background
      #leg2.Draw()     
      pad2.SetGrid()
      pad2.cd()
      #h_ratio = h_data15 + h_data16
      #h_ratio = h_data.Clone()
      #
      
      h_ratio = h_data.Clone()
      h_ratio.Divide(h_mc)
      if "TwoTags" in btagStrategy:
        h_ratio.GetYaxis().SetRangeUser(0.55,1.40)
      if "TwoTags" in btagStrategy and "nJet" in HistoName:
        h_ratio.GetYaxis().SetRangeUser(0,2)  
      if "ThreeTags" in btagStrategy :
        h_ratio.GetYaxis().SetRangeUser(0.0,3)
      if "FourPlusTags" in btagStrategy :
        h_ratio.GetYaxis().SetRangeUser(0.0,3)    
      h_ratio.SetStats(0)
      h_ratio.SetMarkerStyle(20)
      h_ratio.SetMarkerSize(1.0)
      h_ratio.SetMarkerColor(1)
      h_ratio.SetLabelSize(0.082,"X")
      h_ratio.SetLabelSize(0.082,"Y")
      h_ratio.SetTitleSize(0.082,"X")
      h_ratio.SetTitleSize(0.082,"Y")
      h_ratio.SetTitleOffset(0.48,"Y")
      h_ratio.SetTitleOffset(1.2,"X")
      h_ratio.GetYaxis().SetNdivisions(505)
      h_ratio.SetTickLength(0.005,"X") #0.05
      h_ratio.SetLabelOffset(0.01,"X")
      h_ratio.GetXaxis().SetTitle(Xaxis_label)
      h_ratio.GetYaxis().SetTitle("Data/Pred.")
      h_ratio.Draw("EP")
      

      #stat= TGraphAsymmErrors(nbins,np.array(x_values),np.array(y_values),np.array(x_error_down),np.array(x_error_up),np.array(y_error_down),np.array(y_error_up))
      #print x_values
      

      #leg.AddEntry(totsys,"total uncertainty","f")
      

      
      #h_other_background.Draw("HIST")
      #h_ttbar_background.Draw("HISTSAME")
      

      #if HistoName in "maxMVAResponse":
         #leg = TLegend(0.2,0.65,0.725,0.855)
      #else:
         #leg = TLegend(0.45,0.65,0.925,0.855)
      #ATLAS_LABEL(0.20,0.885," Simulation Internal",1,0.19);
      
      
      #h_other_background.GetYaxis().SetRangeUser(0.001,round(ymax*1.25,3)+0.001)

      #legend = Legend([h1, h2, h3], leftmargin=0.45, margin=0.3)
      #legend.Draw()
      #canvas.Modified()
      #canvas.Update()
      pad1.cd()
      myText(0.45,0.75,1,"#sqrt{s}=13 TeV, 139 fb^{-1}")
      if "TwoTags" in btagStrategy:
          myText(0.45,0.70,1,"l^{-}+jets Resolved: 5 jet, 2 b-tag")
          #myText(0.20,0.65,1,"Pre-fit")
          #myText(0.20,0.65,1,"SR cuts")
      if "ThreeTags" in btagStrategy :
          myText(0.45,0.70,1,"l^{-}+jets Resolved: 5 jet, 3 b-tag")
          #myText(0.20,0.65,1,"CR cuts")
      if "FourPlusTags" in btagStrategy :
          myText(0.45,0.70,1,"l^{-}+jets Resolved: 5 jet, at least 4 b-tag")
          #myText(0.20,0.65,1,"CR cuts")
      if "Inclusive" in btagStrategy :  
          myText(0.45,0.70,1,"l^{-}+jets Resolved: 5 jet, Inclusive")
          #myText(0.20,0.67,1,"")
      c1.RedrawAxis()
      c1.Update()
      c1.RedrawAxis()
      #c1.SaveAs("../Plots/datavmc/DatavMC_%s_qqbb_datavmc_V2Ntup_300721_225constr_minXt_norew.pdf" % (HistoName+"_"+btagStrategy))

