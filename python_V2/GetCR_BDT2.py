# -*- coding: utf-8 -*-
#python
import sys
import glob
import math
import re
from ROOT import *
from array import *
#from ROOT import TCanvas, TFile, TLine, TProfile, TNtuple, TH1F, TH2F
#import ROOT
#import array

gROOT.SetBatch(True)
gStyle.SetOptStat(0)
gStyle.SetPalette(1)
gROOT.LoadMacro("../style/AtlasStyle.C")
gROOT.LoadMacro("../style/AtlasUtils.C")
SetAtlasStyle()

y_axis_label="Event fraction"

c_blue   = TColor.GetColor("#3366ff")
c_red    = TColor.GetColor("#ee3311")
c_orange = TColor.GetColor("#ff9900")

def NormalizeHisto(histo):
     n_events=histo.Integral(-1,histo.GetNbinsX()+1)
     #n_events=histo.Integral()
     if n_events == 0:
         return
     print n_events, histo.Integral(histo.GetNbinsX(),histo.GetNbinsX()+1)
     histo.Scale(1./n_events)
     histo.SetLineWidth(3)
     histo.SetStats(0)
     histo.SetFillStyle(3001)
     histo.SetMarkerColor(histo.GetLineColor())
     histo.SetMarkerSize(0.0)
     histo.GetXaxis().SetTitleOffset(1.2)
     histo.GetYaxis().SetTitleOffset(1.52)
     histo.GetXaxis().SetLabelSize(0.05)
     histo.GetYaxis().SetLabelSize(0.05)
     histo.GetXaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetNdivisions(504)
     histo.GetXaxis().SetNdivisions(504)

c1 = TCanvas("CRPlot","",500,500)

#for HistoName in ["nBTags"]:
#for HistoName in ["nBTags","nJets","HT","HT_bjets","DeltaPhi_HW","mVH","mH","pTWplus","pTH","maxMVAResponse", "mass_resolution"]: # for resolved, histoname set
for HistoName in ["mVH"]: #for boosted, histoname set
## for bTagStrategy in ["Incl","FourPlusTags","ThreeTags","TwoTags"]:
 for Region in ["Resolved_SR"]:
  for btagStrategy in ["Inclusive"]:
     
      ReBin = False
      YAxisScale = 1.4
      
      if "mVH" in HistoName:
          Xaxis_label="Mass of Charged Higgs [GeV]"
         
      #Xaxis_label=""

      #file1       = TFile.Open("../PlotFiles/ForOptimisation/sig_Hplus_Wh_m400-0_70p.root","READ")
      file_sig400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR_NewBDT/sig_Hplus_Wh_m400-0_70p.root","READ")
      dir_sig400_04       = file_sig400.GetDirectory("Nominal").GetDirectory(HistoName+"_4")
      dir_sig400       = file_sig400.GetDirectory("Nominal").GetDirectory(HistoName)
      h_sig_Hplus_m400_04 = dir_sig400_04.Get("sig_Hplus_Wh_m400-0_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_sig_Hplus_m400 = dir_sig400.Get("sig_Hplus_Wh_m400-0_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_sig_Hplus_m400_04.SetLineColor(kRed)
      h_sig_Hplus_m400_04.SetLineStyle(7)
      if ReBin == True:
          h_sig_Hplus_m400_04.Rebin(2)

      #file6	  = TFile.Open("../PlotFiles/ForOptimisation/sig_Hplus_Wh_m800-0_70p.root","READ")
      file_sig800	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR_NewBDT/sig_Hplus_Wh_m800-0_70p.root","READ")
      dir_sig800_04        = file_sig800.GetDirectory("Nominal").GetDirectory(HistoName+"_4")
      dir_sig800        = file_sig800.GetDirectory("Nominal").GetDirectory(HistoName)
      h_sig_Hplus_m800_04 = dir_sig800_04.Get("sig_Hplus_Wh_m800-0_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_sig_Hplus_m800 = dir_sig800.Get("sig_Hplus_Wh_m800-0_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_sig_Hplus_m800_04.SetLineColor(kBlack)
      h_sig_Hplus_m800_04.SetLineStyle(7)
      if ReBin == True:
          h_sig_Hplus_m800_04.Rebin(2)

      #file7	  = TFile.Open("../PlotFiles/ForOptimisation/sig_Hplus_Wh_m1600-0_70p.root","READ")
      file_sig1600	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR_NewBDT/sig_Hplus_Wh_m1600-0_70p.root","READ")
      dir_sig1600_04        = file_sig1600.GetDirectory("Nominal").GetDirectory(HistoName+"_4")
      dir_sig1600        = file_sig1600.GetDirectory("Nominal").GetDirectory(HistoName)
      h_sig_Hplus_m1600_04 = dir_sig1600_04.Get("sig_Hplus_Wh_m1600-0_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_sig_Hplus_m1600 = dir_sig1600.Get("sig_Hplus_Wh_m1600-0_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_sig_Hplus_m1600_04.SetLineColor(kBlue)
      h_sig_Hplus_m1600_04.SetLineStyle(7)
      if ReBin == True:
          h_sig_Hplus_m1600_04.Rebin(2)
        
     # #file2   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/ttbar_70p.root","READ")

      file_nom   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR_NewBDT/Comb_70p.root","READ")
      dir_nom    = file_nom.GetDirectory("Nominal").GetDirectory(HistoName)
      h_tt_nom = dir_nom.Get("ttbar_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_t_nom = dir_nom.Get("singleTop_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Zjets_nom = dir_nom.Get("Zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Wjets_nom = dir_nom.Get("Wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_diboson_nom = dir_nom.Get("Zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_nom= h_tt_nom+h_t_nom+h_Zjets_nom+h_Wjets_nom+h_diboson_nom
      h_nom.SetLineWidth(7)
      h_nom.SetLineColor(kGreen)
      h_nom.SetLineStyle(3)
      
      if ReBin == True:
          h_nom.Rebin(2)

 
      file_04   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR_NewBDT/Comb_70p.root","READ")
      dir_04    = file_04.GetDirectory("Nominal").GetDirectory(HistoName+"_04")
      h_tt_04 = dir_04.Get("ttbar_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_t_04 = dir_04.Get("singleTop_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Zjets_04 = dir_04.Get("Zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Wjets_04 = dir_04.Get("Wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_diboson_04 = dir_04.Get("diboson_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_04= h_tt_04+h_t_04+h_Zjets_04+h_Wjets_04+h_diboson_04
      h_04.SetLineWidth(7)
      h_04.SetLineColor(kGreen)
      h_04.SetLineStyle(3)
      
      if ReBin == True:
          h_04.Rebin(2)

      file_34   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR_NewBDT/Comb_70p.root","READ")
      dir_34    = file_34.GetDirectory("Nominal").GetDirectory(HistoName+"_34")
      h_tt_34 = dir_34.Get("ttbar_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_t_34 = dir_34.Get("singleTop_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Zjets_34 = dir_34.Get("Zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Wjets_34 = dir_34.Get("Wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_diboson_34 = dir_34.Get("diboson_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_34= h_tt_34+h_t_34+h_Zjets_34+h_Wjets_34+h_diboson_34
      h_34.SetLineWidth(7)
      h_34.SetLineColor(kCyan)
      h_34.SetLineStyle(3)
      
      if ReBin == True:
          h_34.Rebin(2) 
      
      file_44   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR_NewBDT/Comb_70p.root","READ")
      dir_44    = file_44.GetDirectory("Nominal").GetDirectory(HistoName+"_44")
      h_tt_44 = dir_44.Get("ttbar_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_t_44 = dir_44.Get("singleTop_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Zjets_44 = dir_44.Get("Zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Wjets_44 = dir_44.Get("Wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_diboson_44 = dir_44.Get("diboson_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_44= h_tt_44+h_t_44+h_Zjets_44+h_Wjets_44+h_diboson_44
      h_44.SetLineWidth(7)
      h_44.SetLineColor(kMagenta)
      h_44.SetLineStyle(3)
      
      if ReBin == True:
          h_44.Rebin(2)


      file_54   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR_NewBDT/Comb_70p.root","READ")
      dir_54    = file_54.GetDirectory("Nominal").GetDirectory(HistoName+"_54")
      h_tt_54 = dir_54.Get("ttbar_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_t_54 = dir_54.Get("singleTop_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Zjets_54 = dir_54.Get("Zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Wjets_54 = dir_54.Get("Wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_diboson_54 = dir_54.Get("diboson_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_54= h_tt_54+h_t_54+h_Zjets_54+h_Wjets_54+h_diboson_54
      h_54.SetLineWidth(7)
      h_54.SetLineColor(kTeal)
      h_54.SetLineStyle(3)
      
      if ReBin == True:
          h_54.Rebin(2)     

      file_03   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR_NewBDT/Comb_70p.root","READ")
      dir_03    = file_03.GetDirectory("Nominal").GetDirectory(HistoName+"_03")
      h_tt_03 = dir_03.Get("ttbar_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_t_03 = dir_03.Get("singleTop_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Zjets_03 = dir_03.Get("Zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Wjets_03 = dir_03.Get("Wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_diboson_03 = dir_03.Get("diboson_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_03= h_tt_03+h_t_03+h_Zjets_03+h_Wjets_03+h_diboson_03
      h_03.SetLineWidth(7)
      h_03.SetLineColor(kGreen+4)
      h_03.SetLineStyle(3)
      
      if ReBin == True:
          h_03.Rebin(2) 

      file_33   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR_NewBDT/Comb_70p.root","READ")
      dir_33    = file_33.GetDirectory("Nominal").GetDirectory(HistoName+"_33")
      h_tt_33 = dir_33.Get("ttbar_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_t_33 = dir_33.Get("singleTop_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Zjets_33 = dir_33.Get("Zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Wjets_33 = dir_33.Get("Wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_diboson_33 = dir_33.Get("diboson_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_33= h_tt_33+h_t_33+h_Zjets_33+h_Wjets_33+h_diboson_33
      h_33.SetLineWidth(7)
      h_33.SetLineColor(kSpring)
      h_33.SetLineStyle(3)
      
      if ReBin == True:
          h_33.Rebin(2) 

      file_53   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR_NewBDT/Comb_70p.root","READ")
      dir_53    = file_53.GetDirectory("Nominal").GetDirectory(HistoName+"_53")
      h_tt_53 = dir_53.Get("ttbar_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_t_53 = dir_53.Get("singleTop_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Zjets_53 = dir_53.Get("Zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Wjets_53 = dir_53.Get("Wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_diboson_53 = dir_53.Get("diboson_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_53= h_tt_53+h_t_53+h_Zjets_53+h_Wjets_53+h_diboson_53
      h_53.SetLineWidth(7)
      h_53.SetLineColor(kYellow)
      h_53.SetLineStyle(3)
      
      if ReBin == True:
          h_53.Rebin(2)

      file_12   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR_NewBDT/Comb_70p.root","READ")
      dir_12    = file_12.GetDirectory("Nominal").GetDirectory(HistoName+"_12")
      h_tt_12 = dir_12.Get("ttbar_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_t_12 = dir_12.Get("singleTop_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Zjets_12 = dir_12.Get("Zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Wjets_12 = dir_12.Get("Wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_diboson_12 = dir_12.Get("diboson_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_12= h_tt_12+h_t_12+h_Zjets_12+h_Wjets_12+h_diboson_12
      h_12.SetLineWidth(7)
      h_12.SetLineColor(kOrange)
      h_12.SetLineStyle(3)
      
      if ReBin == True:
          h_12.Rebin(2) 

      file_32   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR_NewBDT/Comb_70p.root","READ")
      dir_32    = file_32.GetDirectory("Nominal").GetDirectory(HistoName+"_32")
      h_tt_32 = dir_32.Get("ttbar_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_t_32 = dir_32.Get("singleTop_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Zjets_32 = dir_32.Get("Zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Wjets_32 = dir_32.Get("Wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_diboson_32 = dir_32.Get("diboson_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_32= h_tt_32+h_t_32+h_Zjets_32+h_Wjets_32+h_diboson_32
      h_32.SetLineWidth(7)
      h_32.SetLineColor(kPink)
      h_32.SetLineStyle(3)
      
      if ReBin == True:
          h_32.Rebin(2) 

      file_52   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR_NewBDT/Comb_70p.root","READ")
      dir_52    = file_52.GetDirectory("Nominal").GetDirectory(HistoName+"_52")
      h_tt_52 = dir_52.Get("ttbar_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_t_52 = dir_52.Get("singleTop_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Zjets_52 = dir_52.Get("Zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Wjets_52 = dir_52.Get("Wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_diboson_52 = dir_52.Get("diboson_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_52= h_tt_52+h_t_52+h_Zjets_52+h_Wjets_52+h_diboson_52
      h_52.SetLineWidth(7)
      h_52.SetLineColor(kMagenta)
      h_52.SetLineStyle(3)
      
      if ReBin == True:
          h_52.Rebin(2)

      file_62   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR_NewBDT/Comb_70p.root","READ")
      dir_62    = file_62.GetDirectory("Nominal").GetDirectory(HistoName+"_62")
      h_tt_62 = dir_62.Get("ttbar_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_t_62 = dir_62.Get("singleTop_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Zjets_62 = dir_62.Get("Zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Wjets_62 = dir_62.Get("Wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_diboson_62 = dir_62.Get("diboson_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_62= h_tt_62+h_t_62+h_Zjets_62+h_Wjets_62+h_diboson_62
      h_62.SetLineWidth(7)
      h_62.SetLineColor(kTeal+3)
      h_62.SetLineStyle(3)
      
      if ReBin == True:
          h_62.Rebin(2)

      file_51   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR_NewBDT/Comb_70p.root","READ")
      dir_51    = file_51.GetDirectory("Nominal").GetDirectory(HistoName+"_51")
      h_tt_51 = dir_51.Get("ttbar_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_t_51 = dir_51.Get("singleTop_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Zjets_51 = dir_51.Get("Zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Wjets_51 = dir_51.Get("Wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_diboson_51 = dir_51.Get("diboson_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_51= h_tt_51+h_t_51+h_Zjets_51+h_Wjets_51+h_diboson_51
      h_51.SetLineWidth(7)
      h_51.SetLineColor(kMagenta)
      h_51.SetLineStyle(3)
      
      if ReBin == True:
          h_51.Rebin(2)

      file_63   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR_NewBDT/Comb_70p.root","READ")
      dir_63    = file_63.GetDirectory("Nominal").GetDirectory(HistoName+"_63")
      h_tt_63 = dir_63.Get("ttbar_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_t_63 = dir_63.Get("singleTop_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Zjets_63 = dir_63.Get("Zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Wjets_63 = dir_63.Get("Wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_diboson_63 = dir_63.Get("diboson_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_63= h_tt_63+h_t_63+h_Zjets_63+h_Wjets_63+h_diboson_63
      h_63.SetLineWidth(7)
      h_63.SetLineColor(kTeal+3)
      h_63.SetLineStyle(3)
      
      if ReBin == True:
          h_63.Rebin(2)

      file_61   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR_NewBDT/Comb_70p.root","READ")
      dir_61    = file_61.GetDirectory("Nominal").GetDirectory(HistoName+"_61")
      h_tt_61 = dir_61.Get("ttbar_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_t_61 = dir_61.Get("singleTop_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Zjets_61 = dir_61.Get("Zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Wjets_61 = dir_61.Get("Wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_diboson_61 = dir_61.Get("diboson_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_61= h_tt_61+h_t_61+h_Zjets_61+h_Wjets_61+h_diboson_61
      h_61.SetLineWidth(7)
      h_61.SetLineColor(kTeal+3)
      h_61.SetLineStyle(3)
      
      if ReBin == True:
          h_61.Rebin(2)


      file_10   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR_NewBDT/Comb_70p.root","READ")
      dir_10    = file_10.GetDirectory("Nominal").GetDirectory(HistoName+"_10")
      h_tt_10 = dir_10.Get("ttbar_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_t_10 = dir_10.Get("singleTop_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Zjets_10 = dir_10.Get("Zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Wjets_10 = dir_10.Get("Wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_diboson_10 = dir_10.Get("diboson_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_10= h_tt_10+h_t_10+h_Zjets_10+h_Wjets_10+h_diboson_10 
      h_10.SetLineWidth(7)
      h_10.SetLineColor(kBlue+4)
      h_10.SetLineStyle(3)
      
      if ReBin == True:
          h_10.Rebin(2)    

      file_50   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR_NewBDT/Comb_70p.root","READ")
      dir_50    = file_50.GetDirectory("Nominal").GetDirectory(HistoName+"_50")
      h_tt_50 = dir_50.Get("ttbar_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_t_50 = dir_50.Get("singleTop_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Zjets_50 = dir_50.Get("Zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Wjets_50 = dir_50.Get("Wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_diboson_50 = dir_50.Get("diboson_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_50= h_tt_50+h_t_50+h_Zjets_50+h_Wjets_50+h_diboson_50 
      h_50.SetLineWidth(7)
      h_50.SetLineColor(kBlue+4)
      h_50.SetLineStyle(3)
      
      if ReBin == True:
          h_50.Rebin(2)


      file_5   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR_NewBDT/Comb_70p.root","READ")
      dir_5    = file_5.GetDirectory("Nominal").GetDirectory(HistoName+"_5")
      h_tt_5 = dir_5.Get("ttbar_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_t_5 = dir_5.Get("singleTop_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Zjets_5 = dir_5.Get("Zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Wjets_5 = dir_5.Get("Wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_diboson_5 = dir_5.Get("diboson_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_5= h_tt_5+h_t_5+h_Zjets_5+h_Wjets_5+h_diboson_5
      h_5.SetLineWidth(7)
      h_5.SetLineColor(kRed)
      h_5.SetLineStyle(1)
      
      if ReBin == True:
          h_5.Rebin(2)

      file_4   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR_NewBDT/Comb_70p.root","READ")
      dir_4    = file_4.GetDirectory("Nominal").GetDirectory(HistoName+"_4")
      h_tt_4 = dir_4.Get("ttbar_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_t_4 = dir_4.Get("singleTop_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Zjets_4 = dir_4.Get("Zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_Wjets_4 = dir_4.Get("Wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_diboson_4 = dir_4.Get("diboson_"+HistoName+"_"+Region+"_"+btagStrategy+"_Nominal")
      h_4= h_tt_4+h_t_4+h_Zjets_4+h_Wjets_4+h_diboson_4
      h_4.SetLineWidth(7)
      h_4.SetLineColor(kRed)
      h_4.SetLineStyle(1)
      
      if ReBin == True:
          h_4.Rebin(2)    

     # h_other_background = h_singleTop_background + h_diboson_background + h_W_background + h_Z_background     
      
      print("h_04 contribution is", h_04.Integral()/h_nom.Integral())
      print("h_34 contribution is", h_34.Integral()/h_nom.Integral())
      print("h_44 contribution is", h_44.Integral()/h_nom.Integral())
      print("h_54 contribution is", h_54.Integral()/h_nom.Integral())
      print("h_03 contribution is", h_03.Integral()/h_nom.Integral())
      print("h_33 contribution is", h_33.Integral()/h_nom.Integral())
      print("h_53 contribution is", h_53.Integral()/h_nom.Integral())
      print("h_12 contribution is", h_12.Integral()/h_nom.Integral())
      print("h_32 contribution is", h_32.Integral()/h_nom.Integral())
      print("h_52 contribution is", h_52.Integral()/h_nom.Integral())
      print("h_62 contribution is", h_62.Integral()/h_nom.Integral())
      print("h_51 contribution is", h_51.Integral()/h_nom.Integral())
      print("h_63 contribution is", h_63.Integral()/h_nom.Integral())
      print("h_61 contribution is", h_61.Integral()/h_nom.Integral())
      print("h_10 contribution is", h_10.Integral()/h_nom.Integral())
      print("h_50 contribution is", h_50.Integral()/h_nom.Integral())
      print("h_4 contribution is", h_4.Integral()/h_nom.Integral())


      NormalizeHisto(h_04)
      NormalizeHisto(h_34)
      NormalizeHisto(h_44)
      NormalizeHisto(h_54)
      NormalizeHisto(h_03)
      NormalizeHisto(h_33)
      NormalizeHisto(h_53)
      NormalizeHisto(h_12)
      NormalizeHisto(h_32)
      NormalizeHisto(h_52)
      NormalizeHisto(h_62)
      NormalizeHisto(h_51)
      NormalizeHisto(h_63)
      NormalizeHisto(h_61)
      NormalizeHisto(h_10)
      NormalizeHisto(h_50)
      NormalizeHisto(h_4)
      
      nbins=20
      ymax=0
      
      if ymax<h_03.GetMaximum():
          ymax=h_03.GetMaximum()
      
      if ymax<h_32.GetMaximum():
          ymax=h_32.GetMaximum()
      
      if ymax<h_50.GetMaximum():
          ymax=h_50.GetMaximum()
      
      if ymax<h_54.GetMaximum():
          ymax=h_54.GetMaximum()
      
      if ymax<h_44.GetMaximum():
          ymax=h_44.GetMaximum()
      
      if ymax<h_4.GetMaximum():
          ymax=h_4.GetMaximum()

      

      h_03.Draw("HIST")
      h_32.Draw("HISTSAME")
      #h_61.Draw("HISTSAME")
      #h_04.Draw("HISTSAME")
      #h_10.Draw("HISTSAME")
      h_50.Draw("HISTSAME")
      h_54.Draw("HISTSAME")
      h_44.Draw("HISTSAME")
      h_4.Draw("HISTSAME")

      if HistoName in "maxMVAResponse":
         leg = TLegend(0.2,0.65,0.725,0.855)
      else:
         leg = TLegend(0.45,0.55,0.925,0.755)
      ATLAS_LABEL(0.20,0.885," Simulation",1,0.19);
      leg.SetShadowColor(kWhite)
      leg.SetFillColor(kWhite)
      leg.SetLineColor(kWhite)
      
      leg.AddEntry(h_03,    " MVA Score =(0.0, 0.3)","L")
      leg.AddEntry(h_32,    " MVA Score =(-0.3, 0.2)","L")
      leg.AddEntry(h_50,    " MVA Score =(-0.5, 0.0)","L")
      leg.AddEntry(h_54,    " MVA Score =(-0.5, 0.4)","L")
      leg.AddEntry(h_44,    " MVA Score =(-0.4, 0.4)","L")
      leg.AddEntry(h_4,    " MVA Score =(0.4, 1.0)","L")
      
      leg.SetTextSize(0.0250)
      leg.Draw()
      h_03.GetXaxis().SetTitle(Xaxis_label)
      h_03.GetYaxis().SetRangeUser(0.001,round(ymax*1.25,3)+0.001)
      h_03.GetYaxis().SetTitle("Normalised Entries")
      myText(0.20,0.835,1,"work-in-progress")
      c1.RedrawAxis()
      c1.Update()
      c1.RedrawAxis()
      c1.SaveAs("../Plots/mWH_CR_4_NewBDT_imp2sig_DPG.pdf")

base= "/cephfs/user/s6subans/ChargedHiggs_Mock/output_CR_NewBDT/"
for FileName in ["sig_Hplus_Wh_m400-0_", "sig_Hplus_Wh_m800-0_", "sig_Hplus_Wh_m1600-0_"]: #for boosted, histoname set
## for bTagStrategy in ["Incl","FourPlusTags","ThreeTags","TwoTags"]:
   for CR in ["_04", "_34", "_44", "_54", "_03", "_33", "_53", "_12", "_32", "_52", "_62", "_51", "_63", "_61", "_10","_50","_4"]:


                 file       = TFile.Open(base+FileName+"70p.root","READ")
                 dir       = file.GetDirectory("Nominal").GetDirectory("mVH"+CR)
                 dir_nom = file.GetDirectory("Nominal").GetDirectory("mVH")
                 h = dir.Get(FileName+"mVH_Resolved_SR_Inclusive_Nominal")
                 h_nom =  dir_nom.Get(FileName+"mVH_Resolved_SR_Inclusive_Nominal")
                 #h.Divide(h_nom)
                 #NormalizeHisto(h)
                 print(FileName+CR+"contribution is", h.Integral()/h_nom.Integral())
