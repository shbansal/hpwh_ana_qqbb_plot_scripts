# -*- coding: utf-8 -*-
#python
import sys
import glob
import math
import re
from ROOT import *
from array import *
#from ROOT import TCanvas, TFile, TLine, TProfile, TNtuple, TH1F, TH2F
#import ROOT
#import array

gROOT.SetBatch(True)
gStyle.SetOptStat(0)
gStyle.SetPalette(1)
gROOT.LoadMacro("../style/AtlasStyle.C")
gROOT.LoadMacro("../style/AtlasUtils.C")
SetAtlasStyle()

y_axis_label="Event fraction"

c_blue   = TColor.GetColor("#3366ff")
c_red    = TColor.GetColor("#ee3311")
c_orange = TColor.GetColor("#ff9900")

def NormalizeHisto(histo):
     n_events=histo.Integral(-1,histo.GetNbinsX()+1)
     #n_events=histo.Integral()
     if n_events == 0:
         return
     print n_events, histo.Integral(histo.GetNbinsX(),histo.GetNbinsX()+1)
     histo.Scale(1./n_events)
     histo.SetLineWidth(3)
     histo.SetStats(0)
     histo.SetFillStyle(3001)
     histo.SetMarkerColor(histo.GetLineColor())
     histo.SetMarkerSize(0.0)
     histo.GetXaxis().SetTitleOffset(1.2)
     histo.GetYaxis().SetTitleOffset(1.52)
     histo.GetXaxis().SetLabelSize(0.05)
     histo.GetYaxis().SetLabelSize(0.05)
     histo.GetXaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetNdivisions(504)
     histo.GetXaxis().SetNdivisions(504)

c1 = TCanvas("CRPlot","",500,500)

#for HistoName in ["nBTags"]:
#for HistoName in ["nBTags","nJets","HT","HT_bjets","DeltaPhi_HW","mVH","mH","pTWplus","pTH","maxMVAResponse", "mass_resolution"]: # for resolved, histoname set
for HistoName in ["mVH"]: #for boosted, histoname set
## for bTagStrategy in ["Incl","FourPlusTags","ThreeTags","TwoTags"]:
 for Region in ["Resolved_SR"]:
  for btagStrategy in ["FourPlusTags"]:
     
      ReBin = False
      YAxisScale = 1.4
      
      if "mVH" in HistoName:
          Xaxis_label="Mass of Charged Higgs [GeV]"
         
      #Xaxis_label=""
      file_sig250       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_potforEB_SRCR/hp250_AFII.root_77p_225.root","READ")
      dir_sig250_9       = file_sig250.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_9")
      dir_sig250       = file_sig250.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m250_9 = dir_sig250_9.Get("hp250_AFII_"+HistoName+"_9_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m250 = dir_sig250.Get("hp250_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m250_9.SetLineColor(kRed+3)
      h_sig_Hplus_m250_9.SetLineStyle(7)
      if ReBin == True:
          h_sig_Hplus_m250_9.Rebin(2)


      file_sig300       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_potforEB_SRCR/hp300_AFII.root_77p_225.root","READ")
      dir_sig300_9       = file_sig300.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_9")
      dir_sig300       = file_sig300.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m300_9 = dir_sig300_9.Get("hp300_AFII_"+HistoName+"_9_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m300 = dir_sig300.Get("hp300_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m300_9.SetLineColor(kRed-8)
      h_sig_Hplus_m300_9.SetLineStyle(7)
      if ReBin == True:
          h_sig_Hplus_m300_9.Rebin(2)

      file_sig350       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_potforEB_SRCR/hp350_AFII.root_77p_225.root","READ")
      dir_sig350_9       = file_sig350.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_9")
      dir_sig350       = file_sig350.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m350_9 = dir_sig350_9.Get("hp350_AFII_"+HistoName+"_9_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m350 = dir_sig350.Get("hp350_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m350_9.SetLineColor(kRed-1)
      h_sig_Hplus_m350_9.SetLineStyle(7)
      if ReBin == True:
          h_sig_Hplus_m350_9.Rebin(2)    


      #file1       = TFile.Open("../PlotFiles/ForOptimisation/sig_Hplus_Wh_m400-0_70p.root","READ")
      file_sig400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_potforEB_SRCR/hp400_AFII.root_77p_225.root","READ")
      dir_sig400_9       = file_sig400.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_9")
      dir_sig400       = file_sig400.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m400_9 = dir_sig400_9.Get("hp400_AFII_"+HistoName+"_9_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m400 = dir_sig400.Get("hp400_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m400_9.SetLineColor(kRed)
      h_sig_Hplus_m400_9.SetLineStyle(7)
      if ReBin == True:
          h_sig_Hplus_m400_9.Rebin(2)

      file_sig500	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_potforEB_SRCR/hp500_AFII.root_77p_225.root","READ")
      dir_sig500_9        = file_sig500.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_9")
      dir_sig500        = file_sig500.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m500_9 = dir_sig500_9.Get("hp500_AFII_"+HistoName+"_9_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m500 = dir_sig500.Get("hp500_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m500_9.SetLineColor(kPink+6)
      h_sig_Hplus_m500_9.SetLineStyle(7)
      if ReBin == True:
          h_sig_Hplus_m500_9.Rebin(2)    

      file_sig600	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_potforEB_SRCR/hp600_AFII.root_77p_225.root","READ")
      dir_sig600_9        = file_sig600.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_9")
      dir_sig600        = file_sig600.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m600_9 = dir_sig600_9.Get("hp600_AFII_"+HistoName+"_9_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m600 = dir_sig600.Get("hp600_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m600_9.SetLineColor(kPink+1)
      h_sig_Hplus_m600_9.SetLineStyle(7)
      if ReBin == True:
          h_sig_Hplus_m600_9.Rebin(2)    

      file_sig700	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_potforEB_SRCR/hp700_AFII.root_77p_225.root","READ")
      dir_sig700_9        = file_sig700.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_9")
      dir_sig700        = file_sig700.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m700_9 = dir_sig700_9.Get("hp700_AFII_"+HistoName+"_9_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m700 = dir_sig700.Get("hp700_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m700_9.SetLineColor(kViolet+3)
      h_sig_Hplus_m700_9.SetLineStyle(7)
      if ReBin == True:
          h_sig_Hplus_m700_9.Rebin(2)          

      #file6	  = TFile.Open("../PlotFiles/ForOptimisation/sig_Hplus_Wh_m800-0_70p.root","READ")
      file_sig800	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_potforEB_SRCR/hp800_AFII.root_77p_225.root","READ")
      dir_sig800_9        = file_sig800.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_9")
      dir_sig800        = file_sig800.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m800_9 = dir_sig800_9.Get("hp800_AFII_"+HistoName+"_9_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m800 = dir_sig800.Get("hp800_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m800_9.SetLineColor(kBlue-6)
      h_sig_Hplus_m800_9.SetLineStyle(7)
      if ReBin == True:
          h_sig_Hplus_m800_9.Rebin(2)

      file_sig900	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_potforEB_SRCR/hp900_AFII.root_77p_225.root","READ")
      dir_sig900_9        = file_sig900.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_9")
      dir_sig900        = file_sig900.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m900_9 = dir_sig900_9.Get("hp900_AFII_"+HistoName+"_9_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m900 = dir_sig900.Get("hp900_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m900_9.SetLineColor(kAzure-5)
      h_sig_Hplus_m900_9.SetLineStyle(7)
      if ReBin == True:
          h_sig_Hplus_m900_9.Rebin(2)    

      file_sig1000	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_potforEB_SRCR/hp1000_AFII.root_77p_225.root","READ")
      dir_sig1000_9        = file_sig1000.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_9")
      dir_sig1000        = file_sig1000.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m1000_9 = dir_sig1000_9.Get("hp1000_AFII_"+HistoName+"_9_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m1000 = dir_sig1000.Get("hp1000_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m1000_9.SetLineColor(kCyan-9)
      h_sig_Hplus_m1000_9.SetLineStyle(7)
      if ReBin == True:
          h_sig_Hplus_m1000_9.Rebin(2)

      file_sig1200	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_potforEB_SRCR/hp1200_AFII.root_77p_225.root","READ")
      dir_sig1200_9        = file_sig1200.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_9")
      dir_sig1200        = file_sig1200.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m1200_9 = dir_sig1200_9.Get("hp1200_AFII_"+HistoName+"_9_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m1200 = dir_sig1200.Get("hp1200_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m1200_9.SetLineColor(kTeal-6)
      h_sig_Hplus_m1200_9.SetLineStyle(7)
      if ReBin == True:
          h_sig_Hplus_m1200_9.Rebin(2)  

      file_sig1400	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_potforEB_SRCR/hp1400_AFII.root_77p_225.root","READ")
      dir_sig1400_9        = file_sig1400.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_9")
      dir_sig1400        = file_sig1400.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m1400_9 = dir_sig1400_9.Get("hp1400_AFII_"+HistoName+"_9_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m1400 = dir_sig1400.Get("hp1400_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m1400_9.SetLineColor(kGreen+3)
      h_sig_Hplus_m1400_9.SetLineStyle(7)
      if ReBin == True:
          h_sig_Hplus_m1400_9.Rebin(2)        

      #file7	  = TFile.Open("../PlotFiles/ForOptimisation/sig_Hplus_Wh_m1600-0_70p.root","READ")
      file_sig1600	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_potforEB_SRCR/hp1600_AFII.root_77p_225.root","READ")
      dir_sig1600_9        = file_sig1600.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_9")
      dir_sig1600        = file_sig1600.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m1600_9 = dir_sig1600_9.Get("hp1600_AFII_"+HistoName+"_9_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m1600 = dir_sig1600.Get("hp1600_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m1600_9.SetLineColor(kSpring+4)
      h_sig_Hplus_m1600_9.SetLineStyle(7)
      if ReBin == True:
          h_sig_Hplus_m1600_9.Rebin(2)

      file_sig1800	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_potforEB_SRCR/hp1800_AFII.root_77p_225.root","READ")
      dir_sig1800_9        = file_sig1800.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_9")
      dir_sig1800        = file_sig1800.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m1800_9 = dir_sig1800_9.Get("hp1800_AFII_"+HistoName+"_9_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m1800 = dir_sig1800.Get("hp1800_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m1800_9.SetLineColor(kYellow-3)
      h_sig_Hplus_m1800_9.SetLineStyle(7)
      if ReBin == True:
          h_sig_Hplus_m1800_9.Rebin(2) 
          
      file_sig2000	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_potforEB_SRCR/hp2000_AFII.root_77p_225.root","READ")
      dir_sig2000_9        = file_sig2000.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_9")
      dir_sig2000        = file_sig2000.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m2000_9 = dir_sig2000_9.Get("hp2000_AFII_"+HistoName+"_9_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m2000 = dir_sig2000.Get("hp2000_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m2000_9.SetLineColor(kOrange+7)
      h_sig_Hplus_m2000_9.SetLineStyle(7)
      if ReBin == True:
          h_sig_Hplus_m2000_9.Rebin(2) 

      file_sig2500	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_potforEB_SRCR/hp2500_AFII.root_77p_225.root","READ")
      dir_sig2500_9        = file_sig2500.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_9")
      dir_sig2500        = file_sig2500.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m2500_9 = dir_sig2500_9.Get("hp2500_AFII_"+HistoName+"_9_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m2500 = dir_sig2500.Get("hp2500_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m2500_9.SetLineColor(kGray)
      h_sig_Hplus_m2500_9.SetLineStyle(7)
      if ReBin == True:
          h_sig_Hplus_m2500_9.Rebin(2) 

      file_sig3000	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_potforEB_SRCR/hp3000_AFII.root_77p_225.root","READ")
      dir_sig3000_9        = file_sig3000.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_9")
      dir_sig3000        = file_sig3000.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m3000_9 = dir_sig3000_9.Get("hp3000_AFII_"+HistoName+"_9_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m3000 = dir_sig3000.Get("hp3000_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m3000_9.SetLineColor(kBlack)
      h_sig_Hplus_m3000_9.SetLineStyle(7)
      if ReBin == True:
          h_sig_Hplus_m3000_9.Rebin(2)           
       
       # #file2   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/ttbar_70p.root","READ")

      #file_nom   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_potforEB_SRCR/Comb_77p.root","READ")
      #dir_nom    = file_nom.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      #dir_nom_1b = file_nom.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1b")
      #dir_nom_1c = file_nom.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1c")
      #dir_nom_1l = file_nom.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1l")
      #h_tt_nom_1b = dir_nom_1b.Get("tt_PP8_1B_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_tt_nom_1c = dir_nom_1c.Get("tt_PP8_1C_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_tt_nom_1l = dir_nom_1l.Get("tt_PP8_1L_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_ttf_nom_1b = dir_nom_1b.Get("tt_PP8filtered_1B_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_ttf_nom_1c = dir_nom_1c.Get("tt_PP8filtered_1C_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_ttf_nom_1l = dir_nom_1l.Get("tt_PP8filtered_1L_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_st_nom = dir_nom.Get("st_tc_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_sc_nom = dir_nom.Get("st_sc_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_Zjets_nom = dir_nom.Get("zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_Wjets_nom = dir_nom.Get("wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_diboson_nom = dir_nom.Get("db_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_ttll_nom = dir_nom.Get("ttll_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_ttW_nom = dir_nom.Get("ttW_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_tH_nom = dir_nom.Get("tH_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_tWZ_nom = dir_nom.Get("tWZ_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_Wt_nom = dir_nom.Get("Wt_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_ttH_nom = dir_nom.Get("ttH_PP8_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_tZ_nom = dir_nom.Get("tZ_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      #h_nom = h_tt_nom_1b+h_tt_nom_1c+h_tt_nom_1l+h_ttf_nom_1b+h_ttf_nom_1c+h_ttf_nom_1l+h_st_nom+h_sc_nom+h_Zjets_nom+h_Wjets_nom+h_diboson_nom+h_ttll_nom+h_ttW_nom+h_tH_nom+h_tWZ_nom+h_Wt_nom+h_ttH_nom+h_tZ_nom
      #h_nom.SetLineWidth(7)
      #h_nom.SetLineColor(kGreen)
      #h_nom.SetLineStyle(3)
      
      #if ReBin == True:
          #h_nom.Rebin(2)

 
      file_09   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_potforEB_SRCR/Comb_77p.root","READ")
      dir_09    = file_09.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_09")
      dir_09_1b = file_09.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_09_1b")
      dir_09_1c = file_09.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_09_1c")
      dir_09_1l = file_09.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_09_1l")
      h_tt_09_1b = dir_09_1b.Get("tt_PP8_1B_"+HistoName+"_09_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tt_09_1c = dir_09_1c.Get("tt_PP8_1C_"+HistoName+"_09_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tt_09_1l = dir_09_1l.Get("tt_PP8_1L_"+HistoName+"_09_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_09_1b = dir_09_1b.Get("tt_PP8filtered_1B_"+HistoName+"_09_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_09_1c = dir_09_1c.Get("tt_PP8filtered_1C_"+HistoName+"_09_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_09_1l = dir_09_1l.Get("tt_PP8filtered_1L_"+HistoName+"_09_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_st_09 = dir_09.Get("st_tc_"+HistoName+"_09_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sc_09 = dir_09.Get("st_sc_"+HistoName+"_09_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Zjets_09 = dir_09.Get("zjets_"+HistoName+"_09_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Wjets_09 = dir_09.Get("wjets_"+HistoName+"_09_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_diboson_09 = dir_09.Get("db_"+HistoName+"_09_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttll_09 = dir_09.Get("ttll_"+HistoName+"_09_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttW_09 = dir_09.Get("ttW_"+HistoName+"_09_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tH_09 = dir_09.Get("tH_AFII_"+HistoName+"_09_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tWZ_09 = dir_09.Get("tWZ_"+HistoName+"_09_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Wt_09 = dir_09.Get("Wt_"+HistoName+"_09_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttH_09 = dir_09.Get("ttH_PP8_"+HistoName+"_09_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tZ_09 = dir_09.Get("tZ_"+HistoName+"_09_"+Region+"_"+btagStrategy+"_nominal_Loose")

      h_09 = h_tt_09_1b+h_tt_09_1c+h_tt_09_1l+h_ttf_09_1b+h_ttf_09_1c+h_ttf_09_1l+h_st_09+h_sc_09+h_Zjets_09+h_Wjets_09+h_diboson_09+h_ttll_09+h_ttW_09+h_tH_09+h_tWZ_09+h_Wt_09+h_ttH_09+h_tZ_09
      h_09.SetLineWidth(7)
      h_09.SetLineColor(kGreen)
      h_09.SetLineStyle(3)
      
      if ReBin == True:
          h_09.Rebin(2)

      file_29   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_potforEB_SRCR/Comb_77p.root","READ")
      dir_29    = file_29.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_29")
      dir_29_1b = file_29.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_29_1b")
      dir_29_1c = file_29.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_29_1c")
      dir_29_1l = file_29.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_29_1l")
      h_tt_29_1b = dir_29_1b.Get("tt_PP8_1B_"+HistoName+"_29_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tt_29_1c = dir_29_1c.Get("tt_PP8_1C_"+HistoName+"_29_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tt_29_1l = dir_29_1l.Get("tt_PP8_1L_"+HistoName+"_29_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_29_1b = dir_29_1b.Get("tt_PP8filtered_1B_"+HistoName+"_29_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_29_1c = dir_29_1c.Get("tt_PP8filtered_1C_"+HistoName+"_29_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_29_1l = dir_29_1l.Get("tt_PP8filtered_1L_"+HistoName+"_29_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_st_29 = dir_29.Get("st_tc_"+HistoName+"_29_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sc_29 = dir_29.Get("st_sc_"+HistoName+"_29_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Zjets_29 = dir_29.Get("zjets_"+HistoName+"_29_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Wjets_29 = dir_29.Get("wjets_"+HistoName+"_29_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_diboson_29 = dir_29.Get("db_"+HistoName+"_29_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttll_29 = dir_29.Get("ttll_"+HistoName+"_29_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttW_29 = dir_29.Get("ttW_"+HistoName+"_29_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tH_29 = dir_29.Get("tH_AFII_"+HistoName+"_29_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tWZ_29 = dir_29.Get("tWZ_"+HistoName+"_29_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Wt_29 = dir_29.Get("Wt_"+HistoName+"_29_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttH_29 = dir_29.Get("ttH_PP8_"+HistoName+"_29_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tZ_29 = dir_29.Get("tZ_"+HistoName+"_29_"+Region+"_"+btagStrategy+"_nominal_Loose")

      h_29 = h_tt_29_1b+h_tt_29_1c+h_tt_29_1l+h_ttf_29_1b+h_ttf_29_1c+h_ttf_29_1l+h_st_29+h_sc_29+h_Zjets_29+h_Wjets_29+h_diboson_29+h_ttll_29+h_ttW_29+h_tH_29+h_tWZ_29+h_Wt_29+h_ttH_29+h_tZ_29
      h_29.SetLineWidth(7)
      h_29.SetLineColor(kYellow)
      h_29.SetLineStyle(3)
      
      if ReBin == True:
          h_29.Rebin(2)

      file_49   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_potforEB_SRCR/Comb_77p.root","READ")
      dir_49    = file_49.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_49")
      dir_49_1b = file_49.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_49_1b")
      dir_49_1c = file_49.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_49_1c")
      dir_49_1l = file_49.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_49_1l")
      h_tt_49_1b = dir_49_1b.Get("tt_PP8_1B_"+HistoName+"_49_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tt_49_1c = dir_49_1c.Get("tt_PP8_1C_"+HistoName+"_49_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tt_49_1l = dir_49_1l.Get("tt_PP8_1L_"+HistoName+"_49_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_49_1b = dir_49_1b.Get("tt_PP8filtered_1B_"+HistoName+"_49_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_49_1c = dir_49_1c.Get("tt_PP8filtered_1C_"+HistoName+"_49_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_49_1l = dir_49_1l.Get("tt_PP8filtered_1L_"+HistoName+"_49_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_st_49 = dir_49.Get("st_tc_"+HistoName+"_49_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sc_49 = dir_49.Get("st_sc_"+HistoName+"_49_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Zjets_49 = dir_49.Get("zjets_"+HistoName+"_49_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Wjets_49 = dir_49.Get("wjets_"+HistoName+"_49_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_diboson_49 = dir_49.Get("db_"+HistoName+"_49_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttll_49 = dir_49.Get("ttll_"+HistoName+"_49_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttW_49 = dir_49.Get("ttW_"+HistoName+"_49_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tH_49 = dir_49.Get("tH_AFII_"+HistoName+"_49_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tWZ_49 = dir_49.Get("tWZ_"+HistoName+"_49_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Wt_49 = dir_49.Get("Wt_"+HistoName+"_49_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttH_49 = dir_49.Get("ttH_PP8_"+HistoName+"_49_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tZ_49 = dir_49.Get("tZ_"+HistoName+"_49_"+Region+"_"+btagStrategy+"_nominal_Loose")

      h_49 = h_tt_49_1b+h_tt_49_1c+h_tt_49_1l+h_ttf_49_1b+h_ttf_49_1c+h_ttf_49_1l+h_st_49+h_sc_49+h_Zjets_49+h_Wjets_49+h_diboson_49+h_ttll_49+h_ttW_49+h_tH_49+h_tWZ_49+h_Wt_49+h_ttH_49+h_tZ_49
      h_49.SetLineWidth(7)
      h_49.SetLineColor(kOrange)
      h_49.SetLineStyle(3)
      
      if ReBin == True:
          h_49.Rebin(2)     
      
      file_69   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_potforEB_SRCR/Comb_77p.root","READ")
      dir_69    = file_69.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_69")
      dir_69_1b = file_69.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_69_1b")
      dir_69_1c = file_69.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_69_1c")
      dir_69_1l = file_69.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_69_1l")
      h_tt_69_1b = dir_69_1b.Get("tt_PP8_1B_"+HistoName+"_69_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tt_69_1c = dir_69_1c.Get("tt_PP8_1C_"+HistoName+"_69_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tt_69_1l = dir_69_1l.Get("tt_PP8_1L_"+HistoName+"_69_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_69_1b = dir_69_1b.Get("tt_PP8filtered_1B_"+HistoName+"_69_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_69_1c = dir_69_1c.Get("tt_PP8filtered_1C_"+HistoName+"_69_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_69_1l = dir_69_1l.Get("tt_PP8filtered_1L_"+HistoName+"_69_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_st_69 = dir_69.Get("st_tc_"+HistoName+"_69_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sc_69 = dir_69.Get("st_sc_"+HistoName+"_69_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Zjets_69 = dir_69.Get("zjets_"+HistoName+"_69_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Wjets_69 = dir_69.Get("wjets_"+HistoName+"_69_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_diboson_69 = dir_69.Get("db_"+HistoName+"_69_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttll_69 = dir_69.Get("ttll_"+HistoName+"_69_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttW_69 = dir_69.Get("ttW_"+HistoName+"_69_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tH_69 = dir_69.Get("tH_AFII_"+HistoName+"_69_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tWZ_69 = dir_69.Get("tWZ_"+HistoName+"_69_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Wt_69 = dir_69.Get("Wt_"+HistoName+"_69_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttH_69 = dir_69.Get("ttH_PP8_"+HistoName+"_69_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tZ_69 = dir_69.Get("tZ_"+HistoName+"_69_"+Region+"_"+btagStrategy+"_nominal_Loose")

      h_69 = h_tt_69_1b+h_tt_69_1c+h_tt_69_1l+h_ttf_69_1b+h_ttf_69_1c+h_ttf_69_1l+h_st_69+h_sc_69+h_Zjets_69+h_Wjets_69+h_diboson_69+h_ttll_69+h_ttW_69+h_tH_69+h_tWZ_69+h_Wt_69+h_ttH_69+h_tZ_69
      h_69.SetLineWidth(7)
      h_69.SetLineColor(kRed)
      h_69.SetLineStyle(3)
      
      if ReBin == True:
          h_69.Rebin(2)

      file_07   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_potforEB_SRCR/Comb_77p.root","READ")
      dir_07    = file_07.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_07")
      dir_07_1b = file_07.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_07_1b")
      dir_07_1c = file_07.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_07_1c")
      dir_07_1l = file_07.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_07_1l")
      h_tt_07_1b = dir_07_1b.Get("tt_PP8_1B_"+HistoName+"_07_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tt_07_1c = dir_07_1c.Get("tt_PP8_1C_"+HistoName+"_07_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tt_07_1l = dir_07_1l.Get("tt_PP8_1L_"+HistoName+"_07_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_07_1b = dir_07_1b.Get("tt_PP8filtered_1B_"+HistoName+"_07_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_07_1c = dir_07_1c.Get("tt_PP8filtered_1C_"+HistoName+"_07_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_07_1l = dir_07_1l.Get("tt_PP8filtered_1L_"+HistoName+"_07_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_st_07 = dir_07.Get("st_tc_"+HistoName+"_07_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sc_07 = dir_07.Get("st_sc_"+HistoName+"_07_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Zjets_07 = dir_07.Get("zjets_"+HistoName+"_07_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Wjets_07 = dir_07.Get("wjets_"+HistoName+"_07_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_diboson_07 = dir_07.Get("db_"+HistoName+"_07_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttll_07 = dir_07.Get("ttll_"+HistoName+"_07_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttW_07 = dir_07.Get("ttW_"+HistoName+"_07_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tH_07 = dir_07.Get("tH_AFII_"+HistoName+"_07_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tWZ_07 = dir_07.Get("tWZ_"+HistoName+"_07_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Wt_07 = dir_07.Get("Wt_"+HistoName+"_07_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttH_07 = dir_07.Get("ttH_PP8_"+HistoName+"_07_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tZ_07 = dir_07.Get("tZ_"+HistoName+"_07_"+Region+"_"+btagStrategy+"_nominal_Loose")

      h_07 = h_tt_07_1b+h_tt_07_1c+h_tt_07_1l+h_ttf_07_1b+h_ttf_07_1c+h_ttf_07_1l+h_st_07+h_sc_07+h_Zjets_07+h_Wjets_07+h_diboson_07+h_ttll_07+h_ttW_07+h_tH_07+h_tWZ_07+h_Wt_07+h_ttH_07+h_tZ_07
      h_07.SetLineWidth(7)
      h_07.SetLineColor(kPink)
      h_07.SetLineStyle(3)
      
      if ReBin == True:
          h_07.Rebin(2)

      file_27   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_potforEB_SRCR/Comb_77p.root","READ")
      dir_27    = file_27.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_27")
      dir_27_1b = file_27.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_27_1b")
      dir_27_1c = file_27.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_27_1c")
      dir_27_1l = file_27.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_27_1l")
      h_tt_27_1b = dir_27_1b.Get("tt_PP8_1B_"+HistoName+"_27_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tt_27_1c = dir_27_1c.Get("tt_PP8_1C_"+HistoName+"_27_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tt_27_1l = dir_27_1l.Get("tt_PP8_1L_"+HistoName+"_27_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_27_1b = dir_27_1b.Get("tt_PP8filtered_1B_"+HistoName+"_27_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_27_1c = dir_27_1c.Get("tt_PP8filtered_1C_"+HistoName+"_27_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_27_1l = dir_27_1l.Get("tt_PP8filtered_1L_"+HistoName+"_27_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_st_27 = dir_27.Get("st_tc_"+HistoName+"_27_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sc_27 = dir_27.Get("st_sc_"+HistoName+"_27_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Zjets_27 = dir_27.Get("zjets_"+HistoName+"_27_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Wjets_27 = dir_27.Get("wjets_"+HistoName+"_27_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_diboson_27 = dir_27.Get("db_"+HistoName+"_27_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttll_27 = dir_27.Get("ttll_"+HistoName+"_27_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttW_27 = dir_27.Get("ttW_"+HistoName+"_27_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tH_27 = dir_27.Get("tH_AFII_"+HistoName+"_27_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tWZ_27 = dir_27.Get("tWZ_"+HistoName+"_27_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Wt_27 = dir_27.Get("Wt_"+HistoName+"_27_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttH_27 = dir_27.Get("ttH_PP8_"+HistoName+"_27_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tZ_27 = dir_27.Get("tZ_"+HistoName+"_27_"+Region+"_"+btagStrategy+"_nominal_Loose")

      h_27 = h_tt_27_1b+h_tt_27_1c+h_tt_27_1l+h_ttf_27_1b+h_ttf_27_1c+h_ttf_27_1l+h_st_27+h_sc_27+h_Zjets_27+h_Wjets_27+h_diboson_27+h_ttll_27+h_ttW_27+h_tH_27+h_tWZ_27+h_Wt_27+h_ttH_27+h_tZ_27
      h_27.SetLineWidth(7)
      h_27.SetLineColor(kBlack)
      h_27.SetLineStyle(3)
      
      if ReBin == True:
          h_27.Rebin(2)

      file_47   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_potforEB_SRCR/Comb_77p.root","READ")
      dir_47    = file_47.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_47")
      dir_47_1b = file_47.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_47_1b")
      dir_47_1c = file_47.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_47_1c")
      dir_47_1l = file_47.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_47_1l")
      h_tt_47_1b = dir_47_1b.Get("tt_PP8_1B_"+HistoName+"_47_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tt_47_1c = dir_47_1c.Get("tt_PP8_1C_"+HistoName+"_47_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tt_47_1l = dir_47_1l.Get("tt_PP8_1L_"+HistoName+"_47_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_47_1b = dir_47_1b.Get("tt_PP8filtered_1B_"+HistoName+"_47_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_47_1c = dir_47_1c.Get("tt_PP8filtered_1C_"+HistoName+"_47_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_47_1l = dir_47_1l.Get("tt_PP8filtered_1L_"+HistoName+"_47_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_st_47 = dir_47.Get("st_tc_"+HistoName+"_47_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sc_47 = dir_47.Get("st_sc_"+HistoName+"_47_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Zjets_47 = dir_47.Get("zjets_"+HistoName+"_47_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Wjets_47 = dir_47.Get("wjets_"+HistoName+"_47_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_diboson_47 = dir_47.Get("db_"+HistoName+"_47_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttll_47 = dir_47.Get("ttll_"+HistoName+"_47_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttW_47 = dir_47.Get("ttW_"+HistoName+"_47_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tH_47 = dir_47.Get("tH_AFII_"+HistoName+"_47_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tWZ_47 = dir_47.Get("tWZ_"+HistoName+"_47_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Wt_47 = dir_47.Get("Wt_"+HistoName+"_47_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttH_47 = dir_47.Get("ttH_PP8_"+HistoName+"_47_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tZ_47 = dir_47.Get("tZ_"+HistoName+"_47_"+Region+"_"+btagStrategy+"_nominal_Loose")

      h_47 = h_tt_47_1b+h_tt_47_1c+h_tt_47_1l+h_ttf_47_1b+h_ttf_47_1c+h_ttf_47_1l+h_st_47+h_sc_47+h_Zjets_47+h_Wjets_47+h_diboson_47+h_ttll_47+h_ttW_47+h_tH_47+h_tWZ_47+h_Wt_47+h_ttH_47+h_tZ_47
      h_47.SetLineWidth(7)
      h_47.SetLineColor(kBlue)
      h_47.SetLineStyle(3)
      
      if ReBin == True:
          h_47.Rebin(2)   

      file_67   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_potforEB_SRCR/Comb_77p.root","READ")
      dir_67    = file_67.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_67")
      dir_67_1b = file_67.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_67_1b")
      dir_67_1c = file_67.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_67_1c")
      dir_67_1l = file_67.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_67_1l")
      h_tt_67_1b = dir_67_1b.Get("tt_PP8_1B_"+HistoName+"_67_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tt_67_1c = dir_67_1c.Get("tt_PP8_1C_"+HistoName+"_67_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tt_67_1l = dir_67_1l.Get("tt_PP8_1L_"+HistoName+"_67_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_67_1b = dir_67_1b.Get("tt_PP8filtered_1B_"+HistoName+"_67_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_67_1c = dir_67_1c.Get("tt_PP8filtered_1C_"+HistoName+"_67_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_67_1l = dir_67_1l.Get("tt_PP8filtered_1L_"+HistoName+"_67_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_st_67 = dir_67.Get("st_tc_"+HistoName+"_67_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sc_67 = dir_67.Get("st_sc_"+HistoName+"_67_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Zjets_67 = dir_67.Get("zjets_"+HistoName+"_67_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Wjets_67 = dir_67.Get("wjets_"+HistoName+"_67_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_diboson_67 = dir_67.Get("db_"+HistoName+"_67_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttll_67 = dir_67.Get("ttll_"+HistoName+"_67_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttW_67 = dir_67.Get("ttW_"+HistoName+"_67_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tH_67 = dir_67.Get("tH_AFII_"+HistoName+"_67_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tWZ_67 = dir_67.Get("tWZ_"+HistoName+"_67_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Wt_67 = dir_67.Get("Wt_"+HistoName+"_67_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttH_67 = dir_67.Get("ttH_PP8_"+HistoName+"_67_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tZ_67 = dir_67.Get("tZ_"+HistoName+"_67_"+Region+"_"+btagStrategy+"_nominal_Loose")

      h_67 = h_tt_67_1b+h_tt_67_1c+h_tt_67_1l+h_ttf_67_1b+h_ttf_67_1c+h_ttf_67_1l+h_st_67+h_sc_67+h_Zjets_67+h_Wjets_67+h_diboson_67+h_ttll_67+h_ttW_67+h_tH_67+h_tWZ_67+h_Wt_67+h_ttH_67+h_tZ_67
      h_67.SetLineWidth(7)
      h_67.SetLineColor(kViolet)
      h_67.SetLineStyle(3)
      
      if ReBin == True:
          h_67.Rebin(2)

      file_10   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_potforEB_SRCR/Comb_77p.root","READ")
      dir_10    = file_10.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_10")
      dir_10_1b = file_10.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_10_1b")
      dir_10_1c = file_10.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_10_1c")
      dir_10_1l = file_10.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_10_1l")
      h_tt_10_1b = dir_10_1b.Get("tt_PP8_1B_"+HistoName+"_10_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tt_10_1c = dir_10_1c.Get("tt_PP8_1C_"+HistoName+"_10_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tt_10_1l = dir_10_1l.Get("tt_PP8_1L_"+HistoName+"_10_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_10_1b = dir_10_1b.Get("tt_PP8filtered_1B_"+HistoName+"_10_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_10_1c = dir_10_1c.Get("tt_PP8filtered_1C_"+HistoName+"_10_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_10_1l = dir_10_1l.Get("tt_PP8filtered_1L_"+HistoName+"_10_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_st_10 = dir_10.Get("st_tc_"+HistoName+"_10_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sc_10 = dir_10.Get("st_sc_"+HistoName+"_10_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Zjets_10 = dir_10.Get("zjets_"+HistoName+"_10_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Wjets_10 = dir_10.Get("wjets_"+HistoName+"_10_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_diboson_10 = dir_10.Get("db_"+HistoName+"_10_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttll_10 = dir_10.Get("ttll_"+HistoName+"_10_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttW_10 = dir_10.Get("ttW_"+HistoName+"_10_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tH_10 = dir_10.Get("tH_AFII_"+HistoName+"_10_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tWZ_10 = dir_10.Get("tWZ_"+HistoName+"_10_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Wt_10 = dir_10.Get("Wt_"+HistoName+"_10_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttH_10 = dir_10.Get("ttH_PP8_"+HistoName+"_10_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tZ_10 = dir_10.Get("tZ_"+HistoName+"_10_"+Region+"_"+btagStrategy+"_nominal_Loose")

      h_10 = h_tt_10_1b+h_tt_10_1c+h_tt_10_1l+h_ttf_10_1b+h_ttf_10_1c+h_ttf_10_1l+h_st_10+h_sc_10+h_Zjets_10+h_Wjets_10+h_diboson_10+h_ttll_10+h_ttW_10+h_tH_10+h_tWZ_10+h_Wt_10+h_ttH_10+h_tZ_10
      h_10.SetLineWidth(7)
      h_10.SetLineColor(kAzure)
      h_10.SetLineStyle(3)
      
      if ReBin == True:
          h_10.Rebin(2)

      file_50   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_potforEB_SRCR/Comb_77p.root","READ")
      dir_50    = file_50.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_50")
      dir_50_1b = file_50.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_50_1b")
      dir_50_1c = file_50.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_50_1c")
      dir_50_1l = file_50.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_50_1l")
      h_tt_50_1b = dir_50_1b.Get("tt_PP8_1B_"+HistoName+"_50_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tt_50_1c = dir_50_1c.Get("tt_PP8_1C_"+HistoName+"_50_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tt_50_1l = dir_50_1l.Get("tt_PP8_1L_"+HistoName+"_50_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_50_1b = dir_50_1b.Get("tt_PP8filtered_1B_"+HistoName+"_50_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_50_1c = dir_50_1c.Get("tt_PP8filtered_1C_"+HistoName+"_50_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_50_1l = dir_50_1l.Get("tt_PP8filtered_1L_"+HistoName+"_50_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_st_50 = dir_50.Get("st_tc_"+HistoName+"_50_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sc_50 = dir_50.Get("st_sc_"+HistoName+"_50_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Zjets_50 = dir_50.Get("zjets_"+HistoName+"_50_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Wjets_50 = dir_50.Get("wjets_"+HistoName+"_50_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_diboson_50 = dir_50.Get("db_"+HistoName+"_50_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttll_50 = dir_50.Get("ttll_"+HistoName+"_50_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttW_50 = dir_50.Get("ttW_"+HistoName+"_50_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tH_50 = dir_50.Get("tH_AFII_"+HistoName+"_50_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tWZ_50 = dir_50.Get("tWZ_"+HistoName+"_50_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Wt_50 = dir_50.Get("Wt_"+HistoName+"_50_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttH_50 = dir_50.Get("ttH_PP8_"+HistoName+"_50_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tZ_50 = dir_50.Get("tZ_"+HistoName+"_50_"+Region+"_"+btagStrategy+"_nominal_Loose")

      h_50 = h_tt_50_1b+h_tt_50_1c+h_tt_50_1l+h_ttf_50_1b+h_ttf_50_1c+h_ttf_50_1l+h_st_50+h_sc_50+h_Zjets_50+h_Wjets_50+h_diboson_50+h_ttll_50+h_ttW_50+h_tH_50+h_tWZ_50+h_Wt_50+h_ttH_50+h_tZ_50
      h_50.SetLineWidth(7)
      h_50.SetLineColor(kCyan)
      h_50.SetLineStyle(3)
      
      if ReBin == True:
          h_50.Rebin(2)

      file_52   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_potforEB_SRCR/Comb_77p.root","READ")
      dir_52    = file_52.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_52")
      dir_52_1b = file_52.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_52_1b")
      dir_52_1c = file_52.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_52_1c")
      dir_52_1l = file_52.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_52_1l")
      h_tt_52_1b = dir_52_1b.Get("tt_PP8_1B_"+HistoName+"_52_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tt_52_1c = dir_52_1c.Get("tt_PP8_1C_"+HistoName+"_52_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tt_52_1l = dir_52_1l.Get("tt_PP8_1L_"+HistoName+"_52_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_52_1b = dir_52_1b.Get("tt_PP8filtered_1B_"+HistoName+"_52_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_52_1c = dir_52_1c.Get("tt_PP8filtered_1C_"+HistoName+"_52_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_52_1l = dir_52_1l.Get("tt_PP8filtered_1L_"+HistoName+"_52_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_st_52 = dir_52.Get("st_tc_"+HistoName+"_52_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sc_52 = dir_52.Get("st_sc_"+HistoName+"_52_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Zjets_52 = dir_52.Get("zjets_"+HistoName+"_52_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Wjets_52 = dir_52.Get("wjets_"+HistoName+"_52_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_diboson_52 = dir_52.Get("db_"+HistoName+"_52_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttll_52 = dir_52.Get("ttll_"+HistoName+"_52_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttW_52 = dir_52.Get("ttW_"+HistoName+"_52_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tH_52 = dir_52.Get("tH_AFII_"+HistoName+"_52_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tWZ_52 = dir_52.Get("tWZ_"+HistoName+"_52_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Wt_52 = dir_52.Get("Wt_"+HistoName+"_52_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttH_52 = dir_52.Get("ttH_PP8_"+HistoName+"_52_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tZ_52 = dir_52.Get("tZ_"+HistoName+"_52_"+Region+"_"+btagStrategy+"_nominal_Loose")

      h_52 = h_tt_52_1b+h_tt_52_1c+h_tt_52_1l+h_ttf_52_1b+h_ttf_52_1c+h_ttf_52_1l+h_st_52+h_sc_52+h_Zjets_52+h_Wjets_52+h_diboson_52+h_ttll_52+h_ttW_52+h_tH_52+h_tWZ_52+h_Wt_52+h_ttH_52+h_tZ_52
      h_52.SetLineWidth(7)
      h_52.SetLineColor(kGreen+3)
      h_52.SetLineStyle(3)
      
      if ReBin == True:
          h_52.Rebin(2)    

      file_54   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_potforEB_SRCR/Comb_77p.root","READ")
      dir_54    = file_54.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_54")
      dir_54_1b = file_54.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_54_1b")
      dir_54_1c = file_54.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_54_1c")
      dir_54_1l = file_54.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_54_1l")
      h_tt_54_1b = dir_54_1b.Get("tt_PP8_1B_"+HistoName+"_54_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tt_54_1c = dir_54_1c.Get("tt_PP8_1C_"+HistoName+"_54_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tt_54_1l = dir_54_1l.Get("tt_PP8_1L_"+HistoName+"_54_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_54_1b = dir_54_1b.Get("tt_PP8filtered_1B_"+HistoName+"_54_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_54_1c = dir_54_1c.Get("tt_PP8filtered_1C_"+HistoName+"_54_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_54_1l = dir_54_1l.Get("tt_PP8filtered_1L_"+HistoName+"_54_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_st_54 = dir_54.Get("st_tc_"+HistoName+"_54_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sc_54 = dir_54.Get("st_sc_"+HistoName+"_54_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Zjets_54 = dir_54.Get("zjets_"+HistoName+"_54_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Wjets_54 = dir_54.Get("wjets_"+HistoName+"_54_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_diboson_54 = dir_54.Get("db_"+HistoName+"_54_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttll_54 = dir_54.Get("ttll_"+HistoName+"_54_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttW_54 = dir_54.Get("ttW_"+HistoName+"_54_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tH_54 = dir_54.Get("tH_AFII_"+HistoName+"_54_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tWZ_54 = dir_54.Get("tWZ_"+HistoName+"_54_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Wt_54 = dir_54.Get("Wt_"+HistoName+"_54_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttH_54 = dir_54.Get("ttH_PP8_"+HistoName+"_54_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tZ_54 = dir_54.Get("tZ_"+HistoName+"_54_"+Region+"_"+btagStrategy+"_nominal_Loose")

      h_54 = h_tt_54_1b+h_tt_54_1c+h_tt_54_1l+h_ttf_54_1b+h_ttf_54_1c+h_ttf_54_1l+h_st_54+h_sc_54+h_Zjets_54+h_Wjets_54+h_diboson_54+h_ttll_54+h_ttW_54+h_tH_54+h_tWZ_54+h_Wt_54+h_ttH_54+h_tZ_54
      h_54.SetLineWidth(7)
      h_54.SetLineColor(kGreen+3)
      h_54.SetLineStyle(3)
      
      if ReBin == True:
          h_54.Rebin(2) 

      file_56   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_potforEB_SRCR/Comb_77p.root","READ")
      dir_56    = file_56.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_56")
      dir_56_1b = file_56.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_56_1b")
      dir_56_1c = file_56.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_56_1c")
      dir_56_1l = file_56.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_56_1l")
      h_tt_56_1b = dir_56_1b.Get("tt_PP8_1B_"+HistoName+"_56_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tt_56_1c = dir_56_1c.Get("tt_PP8_1C_"+HistoName+"_56_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tt_56_1l = dir_56_1l.Get("tt_PP8_1L_"+HistoName+"_56_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_56_1b = dir_56_1b.Get("tt_PP8filtered_1B_"+HistoName+"_56_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_56_1c = dir_56_1c.Get("tt_PP8filtered_1C_"+HistoName+"_56_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_56_1l = dir_56_1l.Get("tt_PP8filtered_1L_"+HistoName+"_56_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_st_56 = dir_56.Get("st_tc_"+HistoName+"_56_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sc_56 = dir_56.Get("st_sc_"+HistoName+"_56_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Zjets_56 = dir_56.Get("zjets_"+HistoName+"_56_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Wjets_56 = dir_56.Get("wjets_"+HistoName+"_56_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_diboson_56 = dir_56.Get("db_"+HistoName+"_56_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttll_56 = dir_56.Get("ttll_"+HistoName+"_56_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttW_56 = dir_56.Get("ttW_"+HistoName+"_56_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tH_56 = dir_56.Get("tH_AFII_"+HistoName+"_56_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tWZ_56 = dir_56.Get("tWZ_"+HistoName+"_56_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Wt_56 = dir_56.Get("Wt_"+HistoName+"_56_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttH_56 = dir_56.Get("ttH_PP8_"+HistoName+"_56_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tZ_56 = dir_56.Get("tZ_"+HistoName+"_56_"+Region+"_"+btagStrategy+"_nominal_Loose")

      h_56 = h_tt_56_1b+h_tt_56_1c+h_tt_56_1l+h_ttf_56_1b+h_ttf_56_1c+h_ttf_56_1l+h_st_56+h_sc_56+h_Zjets_56+h_Wjets_56+h_diboson_56+h_ttll_56+h_ttW_56+h_tH_56+h_tWZ_56+h_Wt_56+h_ttH_56+h_tZ_56
      #h_56 = h_tt_56_1b+h_tt_56_1c+h_tt_56_1l+h_ttf_56_1b+h_ttf_56_1c+h_ttf_56_1l+h_st_56+h_sc_56+h_Zjets_56+h_Wjets_56
      h_56.SetLineWidth(7)
      h_56.SetLineColor(kGreen+3)
      h_56.SetLineStyle(3)
      
      if ReBin == True:
          h_56.Rebin(2)        
 

      file_19   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_potforEB_SRCR/Comb_77p.root","READ")
      dir_19    = file_19.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_19")
      dir_19_1b = file_19.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_19_1b")
      dir_19_1c = file_19.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_19_1c")
      dir_19_1l = file_19.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_19_1l")
      h_tt_19_1b = dir_19_1b.Get("tt_PP8_1B_"+HistoName+"_19_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tt_19_1c = dir_19_1c.Get("tt_PP8_1C_"+HistoName+"_19_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tt_19_1l = dir_19_1l.Get("tt_PP8_1L_"+HistoName+"_19_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_19_1b = dir_19_1b.Get("tt_PP8filtered_1B_"+HistoName+"_19_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_19_1c = dir_19_1c.Get("tt_PP8filtered_1C_"+HistoName+"_19_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_19_1l = dir_19_1l.Get("tt_PP8filtered_1L_"+HistoName+"_19_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_st_19 = dir_19.Get("st_tc_"+HistoName+"_19_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sc_19 = dir_19.Get("st_sc_"+HistoName+"_19_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Zjets_19 = dir_19.Get("zjets_"+HistoName+"_19_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Wjets_19 = dir_19.Get("wjets_"+HistoName+"_19_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_diboson_19 = dir_19.Get("db_"+HistoName+"_19_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttll_19 = dir_19.Get("ttll_"+HistoName+"_19_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttW_19 = dir_19.Get("ttW_"+HistoName+"_19_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tH_19 = dir_19.Get("tH_AFII_"+HistoName+"_19_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tWZ_19 = dir_19.Get("tWZ_"+HistoName+"_19_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Wt_19 = dir_19.Get("Wt_"+HistoName+"_19_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttH_19 = dir_19.Get("ttH_PP8_"+HistoName+"_19_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tZ_19 = dir_19.Get("tZ_"+HistoName+"_19_"+Region+"_"+btagStrategy+"_nominal_Loose")

      h_19 = h_tt_19_1b+h_tt_19_1c+h_tt_19_1l+h_ttf_19_1b+h_ttf_19_1c+h_ttf_19_1l+h_st_19+h_sc_19+h_Zjets_19+h_Wjets_19+h_diboson_19+h_ttll_19+h_ttW_19+h_tH_19+h_tWZ_19+h_Wt_19+h_ttH_19+h_tZ_19
      h_19.SetLineWidth(7)
      h_19.SetLineColor(kTeal)
      h_19.SetLineStyle(3)
      
      if ReBin == True:
          h_19.Rebin(2)

      file_9   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_potforEB_SRCR/Comb_77p.root","READ")
      dir_9    = file_9.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_9")
      dir_9_1b = file_9.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_9_1b")
      dir_9_1c = file_9.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_9_1c")
      dir_9_1l = file_9.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_9_1l")
      h_tt_9_1b = dir_9_1b.Get("tt_PP8_1B_"+HistoName+"_9_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tt_9_1c = dir_9_1c.Get("tt_PP8_1C_"+HistoName+"_9_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tt_9_1l = dir_9_1l.Get("tt_PP8_1L_"+HistoName+"_9_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_9_1b = dir_9_1b.Get("tt_PP8filtered_1B_"+HistoName+"_9_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_9_1c = dir_9_1c.Get("tt_PP8filtered_1C_"+HistoName+"_9_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttf_9_1l = dir_9_1l.Get("tt_PP8filtered_1L_"+HistoName+"_9_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_st_9 = dir_9.Get("st_tc_"+HistoName+"_9_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sc_9 = dir_9.Get("st_sc_"+HistoName+"_9_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Zjets_9 = dir_9.Get("zjets_"+HistoName+"_9_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Wjets_9 = dir_9.Get("wjets_"+HistoName+"_9_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_diboson_9 = dir_9.Get("db_"+HistoName+"_9_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttll_9 = dir_9.Get("ttll_"+HistoName+"_9_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttW_9 = dir_9.Get("ttW_"+HistoName+"_9_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tH_9 = dir_9.Get("tH_AFII_"+HistoName+"_9_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tWZ_9 = dir_9.Get("tWZ_"+HistoName+"_9_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_Wt_9 = dir_9.Get("Wt_"+HistoName+"_9_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttH_9 = dir_9.Get("ttH_PP8_"+HistoName+"_9_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_tZ_9 = dir_9.Get("tZ_"+HistoName+"_9_"+Region+"_"+btagStrategy+"_nominal_Loose")

      h_9 = h_tt_9_1b+h_tt_9_1c+h_tt_9_1l+h_ttf_9_1b+h_ttf_9_1c+h_ttf_9_1l+h_st_9+h_sc_9+h_Zjets_9+h_Wjets_9+h_diboson_9+h_ttll_9+h_ttW_9+h_tH_9+h_tWZ_9+h_Wt_9+h_ttH_9+h_tZ_9
      h_9.SetLineWidth(7)
      h_9.SetLineColor(kRed)
      h_9.SetLineStyle(1)
      
      if ReBin == True:
          h_9.Rebin(2)      

     # h_other_background = h_singleTop_background + h_diboson_background + h_W_background + h_Z_background     
      
      #print("h_09 contribution is", h_09.Integral()/h_nom.Integral())
      #print("h_29 contribution is", h_29.Integral()/h_nom.Integral())
      #print("h_49 contribution is", h_49.Integral()/h_nom.Integral())
      #print("h_69 contribution is", h_69.Integral()/h_nom.Integral())
      #print("h_07 contribution is", h_07.Integral()/h_nom.Integral())
      #print("h_27 contribution is", h_27.Integral()/h_nom.Integral())
      #print("h_47 contribution is", h_47.Integral()/h_nom.Integral())
      #print("h_67 contribution is", h_67.Integral()/h_nom.Integral())
      #print("h_10 contribution is", h_10.Integral()/h_nom.Integral())
      #print("h_50 contribution is", h_50.Integral()/h_nom.Integral())
      #print("h_19 contribution is", h_19.Integral()/h_nom.Integral())
      #print("h_9 contribution is", h_9.Integral()/h_nom.Integral())
      #print("h_63 contribution is", h_63.Integral()/h_nom.Integral())
      #print("h_61 contribution is", h_61.Integral()/h_nom.Integral())
      #print("h_10 contribution is", h_10.Integral()/h_nom.Integral())
      #print("h_50 contribution is", h_50.Integral()/h_nom.Integral())
      #print("h_4 contribution is", h_4.Integral()/h_nom.Integral())

      text_file = open("./txt/EB/BkgYields_FourPlusTags_EB.txt","w")
      text_file.write("MVA BkgYields\n")
      text_file.write(str("h_09")+" ")
      text_file.write(str(h_09.Integral())+"\n")
      #text_file.write(str(h_09.Integral()/(h_nom.Integral()*0.9))+"\n")
      text_file.write(str("h_29")+" ")
      text_file.write(str(h_29.Integral())+"\n")
      #text_file.write(str(h_29.Integral()/(h_nom.Integral()*1.1))+"\n")
      text_file.write(str("h_49")+" ")
      text_file.write(str(h_49.Integral())+"\n")
      #text_file.write(str(h_49.Integral()/(h_nom.Integral()*1.3))+"\n")
      text_file.write(str("h_69")+" ")
      text_file.write(str(h_69.Integral())+"\n")
      #text_file.write(str(h_69.Integral()/(h_nom.Integral()*1.5))+"\n")
      text_file.write(str("h_07")+" ")
      text_file.write(str(h_07.Integral())+"\n")
      #text_file.write(str(h_07.Integral()/(h_nom.Integral()*0.7))+"\n")
      text_file.write(str("h_27")+" ")
      text_file.write(str(h_27.Integral())+"\n")
      #text_file.write(str(h_27.Integral()/(h_nom.Integral()*0.9))+"\n")
      text_file.write(str("h_47")+" ")
      text_file.write(str(h_47.Integral())+"\n")
      #text_file.write(str(h_47.Integral()/(h_nom.Integral()*1.1))+"\n")
      text_file.write(str("h_67")+" ")
      text_file.write(str(h_67.Integral())+"\n")
      #text_file.write(str(h_67.Integral()/(h_nom.Integral()*1.3))+"\n")
      text_file.write(str("h_10")+" ")
      text_file.write(str(h_10.Integral())+"\n")
      #text_file.write(str(h_10.Integral()/(h_nom.Integral()*1.0))+"\n")
      text_file.write(str("h_50")+" ")
      text_file.write(str(h_50.Integral())+"\n")
      #text_file.write(str(h_50.Integral()/(h_nom.Integral()*0.5))+"\n")
      text_file.write(str("h_52")+" ")
      text_file.write(str(h_52.Integral())+"\n")
      #text_file.write(str(h_52.Integral()/(h_nom.Integral()*0.7))+"\n")
      text_file.write(str("h_54")+" ")
      text_file.write(str(h_54.Integral())+"\n")
      #text_file.write(str(h_54.Integral()/(h_nom.Integral()*0.9))+"\n")
      text_file.write(str("h_56")+" ")
      text_file.write(str(h_56.Integral())+"\n")
      #text_file.write(str(h_56.Integral()/(h_nom.Integral()*1.1))+"\n")
      text_file.write(str("h_19")+" ")
      text_file.write(str(h_19.Integral())+"\n")
      #text_file.write(str(h_19.Integral()/(h_nom.Integral()*1.9))+"\n")
      text_file.write(str("h_9")+" ")
      text_file.write(str(h_9.Integral())+"\n")
      #text_file.write(str(h_9.Integral()/(h_nom.Integral()*0.1))+"\n")

      NormalizeHisto(h_09)
      NormalizeHisto(h_29)
      NormalizeHisto(h_49)
      NormalizeHisto(h_69)
      NormalizeHisto(h_07)
      NormalizeHisto(h_27)
      NormalizeHisto(h_47)
      NormalizeHisto(h_67)
      NormalizeHisto(h_10)
      NormalizeHisto(h_50)
      NormalizeHisto(h_52)
      NormalizeHisto(h_54)
      NormalizeHisto(h_56)
      NormalizeHisto(h_19)
      NormalizeHisto(h_9)
      #NormalizeHisto(h_63)
      #NormalizeHisto(h_61)
      #NormalizeHisto(h_10)
      #NormalizeHisto(h_50)
      #NormalizeHisto(h_4)
      
      nbins=20
      ymax=0
      
      if ymax<h_09.GetMaximum():
          ymax=h_09.GetMaximum()
      
      if ymax<h_49.GetMaximum():
          ymax=h_49.GetMaximum()
      
      if ymax<h_07.GetMaximum():
          ymax=h_07.GetMaximum()

      if ymax<h_27.GetMaximum():
          ymax=h_27.GetMaximum()    
      
      if ymax<h_67.GetMaximum():
          ymax=h_67.GetMaximum()
      
      if ymax<h_10.GetMaximum():
          ymax=h_10.GetMaximum()

      if ymax<h_19.GetMaximum():
          ymax=h_19.GetMaximum()  

      if ymax<h_50.GetMaximum():
          ymax=h_50.GetMaximum()   

      if ymax<h_52.GetMaximum():
          ymax=h_52.GetMaximum()       

      if ymax<h_54.GetMaximum():
          ymax=h_54.GetMaximum()    

      if ymax<h_56.GetMaximum():
          ymax=h_56.GetMaximum()       
      
      if ymax<h_9.GetMaximum():
          ymax=h_9.GetMaximum()


      h_09.Draw("HIST")
      h_49.Draw("HISTSAME")
      #h_61.Draw("HISTSAME")
      #h_04.Draw("HISTSAME")
      #h_10.Draw("HISTSAME")
      h_07.Draw("HISTSAME")
      h_27.Draw("HISTSAME")
      h_67.Draw("HISTSAME")
      h_10.Draw("HISTSAME")
      h_19.Draw("HISTSAME")
      h_9.Draw("HISTSAME")

      if HistoName in "maxMVAResponse":
         leg = TLegend(0.2,0.65,0.725,0.855)
      else:
         leg = TLegend(0.45,0.55,0.925,0.755)
      ATLAS_LABEL(0.20,0.885," Internal",1,0.19);
      leg.SetShadowColor(kWhite)
      leg.SetFillColor(kWhite)
      leg.SetLineColor(kWhite)
      
      leg.AddEntry(h_09,    " MVA Score =[0.0, 0.9)","L")
      leg.AddEntry(h_49,    " MVA Score =[-0.4, 0.9)","L")
      leg.AddEntry(h_07,    " MVA Score =[0.0, 0.7)","L")
      leg.AddEntry(h_27,    " MVA Score =[-0.2, 0.7)","L")
      leg.AddEntry(h_67,    " MVA Score =[-0.6, 0.7)","L")
      leg.AddEntry(h_10,    " MVA Score =[-1.0, 0.0)","L")
      leg.AddEntry(h_19,    " MVA Score =[-1.0, 0.9)","L")
      leg.AddEntry(h_9,    " MVA Score > = 0.9","L")
      #leg.AddEntry(h_07,    " MVA Score =(0.0, 0.7)","L")
      #leg.AddEntry(h_67,    " MVA Score =(-0.6, 0.7)","L")
      #leg.AddEntry(h_10,    " MVA Score =(-1.0, 0.0)","L")
      #leg.AddEntry(h_9,    " MVA Score =(0.9, 1.0)","L")
      
      leg.SetTextSize(0.0250)
      leg.Draw()
      h_09.GetXaxis().SetTitle(Xaxis_label)
      h_09.GetYaxis().SetRangeUser(0.001,round(ymax*1.25,3)+0.001)
      h_09.GetYaxis().SetTitle("Normalised Entries")
      myText(0.20,0.835,1,"Simulation")
      c1.RedrawAxis()
      c1.Update()
      c1.RedrawAxis()
      c1.SaveAs("../Plots/mWH_CR_crosscheck_FourPlusTags_INT_EB.pdf")
#h_mVH_52
base= "/cephfs/user/s6subans/ChargedHiggs_V2/Hp_potforEB_SRCR/"
text_file = open("./txt/EB/SignalYield_9_FourPlusTags_EB.txt","w")
text_file.write("Mass SigYield\n")
for FileName in ["hp250_AFII", "hp300_AFII", "hp350_AFII", "hp400_AFII", "hp500_AFII", "hp600_AFII", "hp700_AFII", "hp800_AFII", "hp900_AFII", "hp1000_AFII", "hp1200_AFII", "hp1400_AFII", "hp1600_AFII", "hp1800_AFII", "hp2000_AFII", "hp2500_AFII", "hp3000_AFII" ]: #for boosted, histoname set
## for bTagStrategy in ["Incl","FourPlusTags","ThreeTags","TwoTags"]:
   #for CR in ["_09", "_29", "_49", "_69", "_07", "_27", "_47", "_67", "_10", "_50", "_19", "_9"]:
   #for CR in ["_09"]:    
                 file       = TFile.Open(base+FileName+".root_77p_225.root","READ")
                 dir       = file.GetDirectory("nominal_Loose").GetDirectory("mVH_9")
                 dir_nom = file.GetDirectory("nominal_Loose").GetDirectory("mVH")
                 h_nom = dir_nom.Get(FileName+"_mVH_Resolved_SR_FourPlusTags_nominal_Loose")
                 #h =  dir.Get(FileName+"_mVH"+CR+"_Resolved_SR_Inclusive_nominal_Loose")
                 h =  dir.Get(FileName+"_mVH_9_Resolved_SR_FourPlusTags_nominal_Loose")
                 #print(FileName+CR+"contribution is", h.Integral()/h_nom.Integral())
                 text_file.write(str(FileName)+" ")
                 #text_file.write(str(h.Integral()/(h_nom.Integral()*0.1))+"\n")
                 text_file.write(str(h.Integral())+"\n")
                 #text_file.write(str(values[str(mass)+"+2"][0]*normvalue)+" ")
                 #text_file.write(str(values[str(mass)+"+1"][0]*normvalue)+" ")