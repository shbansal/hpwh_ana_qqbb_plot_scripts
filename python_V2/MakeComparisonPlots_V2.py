# -*- coding: utf-8 -*-
#python
import sys
import glob
import math
import re
from ROOT import *
from array import *
from ROOT import TCanvas, TFile, TLine, TProfile, TNtuple, TH1F, TH2F
#import ROOT
#import array

gROOT.SetBatch(True)
gStyle.SetOptStat(0)
gStyle.SetPalette(1)
gROOT.LoadMacro("../style/AtlasStyle.C")
gROOT.LoadMacro("../style/AtlasUtils.C")
SetAtlasStyle()

y_axis_label="Event fraction"

c_blue   = TColor.GetColor("#3366ff")
c_red    = TColor.GetColor("#ee3311")
c_orange = TColor.GetColor("#ff9900")

def NormalizeHisto(histo):
     #n_events=histo.Integral(-1,histo.GetNbinsX()+1)
     n_events=histo.Integral()
     if n_events == 0:
         return
     #print n_events, histo.Integral(histo.GetNbinsX(),histo.GetNbinsX()+1)
     print n_events
     histo.Scale(1./n_events)
     histo.SetLineWidth(3)
     histo.SetStats(0)
     histo.SetFillStyle(3001)
     histo.SetMarkerColor(histo.GetLineColor())
     histo.SetMarkerSize(0.0)
     histo.GetXaxis().SetTitleOffset(1.2)
     histo.GetYaxis().SetTitleOffset(1.52)
     histo.GetXaxis().SetLabelSize(0.05)
     histo.GetYaxis().SetLabelSize(0.05)
     histo.GetXaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetNdivisions(504)
     histo.GetXaxis().SetNdivisions(504)

c1 = TCanvas("ShapePlots","",500,500)

#for HistoName in ["nBTags"]:
#for HistoName in ["nBTags","nJets","HT","HT_bjets","DeltaPhi_HW","mVH","mH","pTWplus","pTH","maxMVAResponse", "mass_resolution"]: # for resolved, histoname set
#for HistoName in ["pTWplus","pTH","mVH","maxMVAResponse"]: # for resolved, histoname set
#for HistoName in ["maxMVAResponse", "mVH_9"]:
#for HistoName in ["nBTags","nJets","HT","HT_bjets","DeltaPhi_HW","mVH","mH","pTWplus","pTH","mass_resolution"]:#for boosted, histoname set
#for HistoName in ["mVH"]:
#for HistoName in ["mVH"]:     
for HistoName in ["maxMVAResponse"]:
## for bTagStrategy in ["Incl","FourPlusTags","ThreeTags","TwoTags"]:
 for Region in ["Resolved_SR"]:
  #for btagStrategy in ["Inclusive"]:
   for btagStrategy in ["FourPlusTags","ThreeTags","TwoTags"]:
     
      ReBin = False
      YAxisScale = 1.4

      if "nBTags" in HistoName:
          Xaxis_label="b-tag multiplicity"
      if "nJet" in HistoName:
          Xaxis_label="Jet Multiplicity"
      if "DeltaPhi_HW" in HistoName:
          Xaxis_label="DeltaPhi_HW"
      if "pTH" in HistoName:
          Xaxis_label="Transverse Momentum of Higgs [GeV]"
      if "pTWplus" in HistoName:
          Xaxis_label="Transverse Momentum of W Boson [GeV]"
      if "mVH" in HistoName:
          Xaxis_label="Mass Wh [GeV]"
      if "mH" in HistoName:
          Xaxis_label="Mass of Higgs [GeV]"
      if "pTWplus" in HistoName:
          Xaxis_label="Transverse Momentum Of Wplus [GeV]"
      if "pTH" in HistoName:
          Xaxis_label="Transverse Momentum Of Higgs [GeV]"
      if "mass_resolution" in HistoName:
          Xaxis_label="Mass Resolution"
      if "HT" in HistoName:
          Xaxis_label="H_{T} (Scalar Transverse Momentum Sum of jets) [GeV]"
      if "HT_bjets" in HistoName:
          Xaxis_label="H_{T_{b-jet}} (Scalar Transverse Momentum Sum of b-jets) [GeV]"
      if "maxMVAResponse" in HistoName:
          Xaxis_label="Maximal MVA Response"    
      #Xaxis_label=""

      #file1       = TFile.Open("../PlotFiles/ForOptimisation/sig_Hplus_Wh_m400-0_70p.root","READ")
      file25       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_plotforEB/hp300_AFII.root_77p_225.root","READ")
      dir25       = file25.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      #dir1        = file1.GetDirectory("Nominal").GetDirectory(HistoName)
      h_sig_Hplus_m300 = dir25.Get("hp300_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_sig_Hplus_m400.SetLineWidth(h_sig_Hplus_m400.GetLineWidth()*100)
      h_sig_Hplus_m300.SetLineColor(kBlue)
      h_sig_Hplus_m300.SetLineStyle(7) #7
      #if ReBin == True:
          #h_sig_Hplus_m400.Rebin(2)

      #print("h_400 Line is", h_sig_Hplus_m400.GetLineWidth())    

      #file6	  = TFile.Open("../PlotFiles/ForOptimisation/sig_Hplus_Wh_m800-0_70p.root","READ")
      file50	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_plotforEB/hp500_AFII.root_77p_225.root","READ")
      dir50        = file50.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      #dir6        = file6.GetDirectory("Nominal").GetDirectory(HistoName)
      h_sig_Hplus_m500 = dir50.Get("hp500_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_sig_Hplus_m800.SetLineWidth(8)
      h_sig_Hplus_m500.SetLineColor(kGreen)
      h_sig_Hplus_m500.SetLineStyle(7) #7
      if ReBin == True:
          h_sig_Hplus_m500.Rebin(2)

      file40       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_plotforEB/hp250_AFII.root_77p_225.root","READ")
      dir40       = file40.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      #dir1        = file1.GetDirectory("Nominal").GetDirectory(HistoName)
      h_sig_Hplus_m400 = dir40.Get("hp250_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_sig_Hplus_m400.SetLineWidth(h_sig_Hplus_m400.GetLineWidth()*100)
      h_sig_Hplus_m400.SetLineColor(kBlue)
      h_sig_Hplus_m400.SetLineStyle(7) #7
      if ReBin == True:
          h_sig_Hplus_m400.Rebin(2)


      #file7	  = TFile.Open("../PlotFiles/ForOptimisation/sig_Hplus_Wh_m1600-0_70p.root","READ")
      file10  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_plotforEB/hp1000_AFII.root_77p_225.root","READ")
      dir10       = file10.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      #dir7        = file7.GetDirectory("Nominal").GetDirectory(HistoName)
      h_sig_Hplus_m1000 = dir10.Get("hp1000_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_sig_Hplus_m1600.SetLineWidth(8)
      h_sig_Hplus_m1000.SetLineColor(kViolet)
      h_sig_Hplus_m1000.SetLineStyle(7) #7
      if ReBin == True:
          h_sig_Hplus_m1600.Rebin(2)

      file18  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_plotforEB/hp1800_AFII.root_77p_225.root","READ")
      dir18        = file18.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      #dir7        = file7.GetDirectory("Nominal").GetDirectory(HistoName)
      h_sig_Hplus_m1800 = dir18.Get("hp1800_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m1800.SetLineColor(kCyan)
      h_sig_Hplus_m1800.SetLineStyle(7) #7
      if ReBin == True:
          h_sig_Hplus_m1800.Rebin(2)

      file25  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_plotforEB/hp2500_AFII.root_77p_225.root","READ")
      dir25        = file25.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      #dir7        = file7.GetDirectory("Nominal").GetDirectory(HistoName)
      h_sig_Hplus_m2500 = dir25.Get("hp2500_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m2500.SetLineColor(kYellow+3)
      h_sig_Hplus_m2500.SetLineStyle(7) #7
      if ReBin == True:
          h_sig_Hplus_m2500.Rebin(2)   


      #file_tt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_potforEB_SRCR/Comb_77p.root","READ")
      file_tt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/tt_PP8.root_77p_225.root","READ")
      #file_tt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PS_weight/tt_PP8.root_77p_225.root","READ")
      #file_tt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_290121/ttbar_70p.root","READ")
      dir_tt_1b   = file_tt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1b")
      h_ttbar_background_1b = dir_tt_1b.Get("tt_PP8_1B_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_ttbar_background_1b = dir_tt_1b.Get("tt_PP8_"+HistoName+"_1b_"+Region+"_"+btagStrategy+"_nominal_Loose")
      dir_tt_1c   = file_tt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1c")
      h_ttbar_background_1c = dir_tt_1c.Get("tt_PP8_1C_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_ttbar_background_1c = dir_tt_1c.Get("tt_PP8_"+HistoName+"_1c_"+Region+"_"+btagStrategy+"_nominal_Loose")
      dir_tt_1l   = file_tt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1l")
      h_ttbar_background_1l = dir_tt_1l.Get("tt_PP8_1L_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_ttbar_background_1l = dir_tt_1l.Get("tt_PP8_"+HistoName+"_1l_"+Region+"_"+btagStrategy+"_nominal_Loose")

      #file_tt_filt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PS_weight_allfloat/tt_PP8filtered.root_77p_225.root","READ")
      #file_tt_filt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_potforEB_SRCR/Comb_77p.root","READ")
      #file_tt_filt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PS_weight/tt_PP8filtered.root_77p_225.root","READ")
      file_tt_filt    = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/tt_PP8filtered.root_77p_225.root","READ")
      dir_tt_1b_filt   = file_tt_filt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1b")
      h_ttbar_background_1b_filt = dir_tt_1b_filt.Get("tt_PP8filtered_1B_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_ttbar_background_1b_filt = dir_tt_1b_filt.Get("tt_PP8filtered_"+HistoName+"_1b_"+Region+"_"+btagStrategy+"_nominal_Loose")
      dir_tt_1c_filt   = file_tt_filt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1c")
      h_ttbar_background_1c_filt = dir_tt_1c_filt.Get("tt_PP8filtered_1C_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_ttbar_background_1c_filt = dir_tt_1c_filt.Get("tt_PP8filtered_"+HistoName+"_1c_"+Region+"_"+btagStrategy+"_nominal_Loose")
      dir_tt_1l_filt   = file_tt_filt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1l")
      h_ttbar_background_1l_filt = dir_tt_1l_filt.Get("tt_PP8filtered_1L_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_ttbar_background_1l_filt = dir_tt_1l_filt.Get("tt_PP8filtered_"+HistoName+"_1l_"+Region+"_"+btagStrategy+"_nominal_Loose")

    
      #h_ttbar_background = h_ttbar_background_1b + h_ttbar_background_1c + h_ttbar_background_1l + h_ttbar_background_1b_filt + h_ttbar_background_1c_filt + h_ttbar_background_1l_filt
      h_ttbar_background_1l = h_ttbar_background_1l + h_ttbar_background_1l_filt
      h_ttbar_background_1c = h_ttbar_background_1c + h_ttbar_background_1c_filt
      h_ttbar_background_1b = h_ttbar_background_1b + h_ttbar_background_1b_filt

      #h_ttbar_background_1c.Scale(1.22)
      #h_ttbar_background_1b.Scale(1.40)
      #h_ttbar_background_1l.Scale(0.93)
      h_ttbar_background = h_ttbar_background_1b + h_ttbar_background_1c + h_ttbar_background_1l


      #h_ttbar_background.SetLineColor(kGreen)
      #h_ttbar_background.SetLineStyle(7) #3
      #if ReBin == True:
          #h_ttbar_background.Rebin(2)

      #file3   = TFile.Open("../PlotFiles/ForOptimisation/Wjets_70p.root","READ")
      file3   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_potforEB_SRCR/Comb_77p.root","READ")
      dir3    = file3.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      #dir3    = file3.GetDirectory("Nominal").GetDirectory(HistoName)
      h_W_background = dir3.Get("wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_W_background.SetLineWidth(8)
      #h_W_background.SetLineColor(kMagenta)
      #h_W_background.SetLineStyle(7) #3
      if ReBin == True:
          h_W_background.Rebin(2)
      
      #file4   = TFile.Open("../PlotFiles/ForOptimisation/diboson_70p.root","READ")
      file4   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_potforEB_SRCR/Comb_77p.root","READ")
      dir4    = file4.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      #dir4    = file4.GetDirectory("Nominal").GetDirectory(HistoName)
      h_diboson_background = dir4.Get("db_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_diboson_background.SetLineWidth(8)
      #h_diboson_background.SetLineColor(kMagenta)
      #h_diboson_background.SetLineStyle(7) #3
      if ReBin == True:
          h_diboson_background.Rebin(2)

      #file5   = TFile.Open("../PlotFiles/ForOptimisation/singleTop_70p.root","READ")
      file5   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_potforEB_SRCR/Comb_77p.root","READ")
      dir5    = file5.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      #dir5    = file5.GetDirectory("Nominal").GetDirectory(HistoName)
      h_singleTop_background_sc = dir5.Get("st_sc_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_singleTop_background.SetLineWidth(8)

      ##h_singleTop_background_sc.SetLineColor(kMagenta)
      ##h_singleTop_background_sc.SetLineStyle(3)  #3
      ##if ReBin == True:
          ##h_singleTop_background_sc.Rebin(2)

     #file5   = TFile.Open("../PlotFiles/ForOptimisation/singleTop_70p.root","READ")
      file9   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_potforEB_SRCR/Comb_77p.root","READ")
      dir9   = file9.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      #dir5    = file5.GetDirectory("Nominal").GetDirectory(HistoName)
      h_singleTop_background_tc = dir9.Get("st_tc_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_singleTop_background.SetLineWidth(8)

      file_Wt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_potforEB_SRCR/Comb_77p.root","READ")
      dir_Wt   = file_Wt.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_Wt = dir_Wt.Get("Wt_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      file_tH   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_potforEB_SRCR/Comb_77p.root","READ")
      dir_tH   = file_tH.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_tH = dir_tH.Get("tH_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      file_tWZ   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_potforEB_SRCR/Comb_77p.root","READ")
      dir_tWZ   = file_tWZ.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_tWZ = dir_tWZ.Get("tWZ_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      file_tZ   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_potforEB_SRCR/Comb_77p.root","READ")
      dir_tZ   = file_tZ.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_tZ = dir_tZ.Get("tZ_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      h_singleTop_background = h_singleTop_background_tc + h_singleTop_background_sc + h_Wt + h_tH + h_tWZ + h_tZ
      #h_singleTop_background.SetLineColor(kMagenta)
      #h_singleTop_background.SetLineStyle(7)  #3
      if ReBin == True:
          h_singleTop_background.Rebin(2) 

      file_ttW   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_potforEB_SRCR/Comb_77p.root","READ")
      #file_tt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_290121/ttbar_70p.root","READ")
      dir_ttW    = file_ttW.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_ttW_background = dir_ttW.Get("ttW_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_ttW_background.SetLineColor(kMagenta)
      #h_ttW_background.SetLineStyle(7) #3
      if ReBin == True:
          h_ttW_background.Rebin(2)

      file_ttH   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_potforEB_SRCR/Comb_77p.root","READ")
      #file_tt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_290121/ttbar_70p.root","READ")
      dir_ttH    = file_ttH.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_ttH_background = dir_ttH.Get("ttH_PP8_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")    
      #h_ttH_background.SetLineColor(kMagenta)
      #h_ttH_background.SetLineStyle(7) #3
      if ReBin == True:
          h_ttH_background.Rebin(2)

      #file8   = TFile.Open("../PlotFiles/ForOptimisation/Zjets_70p.root","READ")
      file8   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_potforEB_SRCR/Comb_77p.root","READ")
      dir8    = file8.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      #dir8    = file8.GetDirectory("Nominal").GetDirectory(HistoName)
      h_Z_background = dir8.Get("zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_Z_background.SetLineWidth(8)
      #h_Z_background.SetLineColor(kMagenta)
      #h_Z_background.SetLineStyle(7)  #3
      if ReBin == True:
          h_Z_background.Rebin(2)  

      #file8   = TFile.Open("../PlotFiles/ForOptimisation/Zjets_70p.root","READ")
      file_ttZ   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_potforEB_SRCR/Comb_77p.root","READ")
      dir_ttZ    = file_ttZ.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      #dir8    = file8.GetDirectory("Nominal").GetDirectory(HistoName)
      h_ttZ_background = dir_ttZ.Get("ttll_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_Z_background.SetLineWidth(8)
      #h_ttZ_background.SetLineColor(kMagenta)
      #h_ttZ_background.SetLineStyle(7)  #3
      if ReBin == True:
          h_ttZ_background.Rebin(2)      

      h_other_background = h_diboson_background + h_W_background + h_Z_background + h_singleTop_background + h_ttH_background + h_ttW_background + h_ttZ_background
      h_tot_background = h_other_background + h_ttbar_background
      #h_tot_background = h_ttbar_background
      h_tot_background.SetFillColor(kGray)
      h_tot_background.SetFillStyle(7)
      h_tot_background.SetLineColor(kBlack)


      nbins=20
      ymax=0
      #NormalizeHisto(h_other_background)
      #if ymax<h_other_background.GetMaximum():
          #ymax=h_other_background.GetMaximum()
      NormalizeHisto(h_tot_background)
      if ymax<h_tot_background.GetMaximum():
          ymax=h_tot_background.GetMaximum()
      NormalizeHisto(h_sig_Hplus_m400)
      if ymax<h_sig_Hplus_m400.GetMaximum():
          ymax=h_sig_Hplus_m400.GetMaximum()
      NormalizeHisto(h_sig_Hplus_m500)
      if ymax<h_sig_Hplus_m500.GetMaximum():
          ymax=h_sig_Hplus_m500.GetMaximum()
      NormalizeHisto(h_sig_Hplus_m1000)
      if ymax<h_sig_Hplus_m1000.GetMaximum():
          ymax=h_sig_Hplus_m1000.GetMaximum()
      NormalizeHisto(h_sig_Hplus_m1800)
      if ymax<h_sig_Hplus_m1800.GetMaximum():
          ymax=h_sig_Hplus_m1800.GetMaximum()
      NormalizeHisto(h_sig_Hplus_m2500)
      if ymax<h_sig_Hplus_m2500.GetMaximum():
          ymax=h_sig_Hplus_m2500.GetMaximum()
      

      h_tot_background.Draw("HIST")
      #h_ttbar_background.Draw("HISTSAME")
      h_sig_Hplus_m400.Draw("HISTSAME")
      h_sig_Hplus_m500.Draw("HISTSAME")
      h_sig_Hplus_m1000.Draw("HISTSAME")
      h_sig_Hplus_m1800.Draw("HISTSAME")
      h_sig_Hplus_m2500.Draw("HISTSAME")
      #h_sig_Hplus_m500.Draw("HISTSAME")
      #h_sig_Hplus_m2000.Draw("HISTSAME")
      #h_sig_Hplus_m2500.Draw("HISTSAME")
      #h_sig_Hplus_m3000.Draw("HISTSAME")
      #h_sig_Hplus_m1600.Draw("HISTSAME")
      #h_sig_Hplus_m2000.Draw("HISTSAME")
      #h_sig_Hplus_m1200.Draw("HISTSAME")

      if HistoName in "maxMVAResponse":
         leg = TLegend(0.25,0.55,0.825,0.755)
      else:
         leg = TLegend(0.55,0.65,0.955,0.855)
      ATLAS_LABEL(0.20,0.875,"Internal",1,0.19);
      #ATLAS_LABEL(0.20,0.80,"Simulation",1,0.09)
      leg.SetShadowColor(kWhite)
      leg.SetFillColor(kWhite)
      leg.SetLineColor(kWhite)
      
      leg.AddEntry(h_sig_Hplus_m400,    "H^{+}#rightarrow W^{+}h(m_{H^{+}} = 250 GeV)","L")
      leg.AddEntry(h_sig_Hplus_m500,    "H^{+}#rightarrow W^{+}h(m_{H^{+}} = 500 GeV)","L")
      leg.AddEntry(h_sig_Hplus_m1000,    "H^{+}#rightarrow W^{+}h(m_{H^{+}} = 1000 GeV)","L")
      #leg.AddEntry(h_sig_Hplus_m2500,    "H^{+}#rightarrow W^{+}h(m_{H^{+}} = 3000 GeV)","L")
      leg.AddEntry(h_sig_Hplus_m1800,   "H^{+}#rightarrow W^{+}h(m_{H^{+}} = 1800 GeV)","L")
      leg.AddEntry(h_sig_Hplus_m2500,   "H^{+}#rightarrow W^{+}h(m_{H^{+}} = 2500 GeV)","L")
      #leg.AddEntry(h_sig_Hplus_m2000,   "H^{+}#rightarrow W^{+}h(m_{H^{+}} = 2000GeV)","L")
      #leg.AddEntry(h_sig_Hplus_m1200,   "H^{+}#rightarrow W^{+}h(m_{H^{+}} = 1200GeV)","L")
      leg.AddEntry(h_tot_background,  "Background sum","f")
      #leg.AddEntry(h_other_background,  "other backgrounds","L")
      leg.SetTextSize(0.0250)
      leg.Draw()
      h_tot_background.GetXaxis().SetTitle(Xaxis_label)
      h_tot_background.GetYaxis().SetTitle("Event fraction")
      h_tot_background.GetYaxis().SetRangeUser(0.001,round(ymax*1.25,3)+0.001)
      #h_sig_Hplus_m400.GetXaxis().SetTitle(Xaxis_label)
      #h_sig_Hplus_m400.GetYaxis().SetRangeUser(0.001,round(ymax*1.25,3)+0.001)
      #pad1.cd()
      #myText(0.20,0.825,1,"Internal")
      myText(0.20,0.825,1,"H^{+}#rightarrow W^{+}h#rightarrow qqbb")
      c1.RedrawAxis()
      c1.Update()
      c1.RedrawAxis()
      c1.SaveAs("../Plots/ShapePlot_%s_qqbb_Kinem_EBRequest.pdf" % (HistoName+"_"+btagStrategy))
