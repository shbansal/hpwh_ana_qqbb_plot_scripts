# -*- coding: utf-8 -*-
#python
import sys
import glob
import math
import re
from ROOT import *
from array import *
from ROOT import TCanvas, TFile, TLine, TProfile, TNtuple, TH1F, TH2F
#import ROOT
#import array

gROOT.SetBatch(True)
gStyle.SetOptStat(0)
gStyle.SetPalette(1)
gROOT.LoadMacro("../style/AtlasStyle.C")
gROOT.LoadMacro("../style/AtlasUtils.C")
SetAtlasStyle()

y_axis_label="Event fraction"

c_blue   = TColor.GetColor("#3366ff")
c_red    = TColor.GetColor("#ee3311")
c_orange = TColor.GetColor("#ff9900")

def NormalizeHisto_1(histo):
     n_events=histo.Integral(-1,histo.GetNbinsX()+1)
     #n_events=histo.Integral()
     if n_events == 0:
         return
     print n_events, histo.Integral(histo.GetNbinsX(),histo.GetNbinsX()+1)
     histo.Scale(1./n_events)
     histo.SetLineWidth(3)
     histo.SetStats(0)
     histo.SetFillStyle(3001)
     histo.SetMarkerColor(histo.GetLineColor())
     histo.SetMarkerSize(0.0)
     histo.GetXaxis().SetTitleOffset(1.2)
     histo.GetYaxis().SetTitleOffset(1.52)
     histo.GetXaxis().SetLabelSize(0.05)
     histo.GetYaxis().SetLabelSize(0.05)
     histo.GetXaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetNdivisions(504)
     histo.GetXaxis().SetNdivisions(504)

def NormalizeHisto_2(histo):
     n_events=histo.Integral(-1,histo.GetNbinsX()+1)
     #n_events=histo.Integral()
     if n_events == 0:
         return
     #print histo.Integral(-1,0)    
     print n_events, histo.Integral(-1,0), histo.Integral(histo.GetNbinsX(),histo.GetNbinsX()+1)
     histo.Scale(1./n_events)
     histo.SetLineWidth(3)
     histo.SetStats(0)
     histo.SetFillStyle(3001)
     histo.SetMarkerColor(histo.GetLineColor())
     histo.SetMarkerSize(0.0)
     histo.GetXaxis().SetTitleOffset(1.2)
     histo.GetYaxis().SetTitleOffset(1.52)
     histo.GetXaxis().SetLabelSize(0.05)
     histo.GetYaxis().SetLabelSize(0.05)
     histo.GetXaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetNdivisions(504)
     histo.GetXaxis().SetNdivisions(504)

c1 = TCanvas("ShapePlots","",500,500)

HistoNameR = "lep_top_mass"

#file_in       = TFile.Open("/cephfs/user/s6subans/TRExFitter/qqlv_225_Asimov/Hp1000_qqlv/Histograms/Hp1000_qqlv_SR_qqbb_6jin4bin_histos.root","READ")
file_in       = TFile.Open("/cephfs/user/s6subans/TRExFitter/Hp600_Bkg/Histograms/Hp600_Bkg_qqbb_5jex4bin_histos.root","READ")
#h_sig_Hplus    = file_in.Get("SR_qqbb_6jin4bin_Hp1000_orig")
#h_sig_Hplus.SetLineColor(kBlue)
#h_sig_Hplus.SetLineStyle(1)

h_ttl    = file_in.Get("qqbb_5jex4bin_ttlight_orig")
h_ttb    = file_in.Get("qqbb_5jex4bin_ttb_orig")
h_ttc    = file_in.Get("qqbb_5jex4bin_ttc_orig")
errorttl=double(0.0)
errorttb=double(0.0)
errorttc=double(0.0)
ttl = h_ttl.IntegralAndError(0,16,errorttl, "")
ttb = h_ttb.IntegralAndError(0,16,errorttb, "")
ttc = h_ttc.IntegralAndError(0,16,errorttc, "")
print "yield_ttl", ttl, errorttl
print "yield_ttb", ttb, errorttb
print "yield_ttc", ttc, errorttc
#print "yield_sig", h_sig_Hplus.Integral()

h_tt_tot = h_ttl + h_ttb + h_ttc
h_tt_tot.SetLineColor(kRed)
h_tt_tot.SetLineStyle(1)




#h_sig_Hplus.Draw("HIST")
#h_tt_tot.Draw("HISTSAME")  
leg = TLegend(0.48,0.50,0.68,0.75)
ATLAS_LABEL(0.20,0.85,"Internal",1,0.19); 
#myText(0.20,0.85,1,"m = 250 GeV")  
#myText(0.20,0.80,1,"at least 6 jet, 3 b-tags (Pre-Selection)")
myText(0.20,0.80,1,"5 jet, at least 3 b-tags (lvbb SR)")
leg.SetShadowColor(kWhite)
leg.SetFillColor(kWhite)
leg.SetLineColor(kWhite)
#leg.AddEntry(h_sig_Hplus, "Hp = 1000 GeV","L")
leg.AddEntry(h_tt_tot, "tt","L")

leg.SetTextSize(0.0250)
leg.Draw()

c1.RedrawAxis()
c1.Update()
c1.RedrawAxis()
#c1.SaveAs("../Plots/tt_Sig_5jex3bin_SR_lvbb_Hp1000.pdf")
