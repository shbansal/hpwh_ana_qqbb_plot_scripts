# -*- coding: utf-8 -*-
#python
import ROOT
import sys
import glob
import math
import re
from ROOT import *
from array import *

#################----------#################################################
#This macro plots the weighted MC after two methods of reweighting overlayed
#################----------#################################################

#from ROOT import rootpy.plotting

import numpy as np
from numpy import ndarray
import array
#from rootpy.plotting import Hist, HistStack, Legend, Canvas
#from rootpy.plotting.style import get_style, set_style
#from rootpy.plotting.utils import draw
#from rootpy.interactive import wait
#import rootpy.plotting.root2matplotlib as rplt
from ROOT import TCanvas, TFile, TPad, THStack, TLine,TGraphAsymmErrors, TNtuple, TH1F, TH2F
#from ROOT import TGraphAsymmErrors
#from array import *
#from ROOT import TLatex
#import ROOT
#import array

gROOT.SetBatch(True)
gStyle.SetOptStat(0)
gStyle.SetPalette(1)
gROOT.LoadMacro("../style/AtlasStyle.C")
gROOT.LoadMacro("../style/AtlasUtils.C")
SetAtlasStyle()

from array import *
y_axis_label="Event fraction"

c_blue   = TColor.GetColor("#3366ff")
c_red    = TColor.GetColor("#ee3311")
c_orange = TColor.GetColor("#ff9900")

def NormalizeHisto(histo):
     n_events=histo.Integral(-1,histo.GetNbinsX()+1)
     if n_events == 0:
         return
     print n_events, histo.Integral(histo.GetNbinsX(),histo.GetNbinsX()+1)
     histo.Scale(1./n_events)
     histo.SetLineWidth(2)
     histo.SetStats(0)
     histo.SetFillStyle(3001)
     histo.SetMarkerColor(histo.GetLineColor())
     histo.SetMarkerSize(0.0)
     histo.GetXaxis().SetTitleOffset(1.2)
     histo.GetYaxis().SetTitleOffset(1.52)
     histo.GetXaxis().SetLabelSize(0.05)
     histo.GetYaxis().SetLabelSize(0.05)
     histo.GetXaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetNdivisions(504)
     histo.GetXaxis().SetNdivisions(504)

c1 = TCanvas("DatavMC-qqbb","",700,500)
pad1 = TPad("pad1", "pad1", 0.0, 0.35, 1,1)
#pad1.SetLogy()
pad2 = TPad("pad2", "pad2", 0, 0.0, 1,0.35)
#pad3 = TPad("pad3", "pad3", 0, 0.0, 1,0.35)
pad1.SetLeftMargin(0.15)
pad1.SetTopMargin(0.1)
pad1.SetBottomMargin(0.0105)
pad1.SetRightMargin(0.08)
pad1.Draw()
pad1.SetTicks()

pad2.SetLeftMargin(0.15)
pad2.SetTopMargin(0.04)
pad2.SetBottomMargin(0.35)
pad2.SetRightMargin(0.08)
pad2.Draw()
pad2.SetTicks()

#pad3.SetLeftMargin(0.15)
#pad3.SetTopMargin(0.001)
#pad3.SetBottomMargin(0.15)
#pad3.SetRightMargin(0.08)
#pad3.Draw()
#pad3.SetTicks()

HistoNameR = "maxMVAResponse"
#for HistoName in ["nBTags"]:
#for HistoName in ["nBTags","nJets","HT","HT_bjets","DeltaPhi_HW","mVH","mH","pTWplus","pTH","maxMVAResponse", "mass_resolution"]: # for resolved, histoname set
#for HistoName in ["nJets","HT","HT_bjets","mVH","mH","pTWplus","pTH","DeltaPhi_HW","pTH_over_mVH","pTW_over_mVH","DeltaEta_HW"]:
#for HistoName in ["mVH","nJet","DeltaPhi_HW","DeltaEta_HW","maxMVAResponse"]:
#for HistoName in ["maxMVAResponse","DeltaEta_HW","DeltaPhi_HW"]: 
#for HistoName in ["nJet"]:
#for HistoName in ["HT","pTWplus","pTH","nJet","mVH","maxMVAResponse","DeltaEta_HW","DeltaPhi_HW"]:
#for HistoName in ["pTWplus_5j", "pTH_5j","maxMVAResponse_5j","DeltaEta_HW_5j","DeltaPhi_HW_5j"]:
#for HistoName in ["pTWplus_5j", "pTH_5j"]:
#for HistoName in ["pTWplus"]:
#for HistoName in ["HT_all_5j"]:  
for HistoName in ["HT_all_5j","HT_all_6j","HT_all_7j","HT_all_8j"]:     
#for HistoName in ["mVH"]:
#for HistoName in ["nJet","maxMVAResponse","DeltaPhi_HW","pTH_over_mVH","pTW_over_mVH","DeltaEta_HW"]:
#for HistoName in ["DeltaEta_HW"]: 
#for HistoName in ["Eta_j1j2","Eta_j1j3","Eta_j1j4","Eta_j1j5","Eta_j1j6","Eta_j1j7","Eta_j2j3","Eta_j2j4","Eta_j2j5","Eta_j2j6","Eta_j2j7","Eta_j3j4","Eta_j3j5","Eta_j3j6","Eta_j3j7","Eta_j4j5","Eta_j4j6","Eta_j4j7","Eta_j5j6","Eta_j5j7","Eta_j6j7"]:
#for HistoName in ["maxMVAResponse_15","maxMVAResponse_10","maxMVAResponse_19","maxMVAResponse_18"]: 
## for bTagStrategy in ["Incl","FourPlusTags","ThreeTags","TwoTags"]:
 for Region in ["Resolved_SR"]:
  #for btagStrategy in ["TwoTags"]:
  #for btagStrategy in ["TwoTags","FourPlusTags","ThreeTags"]:
  for btagStrategy in ["FourPlusTags","ThreeTags"]:    
     
      ReBin = False
      YAxisScale = 1.4
      
      if "Eta_j1j2" in HistoName:
          Xaxis_label="\eta_{j1j2}"
      if "Eta_j1j3" in HistoName:
          Xaxis_label="\eta_{j1j3}"
      if "Eta_j1j4" in HistoName:
          Xaxis_label="\eta_{j1j4}"
      if "Eta_j1j5" in HistoName:
          Xaxis_label="\eta_{j1j5}"
      if "Eta_j1j6" in HistoName:
          Xaxis_label="\eta_{j1j6}"
      if "Eta_j1j7" in HistoName:
          Xaxis_label="\eta_{j1j7}" 
      if "Eta_j2j3" in HistoName:
          Xaxis_label="#eta_{j2j3}"
      if "Eta_j2j4" in HistoName:
          Xaxis_label="#eta_{j2j4}"
      if "Eta_j2j5" in HistoName:
          Xaxis_label="#eta_{j2j5}"
      if "Eta_j2j6" in HistoName:
          Xaxis_label="#eta_{j2j6}"
      if "Eta_j2j7" in HistoName:
          Xaxis_label="#eta_{j2j7}"
      if "Eta_j3j4" in HistoName:
          Xaxis_label="#eta_{j3j4}"
      if "Eta_j3j5" in HistoName:
          Xaxis_label="#eta_{j3j5}"
      if "Eta_j3j6" in HistoName:
          Xaxis_label="#eta_{j3j6}"
      if "Eta_j3j7" in HistoName:
          Xaxis_label="#eta_{j3j7}" 
      if "Eta_j4j5" in HistoName:
          Xaxis_label="#eta_{j4j5}"
      if "Eta_j4j6" in HistoName:
          Xaxis_label="#eta_{j4j6}"
      if "Eta_j4j7" in HistoName:
          Xaxis_label="#eta_{j4j7}"
      if "Eta_j5j6" in HistoName:
          Xaxis_label="#eta_{j5j6}"
      if "Eta_j5j7" in HistoName:
          Xaxis_label="#eta_{j5j7}"
      if "Eta_j6j7" in HistoName:
          Xaxis_label="#eta_{j6j7}"               

      if "nBTags" in HistoName:
          Xaxis_label="b-tag multiplicity"
      if "nJet" in HistoName:
          Xaxis_label="Jet multiplicity"
      if "DeltaPhi_HW" in HistoName:
          Xaxis_label="#Delta #Phi_{HW}"
      if "DeltaEta_HW" in HistoName:
          Xaxis_label="#Delta #eta_{HW}"
      if "pTWplus" in HistoName:
          Xaxis_label="Transverse momentum of W Boson [GeV]"
      if "pTH" in HistoName:
          Xaxis_label="Transverse momentum of Higgs [GeV]"    
      if  HistoName == "pTH_over_mVH":
          Xaxis_label="pT_{H}/m_{VH}"
      if  HistoName == "pTW_over_mVH":
          Xaxis_label="pT_{W}/m_{VH}"            
      if  HistoName == "pTH":
          Xaxis_label="Transverse momentum of Higgs [GeV]"
      if  HistoName == "pTWplus":
          Xaxis_label="Transverse momentum of W Boson [GeV]"
      if HistoName == "mVH":
          Xaxis_label="Mass of charged Higgs [GeV]"
      if "mH" in HistoName:
          Xaxis_label="Mass of Higgs [GeV]"
      if "mass_resolution" in HistoName:
          Xaxis_label="Mass resolution [GeV]"
      if "HT" in HistoName:
          Xaxis_label="H_{T} (Transverse momentum sum of jets) [GeV]"
      if "HT_all" in HistoName:
          Xaxis_label="H_{T}_{all} [GeV]"
      #if "HT_all_5j" in HistoName:
          #Xaxis_label="H_{T}_{all} exc. for 5 jets [GeV]"        
      if "HT_bjets" in HistoName:
          Xaxis_label="H_{T_{b-jets}} (Transverse momentum sum of b-jets) [GeV]"
      if "maxMVAResponse" in HistoName:
          Xaxis_label="BDT score (Signal reconstruction)"
      if "maxMVAResponse_15" in HistoName:
          Xaxis_label="BDT score (Signal reco)"
      if "maxMVAResponse_10" in HistoName:
          Xaxis_label="BDT score (Signal reco)"            
      #Xaxis_label=""
      
      #file_data15       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/data_leptop_NewTrain_ttRew_PSel_imp/data_2015.root_77p_225_.root","READ")
      file_data15       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/data_leptop_NewTrain_ttRew_PSel_modHT/data_2015.root_77p_225_.root","READ")
      dir_15        = file_data15.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_data15 = dir_15.Get("data_2015_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_data15.GetYaxis.SetRange(0,10e6)
    
      #file_data16       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/data_leptop_NewTrain_ttRew_PSel_imp/data_2016.root_77p_225_.root","READ")  
      file_data16       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/data_leptop_NewTrain_ttRew_PSel_modHT/data_2016.root_77p_225_.root","READ")
      dir_16        = file_data16.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_data16 = dir_16.Get("data_2016_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_data16.GetYaxis.SetRange(0,10e6)

      #file_data17       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/data_leptop_NewTrain_ttRew_PSel_imp/data_2017.root_77p_225_.root","READ")
      file_data17       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/data_leptop_NewTrain_ttRew_PSel_modHT/data_2017.root_77p_225_.root","READ")
      dir_17        = file_data17.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_data17 = dir_17.Get("data_2017_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_data17.GetYaxis.SetRangeUser(0,10e6)

      #file_data18      = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/data_leptop_NewTrain_ttRew_PSel_imp/data_2018.root_77p_225_.root","READ")
      file_data18      = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/data_leptop_NewTrain_ttRew_PSel_modHT/data_2018.root_77p_225_.root","READ")
      dir_18        = file_data18.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_data18 = dir_18.Get("data_2018_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_data18.GetYaxis.SetRangeUser(0,10e6)
      
      #file_400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_Signal_225Con_200721/hp400_AFII.root_77p_225.root","READ")
      #dir_400        = file_400.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      #h_sig_Hplus_m400 = dir_400.Get("hp400_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_sig_Hplus_m400.SetLineColor(kRed)
      #h_sig_Hplus_m400.SetLineStyle(7)
      ##h_sig_Hplus_m400.Scale(139*1000000)
      #if ReBin == True:
          #h_sig_Hplus_m400.Rebin(2)

      
      #file_800       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_Signal_225Con_200721/hp800_AFII.root_77p_225.root","READ")
      #dir_800       = file_800.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      #h_sig_Hplus_m800 = dir_800.Get("hp800_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_sig_Hplus_m800.SetLineColor(kBlack)
      #h_sig_Hplus_m800.SetLineStyle(7)
      ##h_sig_Hplus_m800.Scale(139*1000000)
      #if ReBin == True:
          #h_sig_Hplus_m800.Rebin(2)


      #file_1600	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_Signal_225Con_200721/hp1600_AFII.root_77p_225.root","READ")
      #dir_1600        = file_1600.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      #h_sig_Hplus_m1600 = dir_1600.Get("hp1600_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_sig_Hplus_m1600.SetLineColor(kBlue)
      #h_sig_Hplus_m1600.SetLineStyle(7)
      ##h_sig_Hplus_m1600.Scale(139*1000000)
      #if ReBin == True:
          #h_sig_Hplus_m1600.Rebin(2)
        
      #file_tt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_imp/tt_PP8.root_77p_225.root","READ")
      file_tt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_modHT/tt_PP8.root_77p_225.root","READ")
      #file_tt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_290121/ttbar_70p.root","READ")
      dir_tt_1b   = file_tt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1b")
      #h_ttbar_background_1b = dir_tt_1b.Get("tt_PP8_1B_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttbar_background_1b = dir_tt_1b.Get("tt_PP8_"+HistoName+"_1b_"+Region+"_"+btagStrategy+"_nominal_Loose")
      dir_tt_1c   = file_tt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1c")
      #h_ttbar_background_1c = dir_tt_1c.Get("tt_PP8_1C_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttbar_background_1c = dir_tt_1c.Get("tt_PP8_"+HistoName+"_1c_"+Region+"_"+btagStrategy+"_nominal_Loose")
      dir_tt_1l   = file_tt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1l")
      #h_ttbar_background_1l = dir_tt_1l.Get("tt_PP8_1L_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttbar_background_1l = dir_tt_1l.Get("tt_PP8_"+HistoName+"_1l_"+Region+"_"+btagStrategy+"_nominal_Loose")

      #file_tt_filt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_imp/tt_PP8filtered.root_77p_225.root","READ")
      file_tt_filt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_modHT/tt_PP8filtered.root_77p_225.root","READ")
      dir_tt_1b_filt   = file_tt_filt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1b")
      #h_ttbar_background_1b_filt = dir_tt_1b_filt.Get("tt_PP8filtered_1B_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttbar_background_1b_filt = dir_tt_1b_filt.Get("tt_PP8filtered_"+HistoName+"_1b_"+Region+"_"+btagStrategy+"_nominal_Loose")
      dir_tt_1c_filt   = file_tt_filt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1c")
      #h_ttbar_background_1c_filt = dir_tt_1c_filt.Get("tt_PP8filtered_1C_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttbar_background_1c_filt = dir_tt_1c_filt.Get("tt_PP8filtered_"+HistoName+"_1c_"+Region+"_"+btagStrategy+"_nominal_Loose")
      dir_tt_1l_filt   = file_tt_filt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1l")
      #h_ttbar_background_1l_filt = dir_tt_1l_filt.Get("tt_PP8filtered_1L_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_ttbar_background_1l_filt = dir_tt_1l_filt.Get("tt_PP8filtered_"+HistoName+"_1l_"+Region+"_"+btagStrategy+"_nominal_Loose")

    
      
      h_ttbar_background_1l = h_ttbar_background_1l + h_ttbar_background_1l_filt
      h_ttbar_background_1b = h_ttbar_background_1b + h_ttbar_background_1b_filt
      h_ttbar_background_1c = h_ttbar_background_1c + h_ttbar_background_1c_filt
      #h_ttbar_background_1b.Scale(1.40)
      #h_ttbar_background_1c.Scale(1.22)
      #h_ttbar_background_1l.Scale(0.93)

      #h_ttbar_background_1b.Scale(1.38)
      #h_ttbar_background_1c.Scale(0.84)
      h_ttbar_background = h_ttbar_background_1b + h_ttbar_background_1c + h_ttbar_background_1l 
      h_ttbar_background_tb = h_ttbar_background.Clone()
      h_ttbar_background_1b_tb = h_ttbar_background_1b.Clone()
      h_ttbar_background_1l_tb = h_ttbar_background_1l.Clone()
      h_ttbar_background_1c_tb = h_ttbar_background_1c.Clone()
      #h_ttbar_background_1b_tb.Scale(1.38)
      #h_ttbar_background_1c_tb.Scale(0.84)

      #########################Hyperbola+Sigmoid Functions################################
      #Rew = TF1("Rew","0.938 + 1033053.698/(x**2.924) - 0.241/(1+exp(11.994 - 0.011*x))",0,2000)   #5 jet
      #Rew = TF1("Rew","0.937 + 3757191.533/(x**3.011) - 0.325/(1+exp(6.707 - 0.006*x))",0,2000)    #6 jet
      #Rew = TF1("Rew","0.811 + 2118.453/(x**1.506) - 0.255/(1+exp(5.868 - 0.004*x))",0,2000)       #7 jet
      #Rew = TF1("Rew","0.777 + 3208.699/(x**1.505) - 0.233/(1+exp(8.134 - 0.006*x))",0,2000)       #8 jet
      #Rew = TF1("Rew","0.891 + 506302.812/(x**2.328) - 0.126/(1+exp(20.733 - 0.019*x))",0,2000)    #ge9 jet
      #Rew = TF1("Rew","0.966 + 577434.102/(x**2.831) - 0.273/(1+exp(5.833 - 0.005*x))",0,3500)    #ge5 jet
      ####################################################################################

      #########################Exponential+Sigmoid Functions################################
      #Rew = TF1("Rew","0.688 + 3.269*exp(-0.014*x) - (-0.263)/(1+exp(-9.769 - (-0.009)*x))",0,2000)   #5 jet
      #Rew = TF1("Rew","0.592 + 5.441*exp(-0.013*x) - (-0.378)/(1+exp(-5.300 - (-0.005)*x))",0,2000)    #6 jet
      #Rew = TF1("Rew","0.442 + 2.446*exp(-0.008*x) - (-0.574)/(1+exp(-2.710 - (-0.002)*x))",0,2000)       #7 jet
      #Rew = TF1("Rew","0.571 + 2.361*exp(-0.005*x) - (-0.329)/(1+exp(-6.273 - (-0.005)*x))",0,2000)       #8 jet
      #Rew = TF1("Rew","0.786 + 4.902*exp(-0.006*x) - (-0.165)/(1+exp(-17.300 - (-0.016)*x))",0,2000)    #ge9 jet
      ####################################################################################

      #########################Hyperbola+Sigmoid Functions for HT_all distributions without any norm contribution################################
      #Rew = TF1("Rew","0.598 + 173433239.982/(x**3.598) - (-0.331)/(1+exp(-7.336 - (-0.005)*x))",0,2000)   #5 jet
      #Rew = TF1("Rew","0.926 + 54110747.299/(x**3.283) - 0.421/(1+exp(6.184 - 0.004*x))",0,2000)    #6 jet
      #Rew = TF1("Rew","1.031 + 35793248.291/(x**3.118) - 33.499/(1+exp(6.482 - 0.001*x))",0,2000)       #7 jet
      #Rew = TF1("Rew","0.869 + 66112.487/(x**1.942) - 0.160/(1+exp(12.414 - 0.008*x))",0,2000)       #ge8 jet
      ####################################################################################  

      #########################Hyperbola+Sigmoid Functions for HT_all distributions with norm contribution from H+tb analysis################################
      #Rew = TF1("Rew","0.594 + 141186348.143/(x**3.560) - (-0.338)/(1+exp(-7.240 - (-0.005)*x))",0,2000)   #5 jet
      #Rew = TF1("Rew","0.929 + 47710259.344/(x**3.260) - 0.429/(1+exp(6.113 - 0.004*x))",0,2000)    #6 jet
      #Rew = TF1("Rew","1.032 + 28848057.308/(x**3.080) - 30.621/(1+exp(6.411 - 0.001*x))",0,2000)       #7 jet
      #Rew = TF1("Rew","0.867 + 48828.631/(x**1.889) - 0.160/(1+exp(12.555 - 0.008*x))",0,2000)       #ge8 jet
      #################################################################################### 
      
      #########################Hyperbola+Sigmoid Functions for HT_all distributions without any norm contribution################################
      if "HT_all_5j" in HistoName:
           Rew = TF1("Rew","0.605 + 34834290.982/(x**3.426) - (-0.326)/(1+exp(-7.489 - (-0.006)*x))",0,2000)
      if "HT_all_6j" in HistoName:
           Rew = TF1("Rew","0.928 + 20467135.633/(x**3.205) - 0.360/(1+exp(6.305 - 0.005*x))",0,2000) 
      if "HT_all_7j" in HistoName:
           Rew = TF1("Rew","0.863 + 132525.193/(x**2.188) - 0.219/(1+exp(10.641 - 0.008*x))",0,2000)   
      if "HT_all_8j" in HistoName:
           Rew = TF1("Rew","0.850 + 13399.837/(x**1.712) - 0.140/(1+exp(15.310 - 0.011*x))",0,2000)  

      #########################Hyperbola+Sigmoid Functions for HT_all distributions with norm contribution from H+tb analysis#####################
      #if "HT_all_5j" in HistoName:
           #Rew_tb = TF1("Rew_tb","0.594 + 141186348.143/(x**3.560) - (-0.338)/(1+exp(-7.240 - (-0.005)*x))",0,2000)
      #if "HT_all_6j" in HistoName:
           #Rew_tb = TF1("Rew_tb","0.929 + 47710259.344/(x**3.260) - 0.429/(1+exp(6.113 - 0.004*x))",0,2000)
      #if "HT_all_7j" in HistoName:
           #Rew_tb = TF1("Rew_tb","1.032 + 28848057.308/(x**3.080) - 30.621/(1+exp(6.411 - 0.001*x))",0,2000)
      #if "HT_all_8j" in HistoName:
           #Rew_tb = TF1("Rew_tb","0.867 + 48828.631/(x**1.889) - 0.160/(1+exp(12.555 - 0.008*x))",0,2000)
      #######################################################################################################  

      #########################Hyperbola+Sigmoid Functions for HT_all distributions with norm contribution via bcfloat, ttcon#####################
      if "HT_all_5j" in HistoName:
           Rew_tb = TF1("Rew_tb","0.931 + 80580988.124/(x**3.591) - 27973.09/(1+exp(14.52 - 0.002*x))",0,2000)
      if "HT_all_6j" in HistoName:
           Rew_tb = TF1("Rew_tb","0.878 + 9397590.530/(x**3.067) - 0.354/(1+exp(6.101 - 0.005*x))",0,2000)
      if "HT_all_7j" in HistoName:
           Rew_tb = TF1("Rew_tb","0.800 + 86815.502/(x**2.117) - 0.215/(1+exp(10.017 - 0.008*x))",0,2000)
      if "HT_all_8j" in HistoName:
           Rew_tb = TF1("Rew_tb","0.770 + 11112.111/(x**1.682) - 0.135/(1+exp(14.400 - 0.010*x))",0,2000)
      ####################################################################################################### 
      
      #########################Hyperbola+Sigmoid Functions for HT_all distributions with norm contribution via all float from nBTags#####################
      #if "HT_all_5j" in HistoName:
            #Rew_allfnB = TF1("Rew_allfnB","0.585 + 7628568.265/(x**3.132) - (-0.359)/(1+exp(-6.884 - (-0.005)*x))",0,2000)
      #if "HT_all_6j" in HistoName:
            #Rew_allfnB = TF1("Rew_allfnB","0.921 + 6846955.340/(x**2.997) - 0.377/(1+exp(6.027 - 0.005*x))",0,2000)
      #if "HT_all_7j" in HistoName:
            #Rew_allfnB = TF1("Rew_allfnB","0.830 + 75501.481/(x**2.081) - 0.227/(1+exp(9.872 - 0.008*x))",0,2000)
      #if "HT_all_8j" in HistoName:
            #Rew_allfnB = TF1("Rew_allfnB","0.782 + 8675.097/(x**1.627) - (0.138)/(1+exp(14.654 - 0.011*x))",0,2000)
      #######################################################################################################

      #########################Hyperbola+Sigmoid Functions for HT_all distributions with norm contribution via all float#####################
      if "HT_all_5j" in HistoName:
           Rew_allf = TF1("Rew_allf","0.587 + 9131519.075/(x**3.166) - (-0.356)/(1+exp(-6.942 - (-0.005)*x))",0,2000)
      if "HT_all_6j" in HistoName:
           Rew_allf = TF1("Rew_allf","0.925 + 7829957.660/(x**3.022) - 0.377/(1+exp(6.047 - 0.005*x))",0,2000)
      if "HT_all_7j" in HistoName:
           Rew_allf = TF1("Rew_allf","0.837 + 81245.698/(x**2.094) - 0.228/(1+exp(9.878 - 0.008*x))",0,2000)
      if "HT_all_8j" in HistoName:
           Rew_allf = TF1("Rew_allf","0.793 + 8864.287/(x**1.631) - (0.139)/(1+exp(14.687 - 0.011*x))",0,2000)
      ####################################################################################################### 

      

      #########################Hyperbola+Sigmoid Functions for HT_all distributions with norm contribution via all HF component float#####################
      #if "HT_all_5j" in HistoName:
           #Rew = TF1("Rew","0.585 + 40357466.753/(x**3.325) - (-0.358)/(1+exp(-6.955 - (-0.005)*x))",0,2000)
      #if "HT_all_6j" in HistoName:
           #Rew = TF1("Rew","0.924 + 19589230.351/(x**3.095) - 0.439/(1+exp(5.938 - 0.004*x))",0,2000)
      #if "HT_all_7j" in HistoName:
           #Rew = TF1("Rew","1.012 + 15127845.476/(x**2.964) - 3.349/(1+exp(4.190 - 0.001*x))",0,2000)
      #if "HT_all_8j" in HistoName:
           #Rew = TF1("Rew","0.819 + 43195.465/(x**1.862) - 0.161/(1+exp(11.765 - 0.008*x))",0,2000)
      #######################################################################################################   

      #Rew = TF1("Rew","1.0112+ (-0.00011)*x + -5.618e-08*x*x + 2.531*exp(-0.008*x)",0,1000)
      #print 2**2.924
      #weight      = array( 'd', []*6)
      nbins = h_ttbar_background.GetNbinsX()
      #weight      = array( 'd', [0.0]*40)
      

      for i in range (0,nbins):         
          scale_bin= Rew.Eval(h_ttbar_background.GetBinCenter(i+1))
          scale_bin_tb= Rew_allf.Eval(h_ttbar_background_tb.GetBinCenter(i+1))
          h_ttbar_background_tb.SetBinContent(i+1,scale_bin_tb*h_ttbar_background_tb.GetBinContent(i+1))
          h_ttbar_background.SetBinContent(i+1,scale_bin*h_ttbar_background.GetBinContent(i+1))

          scale_bin_1b= Rew.Eval(h_ttbar_background_1b.GetBinCenter(i+1))
          scale_bin_1b_tb= Rew_allf.Eval(h_ttbar_background_1b_tb.GetBinCenter(i+1))
          h_ttbar_background_1b.SetBinContent(i+1,scale_bin_1b*h_ttbar_background_1b.GetBinContent(i+1))     
          h_ttbar_background_1b_tb.SetBinContent(i+1,scale_bin_1b_tb*h_ttbar_background_1b_tb.GetBinContent(i+1))

          scale_bin_1c= Rew.Eval(h_ttbar_background_1c.GetBinCenter(i+1))
          scale_bin_1c_tb= Rew_allf.Eval(h_ttbar_background_1c_tb.GetBinCenter(i+1))
          h_ttbar_background_1c.SetBinContent(i+1,scale_bin_1c*h_ttbar_background_1c.GetBinContent(i+1)) 
          h_ttbar_background_1c_tb.SetBinContent(i+1,scale_bin_1c_tb*h_ttbar_background_1c_tb.GetBinContent(i+1))

          scale_bin_1l= Rew.Eval(h_ttbar_background_1l.GetBinCenter(i+1))
          scale_bin_1l_tb= Rew_allf.Eval(h_ttbar_background_1l_tb.GetBinCenter(i+1))
          h_ttbar_background_1l.SetBinContent(i+1,scale_bin_1l*h_ttbar_background_1l.GetBinContent(i+1))
          h_ttbar_background_1l_tb.SetBinContent(i+1,scale_bin_1l_tb*h_ttbar_background_1l_tb.GetBinContent(i+1))
      
      h_ttbar_background_V2 = h_ttbar_background_1b +  h_ttbar_background_1c + h_ttbar_background_1l  
      h_ttbar_background_V2_tb = h_ttbar_background_1b_tb +  h_ttbar_background_1c_tb + h_ttbar_background_1l_tb 
      #h_ttbar_background_1b.Scale(1.40)
      #h_ttbar_background_1c.Scale(1.22)  

      #h_ttbar_background_1c_tb.SetFillColor(kBlue-9)
      #h_ttbar_background_1c_tb.SetLineColor(kBlack)
      #h_ttbar_background_1c_tb.SetLineWidth(1)

      #h_ttbar_background_1l_tb.SetFillColor(kGray)
      #h_ttbar_background_1l_tb.SetLineColor(kBlack)
      #h_ttbar_background_1l_tb.SetLineWidth(1)

      #h_ttbar_background_1b_tb.SetFillColor(kBlue-5)
      #h_ttbar_background_1b_tb.SetLineColor(kBlack)
      #h_ttbar_background_1b_tb.SetLineWidth(1)

      #h_ttbar_background.SetFillColor(kBlue-10)
      #h_ttbar_background.SetLineColor(kBlack)
      #h_ttbar_background.SetLineWidth(1) 

      
      
      #file_dib   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_imp/db.root_77p_225.root","READ")
      file_dib   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_modHT/db.root_77p_225.root","READ")
      dir_dib   = file_dib.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_diboson_background = dir_dib.Get("db_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
         
      #file_st_tc   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_imp/st_tc.root_77p_225.root","READ")
      file_st_tc   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_modHT/st_tc.root_77p_225.root","READ")
      dir_st_tc   = file_st_tc.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_singleTop_background_tc = dir_st_tc.Get("st_tc_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      
      #file_Wt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_imp/Wt.root_77p_225.root","READ")
      file_Wt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_modHT/Wt.root_77p_225.root","READ")
      dir_Wt   = file_Wt.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_Wt = dir_Wt.Get("Wt_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      #file_tH   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_imp/tH_AFII.root_77p_225.root","READ")
      file_tH   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_modHT/tH_AFII.root_77p_225.root","READ")
      dir_tH   = file_tH.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_tH = dir_tH.Get("tH_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      #file_tWZ   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_imp/tWZ.root_77p_225.root","READ")
      file_tWZ   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_modHT/tWZ.root_77p_225.root","READ")
      dir_tWZ   = file_tWZ.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_tWZ = dir_tWZ.Get("tWZ_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      #file_tZ   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_imp/tZ.root_77p_225.root","READ")
      file_tZ   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_modHT/tZ.root_77p_225.root","READ")
      dir_tZ   = file_tZ.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_tZ = dir_tZ.Get("tZ_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      #file_st_sc   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_imp/st_sc.root_77p_225.root","READ")
      file_st_sc   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_modHT/st_sc.root_77p_225.root","READ")
      dir_st_sc   = file_st_sc.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_singleTop_background_sch = dir_st_sc.Get("st_sc_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      #file_Zjet   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_imp/zjets_77p_225.root","READ")
      file_Zjet   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_modHT/zjets_77p_225.root","READ")
      dir_Zjet    = file_Zjet.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_Z_background = dir_Zjet.Get("zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      

      #file_Wjet   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_imp/wjets_77p_225.root","READ")
      file_Wjet   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_modHT/wjets_77p_225.root","READ")
      dir_Wjet    = file_Wjet.GetDirectory("nominal_Loose").GetDirectory(HistoName)        
      h_W_background = dir_Wjet.Get("wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
              
      #file_ttW   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_imp/ttW.root_77p_225.root","READ")
      file_ttW   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_modHT/ttW.root_77p_225.root","READ")
      dir_ttW    = file_ttW.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_ttW_background = dir_ttW.Get("ttW_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      

      #file_ttH   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_imp/ttH.root_77p_225.root","READ")
      file_ttH   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_modHT/ttH.root_77p_225.root","READ")
      dir_ttH    = file_ttH.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_ttH_background = dir_ttH.Get("ttH_PP8_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      

      #file_ttZ   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_imp/ttll.root_77p_225.root","READ")
      file_ttZ   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_modHT/ttll.root_77p_225.root","READ")
      dir_ttZ    = file_ttZ.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_ttZ_background = dir_ttZ.Get("ttll_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      h_singleTop_background = h_singleTop_background_tc + h_singleTop_background_sch + h_Wt + h_tH + h_tWZ + h_tZ
      h_mc = h_diboson_background + h_Z_background + h_ttbar_background_V2 + h_singleTop_background + h_W_background + h_ttW_background + h_ttH_background + h_ttZ_background
      h_mc_tb = h_diboson_background + h_Z_background + h_ttbar_background_V2_tb + h_singleTop_background + h_W_background + h_ttW_background + h_ttH_background + h_ttZ_background

      h_ttbar_background_1l.SetFillColor(kWhite)
      h_ttbar_background_1l.SetLineColor(kBlack)
      h_ttbar_background_1l.SetLineWidth(1)

      h_ttbar_background_1c.SetFillColor(kBlue-10)
      h_ttbar_background_1c.SetLineColor(kBlack)
      h_ttbar_background_1c.SetLineWidth(1)

      h_ttbar_background_1b.SetFillColor(kBlue-7)
      h_ttbar_background_1b.SetLineColor(kBlack)
      h_ttbar_background_1b.SetLineWidth(1)    

      h_diboson_background.SetFillColor(kYellow)
      h_diboson_background.SetLineColor(kBlack)
      #h_diboson_background.Scale(139*1000000)
      h_diboson_background.SetLineWidth(1)
      if ReBin == True:
          h_diboson_background.Rebin(2) 

      
      #h_singleTop_background = h_singleTop_background_tc + h_singleTop_background_sch
      h_singleTop_background.SetFillColor(kGreen-7)
      h_singleTop_background.SetLineColor(kBlack)
      #h_singleTop_background.Scale(139*1000000)
      h_singleTop_background.SetLineWidth(1)
      if ReBin == True:
          h_singleTop_background.Rebin(2)

      h_Z_background.SetFillColor(kRed-1)
      h_Z_background.SetLineColor(kBlack)
      h_Z_background.SetLineWidth(1)
      if ReBin == True:
          h_Z_background.Rebin(2)

      h_W_background.SetFillColor(kRed-8)
      h_W_background.SetLineColor(kBlack)
      h_W_background.SetLineWidth(1)
      if ReBin == True:
          h_W_background.Rebin(2)

      h_ttW_background.SetFillColor(kMagenta)
      h_ttW_background.SetLineColor(kBlack)
      h_ttW_background.SetLineWidth(1)
      if ReBin == True:
          h_ttW_background.Rebin(2)

      h_ttH_background.SetFillColor(kTeal)
      h_ttH_background.SetLineColor(kBlack)
      h_ttH_background.SetLineWidth(1)
      if ReBin == True:
          h_ttH_background.Rebin(2)

      h_ttZ_background.SetFillColor(kOrange+1)
      h_ttZ_background.SetLineColor(kBlack)
      h_ttZ_background.SetLineWidth(1)
      if ReBin == True:
          h_ttZ_background.Rebin(2)    


      h_data = h_data15 + h_data16 + h_data17 + h_data18
      #h_data =  h_data1518
      #h_data = h_data15 + h_data16
      #h_data = h_data18
            
      #h_data = h_data17
      h_data.SetMarkerStyle(20)
      h_data.SetMarkerSize(1.0)
      h_data.SetMarkerColor(1)
      stack = THStack()

      #stack.Add(h_sig_Hplus_m400)
      #stack.Add(h_sig_Hplus_m800)
      #stack.Add(h_sig_Hplus_m1600)  

      #stack.Add(h_W_background)
      h_mc.SetLineColor(kRed)
      h_mc.SetLineStyle(7)
      h_mc_tb.SetLineColor(kBlue)
      h_mc_tb.SetLineStyle(7)

      stack.Add(h_ttH_background)
      stack.Add(h_ttZ_background)
      stack.Add(h_ttW_background)
      stack.Add(h_diboson_background)
      stack.Add(h_Z_background)
      stack.Add(h_W_background)
      stack.Add(h_singleTop_background)
      #stack.Add(h_ttbar_background)
      stack.Add(h_ttbar_background_1b)
      stack.Add(h_ttbar_background_1c)
      stack.Add(h_ttbar_background_1l)
      pad1.cd()
      
      #pad1.SetMaximum(h_data.GetYaxis())
      #y2 = h_ttbar_background.GetMaximum()+(h_ttbar_background.GetMaximum()*0.8)
      if "Inclusive" in btagStrategy:
         stack.SetMaximum(stack.GetMaximum())
      if "TwoTags" in btagStrategy:
         stack.SetMaximum(stack.GetMaximum())
      if "ThreeTags" in btagStrategy :
         stack.SetMaximum (stack.GetMaximum()*1.5)
      if "FourPlusTags" in btagStrategy :
         stack.SetMaximum (stack.GetMaximum()*1.7)

      #h_mc.Draw("")
      #h_mc_tb.Draw("SAME")
      stack.Draw("HIST")
      h_mc.Draw("HISTSAME")
      h_mc_tb.Draw("HISTSAME")
      h_data.Draw("EP SAME")
      
      stack.GetYaxis().SetTitleOffset(0.9)
      stack.GetYaxis().SetTitle("Events")
      nbins=20
      ymax=0
      #h_sig_Hplus_m400.Draw("HISTSAME")
      #h_sig_Hplus_m800.Draw("HISTSAME")
      #h_sig_Hplus_m1600.Draw("HISTSAME")
      #yield_H800 = h_sig_Hplus_m800.Integral()
      #yield_H400 = h_sig_Hplus_m400.Integral()
      #yield_H1600 = h_sig_Hplus_m1600.Integral()
      
      #print yield_H400
      #print yield_H800
      #print yield_H1600

      #yield_tt = h_ttbar_background.Integral()
      #str_yield_tt = str(round(yield_tt,1))
      yield_tt_1b = h_ttbar_background_1b.Integral()
      
      str_yield_tt_1b = str(round(yield_tt_1b,1))
      yield_tt_1c = h_ttbar_background_1c.Integral()
      
      str_yield_tt_1c = str(round(yield_tt_1c,1))
      yield_tt_1l = h_ttbar_background_1l.Integral()
      
      str_yield_tt_1l = str(round(yield_tt_1l,1))
      yield_tt = h_ttbar_background.Integral()
      
      str_yield_tt = str(round(yield_tt,1))

      yield_Z = h_Z_background.Integral()
      
      str_yield_Z = str(round(yield_Z,1))
      yield_W = h_W_background.Integral()
      
      str_yield_W = str(round(yield_W,1))
      yield_SingleTop = h_singleTop_background.Integral()
      
      str_yield_SingleTop = str(round(yield_SingleTop,1))
      yield_diBoson = h_diboson_background.Integral()
      
      str_yield_diBoson = str(round(yield_diBoson,1))
      yield_ttH = h_ttH_background.Integral()
      
      str_yield_ttH = str(round(yield_ttH,1))
      yield_ttW = h_ttW_background.Integral()
      
      str_yield_ttW = str(round(yield_ttW,1))
      yield_ttZ = h_ttZ_background.Integral()
      
      str_yield_ttZ = str(round(yield_ttZ,1))

      #yield_Total = yield_tt_1b+yield_tt_1c+yield_tt_1l+yield_Z+yield_SingleTop+yield_diBoson+yield_ttH+yield_ttW
      yield_Total = yield_tt_1b+yield_tt_1c+yield_tt_1l+yield_Z+yield_SingleTop+yield_diBoson+yield_W+yield_ttH+yield_ttW+yield_ttZ
      
      str_yield_Total = str(round(yield_Total,1))

      print yield_tt_1l
      print yield_tt_1c
      print yield_tt_1b
      print yield_SingleTop
      print yield_Z
      print yield_W
      print yield_ttH
      print yield_ttW
      print yield_ttZ
      print yield_diBoson
      print yield_Total

      yield_Data = h_data.Integral()
      str_yield_Data = str(round(yield_Data,2))
      #if "ThreeTags" or "FourPlusTags" in btagStrategy:
        #leg = TLegend(0.70,0.45,0.825,0.860)
      #else :
        #leg = TLegend(0.65,0.45,0.825,0.855)
      if "maxMVAResponse" in HistoName:  
         leg = TLegend(0.25,0.40,0.425,0.805)  
      elif "DeltaPhi_HW" in HistoName:  
         leg = TLegend(0.25,0.40,0.425,0.805)   
      elif "nJet" in HistoName:  
         leg = TLegend(0.74,0.25,0.875,0.655)   
      else:
         leg = TLegend(0.62,0.25,0.855,0.685) 
         #leg = TLegend(0.65,0.45,0.825,0.855)  
      ATLAS_LABEL(0.47,0.80,"Internal",1,0.09)
      #myText(0.785,0.825,1,"139 fb^{-1}")
      
      leg.SetShadowColor(kWhite)
      leg.SetFillColor(kWhite)
      leg.SetLineColor(kWhite)
      leg.SetNColumns(2)
      leg.AddEntry(h_data,"Data","epl")
      leg.AddEntry(None, str_yield_Data,"")
      leg.AddEntry(h_ttbar_background_1l,"t#bar{t}+ light","f")
      leg.AddEntry(None, str_yield_tt_1l, "")
      leg.AddEntry(h_ttbar_background_1c,"t#bar{t}+ >=1c","f")
      leg.AddEntry(None, str_yield_tt_1c, "")
      leg.AddEntry(h_ttbar_background_1b,"t#bar{t}+ >=1b","f")
      leg.AddEntry(None, str_yield_tt_1b, "")
      #leg.AddEntry(h_ttbar_background,"t#bar{t}+ jet","f")
      #leg.AddEntry(None, str_yield_tt, "")
      leg.AddEntry(h_singleTop_background, "single Top","f")
      leg.AddEntry(None, str_yield_SingleTop, "")
      
      leg.AddEntry(h_W_background,"W+jet","f")
      leg.AddEntry(None, str_yield_W, "")
      leg.AddEntry(h_Z_background,"Z+jet","f")
      leg.AddEntry(None, str_yield_Z, "")
      leg.AddEntry(h_ttW_background,"t#bar{t}+W","f")
      leg.AddEntry(None, str_yield_ttW, "")
      leg.AddEntry(h_ttH_background,"t#bar{t}+H","f")
      leg.AddEntry(None, str_yield_ttH, "")
      leg.AddEntry(h_diboson_background, "diboson","f")
      leg.AddEntry(None, str_yield_diBoson, "") 
      leg.AddEntry(h_ttZ_background,"t#bar{t}+Z","f")
      leg.AddEntry(None, str_yield_ttZ, "")
      
      #leg.AddEntry(h_W_background,"W+jet","f")
      #leg.AddEntry(None, str_yield_W, "") 
      leg.AddEntry(None, "Total","")
      leg.AddEntry(None, str_yield_Total, "")

      leg.AddEntry(h_mc,"Rew. bkg.","L")
      leg.AddEntry(None, "", "") 
      leg.AddEntry(h_mc_tb, "Rew. bkg.(tt+HF NF app. while rew.)","L")
      leg.AddEntry(None, "", "")

      #leg.AddEntry(h_sig_Hplus_m400, "400 GeV H^{+}","L")
      #leg.AddEntry(h_sig_Hplus_m800, "800 GeV H^{+}","L")
      #leg.AddEntry(h_sig_Hplus_m1600,"1600 GeV H^{+}","L")
      #leg.AddEntry(h_data,"Data","epl")
      leg.SetTextSize(0.0350)
      ##leg2.SetTextSize(0.0350)
      leg.Draw()
      
      #h_mc = h_diboson_background + h_Z_background + h_ttbar_background + h_singleTop_background + h_W_background + h_ttW_background + h_ttH_background + h_ttZ_background
      

      #leg2.Draw()     
      pad2.SetGrid()
      pad2.cd()    
      #prob         = array ('d', [0.0]*1)
      #prob         = h_data.Chi2TestX (h_mc, chi2, ndf, igood, option = "WW",res = 0)
      #print "Probability", prob
      h_ratio = h_data.Clone()
      h_ratio_tb = h_data.Clone()

      h_ratio.Divide(h_mc)
      h_ratio_tb.Divide(h_mc_tb)

      if "TwoTags" in btagStrategy:
        h_ratio.GetYaxis().SetRangeUser(0.70,1.30)
      if "TwoTags" in btagStrategy and "nJet" in HistoName:
        h_ratio.GetYaxis().SetRangeUser(0,2)  
      if "ThreeTags" in btagStrategy :
        h_ratio.GetYaxis().SetRangeUser(0.50,1.50)
      if "FourPlusTags" in btagStrategy :
        h_ratio.GetYaxis().SetRangeUser(0.50,1.50)    
      h_ratio.SetStats(0)
      h_ratio.SetMarkerStyle(20)
      h_ratio.SetMarkerSize(1.0)
      h_ratio.SetMarkerColor(632)
      h_ratio_tb.SetStats(0)
      h_ratio_tb.SetMarkerStyle(20)
      h_ratio_tb.SetMarkerSize(1.0)
      h_ratio_tb.SetMarkerColor(600)
      h_dratio = h_ratio.Clone()
      h_dratio.Divide(h_ratio_tb)
      h_dratio.SetStats(0)
      h_dratio.SetMarkerStyle(20)
      h_dratio.SetMarkerSize(1.0)
      h_dratio.SetMarkerColor(kGreen)
      h_ratio.SetLabelSize(0.082,"X")
      h_ratio.SetLabelSize(0.082,"Y")
      h_ratio.SetTitleSize(0.082,"X")
      h_ratio.SetTitleSize(0.082,"Y")
      h_ratio.SetTitleOffset(0.48,"Y")
      h_ratio.SetTitleOffset(1.2,"X")
      h_ratio.GetYaxis().SetNdivisions(505)
      h_ratio.SetTickLength(0.005,"X") #0.05
      h_ratio.SetLabelOffset(0.01,"X")
      h_ratio.GetXaxis().SetTitle(Xaxis_label)
      h_ratio.GetYaxis().SetTitle("Data/Pred.")
      h_ratio.Draw("EP")
      h_ratio_tb.Draw("EPSAME")
      h_dratio.Draw("EPSAME")


      #stat= TGraphAsymmErrors(nbins,np.array(x_values),np.array(y_values),np.array(x_error_down),np.array(x_error_up),np.array(y_error_down),np.array(y_error_up))
      #print x_values
      

      #leg.AddEntry(totsys,"total uncertainty","f")
      

      
      #h_other_background.Draw("HIST")
      #h_ttbar_background.Draw("HISTSAME")
      

      #if HistoName in "maxMVAResponse":
         #leg = TLegend(0.2,0.65,0.725,0.855)
      #else:
         #leg = TLegend(0.45,0.65,0.925,0.855)
      #ATLAS_LABEL(0.20,0.885," Simulation Internal",1,0.19);
      
      
      #h_other_background.GetYaxis().SetRangeUser(0.001,round(ymax*1.25,3)+0.001)

      #legend = Legend([h1, h2, h3], leftmargin=0.45, margin=0.3)
      #legend.Draw()
      #canvas.Modified()
      #canvas.Update()
      pad1.cd()
      myText(0.47,0.75,1,"#sqrt{s}=13 TeV, 139 fb^{-1}")
      #output      = array( 'd', [0.0]*1)
      #output = h_data.Chi2Test(h_mc, "UW CHI2/NDF")
      #str_redchi2 = str(round(output,2))      
      if "TwoTags" in btagStrategy:
          #myText(0.45,0.70,1,"l^{-}+jets Resolved: 5 jet, 2 b-tag")
          #myText(0.47,0.70,1,"at least 8 jet, 2 b-tag (Pre-Selection)")
          if "HT_all_5j" in HistoName:
             myText(0.47,0.70,1,"5 jet, 2 b-tag (Pre-Selection)")
          if "HT_all_6j" in HistoName:
             myText(0.47,0.70,1,"6 jet, 2 b-tag (Pre-Selection)") 
          if "HT_all_7j" in HistoName:
             myText(0.47,0.70,1,"7 jet, 2 b-tag (Pre-Selection)")  
          if "HT_all_8j" in HistoName:
             myText(0.47,0.70,1,"at least 8 jet, 2 b-tag (Pre-Selection)")      
          #myText(0.47,0.70,1,"at least 8 jet, 2 b-tag (Pre-Selection)")
          #myText(0.47,0.70,1,"at least 8 jet, 2 b-tag (Pre-Selection)")
          #myText(0.20,0.65,1,"Pre-fit")
          #myText(0.20,0.65,1,"SR cuts")
      if "ThreeTags" in btagStrategy :
          #myText(0.45,0.70,1,"l^{-}+jets Resolved: 5 jet, 3 b-tag")
          #myText(0.47,0.70,1,"at least 8 jet, 3 b-tag (Pre-Selection)")
          if "HT_all_5j" in HistoName:
             myText(0.47,0.70,1,"5 jet, 3 b-tag (Pre-Selection)")
          if "HT_all_6j" in HistoName:
             myText(0.47,0.70,1,"6 jet, 3 b-tag (Pre-Selection)") 
          if "HT_all_7j" in HistoName:
             myText(0.47,0.70,1,"7 jet, 3 b-tag (Pre-Selection)")  
          if "HT_all_8j" in HistoName:
             myText(0.47,0.70,1,"at least 8 jet, 3 b-tag (Pre-Selection)")
          #myText(0.47,0.70,1,"at least 8 jet, 3 b-tag (Pre-Selection)")
          #myText(0.47,0.70,1,"at least 8 jet, 3 b-tag (Pre-Selection)")
          #myText(0.20,0.65,1,"CR cuts")
      if "FourPlusTags" in btagStrategy :
          #myText(0.45,0.70,1,"l^{-}+jets Resolved: 5 jet, at least 4 b-tag")
          #myText(0.47,0.70,1,"at least 8 jet, at least 4 b-tag (Pre-Selection)")
          if "HT_all_5j" in HistoName:
             myText(0.47,0.70,1,"5 jet, at least 4 b-tag (Pre-Selection)")
          if "HT_all_6j" in HistoName:
             myText(0.47,0.70,1,"6 jet, at least 4 b-tag (Pre-Selection)") 
          if "HT_all_7j" in HistoName:
             myText(0.47,0.70,1,"7 jet, at least 4 b-tag (Pre-Selection)")  
          if "HT_all_8j" in HistoName:
             myText(0.47,0.70,1,"at least 8 jet, at least 4 b-tag (Pre-Selection)")
          #myText(0.47,0.70,1,"at least 8 jet, at least 4 b-tag (Pre-Selection)")
          #myText(0.47,0.70,1,"at least 8 jet, at least 4 b-tag (Pre-Selection)")
          #myText(0.20,0.65,1,"CR cuts")
      if "Inclusive" in btagStrategy :  
          #myText(0.45,0.70,1,"l^{-}+jets Resolved: 5 jet, Inclusive")
          if "HT_all_5j" in HistoName:
             myText(0.47,0.70,1,"5 jet, Inclusive (Pre-Selection)")
          if "HT_all_6j" in HistoName:
             myText(0.47,0.70,1,"6 jet, Inclusive (Pre-Selection)") 
          if "HT_all_7j" in HistoName:
             myText(0.47,0.70,1,"7 jet, Inclusive (Pre-Selection)")  
          if "HT_all_8j" in HistoName:
             myText(0.47,0.70,1,"at least 8 jet, Inclusive (Pre-Selection)")
          #myText(0.47,0.70,1,"at least 8 jet, Inclusive (Pre-Selection)")
          #myText(0.20,0.67,1,"")
      pad2.cd()
      output      = array( 'd', [0.0]*1)
      output_redchi2 = h_data.Chi2Test(h_mc, "UW CHI2/NDF")
      output_chi2 = h_data.Chi2Test(h_mc, "UW CHI2")
      output_ndf = output_chi2 / output_redchi2
      output_prob = h_data.Chi2Test(h_mc, "UW")
      str_chi2 = str(round(output_chi2,2)) 
      str_ndf = str(round(output_ndf,1))
      str_prob    = str(round(output_prob,2))   
      #myText(0.38,0.85,1,"#chi^{2}/ndf :"+str_chi2+"/"+str_ndf) 
      leg_drat = TLegend(0.35,0.385,0.45,0.585)
      leg_drat.SetShadowColor(kWhite)
      leg_drat.SetFillColor(kWhite)
      leg_drat.SetLineColor(kWhite)
      leg_drat.AddEntry(h_dratio,"Double Ratio","epl")
      leg_drat.Draw()

      #myText(0.50,0.85,1,"#chi^{2} prob :"+str_prob)   
      c1.RedrawAxis()
      c1.Update()
      c1.RedrawAxis()
      c1.SaveAs("../Plots/datavmc_HTallMod/DatavMC_%s_Int_datavmc_V2Ntup_HypSig_wnorm_wonorm_allfloat_fMwt_dratio.pdf" % (HistoName+"_"+btagStrategy))
      #c1.SaveAs("../Plots/datavmc_HTall/DatavMC_%s_qqbb_datavmc_V2Ntup_010921_wRew_HypSig_wnorm_Hptb_HFnormapplied_withchi2.pdf" % (HistoName+"_"+btagStrategy))
      #c1.SaveAs("../Plots/datavmc_HTall/DatavMC_%s_qqbb_datavmc_V2Ntup_010921_wRew_HypSig_wnorm_Hptb_withchi2.pdf" % (HistoName+"_"+btagStrategy))
      #c1.SaveAs("../Plots/datavmc_HTall/DatavMC_%s_qqbb_datavmc_V2Ntup_010921_woRew_withchi2.pdf" % (HistoName+"_"+btagStrategy))
      #c1.SaveAs("../Plots/datavmc_HTall_NF/DatavMC_%s_qqbb_datavmc_V2Ntup_010921_wRew_HypSig_wnorm_wonorm_Hptb_withchi2.pdf" % (HistoName+"_"+btagStrategy))
