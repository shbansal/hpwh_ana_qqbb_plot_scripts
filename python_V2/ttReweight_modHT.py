# -*- coding: utf-8 -*-
#python
import ROOT
import sys
import glob
import math
import re
from ROOT import *
from array import *
from ROOT import TF1
import numpy as np
from scipy.linalg import eig, inv
import numpy as np
from scipy.linalg import eig, inv
#include "TFitResultPtr.h"

gROOT.SetBatch(True)
gStyle.SetOptStat(0)
gStyle.SetPalette(1)
gROOT.LoadMacro("../style/AtlasStyle.C")
gROOT.LoadMacro("../style/AtlasUtils.C")
SetAtlasStyle()
#from ROOT import rootpy.plotting
#gMinuit.SetMaxIterations(5000)
import numpy as np
from numpy import ndarray
import array
#from rootpy.plotting import Hist, HistStack, Legend, Canvas
#from rootpy.plotting.style import get_style, set_style
#from rootpy.plotting.utils import draw
#from rootpy.interactive import wait
#import rootpy.plotting.root2matplotlib as rplt
from ROOT import TCanvas, TFile, TPad, THStack, TLine,TGraphAsymmErrors, TNtuple, TH1F, TH2F
#from ROOT import TGraphAsymmErrors
from array import *
#from ROOT import TLatex
#import ROOT
#import array
#TVirtualFitter::SetMaxIterations(5000)
y_axis_label="Event fraction"

c_blue   = TColor.GetColor("#3366ff")
c_red    = TColor.GetColor("#ee3311")
c_orange = TColor.GetColor("#ff9900")

c1 = TCanvas("WeightnJet-qqbb","",700,500)
#pad1 = TPad("pad1", "pad1", 0.0, 0.05, 1,1)

#pad1.SetLeftMargin(0.15)
#pad1.SetTopMargin(0.1)
##pad1.SetBottomMargin(0.0105)
#pad1.SetRightMargin(0.08)
#pad1.Draw()
#pad1.SetTicks()

#pad2.SetLeftMargin(0.15)
#pad2.SetTopMargin(0.04)
#pad2.SetBottomMargin(0.35)
#pad2.SetRightMargin(0.08)
#pad2.Draw()
#pad2.SetTicks()
def pyf_tf1_params(x, p):
    return p[0] + p[1] * x[0] + p[2] * x[0] * x[0] + p[3] * x[0] * x[0] * x[0] + p[4] * x[0] * x[0] * x[0] * x[0]

for HistoName in ["HT_all_8j"]:       
 for Region in ["Resolved_SR"]:
  for btagStrategy in ["TwoTags"]:    
     
      ReBin = False
      YAxisScale = 1.4               
      #if "nJet" in HistoName:
          #Xaxis_label="Jet multiplicity"           
      
      #file_data15       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/data_leptop_NewTrain_ttRew_PSel_imp/data_2015.root_77p_225_.root","READ")
      #file_data15       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_modHT/data_2015.root_77p_225_.root","READ")
      file_data15       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/BkgDat_PSel_LepTop/data_2015.root_77p_225_.root","READ")
      dir_15        = file_data15.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_data15 = dir_15.Get("data_2015_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      
      #file_data16       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/data_leptop_NewTrain_ttRew_PSel_imp/data_2016.root_77p_225_.root","READ") 
      #file_data16       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_modHT/data_2016.root_77p_225_.root","READ") 
      file_data16       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/BkgDat_PSel_LepTop/data_2016.root_77p_225_.root","READ")
      dir_16        = file_data16.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_data16 = dir_16.Get("data_2016_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      #file_data17       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/data_leptop_NewTrain_ttRew_PSel_imp/data_2017.root_77p_225_.root","READ")
      #file_data17       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_modHT/data_2017.root_77p_225_.root","READ")
      file_data17       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/BkgDat_PSel_LepTop/data_2017.root_77p_225_.root","READ")
      dir_17        = file_data17.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_data17 = dir_17.Get("data_2017_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      #file_data18      = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/data_leptop_NewTrain_ttRew_PSel_imp/data_2018.root_77p_225_.root","READ")
      #file_data18      = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_modHT/data_2018.root_77p_225_.root","READ")
      file_data18      = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/BkgDat_PSel_LepTop/data_2018.root_77p_225_.root","READ")
      dir_18        = file_data18.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_data18 = dir_18.Get("data_2018_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")


      #file_tt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_imp/tt_PP8.root_77p_225.root","READ")
      #file_tt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_modHT/tt_PP8.root_77p_225.root","READ")
      file_tt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/BkgDat_PSel_LepTop/tt_PP8.root_77p_225.root","READ")
      dir_tt_1b   = file_tt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1b")
      h_ttbar_background_1b = dir_tt_1b.Get("tt_PP8_"+HistoName+"_1b_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_ttbar_background_1b = dir_tt_1b.Get("tt_PP8_1B_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      dir_tt_1c   = file_tt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1c")
      h_ttbar_background_1c = dir_tt_1c.Get("tt_PP8_"+HistoName+"_1c_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_ttbar_background_1c = dir_tt_1c.Get("tt_PP8_1C_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      dir_tt_1l   = file_tt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1l")
      h_ttbar_background_1l = dir_tt_1l.Get("tt_PP8_"+HistoName+"_1l_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_ttbar_background_1l = dir_tt_1l.Get("tt_PP8_1L_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      #file_tt_filt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_imp/tt_PP8filtered.root_77p_225.root","READ")
      #file_tt_filt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_modHT/tt_PP8filtered.root_77p_225.root","READ")
      file_tt_filt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/BkgDat_PSel_LepTop/tt_PP8filtered.root_77p_225.root","READ")
      dir_tt_1b_filt   = file_tt_filt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1b")
      h_ttbar_background_1b_filt = dir_tt_1b_filt.Get("tt_PP8filtered_"+HistoName+"_1b_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_ttbar_background_1b_filt = dir_tt_1b_filt.Get("tt_PP8filtered_1B_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      dir_tt_1c_filt   = file_tt_filt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1c")
      h_ttbar_background_1c_filt = dir_tt_1c_filt.Get("tt_PP8filtered_"+HistoName+"_1c_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_ttbar_background_1c_filt = dir_tt_1c_filt.Get("tt_PP8filtered_1C_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      dir_tt_1l_filt   = file_tt_filt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1l")
      h_ttbar_background_1l_filt = dir_tt_1l_filt.Get("tt_PP8filtered_"+HistoName+"_1l_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_ttbar_background_1l_filt = dir_tt_1l_filt.Get("tt_PP8filtered_1L_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      
      #parameters derived from the Mwt distribution
      #h_ttbar_background_1b.Scale(1.40)
      #h_ttbar_background_1b_filt.Scale(1.40)
      #h_ttbar_background_1c.Scale(1.23)
      #h_ttbar_background_1c_filt.Scale(1.23)
      #h_ttbar_background_1l.Scale(0.93)
      #h_ttbar_background_1l_filt.Scale(0.93)
      
      #parameters derived from the nBTags distribution
      #h_ttbar_background_1b.Scale(1.36)
      #h_ttbar_background_1b_filt.Scale(1.36)
      #h_ttbar_background_1c.Scale(1.32)
      #h_ttbar_background_1c_filt.Scale(1.32)
      #h_ttbar_background_1l.Scale(0.92)
      #h_ttbar_background_1l_filt.Scale(0.92)


      h_ttbar_background = h_ttbar_background_1b + h_ttbar_background_1c + h_ttbar_background_1l + h_ttbar_background_1b_filt + h_ttbar_background_1c_filt + h_ttbar_background_1l_filt
      #h_ttbar_background = h_ttbar_background_1b + h_ttbar_background_1c + h_ttbar_background_1l
               
      #file_st_tc   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_imp/st_tc.root_77p_225.root","READ")
      #file_st_tc   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_modHT/st_tc.root_77p_225.root","READ")
      file_st_tc   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/BkgDat_PSel_LepTop/st_tc.root_77p_225.root","READ")
      dir_st_tc   = file_st_tc.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_singleTop_background_tc = dir_st_tc.Get("st_tc_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      #file_st_sc   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_imp/st_sc.root_77p_225.root","READ")
      #file_st_sc   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_modHT/st_sc.root_77p_225.root","READ")
      file_st_sc   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/BkgDat_PSel_LepTop/st_sc.root_77p_225.root","READ")
      dir_st_sc   = file_st_sc.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_singleTop_background_sch = dir_st_sc.Get("st_sc_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      
      #file_Wt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_imp/Wt.root_77p_225.root","READ")
      #file_Wt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_modHT/Wt.root_77p_225.root","READ")
      file_Wt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/BkgDat_PSel_LepTop/Wt.root_77p_225.root","READ")
      dir_Wt   = file_Wt.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_Wt = dir_Wt.Get("Wt_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      #file_tH   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_imp/tH_AFII.root_77p_225.root","READ")
      #file_tH   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_modHT/tH_AFII.root_77p_225.root","READ")
      file_tH   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/BkgDat_PSel_LepTop/tH_AFII.root_77p_225.root","READ")
      dir_tH   = file_tH.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_tH = dir_tH.Get("tH_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      #file_tWZ   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_imp/tWZ.root_77p_225.root","READ")
      #file_tWZ   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_modHT/tWZ.root_77p_225.root","READ")
      file_tWZ   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/BkgDat_PSel_LepTop/tWZ.root_77p_225.root","READ")
      dir_tWZ   = file_tWZ.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_tWZ = dir_tWZ.Get("tWZ_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      #file_tZ   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_imp/tZ.root_77p_225.root","READ")
      #file_tZ   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_modHT/tZ.root_77p_225.root","READ")
      file_tZ   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/BkgDat_PSel_LepTop/tZ.root_77p_225.root","READ")
      dir_tZ   = file_tZ.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_tZ = dir_tZ.Get("tZ_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      #file_Wjet   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_imp/wjets_77p_225.root","READ")
      #file_Wjet   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_modHT/wjets_77p_225.root","READ")
      file_Wjet   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/BkgDat_PSel_LepTop/wjets_77p_225.root","READ")
      dir_Wjet    = file_Wjet.GetDirectory("nominal_Loose").GetDirectory(HistoName)        
      h_Wjet = dir_Wjet.Get("wjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      #file_Zjet   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_imp/zjets_77p_225.root","READ")
      #file_Zjet   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_modHT/zjets_77p_225.root","READ")
      file_Zjet   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/BkgDat_PSel_LepTop/zjets_77p_225.root","READ")
      dir_Zjet    = file_Zjet.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_Zjet = dir_Zjet.Get("zjets_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      
      #file_ttW   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_imp/ttW.root_77p_225.root","READ")
      #file_ttW   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_modHT/ttW.root_77p_225.root","READ")
      file_ttW   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/BkgDat_PSel_LepTop/ttW.root_77p_225.root","READ")
      dir_ttW    = file_ttW.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_ttW_background = dir_ttW.Get("ttW_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      #file_ttH   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_imp/ttH.root_77p_225.root","READ")
      #file_ttH   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_modHT/ttH.root_77p_225.root","READ")
      file_ttH   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/BkgDat_PSel_LepTop/ttH.root_77p_225.root","READ")
      dir_ttH    = file_ttH.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_ttH_background = dir_ttH.Get("ttH_PP8_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      #file_ttZ   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_imp/ttll.root_77p_225.root","READ")
      #file_ttZ   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_modHT/ttll.root_77p_225.root","READ")
      file_ttZ   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/BkgDat_PSel_LepTop/ttll.root_77p_225.root","READ")
      dir_ttZ    = file_ttZ.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_ttZ_background = dir_ttZ.Get("ttll_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      #file_dib   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_imp/db.root_77p_225.root","READ")
      #file_dib   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Bkg_leptop_NewTrain_ttRew_PSel_modHT/db.root_77p_225.root","READ")
      file_dib   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/BkgDat_PSel_LepTop/db.root_77p_225.root","READ")
      dir_dib   = file_dib.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_diboson_background = dir_dib.Get("db_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      
      h_data = h_data15 + h_data16 + h_data17 + h_data18
      h_nontt = h_diboson_background + h_singleTop_background_tc + h_singleTop_background_sch + h_Wt + h_tH + h_tWZ + h_tZ + h_Zjet + h_Wjet + h_ttW_background + h_ttH_background + h_ttZ_background
      #h_ttbar_background.Scale(0.9964) #correction for 5jet
      #h_ttbar_background.Scale(0.9912) #correction for 6jet
      #h_ttbar_background.Scale(1.0032) #correction for 7jet
      #h_ttbar_background.Scale(1.0285) #correction for 8jet
      #h_ttbar_background.Scale(1.0878) #correction for ge9jet
      #h_ttbar_background.Scale(0.99762) # correction for ge5jet

      npars = 6 #6 parameters for hypsigm and expsigm functions
      #npars = 5
      #npars = 7
      h_weight = h_data.Clone()
      h_weight = h_weight - h_nontt
      h_weight.Divide(h_ttbar_background)
      
      hypsigm_tf1 = TF1("hypsigm_tf1","[0] + [1]/(x**[2]) - [3]/(1+exp([4] - [5]*x))",200,2000)
      #expsigm_tf1 = TF1("expsigm_tf1","[0] + [1]*exp([2]*x) - [3]/(1+exp([4] - [5]*x))",200,2000)
      #poly2_tf1 = TF1("poly2_tf1","[0] + [1]*x + [2]*x*x",0,2000)
      #poly3_tf1 = TF1("poly3_tf1","[0] + [1]*x + [2]*x*x + [3]*x*x*x",0,2000)
      #poly4_tf1 = TF1("poly4_tf1","[0] + [1]*x + [2]*x*x + [3]*x*x*x + [4]*x*x*x*x",0,2000)
      #exppoly4_tf1 = TF1("exppoly4_tf1","[0]*exp([1]*x + [2]*x*x + [3]*x*x*x + [4]*x*x*x*x)",0,2000)
      #poly1exp3_tf1 = TF1("poly1exp3_tf1","[0]+ [1]*x + [2]*exp([3]*x + [4]*x*x + [5]*x*x*x)",0,2000)
      #poly2exp1_tf1 = TF1("poly2exp1_tf1","[0]+ [1]*x + [2]*x*x + [3]*exp([4]*x)",200,2000)
      #hyp_tf1 = TF1("hyp_tf1","[0]+ [1]/(x**[2])",0,2000)
      #poly2exp2_tf1 = TF1("poly2exp2_tf1","[0]+ [1]*x + [2]*x*x + [3]*exp([4]*x + [5]*x*x)",0,2000)
      #poly3exp1_tf1 = TF1("poly3exp1_tf1","[0]+ [1]*x + [2]*x*x + [3]*x*x*x + [4]*exp([5]*x)",0,2000)
      #poly3exp2_tf1 = TF1("poly3exp2_tf1","[0]+ [1]*x + [2]*x*x + [3]*x*x*x + [4]*exp([5]*x + [6]*x*x)",0,2000)

      sigPars      = array( 'd', [0.0]*6)
      chi2         = array ('d', [0.0]*1)
      #cov          = np.empty((6,6))
      cov = np.matrix([[0.000,  0.000,  0.000,  0.000, 0.000, 0.000],
                  [0.000,  0.000,  0.000,  0.000, 0.000, 0.000],
                  [0.000,  0.000,  0.000,  0.000, 0.000, 0.000],
                  [0.000,  0.000,  0.000,  0.000, 0.000, 0.000],
                  [0.000,  0.000,  0.000,  0.000, 0.000, 0.000],
                  [0.000,  0.000,  0.000,  0.000, 0.000, 0.000]])
      #Nbins_X = h_weight.GetNbinsX()
      #print Nbins_X
      #for i in range (1,Nbins_X):
          #print h_weight.GetBinContent(i)
      
      #kind of optimal setting for function param for hyperbola + sigmoid function
      #hypsigm_tf1.SetParameter(0,0.9)
      #hypsigm_tf1.SetParameter(1,2000)
      #hypsigm_tf1.SetParameter(2,2.0) #1.5 
      #hypsigm_tf1.SetParameter(3,1.0) #0.5
      #hypsigm_tf1.SetParameter(4,10)
      #hypsigm_tf1.SetParameter(5,0.010)  
      
      #Parameters for Hyperbola+Sigmoid functional form
      #hypsigm_tf1.SetParameter(0,0.87) #0.80
      #hypsigm_tf1.SetParameter(1,3000.5) #8000
      #hypsigm_tf1.SetParameter(2,1.92) #2.0
      #hypsigm_tf1.SetParameter(3,0.55) #1.0
      #hypsigm_tf1.SetParameter(4,12.5) #10
      #hypsigm_tf1.SetParameter(5,0.008) #0.006

      #hypsigm_tf1.SetParameter(0,0.81) #0.80
      #hypsigm_tf1.SetParameter(1,17081.7) #8000
      #hypsigm_tf1.SetParameter(2,1.80) #2.0
      #hypsigm_tf1.SetParameter(3,0.27) #1.0
      #hypsigm_tf1.SetParameter(4,6.5) #10
      #hypsigm_tf1.SetParameter(5,0.0049) #0.006
      
      #start par with norm. correction of tt+1b and tt+1c applied 
      #hypsigm_tf1.SetParameter(0,0.81) #0.80
      #hypsigm_tf1.SetParameter(1,10e3) #8000
      #hypsigm_tf1.SetParameter(2,1.80) #2.0
      #hypsigm_tf1.SetParameter(3,0.58) #1.0
      #hypsigm_tf1.SetParameter(4,5.5) #10
      #hypsigm_tf1.SetParameter(5,0.0027) #0.006
      
      #*************************Hyperbola+Sigmoid Function**************************
      #start par w/o norm. correction of tt+1b and tt+1c applied (5 jet case, wonorm)
      #hypsigm_tf1.SetParameter(0,0.31) #0.80
      #hypsigm_tf1.SetParameter(1,8e3) #8000
      #hypsigm_tf1.SetParameter(2,1.80) #2.0
      #hypsigm_tf1.SetParameter(3,0.58) #1.0
      #hypsigm_tf1.SetParameter(4,5.5) #10
      #hypsigm_tf1.SetParameter(5,0.0027) #0.006 

      #start par w/o norm. correction of tt+1b and tt+1c applied (5 jet case, wnorm from Mwt, all float, wnorm from nBtags allfloat)
      #hypsigm_tf1.SetParameter(0,0.31) #0.80
      #hypsigm_tf1.SetParameter(1,9e3) #8000
      #hypsigm_tf1.SetParameter(2,2.20) #2.0
      #hypsigm_tf1.SetParameter(3,0.48) #1.0
      #hypsigm_tf1.SetParameter(4,5.5) #10
      #hypsigm_tf1.SetParameter(5,0.0017) #0.006 

      #start par w/o norm. correction of tt+1b and tt+1c applied (5 jet case, wnorm from Mwt, bcfloat)
      #hypsigm_tf1.SetParameter(0,0.51) #0.51
      #hypsigm_tf1.SetParameter(1,7e3) #7e3
      #hypsigm_tf1.SetParameter(2,2.20) #2.2
      #hypsigm_tf1.SetParameter(3,0.48) #1.0
      #hypsigm_tf1.SetParameter(4,5.5) #10
      #hypsigm_tf1.SetParameter(5,0.0017) #0.006

      #start par w/o norm. correction of tt+1b and tt+1c applied (6 jet case, wonorm)
      #hypsigm_tf1.SetParameter(0,0.81) #0.80
      #hypsigm_tf1.SetParameter(1,8e3) #8000
      #hypsigm_tf1.SetParameter(2,2.20) #2.0
      #hypsigm_tf1.SetParameter(3,0.88) #1.0
      #hypsigm_tf1.SetParameter(4,8.5) #10
      #hypsigm_tf1.SetParameter(5,0.0047) #0.006

      #start par w/o norm. correction of tt+1b and tt+1c applied (6 jet case, wnorm from Mwt, all float, bcfloat)
      #hypsigm_tf1.SetParameter(0,0.81) #0.80
      #hypsigm_tf1.SetParameter(1,8e3) #8000
      #hypsigm_tf1.SetParameter(2,2.20) #2.0
      #hypsigm_tf1.SetParameter(3,0.88) #1.0
      #hypsigm_tf1.SetParameter(4,8.5) #10
      #hypsigm_tf1.SetParameter(5,0.0047) #0.006

      #start par w/o norm. correction of tt+1b and tt+1c applied (6 jet case, wnorm from nBTags all float)
      #hypsigm_tf1.SetParameter(0,0.31) #0.80
      #hypsigm_tf1.SetParameter(1,8e3) #8000
      #hypsigm_tf1.SetParameter(2,2.20) #2.0
      #hypsigm_tf1.SetParameter(3,0.48) #1.0
      #hypsigm_tf1.SetParameter(4,5.5) #10
      #hypsigm_tf1.SetParameter(5,0.0017) #0.006

      #start par w/o norm. correction of tt+1b and tt+1c applied (7 jet case, wonorm: OLD ONES)
      #hypsigm_tf1.SetParameter(0,0.81) #0.80
      #hypsigm_tf1.SetParameter(1,8e3) #8000
      #hypsigm_tf1.SetParameter(2,2.20) #2.20
      #hypsigm_tf1.SetParameter(3,0.88) #1.0
      #hypsigm_tf1.SetParameter(4,8.5) #10
      #hypsigm_tf1.SetParameter(5,0.0047) #0.006

      #start par w/o norm. correction of tt+1b and tt+1c applied (7 jet case, wnorm from Mwt, bcfloat)
      #hypsigm_tf1.SetParameter(0,0.81) #0.80
      #hypsigm_tf1.SetParameter(1,8e3) #8000
      #hypsigm_tf1.SetParameter(2,2.20) #2.0
      #hypsigm_tf1.SetParameter(3,0.88) #1.0
      #hypsigm_tf1.SetParameter(4,8.5) #10
      #hypsigm_tf1.SetParameter(5,0.0047) #0.006

      #start par w/o norm. correction of tt+1b and tt+1c applied (7 jet case, wnorm from Mwt, allfloat)
      #hypsigm_tf1.SetParameter(0,0.81) #0.80
      #hypsigm_tf1.SetParameter(1,5e3) #8000
      #hypsigm_tf1.SetParameter(2,2.20) #2.0
      #hypsigm_tf1.SetParameter(3,0.88) #1.0
      #hypsigm_tf1.SetParameter(4,8.5) #10
      #hypsigm_tf1.SetParameter(5,0.0047) #0.006
      
      #start par w/o norm. correction of tt+1b and tt+1c applied (at least 8 jet case, wonorm)
      hypsigm_tf1.SetParameter(0,0.81) #0.80
      hypsigm_tf1.SetParameter(1,9e3) #8000
      hypsigm_tf1.SetParameter(2,2.20) #2.0
      hypsigm_tf1.SetParameter(3,0.88) #1.0
      hypsigm_tf1.SetParameter(4,8.5) #10
      hypsigm_tf1.SetParameter(5,0.0047) #0.006

      #start par w/o norm. correction of tt+1b and tt+1c applied (at least 8 jet case, wnorm from Mwt, all float, bcfloat)
      #hypsigm_tf1.SetParameter(0,0.81) #0.80
      #hypsigm_tf1.SetParameter(1,9e3) #8000
      #hypsigm_tf1.SetParameter(2,2.20) #2.0
      #hypsigm_tf1.SetParameter(3,0.88) #1.0
      #hypsigm_tf1.SetParameter(4,8.5) #10
      #hypsigm_tf1.SetParameter(5,0.0047) #0.006
      
      #*************************Hyperbola+Sigmoid Function**************************

      #*************************Exp+Sigmoid Function**************************
      
      #start par w/ norm. correction of tt+1b and tt+1c applied (5 jet, 6 jet, 7 jet, ge8 jet case, wnorm from Mwt, all float and 6 jet, 7 jet, ge8 jet case, wnorm from Mwt, bcfloat)
      #expsigm_tf1.SetParameter(0,0.671)
      #expsigm_tf1.SetParameter(1,3.066)
      #expsigm_tf1.SetParameter(2,-0.015) #1.5 
      #expsigm_tf1.SetParameter(3,-0.321) #0.5
      #expsigm_tf1.SetParameter(4,-4.582) #2.5
      #expsigm_tf1.SetParameter(5,-0.004)
      
      #start par w/ norm correction for 5 jet (from Mwt, bcfloat)
      #expsigm_tf1.SetParameter(0,0.571)
      #expsigm_tf1.SetParameter(1,2.066)
      #expsigm_tf1.SetParameter(2,-0.025) #1.5 
      #expsigm_tf1.SetParameter(3,-0.521) #0.5
      #expsigm_tf1.SetParameter(4,-4.582) #2.5
      #expsigm_tf1.SetParameter(5,-0.003)
      
      #************************Exp+Sigmoid Function**************************
      
      #************************Poly2+Exp1 Function***************************
      
      #poly2exp1_tf1.SetParameter(0,0.0001)
      #poly2exp1_tf1.SetParameter(1,0.00001)
      #poly2exp1_tf1.SetParameter(2,0.000001)
      #poly2exp1_tf1.SetParameter(3,0.0000001)
      #poly2exp1_tf1.SetParameter(4,0.00000001)

      #************************Poly2+Exp1 Function***************************

      #************************Hyperbola Function****************************

      #hyp_tf1.SetParameter(0,0.41)
      #hyp_tf1.SetParameter(1,5000)
      #hyp_tf1.SetParameter(2,2.2)
    
      #***********************Hyperbola Function****************************

      #************************Poly2+Exp2 Function***************************
      
      #poly2exp2_tf1.SetParameter(0,0.0001)
      #poly2exp2_tf1.SetParameter(1,0.00001)
      #poly2exp2_tf1.SetParameter(2,0.000001)
      #poly2exp2_tf1.SetParameter(3,0.0000001)
      #poly2exp2_tf1.SetParameter(4,0.00000001)
      #poly2exp2_tf1.SetParameter(5,0.00000001)

      #************************Poly2+Exp2 Function***************************

      #************************Poly3+Exp1 Function***************************
      
      #poly3exp1_tf1.SetParameter(0,0.0001)
      #poly3exp1_tf1.SetParameter(1,0.00001)
      #poly3exp1_tf1.SetParameter(2,0.000001)
      #poly3exp1_tf1.SetParameter(3,0.0000001)
      #poly3exp1_tf1.SetParameter(4,0.00000001)
      #poly3exp1_tf1.SetParameter(5,0.00000001)

      #************************Poly3+Exp1 Function***************************

      #start par w/o norm. correction of tt+1b and tt+1c applied (9 jet case, wnorm and wonorm)
      #hypsigm_tf1.SetParameter(0,0.21) #0.80
      #hypsigm_tf1.SetParameter(1,5e3) #8000
      #hypsigm_tf1.SetParameter(2,2.20) #2.0
      #hypsigm_tf1.SetParameter(3,0.58) #1.0
      #hypsigm_tf1.SetParameter(4,6.5) #10
      #hypsigm_tf1.SetParameter(5,0.0030) #0.006
    

      #hypsigm_tf1.SetParameter(0,0.57) #0.80
      #hypsigm_tf1.SetParameter(1,50000) #8000
      #hypsigm_tf1.SetParameter(2,8.82) #2.0
      #hypsigm_tf1.SetParameter(3,1.30) #1.0
      #hypsigm_tf1.SetParameter(4,6.5) #10
      #hypsigm_tf1.SetParameter(5,0.008) #0.006

      #Parameters for Exponential+Sigmoid functional form
      #expsigm_tf1.SetParameter(0,0.671)
      #expsigm_tf1.SetParameter(1,3.066)
      #expsigm_tf1.SetParameter(2,-0.015) #1.5 
      #expsigm_tf1.SetParameter(3,-0.321) #0.5
      #expsigm_tf1.SetParameter(4,-4.582) #2.5
      #expsigm_tf1.SetParameter(5,-0.004) 
      
      #Parameters for second order poly functional form
      #poly3_tf1.SetParameter(0,1000)
      #poly3_tf1.SetParameter(1,100)
      #poly3_tf1.SetParameter(2,10)
      #poly3_tf1.SetParameter(3,10)
      
      #second order exponential function
      #exppoly2_tf1.SetParameter(0,0.00001)
      #exppoly2_tf1.SetParameter(1,0.000001)
      #exppoly2_tf1.SetParameter(2,0.0000001)

      #second order exponential function
      #exppoly4_tf1.SetParameter(0,0.000000001)
      #exppoly4_tf1.SetParameter(1,0.00000000001)
      #exppoly4_tf1.SetParameter(2,0.000000000001)
      #exppoly4_tf1.SetParameter(3,0.0000000000001)
      #exppoly4_tf1.SetParameter(4,0.00000000000001)

      #Polynomial 1 exponential  1/1 slightly different parameters 1/2 function
      #poly1exp2_tf1.SetParameter(0,0.00001)
      #poly1exp2_tf1.SetParameter(1,0.000001)
      #poly1exp2_tf1.SetParameter(2,0.0000001)
      #poly1exp2_tf1.SetParameter(3,0.00000001)
      #poly1exp2_tf1.SetParameter(4,0.000000001)
      
      #Polynomial 1 exponential 3
      #poly1exp3_tf1.SetParameter(0,0.000001)
      #poly1exp3_tf1.SetParameter(1,0.0000001)
      #poly1exp3_tf1.SetParameter(2,0.00000001)
      #poly1exp3_tf1.SetParameter(3,0.000000001)
      #poly1exp3_tf1.SetParameter(4,0.0000000001)
      #poly1exp3_tf1.SetParameter(5,0.00000000001)

      #Polynomial 2 exponential 1
      #poly2exp1_tf1.SetParameter(0,0.0001)
      #poly2exp1_tf1.SetParameter(1,0.00001)
      #poly2exp1_tf1.SetParameter(2,0.000001)
      #poly2exp1_tf1.SetParameter(3,0.0000001)
      #poly2exp1_tf1.SetParameter(4,0.00000001)

      #Polynomial 2 exponential 2
      #poly2exp2_tf1.SetParameter(0,0.0001)
      #poly2exp2_tf1.SetParameter(1,0.00001)
      #poly2exp2_tf1.SetParameter(2,0.000001)
      #poly2exp2_tf1.SetParameter(3,0.0000001)
      #poly2exp2_tf1.SetParameter(4,0.00000001)
      #poly2exp2_tf1.SetParameter(5,0.000000001)

      #Polynomial 3 exponential 1 (5jet, 8jet, ge9 jet configuration)
      #poly3exp1_tf1.SetParameter(0,0.00001)
      #poly3exp1_tf1.SetParameter(1,0.000001)
      #poly3exp1_tf1.SetParameter(2,0.0000001)
      #poly3exp1_tf1.SetParameter(3,0.00000001)
      #poly3exp1_tf1.SetParameter(4,0.000000001)
      #poly3exp1_tf1.SetParameter(5,0.0000000001)

      #Polynomial 3 exponential 1 (6jet, 7jet, ge5j configuration)
      #poly3exp1_tf1.SetParameter(0,0.000001)
      #poly3exp1_tf1.SetParameter(1,0.0000001)
      #poly3exp1_tf1.SetParameter(2,0.00000001)
      #poly3exp1_tf1.SetParameter(3,0.000000001)
      #poly3exp1_tf1.SetParameter(4,0.0000000001)
      #poly3exp1_tf1.SetParameter(5,0.00000000001)

      #Polynomial 3 exponential 2 (6jet, 7jet, ge5j configuration)
      #poly3exp2_tf1.SetParameter(0,0.001)
      #poly3exp2_tf1.SetParameter(1,0.0001)
      #poly3exp2_tf1.SetParameter(2,0.00001)
      #poly3exp2_tf1.SetParameter(3,0.000001)
      #poly3exp2_tf1.SetParameter(4,0.0000001)
      #poly3exp2_tf1.SetParameter(5,0.00000001)
      #poly3exp2_tf1.SetParameter(6,0.000000001)


      #optimal parameters:
      # 0.9434
      # 13418.67
      # 2.121
      # 0.2213
      # 7.99
      # 0.0073

      newBins = array('d', [0,25,50,75,100,125,150,175,200,225,250,275,300,325,350,375,400,450,500,550,600,650,700,750,800,850,900,950,1000,1050,1100,1150,1200,1250,1300,1350,1400,1450,1500,1550,1600,1650,1700,1750,1800,1850,1900,1950,2000])
      #newBins = array('d', [0,200,300,350,400,450,500,550,600,650,700,750,800,850,900,950,1000,1050,1100,1150,1200,1250,1300,1350,1400,1450,1500,1550,1600,1650,1700,1750,1800,1850,1900,1950,2000])
      #h_weight_binned = h_weight.Rebin(48,'h_weight_binned',newBins) 
      h_weight_binned = h_weight.Rebin(1)
      #h_weight_binned.SetBinContent(5, 0)
      #h_weight_binned.SetBinError(5, 0)
      #for i in range(0, hypsig_EV0_Dummy.GetNbinsX()):
      fitter = TVirtualFitter.Fitter(h_weight_binned)
      #fitter.SetMaxIterations(100000) #needed for poly3exp1
      fitter.SetMaxIterations(300000)
      fitStatus =  h_weight_binned.Fit("hypsigm_tf1", "V", "", 200, 2000)
      r = h_weight_binned.Fit("hypsigm_tf1","S","", 200, 2000)
      myfunc2   =  h_weight_binned.GetFunction("hypsigm_tf1")

      #fitStatus =  h_weight_binned.Fit("expsigm_tf1", "V", "", 200, 2000)
      #r = h_weight_binned.Fit("expsigm_tf1","S","", 200, 2000)
      #myfunc2   =  h_weight_binned.GetFunction("expsigm_tf1")

      #fitStatus =  h_weight_binned.Fit("poly2exp1_tf1", "V", "", 200, 2000)
      #r = h_weight_binned.Fit("poly2exp1_tf1","S","", 200, 2000)
      #myfunc2   =  h_weight_binned.GetFunction("poly2exp1_tf1")


      
      #myfunc2.GetCovarianceMatrix()
      myfunc2.GetParameters(sigPars)
      #myfunc2.GetChisquare()
      myfunc2.SetLineColor(kBlue)
      myfunc2.SetLineWidth(2)
      #myfunc2.SetLineStyle(7)
      #Double chi = myfunc2.GetChisquare()
      Chi2 = myfunc2.GetChisquare()
      NDF = myfunc2.GetNDF()
      RedChi2 = Chi2/NDF
      #TFitResultPtr* r =h_weight.Fit("hypsigm_tf1","S")
      cov = r.GetCovarianceMatrix()
      chi2   = r.Chi2()                 
      par0   = r.Parameter(0);            
      err0   = r.ParError(0);             
      #TMatrixDSym cov.Use(fitter.GetNumberTotalParameters(),fitter.GetCovarianceMatrix())
      #cov.Use(fitter.GetNumberTotalParameters(),fitter.GetCovarianceMatrix())
      #cov = fitter.GetCovarianceMatrix()
      #TVectorD var = TMatrixDDiag(cov)
      #print cov
      r.Print("V")
      print "chi2 :", myfunc2.GetChisquare()
      print "NDF :", myfunc2.GetNDF()
      #print "Red chi2 :" myfunc2.GetChisquare()/myfunc2.GetNDF()
      #pad1.cd()
      h_weight_binned.SetMarkerStyle(20)
      h_weight_binned.SetMarkerSize(1.0)
      h_weight_binned.SetMarkerColor(1)
      h_weight_binned.GetXaxis().SetTitle("H_{T}_{all} [GeV]")
      h_weight_binned.GetYaxis().SetTitle("Weight factor")
      #h_weight_binned.GetYaxis().SetRangeUser(0.0,2.5)
      h_weight_binned.GetYaxis().SetRangeUser(0.0,8.0)
      #h_weight.GetXaxis().SetRangeUser(4.5,12.5)
      A = np.matrix([[1.000, -0.389, -0.395,  0.998, -0.687, -0.801],
               [-0.389,  1.000,  1.000, -0.443,  0.647,  0.599],
               [ -0.395,  1.000,  1.000, -0.450,  0.656,  0.608],
               [0.998, -0.443, -0.450,  1.000, -0.726, -0.830],
               [ -0.687,  0.647,  0.656, -0.726,  1.000,  0.980],
               [ -0.801,  0.599,  0.608, -0.830,  0.980,  1.000]] ) 

      B = np.matrix([[1.000,  0.796,  0.813,  0.573, -0.874, -0.803],
               [0.796,  1.000,  0.999,  0.320, -0.522, -0.448],
               [0.813,  0.999,  1.000,  0.331, -0.539, -0.464],
               [0.573,  0.320,  0.331,  1.000, -0.669, -0.806],
               [-0.874, -0.522, -0.539, -0.669,  1.000,  0.970],
               [-0.803, -0.448, -0.464, -0.806,  0.970,  1.000]])   

      C = np.matrix([[1.000,  0.934,  0.943,  0.768, -0.804, -0.783],
                  [0.934,  1.000,  1.000,  0.643, -0.643, -0.624],
                  [0.943,  1.000,  1.000,  0.654, -0.656, -0.636],
                  [0.768,  0.643,  0.654,  1.000, -0.874, -0.892],
                  [-0.804, -0.643, -0.656, -0.874,  1.000,  0.997],
                  [-0.783, -0.624, -0.636, -0.892,  0.997,  1.000]])

      D = np.matrix([[1.000,  0.954,  0.961,  0.599, -0.392, -0.348],
                  [0.954,  1.000,  1.000,  0.528, -0.295, -0.259],
                  [0.961,  1.000,  1.000,  0.536, -0.303, -0.266],
                  [0.599,  0.528,  0.536,  1.000, -0.640, -0.663],
                  [-0.392, -0.295, -0.303, -0.640,  1.000,  0.994],
                  [-0.348, -0.259, -0.266, -0.663,  0.994,  1.000]])
      eVals_5j,eVecs_5j = eig(B)    
      Evariations = np.matrix([[0.000,  0.000,  0.000,  0.000, 0.000, 0.000],
                  [0.000,  0.000,  0.000,  0.000, 0.000, 0.000],
                  [0.000,  0.000,  0.000,  0.000, 0.000, 0.000],
                  [0.000,  0.000,  0.000,  0.000, 0.000, 0.000],
                  [0.000,  0.000,  0.000,  0.000, 0.000, 0.000],
                  [0.000,  0.000,  0.000,  0.000, 0.000, 0.000]])  
      par = np.matrix([0.605, 34834290.982, 3.426, -0.326, -7.489, -0.006])
      #par_6j = np.matrix([0.928, 20467135.633, 3.205, 0.360, 6.305, 0.005])
      #par = np.matrix([0.863, 132525.193, 2.188, 0.219, 10.640, 0.008])
      #par = np.matrix([0.850, 13399.837, 1.712, 0.140, 15.310, 0.011])
      par_mod_0 = np.matrix([0, 0, 0, 0, 0, 0])
      par_mod_1 = np.matrix([0, 0, 0, 0, 0, 0])
      par_mod_2 = np.matrix([0, 0, 0, 0, 0, 0])
      par_mod_3 = np.matrix([0, 0, 0, 0, 0, 0])
      par_mod_4 = np.matrix([0, 0, 0, 0, 0, 0])
      par_mod_5 = np.matrix([0, 0, 0, 0, 0, 0])
      n = 0
      for val,vec in zip(eVals_5j,np.transpose(eVecs_5j)):
          aux = vec.dot(np.sqrt(val))
          #print(aux)
          Evariations[n]=aux
          n=n+1
      #Evariations.append(aux)

      par_mod_0 = par + Evariations[0]
      par_mod_1 = par + Evariations[1]
      par_mod_2 = par + Evariations[2]
      par_mod_3 = par + Evariations[3]
      par_mod_4 = par + Evariations[4]
      par_mod_5 = par + Evariations[5]
      
      ########################5jetcase UP TRAFO##########################################
      #hypsigm_tf1_Nom = TF1("hypsigm_tf1_Nom","0.616 + 19204385.15/(x**3.317) - (-0.312)/(1+exp(-7.923 - (-0.006)*x))",200,2000)
      #hypsigm_tf1_EV0_UP = TF1("hypsigm_tf1_EV0_UP","6.32855624e-01 + (5.16693524e+06)/(x**3.18250976) - (-2.91259053e-01)/(1+exp(-8.62245040 - (-6.63714564e-03)*x))",200,2000)
      #hypsigm_tf1_EV1_UP = TF1("hypsigm_tf1_EV1_UP","5.78340638e-01 + (1.92043851e+07)/(x**3.31910871e+00) - (-3.51785610e-01)/(1+exp(-6.72543594e+00 - (-4.79381218e-03)*x))",200,2000)     
      #hypsigm_tf1_EV2_UP = TF1("hypsigm_tf1_EV2_UP","6.63552307e-01 + (1.92043851e+07)/(x**3.31714851e+00) - (-2.64480892e-01)/(1+exp(-7.91992597e+00 - ( -6.24046934e-03)*x))",200,2000)

      #hypsigm_tf1_EV3_UP = TF1("hypsigm_tf1_EV3_UP","6.16652708e-01 + 1.92043851e+07/(x**3.32044611e+00) - (-3.12663611e-01)/(1+exp(-7.92300756e+00 - (-6.03283651e-03)*x))",200,2000)
      #hypsigm_tf1_EV4_UP = TF1("hypsigm_tf1_EV4_UP","6.16460888e-01 + 1.92043851e+07/(x**3.31682367e+00) - (-3.12460788e-01)/(1+exp(-7.92300047e+00 - (-6.03146472e-03)*x))",200,2000)     
      #hypsigm_tf1_EV5_UP = TF1("hypsigm_tf1_EV5_UP","6.16004485e-01 + 1.92043851e+07/(x**3.31699959e+00) - (-3.12003850e-01)/(1+exp(-7.92300011e+00 - (-5.87565126e-03)*x))",200,2000)
      #####################5jetcase###########################################

      ########################5jetcase DOWN TRAFO##########################################
      #hypsigm_tf1_Nom = TF1("hypsigm_tf1_Nom","0.616 + 19204385.15/(x**3.317) - (-0.312)/(1+exp(-7.923 - (-0.006)*x))",200,2000)
      #hypsigm_tf1_EV0_UP = TF1("hypsigm_tf1_EV0_UP","5.99144376e-01 + (3.32418351e+07)/(x**3.45149024) - (-3.32740947e-01)/(1+exp(-7.22354960e+00 - (-5.36285436e-03)*x))",200,2000)
      #hypsigm_tf1_EV1_UP = TF1("hypsigm_tf1_EV1_UP","6.53659362e-01 + (1.92043852e+07)/(x**3.31489129e+00) - (-2.72214390e-01)/(1+exp(-9.12056406e+00 - (-7.20618782e-03)*x))",200,2000)     
      #hypsigm_tf1_EV2_UP = TF1("hypsigm_tf1_EV2_UP","5.68447693e-01 + (1.92043851e+07)/(x**3.31685149e+00) - (-3.59519108e-01)/(1+exp(-7.92607403e+00 - (-5.75953066e-03)*x))",200,2000)

      #hypsigm_tf1_EV3_UP = TF1("hypsigm_tf1_EV3_UP","6.15347292e-01 + 1.92043851e+07/(x**3.31355389e+00) - (-3.11336389e-01)/(1+exp(-7.92299244e+00 - (-5.96716349e-03)*x))",200,2000)
      #hypsigm_tf1_EV4_UP = TF1("hypsigm_tf1_EV4_UP","6.15539112e-01 + 1.92043851e+07/(x**3.31717633e+00) - (-3.11539212e-01)/(1+exp(-7.92299953e+00 - (-5.96853528e-03)*x))",200,2000)     
      #hypsigm_tf1_EV5_UP = TF1("hypsigm_tf1_EV5_UP","6.15995515e-01 + 1.92043851e+07/(x**3.31700041e+00) - (-3.11996150e-01)/(1+exp(-7.92299989e+00 - (-6.12434874e-03)*x))",200,2000)
      #####################5jetcase###########################################

      ########################6jetcase UP TRAFO##########################################
    #   hypsigm_tf1_Nom = TF1("hypsigm_tf1_Nom","0.928 + 20059296.790/(x**3.201) - (0.360)/(1+exp(6.313 - (0.005)*x))",200,2000)
    #   hypsigm_tf1_EV0_UP = TF1("hypsigm_tf1_EV0_UP","9.28e-01 + 2.006e+07/(x**3.201) - (0.360)/(1+exp(6.313 - (0.005)*x))",200,2000)
    #   hypsigm_tf1_EV1_UP = TF1("hypsigm_tf1_EV1_UP","9.197428e-01 + (2.969588e+05)/(x**3.02343e+00) - (3.40517e-01)/(1+exp(6.819 - (5.41871564e-03)*x))",200,2000)     
      ##hypsigm_tf1_EV1_UP = TF1("hypsigm_tf1_EV1_UP","9.362e-01 + (3.982e+07)/(x**3.378e+00) - (3.795e-01)/(1+exp(5.81 - (4.581-03)*x))",200,2000)     
    #   hypsigm_tf1_EV2_UP = TF1("hypsigm_tf1_EV2_UP","9.22706901e-01 + 2.00592968e+07/(x**3.19744333e+00) - (3.25813513e-01)/(1+exp(7.10007974e+00 - (5.76675139e-03)*x))",200,2000)

    #   hypsigm_tf1_EV3_UP = TF1("hypsigm_tf1_EV3_UP","9.28006646e-01 + 2.00592968e+07/(x**3.20076433e+00) - (4.02941383e-01)/(1+exp(6.31486426e+00 - (4.81605938e-03)*x))",200,2000)
    #   hypsigm_tf1_EV4_UP = TF1("hypsigm_tf1_EV4_UP","9.25996585e-01 + 2.00592968e+07/(x**3.19688379e+00) - (3.59978101e-01)/(1+exp(6.31298048e+00 - (4.96362817e-03)*x))",200,2000)     
    #   hypsigm_tf1_EV5_UP = TF1("hypsigm_tf1_EV5_UP","9.28086375e-01 + 2.00592968e+07/(x**3.20099991e+00) - (3.60000043e-01)/(1+exp(6.31299999e+00 - (5.01013444e-03)*x))",200,2000)
      #####################6jetcase###########################################

      ########################6jetcase DOWN TRAFO##########################################
      #hypsigm_tf1_Nom = TF1("hypsigm_tf1_Nom","0.928 + 20059296.790/(x**3.201) - (0.360)/(1+exp(6.313 - (0.005)*x))",200,2000)
      #hypsigm_tf1_EV0_UP = TF1("hypsigm_tf1_EV0_UP","9.28e-01 + 2.006e+07/(x**3.201) - (0.360)/(1+exp(6.313 - (0.005)*x))",200,2000)
      #hypsigm_tf1_EV1_UP = TF1("hypsigm_tf1_EV1_UP","9.363e-01 + (3.982e+07)/(x**3.378e+00) - (3.795e-01)/(1+exp(5.81 - (4.581-03)*x))",200,2000)     
      #hypsigm_tf1_EV2_UP = TF1("hypsigm_tf1_EV2_UP","9.33293099e-01 + 2.00592968e+07/(x**3.20455667e+00) - (3.94186487e-01)/(1+exp(5.52592026e+00 - (4.23324861e-03)*x))",200,2000)

      #hypsigm_tf1_EV3_UP = TF1("hypsigm_tf1_EV3_UP","9.27993354e-01 + 2.00592968e+07/(x**3.20123567e+00) - (3.17058617e-01)/(1+exp(6.31113574e+00 - (5.18394062e-03)*x))",200,2000)
      #hypsigm_tf1_EV4_UP = TF1("hypsigm_tf1_EV4_UP","9.30003415e-01 + 2.00592968e+07/(x**3.20511621e+00) - (3.60021899e-01)/(1+exp(6.31301952e+00 - (5.03637183e-03)*x))",200,2000)     
      #hypsigm_tf1_EV5_UP = TF1("hypsigm_tf1_EV5_UP","9.27913625e-01 + 2.00592968e+07/(x**3.20100009e+00) - (3.59999957e-01)/(1+exp(6.31300001e+00- (4.98986556e-03)*x))",200,2000)
      #####################6jetcase###########################################
 
      ########################7jetcase for Hyp+Sig Up/Down##########################################
      #hypsigm_tf1_Nom = TF1("hypsigm_tf1_Nom","0.841 + 31098.02/(x**1.933) - (0.190)/(1+exp(13.816 - (0.011)*x))",200,2000)
      #hypsigm_tf1_EV0_UP = TF1("hypsigm_tf1_EV0_UP","8.59526804e-01 + (6.41404170e+04)/(x**(2.12123998)) - (2.12042590e-01)/(1+exp(1.07072666e+01- (8.44873223e-03)*x))",200,2000)
      #hypsigm_tf1_EV1_UP = TF1("hypsigm_tf1_EV1_UP","8.37055448e-01 + (3.10980205e+04)/(x**1.93030636) - (1.64135538e-01)/(1+exp(1.94263277e+01 - (1.58119850e-02)*x))",200,2000)     
      #hypsigm_tf1_EV2_UP = TF1("hypsigm_tf1_EV2_UP","8.40130416e-01 + 3.10980200e+04/(x**1.93227964) - (1.66162685e-01)/(1+exp(1.38158890e+01 - (1.12192941e-02)*x))",200,2000)

      #hypsigm_tf1_EV3_UP = TF1("hypsigm_tf1_EV3_UP","8.35213249e-01 + 3.10980200e+04/(x**1.92773428) - (1.90369080e-01)/(1+exp(1.38159952e+01 - (1.08725557e-02)*x))",200,2000)
      #hypsigm_tf1_EV4_UP = TF1("hypsigm_tf1_EV4_UP","8.39648950e-01+ 3.10980200e+04/(x**1.93448633) - (1.90003867e-01)/(1+exp(1.38159998e+01 - (1.09452819e-02)*x))",200,2000)     
      #hypsigm_tf1_EV5_UP = TF1("hypsigm_tf1_EV5_UP","8.41007263e-01 + 3.10980200e+04/(x**1.9329977) - (1.89997578e-01)/(1+exp(1.38160002e+01 - (1.07581170e-02)*x))",200,2000)
      #####################7jetcase###########################################

      ########################7jetcase for Exp+Sig Up/Down##########################################
      #hypsigm_tf1_Nom = TF1("hypsigm_tf1_Nom","0.615 + 3.38*(exp(x*(-0.007))) - (-0.315)/(1+exp(-6.630 - (-0.005)*x))",200,2000)
      #hypsigm_tf1_EV0_UP = TF1("hypsigm_tf1_EV0_UP","0.677 + (2.49)*(exp(x*(-0.006))) - (-2.169e-01)/(1+exp(-9.766e+00 - (-7.6615e-03)*x))",200,2000) #from the up trafo
      #hypsigm_tf1_EV0_UP = TF1("hypsigm_tf1_EV0_UP","0.553 + (4.27)*(exp(x*(-0.008))) - (-4.131e-01)/(1+exp(-3.49e+00 - (-2.338e-03)*x))",200,2000) #from the down trafo
      #hypsigm_tf1_EV1_UP = TF1("hypsigm_tf1_EV1_UP","8.55815961e-01 + (1.23499270e+05)/(x**2.16584933e+00) - (1.83729800e-01)/(1+exp(1.47508239e+01 - (1.14481930e-02)*x))",200,2000)     
      #hypsigm_tf1_EV2_UP = TF1("hypsigm_tf1_EV2_UP","8.61534548e-01 + 1.23499270e+05/(x**2.16956663e+00) - (1.90747433e-01)/(1+exp(1.07897628e+01 - (8.21211973e-03)*x))",200,2000)

      #hypsigm_tf1_EV3_UP = TF1("hypsigm_tf1_EV3_UP","8.56439314e-01 + 1.23499270e+05/(x**2.16482822e+00) - (2.18176419e-01)/(1+exp(1.07899875e+01- (7.88408288e-03)*x))",200,2000)
      #hypsigm_tf1_EV4_UP = TF1("hypsigm_tf1_EV4_UP","8.60904008e-01 + 1.23499270e+05/(x**2.17117930e+00) - (2.17999656e-01)/(1+exp(1.07899996e+01 - (7.95974800e-03)*x))",200,2000)     
      #hypsigm_tf1_EV5_UP = TF1("hypsigm_tf1_EV5_UP","8.62005126e-01 + 1.23499270e+05/(x**2.16999853e+00) - (2.17998513e-01)/(1+exp(1.07900002e+01 - (7.81738421e-03)*x))",200,2000)
      #####################7jetcase###########################################

      ########################8jetcase##########################################
      #hypsigm_tf1_Nom = TF1("hypsigm_tf1_Nom","0.850 + 13399.837/(x**1.712) - (0.140)/(1+exp(15.310 - (0.011)*x))",200,2000)
      #hypsigm_tf1_EV0_UP = TF1("hypsigm_tf1_EV0_UP","8.95338397e-01 + (4.01497903e+04)/(x**2.05698004) - (1.66609393e-01)/(1+exp(1.19959379e+01 - (8.69973413e-03)*x))",200,2000)
      #hypsigm_tf1_EV1_UP = TF1("hypsigm_tf1_EV1_UP","8.54448000e-01 + (1.33998361e+04)/(x**1.71447118e+00) - (1.60166722e-01)/(1+exp(8.34372463 - (5.63719183e-03)*x))",200,2000)     
      #hypsigm_tf1_EV2_UP = TF1("hypsigm_tf1_EV2_UP","8.47325010e-01 + 1.33998370e+04/(x**1.71025242) - (1.13354272e-01)/(1+exp(1.53099203e+01 - (1.12943844e-02)*x))",200,2000)

      #hypsigm_tf1_EV3_UP = TF1("hypsigm_tf1_EV3_UP","8.60167050e-01 + 1.33998370e+04 /(x**1.71883188) - (1.38534161e-01)/(1+exp(1.53100045e+01 - (1.12651070e-02)*x))",200,2000)
      #hypsigm_tf1_EV4_UP = TF1("hypsigm_tf1_EV4_UP","8.48850911e-01 + 1.33998370e+04/(x**1.71371326) - (1.40002214e-01)/(1+exp(1.53099999e+01 - (1.09295316e-02)*x))",200,2000)     
      #hypsigm_tf1_EV5_UP = TF1("hypsigm_tf1_EV5_UP","8.49987031e-01 + 1.33998370e+04/(x**1.71200625) - (1.40004909e-01)/(1+exp(1.53099997e+01 - (1.13635323e-02)*x))",200,2000)
      #####################8jetcase###########################################

      ########################8jetcase for Hyp+Sig Down trafo##########################################
      hypsigm_tf1_Nom = TF1("hypsigm_tf1_Nom","0.851 + 13963.312/(x**1.719) - (0.140)/(1+exp(15.248 - (0.0111)*x))",200,2000)
      hypsigm_tf1_EV0_UP = TF1("hypsigm_tf1_EV0_UP","8.81107200e-01 + (3.26967026e+04)/(x**1.94969322) - (1.57750123e-01)/(1+exp(1.30852165e+01 - (9.49872339e-03)*x))",200,2000) # 1down trafo
      hypsigm_tf1_EV1_UP = TF1("hypsigm_tf1_EV1_UP","8.55416683e-01 + (1.39633092e+04)/(x**1.72045781) - (1.60252923e-01)/(1+exp(8.41518400 - (5.73404238e-03)*x))",200,2000)     
      hypsigm_tf1_EV2_UP = TF1("hypsigm_tf1_EV2_UP","8.48351004e-01 + 1.39633100e+04/(x**1.71625719) - (1.13321617e-01)/(1+exp(1.52489184e+01 - (1.12942435e-02)*x))",200,2000)

      hypsigm_tf1_EV3_UP = TF1("hypsigm_tf1_EV3_UP","8.61128505e-01 + 1.39633100e+04/(x**1.72491725) - (1.38545310e-01)/(1+exp(1.52490045e+01 - (1.12630664e-02)*x))",200,2000)
      hypsigm_tf1_EV4_UP = TF1("hypsigm_tf1_EV4_UP","8.49756844e-01 + 1.39633100e+04/(x**1.71982394) - (1.40003434e-01)/(1+exp(1.52489999e+01- (1.09227621e-02)*x))",200,2000)     
      hypsigm_tf1_EV5_UP = TF1("hypsigm_tf1_EV5_UP","8.51013081e-01 + 1.39633100e+04/(x**1.71799359) - (1.39995125e-01)/(1+exp(1.52490003e+01 - (1.06379088e-02)*x))",200,2000)
      #####################8jetcase###########################################


      hypsig_EV0_Dummy = TH1F( 'hypsig_EV0_Dummy', 'EV0_Dummy', 36, 200, 2000)
      hypsig_EV0_Up    = TH1F( 'hypsig_EV0_Up', 'EV0_Up', 36, 200, 2000)  
      hypsig_EV0_Down    = TH1F( 'hypsig_EV0_Down', 'EV0_Down', 36, 200, 2000)

      hypsig_EV1_Dummy = TH1F( 'hypsig_EV1_Dummy', 'EV1_Dummy', 36, 200, 2000)
      hypsig_EV1_Up    = TH1F( 'hypsig_EV1_Up', 'EV1_Up', 36, 200, 2000)  
      hypsig_EV1_Down    = TH1F( 'hypsig_EV1_Down', 'EV1_Down', 36, 200, 2000)

      hypsig_EV2_Dummy = TH1F( 'hypsig_EV2_Dummy', 'EV2_Dummy', 36, 200, 2000)
      hypsig_EV2_Up    = TH1F( 'hypsig_EV2_Up', 'EV2_Up', 36, 200, 2000)  
      hypsig_EV2_Down    = TH1F( 'hypsig_EV2_Down', 'EV2_Down', 36, 200, 2000)
      
      hypsig_EV3_Dummy = TH1F( 'hypsig_EV3_Dummy', 'EV3_Dummy', 36, 200, 2000)
      hypsig_EV3_Up    = TH1F( 'hypsig_EV3_Up', 'EV3_Up', 36, 200, 2000)  
      hypsig_EV3_Down    = TH1F( 'hypsig_EV3_Down', 'EV3_Down', 36, 200, 2000)

      hypsig_EV4_Dummy = TH1F( 'hypsig_EV4_Dummy', 'EV4_Dummy', 36, 200, 2000)
      hypsig_EV4_Up    = TH1F( 'hypsig_EV4_Up', 'EV4_Up', 36, 200, 2000)  
      hypsig_EV4_Down    = TH1F( 'hypsig_EV4_Down', 'EV4_Down', 36, 200, 2000)

      hypsig_EV5_Dummy = TH1F( 'hypsig_EV5_Dummy', 'EV5_Dummy', 36, 200, 2000)
      hypsig_EV5_Up    = TH1F( 'hypsig_EV5_Up', 'EV5_Up', 36, 200, 2000)  
      hypsig_EV5_Down    = TH1F( 'hypsig_EV5_Down', 'EV5_Down', 36, 200, 2000)

      
      #newBins_X = array('d', [200,300,350,400,450,500,550,600,650,700,750,800,850,900,950,1000,1050,1100,1150,1200,1250,1300,1350,1400,1450,1500,1550,1600,1650,1700,1750,1800,1850,1900,1950,2000])
      #hypsig_EV0_Dummy = TH1F( 'hypsig_EV0_Dummy', 'EV0_Dummy', 35, newBins_X)
      #hypsig_EV0_Up    = TH1F( 'hypsig_EV0_Up', 'EV0_Up', 35, newBins_X)  
      #hypsig_EV0_Down    = TH1F( 'hypsig_EV0_Down', 'EV0_Down', 35, newBins_X)

      #hypsig_EV1_Dummy = TH1F( 'hypsig_EV1_Dummy', 'EV1_Dummy', 35, newBins_X)
      #hypsig_EV1_Up    = TH1F( 'hypsig_EV1_Up', 'EV1_Up', 35, newBins_X)  
      #hypsig_EV1_Down    = TH1F( 'hypsig_EV1_Down', 'EV1_Down', 35, newBins_X)

      #hypsig_EV2_Dummy = TH1F( 'hypsig_EV2_Dummy', 'EV2_Dummy', 35, newBins_X)
      #hypsig_EV2_Up    = TH1F( 'hypsig_EV2_Up', 'EV2_Up', 35, newBins_X)  
      #hypsig_EV2_Down    = TH1F( 'hypsig_EV2_Down', 'EV2_Down', 35, newBins_X)
      
      #hypsig_EV3_Dummy = TH1F( 'hypsig_EV3_Dummy', 'EV3_Dummy', 35, newBins_X)
      #hypsig_EV3_Up    = TH1F( 'hypsig_EV3_Up', 'EV3_Up', 35, newBins_X)  
      #hypsig_EV3_Down    = TH1F( 'hypsig_EV3_Down', 'EV3_Down', 35, newBins_X)

      #hypsig_EV4_Dummy = TH1F( 'hypsig_EV4_Dummy', 'EV4_Dummy', 35, newBins_X)
      #hypsig_EV4_Up    = TH1F( 'hypsig_EV4_Up', 'EV4_Up', 35, newBins_X)  
      #hypsig_EV4_Down    = TH1F( 'hypsig_EV4_Down', 'EV4_Down', 35, newBins_X)

      #hypsig_EV5_Dummy = TH1F( 'hypsig_EV5_Dummy', 'EV5_Dummy', 35, newBins_X)
      #hypsig_EV5_Up    = TH1F( 'hypsig_EV5_Up', 'EV5_Up', 35, newBins_X)  
      #hypsig_EV5_Down    = TH1F( 'hypsig_EV5_Down', 'EV5_Down', 35, newBins_X)


      
      #hypsig_EV0_Dummy = TH1F( 'hypsig_EV0_Dummy', 'EV0_Dummy', 40, 0, 2000)
      #hypsig_EV0_Up    = TH1F( 'hypsig_EV0_Up', 'EV0_Up', 40, 0, 2000)  
      #hypsig_EV0_Down    = TH1F( 'hypsig_EV0_Down', 'EV0_Down', 40, 0, 2000)  
      #hypsig_EV1    = TH1F( 'hypsig_EV1', 'EV1', 40, 0, 2000)  
      #hypsig_EV2    = TH1F( 'hypsig_EV2', 'EV2', 40, 0, 2000)     
      #hypsig_EV3    = TH1F( 'hypsig_EV3', 'EV3', 40, 0, 2000) 
      #hypsig_EV4    = TH1F( 'hypsig_EV4', 'EV4', 40, 0, 2000)     
      #hypsig_EV5    = TH1F( 'hypsig_EV5', 'EV5', 40, 0, 2000) 
      bin_cont_EV0_up = 0 
      bin_cont_EV0_down = 0
      bin_cont_EV1_up = 0 
      bin_cont_EV1_down = 0
      bin_cont_EV2_up = 0 
      bin_cont_EV2_down = 0
      bin_cont_EV3_up = 0 
      bin_cont_EV3_down = 0
      bin_cont_EV4_up = 0 
      bin_cont_EV4_down = 0
      bin_cont_EV5_up = 0 
      bin_cont_EV5_down = 0
      for i in range(0, hypsig_EV0_Dummy.GetNbinsX()):
          bin_cont_EV0_up = hypsigm_tf1_EV0_UP.Eval(hypsig_EV0_Dummy.GetBinCenter(i+1)) - hypsigm_tf1_Nom.Eval(hypsig_EV0_Dummy.GetBinCenter(i+1))
          bin_cont_EV1_up = hypsigm_tf1_EV1_UP.Eval(hypsig_EV1_Dummy.GetBinCenter(i+1)) - hypsigm_tf1_Nom.Eval(hypsig_EV1_Dummy.GetBinCenter(i+1))
          bin_cont_EV2_up = hypsigm_tf1_EV2_UP.Eval(hypsig_EV2_Dummy.GetBinCenter(i+1)) - hypsigm_tf1_Nom.Eval(hypsig_EV2_Dummy.GetBinCenter(i+1))
          bin_cont_EV3_up = hypsigm_tf1_EV3_UP.Eval(hypsig_EV3_Dummy.GetBinCenter(i+1)) - hypsigm_tf1_Nom.Eval(hypsig_EV3_Dummy.GetBinCenter(i+1))
          bin_cont_EV4_up = hypsigm_tf1_EV4_UP.Eval(hypsig_EV4_Dummy.GetBinCenter(i+1)) - hypsigm_tf1_Nom.Eval(hypsig_EV4_Dummy.GetBinCenter(i+1))
          bin_cont_EV5_up = hypsigm_tf1_EV5_UP.Eval(hypsig_EV5_Dummy.GetBinCenter(i+1)) - hypsigm_tf1_Nom.Eval(hypsig_EV5_Dummy.GetBinCenter(i+1))
          #bin_cont = hypsigm_tf1_EV0.Eval(hypsig_EV0_Dummy.GetBinCenter(i+1))
          #print ("Nominal function at Bin"+str(i+1),  hypsigm_tf1_Nom.Eval(hypsig_EV0_Dummy.GetBinCenter(i+1)))
          #print ("Bin Content Up at Bin"+str(i+1), abs(bin_cont_up))

          #hypsig_EV0_Up.SetBinContent(i+1, hypsigm_tf1_Nom.Eval(hypsig_EV0_Dummy.GetBinCenter(i+1))+abs(bin_cont_EV0_up))
          #hypsig_EV1_Up.SetBinContent(i+1, hypsigm_tf1_Nom.Eval(hypsig_EV1_Dummy.GetBinCenter(i+1))+abs(bin_cont_EV1_up))
          #hypsig_EV2_Up.SetBinContent(i+1, hypsigm_tf1_Nom.Eval(hypsig_EV2_Dummy.GetBinCenter(i+1))+abs(bin_cont_EV2_up))
          #hypsig_EV3_Up.SetBinContent(i+1, hypsigm_tf1_Nom.Eval(hypsig_EV3_Dummy.GetBinCenter(i+1))+abs(bin_cont_EV3_up))
          #hypsig_EV4_Up.SetBinContent(i+1, hypsigm_tf1_Nom.Eval(hypsig_EV4_Dummy.GetBinCenter(i+1))+abs(bin_cont_EV4_up))
          #hypsig_EV5_Up.SetBinContent(i+1, hypsigm_tf1_Nom.Eval(hypsig_EV5_Dummy.GetBinCenter(i+1))+abs(bin_cont_EV5_up))

          hypsig_EV0_Up.SetBinContent(i+1, hypsigm_tf1_EV0_UP.Eval(hypsig_EV0_Dummy.GetBinCenter(i+1)))

          hypsig_EV1_Up.SetBinContent(i+1, hypsigm_tf1_EV1_UP.Eval(hypsig_EV1_Dummy.GetBinCenter(i+1)))
          hypsig_EV2_Up.SetBinContent(i+1, hypsigm_tf1_EV2_UP.Eval(hypsig_EV2_Dummy.GetBinCenter(i+1)))
          hypsig_EV3_Up.SetBinContent(i+1, hypsigm_tf1_EV3_UP.Eval(hypsig_EV3_Dummy.GetBinCenter(i+1)))
          hypsig_EV4_Up.SetBinContent(i+1, hypsigm_tf1_EV4_UP.Eval(hypsig_EV4_Dummy.GetBinCenter(i+1)))
          hypsig_EV5_Up.SetBinContent(i+1, hypsigm_tf1_EV5_UP.Eval(hypsig_EV5_Dummy.GetBinCenter(i+1)))

          #hypsig_EV0_Up.SetBinContent(i+1, hypsigm_tf1_EV0_UP.Eval(hypsig_EV0_Dummy.GetBinCenter(i+1)))
          #hypsig_EV1_Up.SetBinContent(i+1, hypsigm_tf1_EV1_UP.Eval(hypsig_EV1_Dummy.GetBinCenter(i+1)))
          #hypsig_EV2_Up.SetBinContent(i+1, hypsigm_tf1_EV2_UP.Eval(hypsig_EV2_Dummy.GetBinCenter(i+1)))
          #hypsig_EV3_Up.SetBinContent(i+1, hypsigm_tf1_EV3_UP.Eval(hypsig_EV3_Dummy.GetBinCenter(i+1)))
          #hypsig_EV4_Up.SetBinContent(i+1, hypsigm_tf1_EV4_UP.Eval(hypsig_EV4_Dummy.GetBinCenter(i+1)))
          #hypsig_EV5_Up.SetBinContent(i+1, hypsigm_tf1_EV5_UP.Eval(hypsig_EV5_Dummy.GetBinCenter(i+1)))
          
      for i in range(0, hypsig_EV0_Dummy.GetNbinsX()):
          bin_cont_EV0_down = hypsig_EV0_Up.GetBinContent(i+1) - hypsigm_tf1_Nom.Eval(hypsig_EV0_Dummy.GetBinCenter(i+1))
          bin_cont_EV1_down = hypsig_EV1_Up.GetBinContent(i+1) - hypsigm_tf1_Nom.Eval(hypsig_EV1_Dummy.GetBinCenter(i+1))
          bin_cont_EV2_down = hypsig_EV2_Up.GetBinContent(i+1) - hypsigm_tf1_Nom.Eval(hypsig_EV2_Dummy.GetBinCenter(i+1))
          bin_cont_EV3_down = hypsig_EV3_Up.GetBinContent(i+1) - hypsigm_tf1_Nom.Eval(hypsig_EV3_Dummy.GetBinCenter(i+1))
          bin_cont_EV4_down = hypsig_EV4_Up.GetBinContent(i+1) - hypsigm_tf1_Nom.Eval(hypsig_EV4_Dummy.GetBinCenter(i+1))
          bin_cont_EV5_down = hypsig_EV5_Up.GetBinContent(i+1) - hypsigm_tf1_Nom.Eval(hypsig_EV5_Dummy.GetBinCenter(i+1))
          #print ("Nominal function at Bin"+str(i+1),  hypsigm_tf1_Nom.Eval(hypsig_EV0_Dummy.GetBinCenter(i+1)))
          #print ("Bin Content Down at Bin"+str(i+1), abs(bin_cont_down))
          #bin_cont = hypsigm_tf1_EV0.Eval(hypsig_EV0_Dummy.GetBinCenter(i+1))
          #hypsig_EV0_Up.SetBinContent(i+1, hypsigm_tf1_Nom.Eval(hypsig_EV0_Dummy.GetBinCenter(i+1)) + abs(bin_cont))
          hypsig_EV0_Down.SetBinContent(i+1, hypsigm_tf1_Nom.Eval(hypsig_EV0_Dummy.GetBinCenter(i+1))-bin_cont_EV0_down)
          #hypsig_EV0_Down.SetBinContent(i+1, hypsigm_tf1_EV0_DOWN.Eval(hypsig_EV0_Dummy.GetBinCenter(i+1)))
          hypsig_EV1_Down.SetBinContent(i+1, hypsigm_tf1_Nom.Eval(hypsig_EV1_Dummy.GetBinCenter(i+1))-bin_cont_EV1_down)
          hypsig_EV2_Down.SetBinContent(i+1, hypsigm_tf1_Nom.Eval(hypsig_EV2_Dummy.GetBinCenter(i+1))-bin_cont_EV2_down)
          hypsig_EV3_Down.SetBinContent(i+1, hypsigm_tf1_Nom.Eval(hypsig_EV3_Dummy.GetBinCenter(i+1))-bin_cont_EV3_down)
          hypsig_EV4_Down.SetBinContent(i+1, hypsigm_tf1_Nom.Eval(hypsig_EV4_Dummy.GetBinCenter(i+1))-bin_cont_EV4_down)
          hypsig_EV5_Down.SetBinContent(i+1, hypsigm_tf1_Nom.Eval(hypsig_EV5_Dummy.GetBinCenter(i+1))-bin_cont_EV5_down)
          #print ("Down variation at Bin"+str(i+1), hypsig_EV0_Down.GetBinContent(i+1))
          #print((hypsigm_tf1_Nom.Eval(hypsig_EV0_Dummy.GetBinCenter(i+1)) + abs(bin_cont)), hypsig_EV0_Up.GetBinContent(i+1))
          #print((hypsigm_tf1_Nom.Eval(hypsig_EV0_Dummy.GetBinCenter(i+1)) - abs(bin_cont_down)), hypsig_EV0_Down.GetBinContent(i+1))  
      f=open("./h_weight/WFactor_5j_EB.txt","a")
      f.write("Bin Center"+" "+"Bin Content"+" "+"Bin Error"+"\n")     
      for i in range(0, h_weight_binned.GetNbinsX()):
          #print("Bin Number: ", i+1)
          ##print ("Bin Center: ", h_weight.GetBinCenter(i+1))
          #print ("Bin Content at i+1 bin: ", h_weight_binned.GetBinContent(i+1))
          #print ("Bin Content at i+1 bin: ", hypsigm_tf1_Nom.Eval(hypsig_EV0_Dummy.GetBinCenter(i+1)))
          f.write(str(h_weight_binned.GetBinCenter(i+1))+" "+str(h_weight_binned.GetBinContent(i+1))+" "+str(h_weight_binned.GetBinError(i+1))+"\n")
          #print  hypsig_EV0_Down.GetBinContent(i+1) - hypsigm_tf1_Nom.Eval(hypsig_EV0_Dummy.GetBinCenter(i+1))
          #print  hypsigm_tf1_Nom.Eval(hypsig_EV0_Dummy.GetBinCenter(i+1)) - hypsig_EV0_Up.GetBinContent(i+1)
      hypsig_EV0_Up.SetLineColor(kRed)  
      hypsig_EV0_Up.SetLineWidth(3)
      hypsig_EV0_Up.SetLineStyle(8)  
      hypsig_EV0_Down.SetLineColor(kRed)  
      hypsig_EV0_Down.SetLineWidth(3)
      hypsig_EV0_Down.SetLineStyle(8)

      hypsig_EV1_Up.SetLineColor(kGreen)  
      hypsig_EV1_Up.SetLineWidth(3)
      hypsig_EV1_Up.SetLineStyle(8)  
      hypsig_EV1_Down.SetLineColor(kGreen)  
      hypsig_EV1_Down.SetLineWidth(3)
      hypsig_EV1_Down.SetLineStyle(8)

      hypsig_EV2_Up.SetLineColor(kOrange)  
      hypsig_EV2_Up.SetLineWidth(3)
      hypsig_EV2_Up.SetLineStyle(8)  
      hypsig_EV2_Down.SetLineColor(kOrange)  
      hypsig_EV2_Down.SetLineWidth(3)
      hypsig_EV2_Down.SetLineStyle(8)

      hypsig_EV4_Up.SetLineColor(kMagenta)  
      hypsig_EV4_Up.SetLineWidth(2)
      hypsig_EV4_Up.SetLineStyle(8)  
      hypsig_EV4_Down.SetLineColor(kMagenta)  
      hypsig_EV4_Down.SetLineWidth(3)
      hypsig_EV4_Down.SetLineStyle(8)

      hypsig_EV3_Up.SetLineColor(kBlack)  
      hypsig_EV3_Up.SetLineWidth(3)
      hypsig_EV3_Up.SetLineStyle(8)  
      hypsig_EV3_Down.SetLineColor(kBlack)  
      hypsig_EV3_Down.SetLineWidth(3)
      hypsig_EV3_Down.SetLineStyle(8)

      hypsig_EV5_Up.SetLineColor(kGray)  
      hypsig_EV5_Up.SetLineWidth(2)
      hypsig_EV5_Up.SetLineStyle(8)  
      hypsig_EV5_Down.SetLineColor(kGray)  
      hypsig_EV5_Down.SetLineWidth(2)
      hypsig_EV5_Down.SetLineStyle(8)

      leg = TLegend(0.62,0.485,0.885,0.725)
      leg.SetShadowColor(kWhite)
      leg.SetFillColor(kWhite)
      leg.SetLineColor(kWhite)
      h_weight_binned.GetXaxis().SetRangeUser(200,2000)
      h_weight_binned.Draw("EP")
      #myfunc2.GetXaxis().SetRangeUser(150,2000)

      myfunc2.Draw("SAMEL")   
      hypsig_EV0_Up.Draw("SAMEL")
      hypsig_EV0_Down.Draw("SAMEL")
      hypsig_EV1_Up.Draw("SAMEL")
      hypsig_EV1_Down.Draw("SAMEL")
      hypsig_EV2_Up.Draw("SAMEL")
      hypsig_EV2_Down.Draw("SAMEL")
      hypsig_EV3_Up.Draw("SAMEL")
      hypsig_EV3_Down.Draw("SAMEL")
      hypsig_EV4_Up.Draw("SAMEL")
      hypsig_EV4_Down.Draw("SAMEL")
      hypsig_EV5_Up.Draw("SAMEL")
      hypsig_EV5_Down.Draw("SAMEL")

      leg.AddEntry(h_weight,"Powheg+Pythia8","epl")
      #leg.AddEntry(myfunc2,"Exponential + Sigmoid","l")
      #leg.AddEntry(myfunc2,"Poly 2 + Exp 1","l")

      leg.AddEntry(myfunc2,"Hyperbola + Sigmoid","l")
      leg.AddEntry(hypsig_EV0_Up,"EV1","l")
      leg.AddEntry(hypsig_EV1_Up,"EV2","l")
      leg.AddEntry(hypsig_EV2_Up,"EV3","l")
      leg.AddEntry(hypsig_EV3_Up,"EV4","l")
      leg.AddEntry(hypsig_EV4_Up,"EV5","l")
      leg.AddEntry(hypsig_EV5_Up,"EV6","l")
      
      leg.Draw()
      print "1: ",sigPars[0] ,"2:",sigPars[1],"3:", sigPars[2] ,"4:",sigPars[3] ,"5:",sigPars[4] ,"6:",sigPars[5]
      #print "1: ",sigPars[0] ,"2:",sigPars[1],"3:", sigPars[2] ,"4:",sigPars[3] ,"5:",sigPars[4] ,"6:",sigPars[5],"7:",sigPars[6]
      #print "1: ",sigPars[0] ,"2:",sigPars[1],"3:", sigPars[2] ,"4:",sigPars[3] ,"5:",sigPars[4]
      #print "1: ",sigPars[0] ,"2:",sigPars[1],"3:", sigPars[2] , "4:",sigPars[3]
      #print "1: ",sigPars[0] ,"2:",sigPars[1],"3:", sigPars[2], "4:",sigPars[3], "5:",sigPars[4]
      #pad1.cd()
      #myText(0.35,0.78,1,"#sqrt{s}=13 TeV, 139 fb^{-1}")
      ATLAS_LABEL(0.35,0.83,"Internal",1,0.14)
      if "TwoTags" in btagStrategy:
          myText(0.35,0.78,1,"l+jets Resolved: at least 8 jet, 2 b-tag")
      myText(0.35,0.73,1,"#chi^{2}/ndf :"+str(round(Chi2,2))+"/"+str(round(NDF,2)))  
      f=open("../ttRew/ttRew_qqbb_HypSig_HTall_mod_wonorm_expsig_EB.txt","a")
      f.write(HistoName+str(sigPars[0])+";"+str(sigPars[1])+";"+str(sigPars[2])+";"+str(sigPars[3])+";"+str(sigPars[4])+";"+str(sigPars[5])+"\n")
      #f.write(HistoName+str(sigPars[0])+";"+str(sigPars[1])+";"+str(sigPars[2])+";"+str(sigPars[3])+";"+str(sigPars[4])+";"+str(sigPars[5])+";"+str(sigPars[6])+"\n")
      #f.write(HistoName+str(sigPars[0])+";"+str(sigPars[1])+";"+str(sigPars[2])+";"+str(sigPars[3])+";"+str(sigPars[4])+"\n")
      f.close() 
      #c1.RedrawAxis()
      c1.RedrawAxis()
      c1.Update()
      c1.RedrawAxis()
      #c1.SaveAs("../Plots/weight_HTall_PS/Weight_%s_trial_ttRew_7j_HTall_mod_wonorm_RC_Xcheck_CombBin_ExpSig_EV1_DOWN.pdf" % (HistoName+"_"+btagStrategy))
      c1.SaveAs("../Plots/weight_HTall_PS/Weight_%s_trial_ttRew_8j_HTall_mod_wonorm_ErrorBand_INTReview.pdf" % (HistoName+"_"+btagStrategy))

