# -*- coding: utf-8 -*-
import sys
import glob
import math
import re
from ROOT import *
from array import *
gROOT.SetBatch(True)
gStyle.SetOptStat(0)
gStyle.SetPalette(1)
#gROOT.LoadMacro("../style/AtlasStyle.C")
#gROOT.LoadMacro("../style/AtlasUtils.C")
gROOT.LoadMacro("../style/AtlasStyle.C")
gROOT.LoadMacro("../style/AtlasUtils.C")
#gROOT.LoadMacro("../utilis/AtlasLabels.C")
SetAtlasStyle()
#import ROOT
#import array

#gROOT.SetBatch(True)
#gStyle.SetOptStat(0)
#gStyle.SetPalette(1)
#gROOT.LoadMacro("../AtlasUtils/AtlasStyle.C")
#gROOT.LoadMacro("../AtlasUtils/AtlasUtils.C")
#gROOT.LoadMacro("../AtlasUtils/AtlasLabels.C")
#SetAtlasStyle()

#from array import *
def getTGraphs(): 
    "Function to read mass resolution from text files"
    graphs={}
    filename_MVA09="./txt/EB/SignalYield_09_FourPlusTags_EB.txt"
    filename_MVA29="./txt/EB/SignalYield_29_FourPlusTags_EB.txt"
    filename_MVA49="./txt/EB/SignalYield_49_FourPlusTags_EB.txt"
    filename_MVA69="./txt/EB/SignalYield_69_FourPlusTags_EB.txt"
    filename_MVA07="./txt/EB/SignalYield_07_FourPlusTags_EB.txt"
    filename_MVA27="./txt/EB/SignalYield_27_FourPlusTags_EB.txt"
    filename_MVA47="./txt/EB/SignalYield_47_FourPlusTags_EB.txt"
    filename_MVA67="./txt/EB/SignalYield_67_FourPlusTags_EB.txt"
    filename_MVA10="./txt/EB/SignalYield_10_FourPlusTags_EB.txt"
    filename_MVA50="./txt/EB/SignalYield_50_FourPlusTags_EB.txt"
    filename_MVA52="./txt/EB/SignalYield_52_FourPlusTags_EB.txt"
    filename_MVA54="./txt/EB/SignalYield_54_FourPlusTags_EB.txt"
    filename_MVA56="./txt/EB/SignalYield_56_FourPlusTags_EB.txt"
    filename_MVA19="./txt/EB/SignalYield_19_FourPlusTags_EB.txt"
    filename_MVA9="./txt/EB/SignalYield_9_FourPlusTags_EB.txt"
    filename_Bkg="./txt/EB/BkgYields_FourPlusTags_EB.txt"

    textfile_MVA09 = open(filename_MVA09, "r")
    textfile_MVA29 = open(filename_MVA29, "r")
    textfile_MVA49 = open(filename_MVA49, "r")
    textfile_MVA69 = open(filename_MVA69, "r")
    textfile_MVA07 = open(filename_MVA07, "r")
    textfile_MVA27 = open(filename_MVA27, "r")
    textfile_MVA47 = open(filename_MVA47, "r")
    textfile_MVA67 = open(filename_MVA67, "r")
    textfile_MVA10 = open(filename_MVA10, "r")
    textfile_MVA50 = open(filename_MVA50, "r")
    textfile_MVA52 = open(filename_MVA52, "r")
    textfile_MVA54 = open(filename_MVA54, "r")
    textfile_MVA56 = open(filename_MVA56, "r")
    textfile_MVA19 = open(filename_MVA19, "r")
    textfile_MVA9 = open(filename_MVA9, "r")
    textfile_Bkg = open(filename_Bkg, "r")

    v_mass = array('d')
    mass_list = [250.0, 300.0, 350.0, 400.0, 500.0, 600.0, 700.0, 800.0, 900.0, 1000.0, 1200.0, 1400.0, 1600.0, 1800.0, 2000.0, 2500.0, 3000.0]
    v_Sig_MVA09=array('d')
    v_Sig_MVA29=array('d')
    v_Sig_MVA49=array('d') 
    v_Sig_MVA69=array('d') 
    v_Sig_MVA07=array('d')
    v_Sig_MVA27=array('d')
    v_Sig_MVA47=array('d') 
    v_Sig_MVA67=array('d')
    v_Sig_MVA10=array('d')
    v_Sig_MVA50=array('d')
    v_Sig_MVA52=array('d')
    v_Sig_MVA54=array('d')
    v_Sig_MVA56=array('d')
    v_Sig_MVA19=array('d') 
    v_Sig_MVA9=array('d')
    
    for i in mass_list:
        v_mass.append(float(i))
    #for line, line_bkg in zip(textfile_MVA09,textfile_Bkg):
    for line in textfile_MVA09:    
        if not line.startswith("Mass"):  
           tokens=line.rstrip().split(" ")
           #tokens_bkg=line_bkg.rstrip().split(" ")
           #v_Sig_MVA09.append(float(tokens[1])/math.sqrt(1636785.087)) #Inc
           #v_Sig_MVA09.append(float(tokens[1])/math.sqrt(220996.853549)) #3Tags
           v_Sig_MVA09.append(float(tokens[1])/math.sqrt(16808.7586998)) #4+Tags
    #for line, line_bkg in zip(textfile_MVA29,textfile_Bkg):
    for line in textfile_MVA29:    
        if not line.startswith("Mass"):  
           tokens=line.rstrip().split(" ")
           #tokens_bkg=line_bkg.rstrip().split(" ")
           #v_Sig_MVA29.append(float(tokens[1])/math.sqrt(1728615.518)) #Inc
           #v_Sig_MVA29.append(float(tokens[1])/math.sqrt(230940.029195)) #3Tags
           v_Sig_MVA29.append(float(tokens[1])/math.sqrt(17593.8971702)) #4+Tags
    #for line, line_bkg in zip(textfile_MVA49,textfile_Bkg):
    for line in textfile_MVA49:    
        if not line.startswith("Mass"):  
           tokens=line.rstrip().split(" ")
           #tokens_bkg=line_bkg.rstrip().split(" ")
           #v_Sig_MVA49.append(float(tokens[1])/math.sqrt(1783443.884)) #Inc
           #v_Sig_MVA49.append(float(tokens[1])/math.sqrt(0.615))   #IncDen 
           #v_Sig_MVA49.append(float(tokens[1])/math.sqrt(236935.692083))   #3Tags 
           v_Sig_MVA49.append(float(tokens[1])/math.sqrt(18076.4025265))   #4+Tags
    #for line, line_bkg in zip(textfile_MVA69, textfile_Bkg):
    for line in textfile_MVA69:    
        if not line.startswith("Mass"):  
           tokens=line.rstrip().split(" ")
           #tokens_bkg=line_bkg.rstrip().split(" ")
           #v_Sig_MVA69.append(float(tokens[1])/math.sqrt(1812819.231)) #Inc
           #v_Sig_MVA69.append(float(tokens[1])/math.sqrt(0.541))   #IncDen  
           #v_Sig_MVA69.append(float(tokens[1])/math.sqrt(240158.048818))  #3Tags
           v_Sig_MVA69.append(float(tokens[1])/math.sqrt(18349.8996441))   #4+Tags
    #for line, line_bkg  in zip(textfile_MVA07, textfile_Bkg):
    for line in textfile_MVA07:    
        if not line.startswith("Mass"):  
           tokens=line.rstrip().split(" ")
           #tokens_bkg=line_bkg.rstrip().split(" ")
           #v_Sig_MVA07.append(float(tokens[1])/math.sqrt(923024.585)) #Inc
           #v_Sig_MVA07.append(float(tokens[1])/math.sqrt(0.591))  #IncDen
           #v_Sig_MVA07.append(float(tokens[1])/math.sqrt(114593.043647))  #3Tags
           v_Sig_MVA07.append(float(tokens[1])/math.sqrt(8559.00485151))   #4+Tags
    #for line, line_bkg in zip(textfile_MVA27, textfile_Bkg):
    for line in textfile_MVA27:    
        if not line.startswith("Mass"):  
           tokens=line.rstrip().split(" ")
           #tokens_bkg=line_bkg.rstrip().split(" ")
           #v_Sig_MVA27.append(float(tokens[1])/math.sqrt(1014861.200)) #Inc
           #v_Sig_MVA27.append(float(tokens[1])/math.sqrt(0.505))  #IncDen
           #v_Sig_MVA27.append(float(tokens[1])/math.sqrt(124536.383931))  #3Tags
           v_Sig_MVA27.append(float(tokens[1])/math.sqrt(9344.15087695))  #4+Tags
    #for line, line_bkg in zip(textfile_MVA47, textfile_Bkg):
    for line in textfile_MVA47:    
        if not line.startswith("Mass"):  
           tokens=line.rstrip().split(" ")
           #tokens_bkg=line_bkg.rstrip().split(" ")
           #v_Sig_MVA47.append(float(tokens[1])/math.sqrt(1069690.901)) #Inc
           #v_Sig_MVA47.append(float(tokens[1])/math.sqrt(0.436))   #IncDen 
           #_Sig_MVA47.append(float(tokens[1])/math.sqrt(130532.098495))  #3Tags
           v_Sig_MVA47.append(float(tokens[1])/math.sqrt(9826.6560329))  #4+Tags
    #for line, line_bkg in zip(textfile_MVA67, textfile_Bkg) :
    for line in textfile_MVA67:    
        if not line.startswith("Mass"):  
           tokens=line.rstrip().split(" ")
           #tokens_bkg=line_bkg.rstrip().split(" ")
           #v_Sig_MVA67.append(float(tokens[1])/math.sqrt(1099067.055)) #Inc
           #v_Sig_MVA67.append(float(tokens[1])/math.sqrt(0.379))  #IncDen
           #v_Sig_MVA67.append(float(tokens[1])/math.sqrt(133754.444203))  #3Tags
           v_Sig_MVA67.append(float(tokens[1])/math.sqrt(10100.1521704))  #4+Tags
    #for line, line_bkg in zip(textfile_MVA10, textfile_Bkg):
    for line in textfile_MVA10:    
        if not line.startswith("Mass"):  
           tokens=line.rstrip().split(" ")
           #tokens_bkg=line_bkg.rstrip().split(" ")
           #v_Sig_MVA10.append(float(tokens[1])/math.sqrt(189416.899)) #Inc
           #v_Sig_MVA10.append(float(tokens[1])/math.sqrt(0.085))   #IncDen
           #v_Sig_MVA10.append(float(tokens[1])/math.sqrt(20660.4065581))   #3Tags
           v_Sig_MVA10.append(float(tokens[1])/math.sqrt(1678.54853893))   #4+Tags
    #for line, line_bkg in zip(textfile_MVA50, textfile_Bkg):
    for line in textfile_MVA50:    
        if not line.startswith("Mass"):  
           tokens=line.rstrip().split(" ")
           #tokens_bkg=line_bkg.rstrip().split(" ")
           #v_Sig_MVA50.append(float(tokens[1])/math.sqrt(163948.975)) #Inc
           #v_Sig_MVA50.append(float(tokens[1])/math.sqrt(17835.7536284)) #3Tags
           v_Sig_MVA50.append(float(tokens[1])/math.sqrt(1425.23558939)) #4+Tags
    for line in textfile_MVA52:    
        if not line.startswith("Mass"):  
           tokens=line.rstrip().split(" ")
           #tokens_bkg=line_bkg.rstrip().split(" ")
           #v_Sig_MVA50.append(float(tokens[1])/math.sqrt(163948.975)) #Inc
           #v_Sig_MVA52.append(float(tokens[1])/math.sqrt(33560.4405183)) #3Tags
           v_Sig_MVA52.append(float(tokens[1])/math.sqrt(2615.19546392)) #4+Tags     
    for line in textfile_MVA54:    
        if not line.startswith("Mass"):  
           tokens=line.rstrip().split(" ")
           #tokens_bkg=line_bkg.rstrip().split(" ")
           #v_Sig_MVA50.append(float(tokens[1])/math.sqrt(163948.975)) #Inc
           #v_Sig_MVA54.append(float(tokens[1])/math.sqrt(58730.4220642)) #3Tags
           v_Sig_MVA54.append(float(tokens[1])/math.sqrt(4477.14874643)) #4+Tags       
    for line in textfile_MVA56:    
        if not line.startswith("Mass"):  
           tokens=line.rstrip().split(" ")
           #tokens_bkg=line_bkg.rstrip().split(" ")
           #v_Sig_MVA50.append(float(tokens[1])/math.sqrt(163948.975)) #Inc
           #v_Sig_MVA56.append(float(tokens[1])/math.sqrt(100559.862093)) #3Tags
           v_Sig_MVA56.append(float(tokens[1])/math.sqrt(7589.0011189)) #4+Tags
    #for line, line_bkg in zip(textfile_MVA19, textfile_Bkg):
    for line in textfile_MVA19:    
        if not line.startswith("Mass"):  
           tokens=line.rstrip().split(" ")
           #tokens_bkg=line_bkg.rstrip().split(" ")
           #v_Sig_MVA19.append(float(tokens[1])/math.sqrt(1826177.353)) #Inc
           #v_Sig_MVA19.append(float(tokens[1])/math.sqrt(241657.064552)) #3Tags
           v_Sig_MVA19.append(float(tokens[1])/math.sqrt(18487.2966758)) #4+Tags
    #for line, line_bkg in zip(textfile_MVA9, textfile_Bkg):
    for line in textfile_MVA9:    
        if not line.startswith("Mass"):  
           tokens=line.rstrip().split(" ")
           #tokens_bkg=line_bkg.rstrip().split(" ")
           #v_Sig_MVA9.append(float(tokens[1])/math.sqrt(405800.732)) #Inc
           #v_Sig_MVA9.append(float(tokens[1])/math.sqrt(63706.8661286))  #3Tags 
           v_Sig_MVA9.append(float(tokens[1])/math.sqrt(5405.10803887))  #4+Tags                             
    graphs["MVASig09"]=TGraph(len(v_mass),v_mass, v_Sig_MVA09)
    graphs["MVASig29"]=TGraph(len(v_mass),v_mass, v_Sig_MVA29)
    graphs["MVASig49"]=TGraph(len(v_mass),v_mass, v_Sig_MVA49)
    graphs["MVASig69"]=TGraph(len(v_mass),v_mass, v_Sig_MVA69)
    graphs["MVASig07"]=TGraph(len(v_mass),v_mass, v_Sig_MVA07)
    graphs["MVASig27"]=TGraph(len(v_mass),v_mass, v_Sig_MVA27)
    graphs["MVASig47"]=TGraph(len(v_mass),v_mass, v_Sig_MVA47)
    graphs["MVASig67"]=TGraph(len(v_mass),v_mass, v_Sig_MVA67)
    graphs["MVASig10"]=TGraph(len(v_mass),v_mass, v_Sig_MVA10)
    graphs["MVASig50"]=TGraph(len(v_mass),v_mass, v_Sig_MVA50)
    graphs["MVASig52"]=TGraph(len(v_mass),v_mass, v_Sig_MVA52)
    graphs["MVASig54"]=TGraph(len(v_mass),v_mass, v_Sig_MVA54)
    graphs["MVASig56"]=TGraph(len(v_mass),v_mass, v_Sig_MVA56)
    graphs["MVASig19"]=TGraph(len(v_mass),v_mass, v_Sig_MVA19)
    graphs["MVASig9"]=TGraph(len(v_mass),v_mass, v_Sig_MVA9)
    return graphs

sample = {}
cmassres = TCanvas("mvasig","mvasig",800,600)
frame = TH1F("", "", 35, 250, 3200)
frame.SetMinimum(0.00)
frame.SetMaximum(30.00)
frame.Draw()
frame.GetXaxis().SetTitle("H^{+} mass [GeV]")
frame.GetXaxis().SetTitleOffset(1.1)
#frame.GetYaxis().SetTitle("#sigma(pp #rightarrow tbH^{#pm}) #times BR(H^{#pm}#rightarrow Wh) [pb]")
frame.GetYaxis().SetTitle("S/#sqrt{B}")
frame.GetXaxis().SetRangeUser(250,3000)

#cLimit.SetLogy()

#graphs=getTGraphs(options.inFile)
graphs=getTGraphs()
MVA09=graphs["MVASig09"]
MVA29=graphs["MVASig29"]
MVA49=graphs["MVASig49"]
MVA69=graphs["MVASig69"]
MVA07=graphs["MVASig07"]
MVA27=graphs["MVASig27"]
MVA47=graphs["MVASig47"]
MVA67=graphs["MVASig67"]
MVA10=graphs["MVASig10"]
MVA50=graphs["MVASig50"]
MVA52=graphs["MVASig52"]
MVA54=graphs["MVASig54"]
MVA56=graphs["MVASig56"]
MVA19=graphs["MVASig19"]
MVA9=graphs["MVASig9"]

#leg = TLegend(0.61, 0.7, 0.91, 0.9)
#leg = TLegend(0.61, 0.33, 0.91, 0.785) #ge4b reg
leg = TLegend(0.61, 0.29, 0.91, 0.73) #3b reg
leg.SetFillColor(kWhite)
leg.SetBorderSize(0)
leg.SetLineColor(kWhite)
leg.SetTextFont(43)
leg.SetTextSize(20)

myText(0.20,0.80,1,"#sqrt{s}=13 TeV, 139 fb^{-1}")
#myText(0.20,0.75,1,"at least 5 jet, 3 b-tags and at least 4 b-tags")
myText(0.20,0.75,1,"at least 5 jet, at least 4 b-tags") 
#myText(0.20,0.75,1,"at least 5 jet, 3 b-tags")

MVA09.SetLineColor(kBlack)
MVA09.SetLineStyle(1)
MVA09.SetLineWidth(2)
MVA09.Draw("L") #CSAME

MVA29.SetLineColor(kRed)
MVA29.SetLineStyle(1)
MVA29.SetLineWidth(2)
MVA29.Draw("L") #CSAME

MVA49.SetLineColor(kBlue)
MVA49.SetLineStyle(1)
MVA49.SetLineWidth(2)
MVA49.Draw("SAME L") #CSAME

MVA69.SetLineColor(kViolet)
MVA69.SetLineStyle(1)
MVA69.SetLineWidth(2)
MVA69.Draw("SAME L") #CSAME

MVA07.SetLineColor(kGreen)
MVA07.SetLineStyle(1)
MVA07.SetLineWidth(2)
MVA07.Draw("SAME L") #CSAME

MVA27.SetLineColor(29)
MVA27.SetLineStyle(1)
MVA27.SetLineWidth(2)
MVA27.Draw("SAME L") #CSAME

MVA47.SetLineColor(kMagenta)
MVA47.SetLineStyle(1)
MVA47.SetLineWidth(2)
MVA47.Draw("SAME L") #CSAME

MVA67.SetLineColor(95)
MVA67.SetLineStyle(1)
MVA67.SetLineWidth(2)
MVA67.Draw("SAME L") #CSAME

MVA10.SetLineColor(kYellow)
MVA10.SetLineStyle(1)
MVA10.SetLineWidth(2)
MVA10.Draw("SAME L") #CSAME

MVA50.SetLineColor(213)
MVA50.SetLineStyle(1)
MVA50.SetLineWidth(2)
MVA50.Draw("SAME L") #CSAME

MVA52.SetLineColor(50)
MVA52.SetLineStyle(1)
MVA52.SetLineWidth(2)
MVA52.Draw("SAME L") #CSAME

MVA54.SetLineColor(kGray)
MVA54.SetLineStyle(1)
MVA54.SetLineWidth(2)
MVA54.Draw("SAME L") #CSAME

MVA56.SetLineColor(kGreen+3)
MVA56.SetLineStyle(1)
MVA56.SetLineWidth(2)
MVA56.Draw("SAME L") #CSAME

MVA19.SetLineColor(kRed+3)
MVA19.SetLineStyle(1)
MVA19.SetLineWidth(2)
MVA19.Draw("SAME L") #CSAME

MVA9.SetLineColor(kTeal)
MVA9.SetLineStyle(1)
MVA9.SetLineWidth(2)
MVA9.Draw("SAME L") #CSAME

#observed.GetXaxis().SetRangeUser(200,2000)
#observed.SetLineColor(1)
#observed.SetLineStyle(1)
#observed.SetLineWidth(2)
#observed.SetMarkerStyle(20)
#observed.SetMarkerSize(1.)
    ##observed.Draw("LPSAME")

gPad.RedrawAxis()
    
    #leg.AddEntry(observed,"95% observed CL_{s}", "lp")
leg.AddEntry(MVA09,"0.0 <= maxMVA < 0.9", "l")
leg.AddEntry(MVA19,"-1.0 <= maxMVA < 0.9","l")
leg.AddEntry(MVA29,"-0.2 <= maxMVA < 0.9","l")
leg.AddEntry(MVA49,"-0.4 <= maxMVA < 0.9","l")
leg.AddEntry(MVA69,"-0.6 <= maxMVA < 0.9","l")
leg.AddEntry(MVA07,"0.0 <= maxMVA < 0.7", "l")
leg.AddEntry(MVA27,"-0.2 <= maxMVA < 0.7","l")
leg.AddEntry(MVA47,"-0.4 <= maxMVA < 0.7","l")
leg.AddEntry(MVA67,"-0.6 <= maxMVA < 0.7","l")
leg.AddEntry(MVA50,"-0.5 <= maxMVA < 0.0","l")
leg.AddEntry(MVA52,"-0.5 <= maxMVA < 0.2","l")
leg.AddEntry(MVA54,"-0.5 <= maxMVA < 0.4","l")
leg.AddEntry(MVA56,"-0.5 <= maxMVA < 0.6","l")
leg.AddEntry(MVA10,"-1.0 <= maxMVA < 0.0", "l")
leg.AddEntry(MVA9,"maxMVA >= 0.9 (SR)","l")

leg.Draw()

ATLAS_LABEL(0.2, 0.85, "Internal", 1,0.19)
#ATLAS_LABEL(0.20,0.85,"work-in-progress",1,0.19)
    #ROOT.ATLASLabel(0.32, 0.85, "Preliminary", ROOT.kBlack)
    #ROOT.ATLASLabel(0.32, 0.85, "", ROOT.kBlack)
         

pdffilename="CRYieldSig_FourPlusTags_EB.pdf"
    
cmassres.SaveAs(pdffilename)
#cLimit.Print(pdffilename)    

