# -*- coding: utf-8 -*-
#python
import sys
import glob
import math
import re
from ROOT import *
from array import *
from ROOT import TCanvas, TFile, TLine, TProfile, TNtuple, TH1F, TH2F
#import ROOT
#import array

gROOT.SetBatch(True)
gStyle.SetOptStat(0)
gStyle.SetPalette(1)
gROOT.LoadMacro("../style/AtlasStyle.C")
gROOT.LoadMacro("../style/AtlasUtils.C")
SetAtlasStyle()

y_axis_label="Event fraction"

c_blue   = TColor.GetColor("#3366ff")
c_red    = TColor.GetColor("#ee3311")
c_orange = TColor.GetColor("#ff9900")

def NormalizeHisto_1(histo):
     n_events=histo.Integral(-1,histo.GetNbinsX()+1)
     #n_events=histo.Integral()
     if n_events == 0:
         return
     print n_events, histo.Integral(histo.GetNbinsX(),histo.GetNbinsX()+1)
     histo.Scale(1./n_events)
     histo.SetLineWidth(3)
     histo.SetStats(0)
     histo.SetFillStyle(3001)
     histo.SetMarkerColor(histo.GetLineColor())
     histo.SetMarkerSize(0.0)
     histo.GetXaxis().SetTitleOffset(1.2)
     histo.GetYaxis().SetTitleOffset(1.52)
     histo.GetXaxis().SetLabelSize(0.05)
     histo.GetYaxis().SetLabelSize(0.05)
     histo.GetXaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetNdivisions(504)
     histo.GetXaxis().SetNdivisions(504)

def NormalizeHisto_2(histo):
     n_events=histo.Integral(-1,histo.GetNbinsX()+1)
     #n_events=histo.Integral()
     if n_events == 0:
         return
     #print histo.Integral(-1,0)    
     print n_events, histo.Integral(-1,0), histo.Integral(histo.GetNbinsX(),histo.GetNbinsX()+1)
     histo.Scale(1./n_events)
     histo.SetLineWidth(3)
     histo.SetStats(0)
     histo.SetFillStyle(3001)
     histo.SetMarkerColor(histo.GetLineColor())
     histo.SetMarkerSize(0.0)
     histo.GetXaxis().SetTitleOffset(1.2)
     histo.GetYaxis().SetTitleOffset(1.52)
     histo.GetXaxis().SetLabelSize(0.05)
     histo.GetYaxis().SetLabelSize(0.05)
     histo.GetXaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetNdivisions(504)
     histo.GetXaxis().SetNdivisions(504)

c1 = TCanvas("ShapePlots","",500,500)

HistoNameR = "lep_top_mass"

#file600       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_check_6jin3b_woew/hp600_AFII.root_77p_225.root","READ")
filett       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/tt_PP8.root_77p_225.root","READ")
dirttb       = filett.GetDirectory("nominal_Loose").GetDirectory("mVH_1b")
dirttc       = filett.GetDirectory("nominal_Loose").GetDirectory("mVH_1c")
dirttl       = filett.GetDirectory("nominal_Loose").GetDirectory("mVH_1l")
h_tt_1b     = dirttb.Get("tt_PP8_1B_mVH_Resolved_SR_ThreeTags_nominal_Loose")
h_tt_1c     = dirttc.Get("tt_PP8_1C_mVH_Resolved_SR_ThreeTags_nominal_Loose")
h_tt_1l     = dirttl.Get("tt_PP8_1L_mVH_Resolved_SR_ThreeTags_nominal_Loose")

filettf       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/tt_PP8filtered.root_77p_225.root","READ")
dirttbf      = filettf.GetDirectory("nominal_Loose").GetDirectory("mVH_1b")
dirttcf       = filettf.GetDirectory("nominal_Loose").GetDirectory("mVH_1c")
dirttlf      = filettf.GetDirectory("nominal_Loose").GetDirectory("mVH_1l")
h_tt_1bf     = dirttbf.Get("tt_PP8filtered_1B_mVH_Resolved_SR_ThreeTags_nominal_Loose")
h_tt_1cf     = dirttcf.Get("tt_PP8filtered_1C_mVH_Resolved_SR_ThreeTags_nominal_Loose")
h_tt_1lf     = dirttlf.Get("tt_PP8filtered_1L_mVH_Resolved_SR_ThreeTags_nominal_Loose")

h_tt_background = h_tt_1b + h_tt_1c + h_tt_1l + h_tt_1bf +  h_tt_1cf + h_tt_1lf
#dir600       = file600.GetDirectory("nominal_Loose").GetDirectory("DeltaPhi_HW")
#h_sig_Hplus_m600 = dir600.Get("hp600_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
#h_sig_Hplus_m600 = dir600.Get("hp600_AFII_lep_top_mass_Resolved_SR_ThreeTags_nominal_Loose")
#h_sig_Hplus_m600 = dir600.Get("hp600_AFII_DeltaPhi_HW_Resolved_SR_ThreeTags_nominal_Loose")
#h_sig_Hplus_m600 = dir600.Get("hp250_AFII_mVH_Resolved_SR_ThreeTags_nominal_Loose")
h_tt_background.SetLineColor(kViolet)
h_tt_background.SetLineStyle(1)

file600_V3 = TFile.Open("/cephfs/user/s6subans/TRExFitter/qqlv_225_Mixed_qqlv/Hp250_qqlv/Histograms/Hp250_qqlv_SR_qqbb_5jex3bex_histos.root","READ")
h_ttb_v3  = file600_V3.Get("SR_qqbb_5jex3bex_ttb_orig")
h_ttc_v3  = file600_V3.Get("SR_qqbb_5jex3bex_ttc_orig")
h_ttl_v3  = file600_V3.Get("SR_qqbb_5jex3bex_ttlight_orig")

file600_V32 = TFile.Open("/cephfs/user/s6subans/TRExFitter/qqlv_225_Mixed_qqlv/Hp250_qqlv/Histograms/Hp250_qqlv_SR_qqbb_6jin3bex_histos.root","READ")
h_ttb_v32  = file600_V32.Get("SR_qqbb_6jin3bex_ttb_orig")
h_ttc_v32  = file600_V32.Get("SR_qqbb_6jin3bex_ttc_orig")
h_ttl_v32  = file600_V32.Get("SR_qqbb_6jin3bex_ttlight_orig")
#h_sig_Hplus_m600_v3  = file600_V3.Get("leptopmass_5jex3bex_Hp600_orig")
h_tt_background_v3 = h_ttb_v3 + h_ttc_v3 + h_ttl_v3 + h_ttb_v32 + h_ttc_v32 + h_ttl_v32
h_tt_background_v3.SetLineColor(kRed)
h_tt_background_v3.SetLineStyle(1)

ymax=0
#NormalizeHisto_2(h_sig_Hplus_m600)
if ymax<h_tt_background.GetMaximum():
    ymax=h_tt_background.GetMaximum()
#NormalizeHisto_2(h_sig_Hplus_m600_v3)
if ymax<h_tt_background_v3.GetMaximum():
    ymax=h_tt_background_v3.GetMaximum()

print "V2", h_tt_background.Integral()
print "v3", h_tt_background_v3.Integral()
#h_tt_background.Draw("HIST")
h_tt_background_v3.Draw("HISTSAME")  
leg = TLegend(0.48,0.50,0.68,0.75)
ATLAS_LABEL(0.20,0.90,"Internal",1,0.19); 
myText(0.20,0.85,1,"m = 250 GeV")  
#myText(0.20,0.80,1,"at least 6 jet, 3 b-tags (Pre-Selection)")
myText(0.20,0.80,1,"at least 5 jet, 3 b-tags (Pre-Selection)")
leg.SetShadowColor(kWhite)
leg.SetFillColor(kWhite)
leg.SetLineColor(kWhite)
leg.AddEntry(h_tt_background, "Stand alone code: L2","L")
leg.AddEntry(h_tt_background_v3, "Stat FW: v3 ntuples (Sys version)","L")

leg.SetTextSize(0.0250)
leg.Draw()
h_tt_background.GetXaxis().SetTitle("mVH [TeV]")
#h_sig_Hplus_m600.GetXaxis().SetTitle("Lep Top mass [GeV]")
h_tt_background.GetYaxis().SetRangeUser(0.001,round(ymax*1.5,3)+0.001)
h_tt_background.GetYaxis().SetTitle("Entries")
c1.RedrawAxis()
c1.Update()
c1.RedrawAxis()
c1.SaveAs("../Plots/HistComp_tt_ThreeTags_qqbb_SR_5jin3bex_77.pdf")