# -*- coding: utf-8 -*-
#python
import sys
import glob
import math
import re
from ROOT import *
from array import *
from ROOT import TCanvas, TFile, TLine, TProfile, TNtuple, TH1F, TH2F
#import ROOT
#import array

gROOT.SetBatch(True)
gStyle.SetOptStat(0)
gStyle.SetPalette(1)
gROOT.LoadMacro("../style/AtlasStyle.C")
gROOT.LoadMacro("../style/AtlasUtils.C")
SetAtlasStyle()

y_axis_label="Event fraction"

c_blue   = TColor.GetColor("#3366ff")
c_red    = TColor.GetColor("#ee3311")
c_orange = TColor.GetColor("#ff9900")

def NormalizeHisto(histo):
     n_events=histo.Integral(-1,histo.GetNbinsX()+1)
     #n_events=histo.Integral()
     if n_events == 0:
         return
     print n_events, histo.Integral(histo.GetNbinsX(),histo.GetNbinsX()+1)
     histo.Scale(1./n_events)
     histo.SetLineWidth(3)
     histo.SetStats(0)
     histo.SetFillStyle(3001)
     histo.SetMarkerColor(histo.GetLineColor())
     histo.SetMarkerSize(0.0)
     histo.GetXaxis().SetTitleOffset(1.2)
     histo.GetYaxis().SetTitleOffset(1.52)
     histo.GetXaxis().SetLabelSize(0.05)
     histo.GetYaxis().SetLabelSize(0.05)
     histo.GetXaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetNdivisions(504)
     histo.GetXaxis().SetNdivisions(504)

c1 = TCanvas("ShapePlots","",500,500)

HistoNameR = "lep_top_pT"
#for HistoName in ["nBTags"]:
#for HistoName in ["nBTags","nJets","HT","HT_bjets","DeltaPhi_HW","mVH","mH","pTWplus","pTH","maxMVAResponse", "mass_resolution"]: # for resolved, histoname set
#for HistoName in ["Leptonic_Top_pT"]: # for resolved, histoname set
#for HistoName in ["nBTags","nJets","HT","HT_bjets","DeltaPhi_HW","mVH","mH","pTWplus","pTH","mass_resolution"]:#for boosted, histoname set
#for HistoName in ["mass_resolution"]:
for HistoName in ["mVH"]:     
#for HistoName in ["maxMVAResponse"]:
## for bTagStrategy in ["Incl","FourPlusTags","ThreeTags","TwoTags"]:
 for Region in ["Resolved_SR"]:
  for btagStrategy in ["Inclusive"]:
   #for btagStrategy in ["Inclusive","FourPlusTags","ThreeTags","TwoTags"]:
     
      ReBin = False
      YAxisScale = 1.4

      if "nBTags" in HistoName:
          Xaxis_label="b-tag multiplicity"
      if "pTWminus" in HistoName:
          Xaxis_label="Transverse Momentum Of Wminus [GeV]"
      if "nJet" in HistoName:
          Xaxis_label="Jet Multiplicity"
      if "mWplus" in HistoName:
          Xaxis_label="Mass of Wplus [GeV]"
      if "mH" in HistoName:
          Xaxis_label="Mass of Higgs [GeV]"
      if "DeltaPhi_HW" in HistoName:
          Xaxis_label="DeltaPhi_HW"
      if "pTH" in HistoName:
          Xaxis_label="Transverse Momentum of Higgs [GeV]"
      if "pTWplus" in HistoName:
          Xaxis_label="Transverse Momentum of W Boson [GeV]"
      if "mVH" in HistoName:
          Xaxis_label="Mass of Charged Higgs [GeV]"
      if "mH" in HistoName:
          Xaxis_label="Mass of Higgs [GeV]"
      if "pTWplus" in HistoName:
          Xaxis_label="Transverse Momentum Of Wplus [GeV]"
      if "pTH" in HistoName:
          Xaxis_label="Transverse Momentum Of Higgs [GeV]"
      if "mass_resolution" in HistoName:
          Xaxis_label="Mass Resolution"
      if "HT" in HistoName:
          Xaxis_label="H_{T} (Scalar Transverse Momentum Sum of jets) [GeV]"
      if "HT_bjets" in HistoName:
          Xaxis_label="H_{T_{b-jet}} (Scalar Transverse Momentum Sum of b-jets) [GeV]"
      if "maxMVAResponse" in HistoName:
          Xaxis_label="BDT Score (Signal Reconstruction)"
      if "mass_resolution" in HistoName:
          Xaxis_label="Leptonic Top (GeV)"    
      if "Leptonic_Top_mass" in HistoName:
          Xaxis_label="Leptonic Top mass (GeV)"  
      if "Leptonic_Top_pT" in HistoName:
          Xaxis_label="Leptonic Top pT (GeV)"     
      #Xaxis_label=""
      
      
      file400I       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_DiffConstr/hp1600_AFII.root_70p_250.root","READ")
      dir400I        = file400I.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m400I = dir400I.Get("hp1600_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m400I.SetLineColor(kRed)
      h_sig_Hplus_m400I.SetLineStyle(7) #7
      if ReBin == True:
          h_sig_Hplus_m400I.Rebin(2) 


      file400II       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_DiffConstr_minpT/hp1600_AFII.root_70p_250.root","READ")
      dir400II        = file400II.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m400II = dir400II.Get("hp1600_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m400II.SetLineColor(kBlue)
      h_sig_Hplus_m400II.SetLineStyle(7) #7
      if ReBin == True:
          h_sig_Hplus_m400II.Rebin(2)      

      file400III       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_DiffConstr_xWt/hp1600_AFII.root_70p_250.root","READ")
      dir400III        = file400III.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m400III = dir400III.Get("hp1600_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      h_sig_Hplus_m400III.SetLineColor(kBlack)
      h_sig_Hplus_m400III.SetLineStyle(7) #7
      if ReBin == True:
          h_sig_Hplus_m400III.Rebin(2)

      nbins=20
      ymax=0
      NormalizeHisto(h_sig_Hplus_m400I)
      if ymax<h_sig_Hplus_m400I.GetMaximum():
          ymax=h_sig_Hplus_m400I.GetMaximum()
      NormalizeHisto(h_sig_Hplus_m400II)
      if ymax<h_sig_Hplus_m400II.GetMaximum():
          ymax=h_sig_Hplus_m400II.GetMaximum()
      NormalizeHisto(h_sig_Hplus_m400III)
      if ymax<h_sig_Hplus_m400III.GetMaximum():
          ymax=h_sig_Hplus_m400III.GetMaximum()  
          

      h_sig_Hplus_m400I.Draw("HIST")
      #h_sig_Hplus_m300.Draw("HISTSAME")
      #h_sig_Hplus_m350.Draw("HISTSAME")
      h_sig_Hplus_m400II.Draw("HISTSAME")
      #h_sig_Hplus_m500.Draw("HISTSAME")
      #h_sig_Hplus_m600.Draw("HISTSAME")
      #h_sig_Hplus_m700.Draw("HISTSAME")
      h_sig_Hplus_m400III.Draw("HISTSAME")
      #h_sig_Hplus_m900.Draw("HISTSAME")
      #h_sig_Hplus_m1000.Draw("HISTSAME")
     

      #h_sig_Hplus_m1400.Draw("HISTSAME")
     
      if HistoName in "maxMVAResponse":
         leg = TLegend(0.25,0.55,0.825,0.755)
      else:
         leg = TLegend(0.55,0.75,0.75,0.90)
      #ATLAS_LABEL(0.20,0.875,"Simulation",1,0.19);
      #ATLAS_LABEL(0.20,0.80,"Simulation",1,0.09)
      leg.SetShadowColor(kWhite)
      leg.SetFillColor(kWhite)
      leg.SetLineColor(kWhite)
      
      leg.AddEntry(h_sig_Hplus_m400I,    "Lep Top Reco: Method 1","L")
      #leg.AddEntry(h_sig_Hplus_m300,    "H^{+}#rightarrow W^{+}h(m_{H^{+}} = 300GeV)","L")
      #leg.AddEntry(h_sig_Hplus_m350,   "H^{+}#rightarrow W^{+}h(m_{H^{+}} = 350GeV)","L")
      leg.AddEntry(h_sig_Hplus_m400II,    "Lep Top Reco: Method 2","L")
      leg.AddEntry(h_sig_Hplus_m400III,    "Lep Top Reco: Method 3","L")
      #leg.AddEntry(h_sig_Hplus_m400IV,    "Inc, m_{t_{l}}<=250 GeV","L")
      #leg.AddEntry(h_sig_Hplus_m400V,    "Inc, m_{t_{l}}<=275 GeV","L")
      #leg.AddEntry(h_sig_Hplus_m400VI,    "Inc, m_{t_{l}}<=300 GeV","L")
      #leg.AddEntry(h_sig_Hplus_m400VII,    "Inc, m_{t_{l}}<=325 GeV","L")
      #leg.AddEntry(h_sig_Hplus_m400VIII,    "Inc, m_{t_{l}}<=350 GeV","L")
      
      leg.SetTextSize(0.0250)
      leg.Draw()
      h_sig_Hplus_m400I.GetXaxis().SetTitle(Xaxis_label)
      h_sig_Hplus_m400I.GetYaxis().SetRangeUser(0.001,round(ymax*1.25,3)+0.001)
      #h_sig_Hplus_m400.GetXaxis().SetTitle(Xaxis_label)
      #h_sig_Hplus_m400.GetYaxis().SetRangeUser(0.001,round(ymax*1.25,3)+0.001)
      #pad1.cd()
      #myText(0.20,0.825,1,"work-in-progress")
      c1.RedrawAxis()
      c1.Update()
      c1.RedrawAxis()
      c1.SaveAs("../Plots/Reco_NegLep_V2/ShapePlot_%s_qqbb_SR_Resolved_LepTop_DiffRecoMethod_1600GeV_mvHCon250.pdf" % (HistoName+"_"+btagStrategy))

