# -*- coding: utf-8 -*-
#python
import ROOT
import sys
import glob
import math
import re
from ROOT import *
from array import *

#from ROOT import rootpy.plotting

import numpy as np
from numpy import ndarray
import array
#from rootpy.plotting import Hist, HistStack, Legend, Canvas
#from rootpy.plotting.style import get_style, set_style
#from rootpy.plotting.utils import draw
#from rootpy.interactive import wait
#import rootpy.plotting.root2matplotlib as rplt
from ROOT import TCanvas, TFile, TPad, THStack, TLine,TGraphAsymmErrors, TNtuple, TH1F, TH2F
#from ROOT import TGraphAsymmErrors
from array import *
#from ROOT import TLatex
#import ROOT
#import array

gROOT.SetBatch(True)
gStyle.SetOptStat(0)
gStyle.SetPalette(1)
gROOT.LoadMacro("../style/AtlasStyle.C")
gROOT.LoadMacro("../style/AtlasUtils.C")
SetAtlasStyle()

y_axis_label="Event fraction"

c_blue   = TColor.GetColor("#3366ff")
c_red    = TColor.GetColor("#ee3311")
c_orange = TColor.GetColor("#ff9900")

def NormalizeHisto(histo):
     n_events=histo.Integral(-1,histo.GetNbinsX()+1)
     if n_events == 0:
         return
     print n_events, histo.Integral(histo.GetNbinsX(),histo.GetNbinsX()+1)
     histo.Scale(1./n_events)
     histo.SetLineWidth(2)
     histo.SetStats(0)
     histo.SetFillStyle(3001)
     histo.SetMarkerColor(histo.GetLineColor())
     histo.SetMarkerSize(0.0)
     histo.GetXaxis().SetTitleOffset(1.2)
     histo.GetYaxis().SetTitleOffset(1.52)
     histo.GetXaxis().SetLabelSize(0.05)
     histo.GetYaxis().SetLabelSize(0.05)
     histo.GetXaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetNdivisions(504)
     histo.GetXaxis().SetNdivisions(504)

c1 = TCanvas("DatavMC-qqbb","",700,500)
pad1 = TPad("pad1", "pad1", 0.0, 0.35, 1,1)
#pad1.SetLogy()
pad2 = TPad("pad2", "pad2", 0, 0.0, 1,0.35)
pad1.SetLeftMargin(0.15)
pad1.SetTopMargin(0.1)
pad1.SetBottomMargin(0.0105)
pad1.SetRightMargin(0.08)
pad1.Draw()
pad1.SetTicks()

pad2.SetLeftMargin(0.15)
pad2.SetTopMargin(0.04)
pad2.SetBottomMargin(0.35)
pad2.SetRightMargin(0.08)
pad2.Draw()
pad2.SetTicks()
#HistoNameR ="evw_MVAaccu_MVA095"
#HistoNameR ="evw_Whad_MVA09"
HistoNameR ="evw_trueM_eone_MVA095"
#HistoNameR ="evw_qqbb"
#HistoNameR ="evw_Wlep"
#for HistoName in ["nBTags"]:
#for HistoName in ["nBTags","nJets","HT","HT_bjets","DeltaPhi_HW","mVH","mH","pTWplus","pTH","maxMVAResponse", "mass_resolution"]: # for resolved, histoname set
#for HistoName in ["nJets","HT","HT_bjets","mVH","mH","pTWplus","pTH","DeltaPhi_HW","pTH_over_mVH","pTW_over_mVH","DeltaEta_HW"]:
#for HistoName in ["mVH","nJet","DeltaPhi_HW","DeltaEta_HW","maxMVAResponse"]:
#for HistoName in ["maxMVAResponse","DeltaEta_HW","DeltaPhi_HW"]: 
#for HistoName in ["nJet"]:
#for HistoName in ["HT","pTWplus","pTH"]:       
#for HistoName in ["Event_Weight_Wlep"]:
#for HistoName in ["Event_Weight_Whad_MVA09"]:
for HistoName in ["Event_Weight_eone_MVA095"]:
#for HistoName in ["Event_Weight_MVAccu_MVA095"]:   
#for HistoName in ["nJet","maxMVAResponse","DeltaPhi_HW","pTH_over_mVH","pTW_over_mVH","DeltaEta_HW"]:
#for HistoName in ["DeltaEta_HW"]: 
#for HistoName in ["Eta_j1j2","Eta_j1j3","Eta_j1j4","Eta_j1j5","Eta_j1j6","Eta_j1j7","Eta_j2j3","Eta_j2j4","Eta_j2j5","Eta_j2j6","Eta_j2j7","Eta_j3j4","Eta_j3j5","Eta_j3j6","Eta_j3j7","Eta_j4j5","Eta_j4j6","Eta_j4j7","Eta_j5j6","Eta_j5j7","Eta_j6j7"]:
#for HistoName in ["maxMVAResponse_15","maxMVAResponse_10","maxMVAResponse_19","maxMVAResponse_18"]: 
## for bTagStrategy in ["Incl","FourPlusTags","ThreeTags","TwoTags"]:
 for Region in ["Resolved_SR"]:
  #for btagStrategy in ["FourPlusTags","ThreeTags"]:
  for btagStrategy in ["Inclusive"]:    
     
      ReBin = False
      YAxisScale = 1.4
  
      #file_250       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_qqbb_Corrtthbb_Mod_nolsl_17MPAcc/hp250_AFII.root_77p_225.root","READ")
      #file_250       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_qqbb_Corrtthbb_Mod_nolsl_17MPAcc/hp250_AFII.root_77p_225.root","READ")
      file_250       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_MatchEff/hp250_AFII.root_77p_225.root","READ")
      #file_250       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_FullEvent_Corrtthbb_Mod_nolsl_17MPAcc/hp250_AFII.root_77p_225.root","READ")
      #file_400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_290121/sig_Hplus_Wh_m400-0_70p.root","READ")
      #file_400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_1512/sig_Hplus_Wh_m400-0.root_70p_MC16d.root","READ")
      dir_250        = file_250.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m250 = dir_250.Get("hp250_AFII_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      #file_300       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_qqbb_Corrtthbb_Mod_nolsl_17MPAcc/hp300_AFII.root_77p_225.root","READ")
      file_300       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_MatchEff/hp300_AFII.root_77p_225.root","READ")
      #file_300       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_FullEvent_Corrtthbb_Mod_nolsl_17MPAcc/hp300_AFII.root_77p_225.root","READ")
      #file_400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_290121/sig_Hplus_Wh_m400-0_70p.root","READ")
      #file_400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_1512/sig_Hplus_Wh_m400-0.root_70p_MC16d.root","READ")
      dir_300        = file_300.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m300 = dir_300.Get("hp300_AFII_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      #file_350       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_qqbb_Corrtthbb_Mod_nolsl_17MPAcc/hp350_AFII.root_77p_225.root","READ")
      file_350       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_MatchEff/hp350_AFII.root_77p_225.root","READ")
      #file_350       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_FullEvent_Corrtthbb_Mod_nolsl_17MPAcc/hp350_AFII.root_77p_225.root","READ")
      #file_400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_290121/sig_Hplus_Wh_m400-0_70p.root","READ")
      #file_400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_1512/sig_Hplus_Wh_m400-0.root_70p_MC16d.root","READ")
      dir_350       = file_350.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m350 = dir_350.Get("hp350_AFII_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      #file_400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_NegLep_Correct/hp400_AFII.root_70p.root","READ")
      #dir_400       = file_400.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      #h_sig_Hplus_m400 = dir_400.Get("hp400_AFII_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      #file_500       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_qqbb_Corrtthbb_Mod_nolsl_17MPAcc/hp500_AFII.root_77p_225.root","READ")
      file_500      = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_MatchEff/hp500_AFII.root_77p_225.root","READ")
      #file_500       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_FullEvent_Corrtthbb_Mod_nolsl_17MPAcc/hp500_AFII.root_77p_225.root","READ")
      #file_400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_290121/sig_Hplus_Wh_m400-0_70p.root","READ")
      #file_400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_1512/sig_Hplus_Wh_m400-0.root_70p_MC16d.root","READ")
      dir_500        = file_500.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m500 = dir_500.Get("hp500_AFII_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      #file_600       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_qqbb_Corrtthbb_Mod_nolsl_17MPAcc/hp600_AFII.root_77p_225.root","READ")
      file_600      = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_MatchEff/hp600_AFII.root_77p_225.root","READ")
      #file_600       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_FullEvent_Corrtthbb_Mod_nolsl_17MPAcc/hp600_AFII.root_77p_225.root","READ")
      #file_400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_290121/sig_Hplus_Wh_m400-0_70p.root","READ")
      #file_400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_1512/sig_Hplus_Wh_m400-0.root_70p_MC16d.root","READ")
      dir_600        = file_600.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m600 = dir_600.Get("hp600_AFII_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      #file_700       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_qqbb_Corrtthbb_Mod_nolsl_17MPAcc/hp700_AFII.root_77p_225.root","READ")
      file_700       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_MatchEff/hp700_AFII.root_77p_225.root","READ")
      #file_700       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_FullEvent_Corrtthbb_Mod_nolsl_17MPAcc/hp700_AFII.root_77p_225.root","READ")
      #file_400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_290121/sig_Hplus_Wh_m400-0_70p.root","READ")
      #file_400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_1512/sig_Hplus_Wh_m400-0.root_70p_MC16d.root","READ")
      dir_700        = file_700.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m700 = dir_700.Get("hp700_AFII_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      #file_800       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_qqbb_Corrtthbb_Mod_nolsl_17MPAcc/hp800_AFII.root_77p_225.root","READ")
      #file_800       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_qqbbEvent_3MP_mod_nolsl_AccuV3/hp800_AFII.root_77p_225.root","READ")
      file_800       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_MatchEff/hp800_AFII.root_77p_225.root","READ")
      #file_800       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_FullEvent_Corrtthbb_Mod_nolsl_17MPAcc/hp800_AFII.root_77p_225.root","READ")
      #file_400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_290121/sig_Hplus_Wh_m400-0_70p.root","READ")
      #file_400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_1512/sig_Hplus_Wh_m400-0.root_70p_MC16d.root","READ")
      dir_800        = file_800.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m800 = dir_800.Get("hp800_AFII_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      #file_900       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_qqbb_Corrtthbb_Mod_nolsl_17MPAcc/hp900_AFII.root_77p_225.root","READ")
      file_900      = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_MatchEff/hp900_AFII.root_77p_225.root","READ")
      #file_900       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_FullEvent_Corrtthbb_Mod_nolsl_17MPAcc/hp900_AFII.root_77p_225.root","READ")
      #file_400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_290121/sig_Hplus_Wh_m400-0_70p.root","READ")
      #file_400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_1512/sig_Hplus_Wh_m400-0.root_70p_MC16d.root","READ")
      dir_900        = file_900.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m900 = dir_900.Get("hp900_AFII_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      #file_1000       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_qqbb_Corrtthbb_Mod_nolsl_17MPAcc/hp1000_AFII.root_77p_225.root","READ")
      file_1000       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_MatchEff/hp1000_AFII.root_77p_225.root","READ")
      #file_1000       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_FullEvent_Corrtthbb_Mod_nolsl_17MPAcc/hp1000_AFII.root_77p_225.root","READ")
      #file_400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_290121/sig_Hplus_Wh_m400-0_70p.root","READ")
      #file_400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_1512/sig_Hplus_Wh_m400-0.root_70p_MC16d.root","READ")
      dir_1000        = file_1000.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m1000 = dir_1000.Get("hp1000_AFII_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      #file_1200       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_qqbb_Corrtthbb_Mod_nolsl_17MPAcc/hp1200_AFII.root_77p_225.root","READ")
      file_1200       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_MatchEff/hp1200_AFII.root_77p_225.root","READ")
      #file_1200       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_FullEvent_Corrtthbb_Mod_nolsl_17MPAcc/hp1200_AFII.root_77p_225.root","READ")
      #file_400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_290121/sig_Hplus_Wh_m400-0_70p.root","READ")
      #file_400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_1512/sig_Hplus_Wh_m400-0.root_70p_MC16d.root","READ")
      dir_1200        = file_1200.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m1200 = dir_1200.Get("hp1200_AFII_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      #file_1400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_qqbb_Corrtthbb_Mod_nolsl_17MPAcc/hp1400_AFII.root_77p_225.root","READ")
      file_1400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_MatchEff/hp1400_AFII.root_77p_225.root","READ")
      #file_1400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_FullEvent_Corrtthbb_Mod_nolsl_17MPAcc/hp1400_AFII.root_77p_225.root","READ")
      #file_400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_290121/sig_Hplus_Wh_m400-0_70p.root","READ")
      #file_400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_1512/sig_Hplus_Wh_m400-0.root_70p_MC16d.root","READ")
      dir_1400        = file_1400.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m1400 = dir_1400.Get("hp1400_AFII_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      #file7	  = TFile.Open("../PlotFiles/ForOptimisation/sig_Hplus_Wh_m1600-0_70p.root","READ")
      #file_1600	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_qqbb_Corrtthbb_Mod_nolsl_17MPAcc/hp1600_AFII.root_77p_225.root","READ")
      #file_1600	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_qqbbEvent_3MP_mod_nolsl_AccuV3/hp1600_AFII.root_77p_225.root","READ")
      file_1600	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_MatchEff/hp1600_AFII.root_77p_225.root","READ")
      #file_1600	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_FullEvent_Corrtthbb_Mod_nolsl_17MPAcc/hp1600_AFII.root_77p_225.root","READ")
      #file_1600	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_290121/sig_Hplus_Wh_m1600-0_70p.root","READ")
      #file_1600	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_1512/sig_Hplus_Wh_m1600-0.root_70p_MC16e.root","READ")
      dir_1600        = file_1600.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m1600 = dir_1600.Get("hp1600_AFII_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      #file_1800       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_qqbb_Corrtthbb_Mod_nolsl_17MPAcc/hp1800_AFII.root_77p_225.root","READ")
      file_1800	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_MatchEff/hp1800_AFII.root_77p_225.root","READ")
      #file_1800       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_FullEvent_Corrtthbb_Mod_nolsl_17MPAcc/hp1800_AFII.root_77p_225.root","READ")
      #file_400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_290121/sig_Hplus_Wh_m400-0_70p.root","READ")
      #file_400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_1512/sig_Hplus_Wh_m400-0.root_70p_MC16d.root","READ")
      dir_1800        = file_1800.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m1800 = dir_1800.Get("hp1800_AFII_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      #file_2000       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_qqbb_Corrtthbb_Mod_nolsl_17MPAcc/hp2000_AFII.root_77p_225.root","READ")
      file_2000	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_MatchEff/hp2000_AFII.root_77p_225.root","READ")
      #file_2000       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_FullEvent_Corrtthbb_Mod_nolsl_17MPAcc/hp2000_AFII.root_77p_225.root","READ")
      #file_400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_290121/sig_Hplus_Wh_m400-0_70p.root","READ")
      #file_400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_1512/sig_Hplus_Wh_m400-0.root_70p_MC16d.root","READ")
      dir_2000        = file_2000.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m2000 = dir_2000.Get("hp2000_AFII_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      #file_2500       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_qqbb_Corrtthbb_Mod_nolsl_17MPAcc/hp2500_AFII.root_77p_225.root","READ")
      file_2500	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_MatchEff/hp2500_AFII.root_77p_225.root","READ")
      #file_2500       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_FullEvent_Corrtthbb_Mod_nolsl_17MPAcc/hp2500_AFII.root_77p_225.root","READ")
      #file_400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_290121/sig_Hplus_Wh_m400-0_70p.root","READ")
      #file_400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_1512/sig_Hplus_Wh_m400-0.root_70p_MC16d.root","READ")
      dir_2500        = file_2500.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m2500 = dir_2500.Get("hp2500_AFII_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      #file_3000       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_qqbb_Corrtthbb_Mod_nolsl_17MPAcc/hp3000_AFII.root_77p_225.root","READ")
      file_3000	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_MatchEff/hp3000_AFII.root_77p_225.root","READ")
      #file_3000       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_FullEvent_Corrtthbb_Mod_nolsl_17MPAcc/hp3000_AFII.root_77p_225.root","READ")
      #file_400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_290121/sig_Hplus_Wh_m400-0_70p.root","READ")
      #file_400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_Mock/output_dvmc_1512/sig_Hplus_Wh_m400-0.root_70p_MC16d.root","READ")
      dir_3000        = file_3000.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m3000 = dir_3000.Get("hp3000_AFII_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

      #file_400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_qqbb_Corrtthbb_Mod_nolsl_17MPAcc/hp400_AFII.root_77p_225.root","READ")
      #file_400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_qqbbEvent_3MP_mod_nolsl_AccuV3/hp400_AFII.root_77p_225.root","READ")
      file_400	  = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_MatchEff/hp400_AFII.root_77p_225.root","READ")
      #file_400       = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Hp_leptop_NewTrain_TrMatch_FullEvent_Corrtthbb_Mod_nolsl_17MPAcc/hp400_AFII.root_77p_225.root","READ")
      dir_400       = file_400.GetDirectory("nominal_Loose").GetDirectory(HistoName)
      h_sig_Hplus_m400 = dir_400.Get("hp400_AFII_"+HistoNameR+"_"+Region+"_"+btagStrategy+"_nominal_Loose")

     
      yield_H250 = h_sig_Hplus_m250.Integral(11,20)-h_sig_Hplus_m250.Integral(1,10)
      yield_H300 = h_sig_Hplus_m300.Integral(11,20)-h_sig_Hplus_m300.Integral(1,10)
      yield_H350 = h_sig_Hplus_m350.Integral(11,20)-h_sig_Hplus_m350.Integral(1,10)
      yield_H400 = h_sig_Hplus_m400.Integral(11,20)-h_sig_Hplus_m400.Integral(1,10)
      yield_H500 = h_sig_Hplus_m500.Integral(11,20)-h_sig_Hplus_m500.Integral(1,10)
      yield_H600 = h_sig_Hplus_m600.Integral(11,20)-h_sig_Hplus_m600.Integral(1,10)
      yield_H700 = h_sig_Hplus_m700.Integral(11,20)-h_sig_Hplus_m700.Integral(1,10)
      yield_H800 = h_sig_Hplus_m800.Integral(11,20)-h_sig_Hplus_m800.Integral(1,10)
      yield_H900 = h_sig_Hplus_m900.Integral(11,20)-h_sig_Hplus_m900.Integral(1,10)
      yield_H1000 = h_sig_Hplus_m1000.Integral(11,20)-h_sig_Hplus_m1000.Integral(1,10)
      yield_H1200 = h_sig_Hplus_m1200.Integral(11,20)-h_sig_Hplus_m1200.Integral(1,10) 
      yield_H1400 = h_sig_Hplus_m1400.Integral(11,20)-h_sig_Hplus_m1400.Integral(1,10) 
      yield_H1600 = h_sig_Hplus_m1600.Integral(11,20)-h_sig_Hplus_m1600.Integral(1,10) 
      yield_H1800 = h_sig_Hplus_m1800.Integral(11,20)-h_sig_Hplus_m1800.Integral(1,10) 
      yield_H2000 = h_sig_Hplus_m2000.Integral(11,20)-h_sig_Hplus_m2000.Integral(1,10) 
      yield_H2500 = h_sig_Hplus_m2500.Integral(11,20)-h_sig_Hplus_m2500.Integral(1,10) 
      yield_H3000 = h_sig_Hplus_m3000.Integral(11,20)-h_sig_Hplus_m3000.Integral(1,10) 
 
      print "yield_H250 is", yield_H250
      print "yield_H300 is", yield_H300
      print "yield_H350 is", yield_H350
      print "yield_H400 is", yield_H400
      print "yield_H500 is", yield_H500
      print "yield_H600 is", yield_H600
      print "yield_H700 is", yield_H700
      print "yield_H800 is", yield_H800
      print "yield_H900 is", yield_H900
      print "yield_H1000 is", yield_H1000
      print "yield_H1200 is", yield_H1200
      print "yield_H1400 is", yield_H1400
      print "yield_H1600 is", yield_H1600
      print "yield_H1800 is", yield_H1800
      print "yield_H2000 is", yield_H2000
      print "yield_H2500 is", yield_H2500
      print "yield_H3000 is", yield_H3000
            
      

      
         
      
    

     
