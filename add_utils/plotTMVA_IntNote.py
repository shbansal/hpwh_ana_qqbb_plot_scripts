# -*- coding: utf-8 -*-
#python
from ROOT import *
from array import array

gROOT.SetBatch(False)
gROOT.LoadMacro("../style/AtlasStyle.C")
gROOT.LoadMacro("../style/AtlasUtils.C")
gStyle.SetOptStat(0)
gStyle.SetOptTitle(0)
gStyle.SetPalette(1)


def NormalizeHisto(histo):
     n_events=histo.Integral(-1,histo.GetNbinsX()+1)
     if n_events == 0:
         return

     histo.Scale(1./n_events)
     histo.SetLineWidth(1)
     histo.SetStats(0)
     histo.SetFillStyle(3001)
     histo.SetMarkerColor(histo.GetLineColor())
     histo.SetMarkerSize(0.0)
     histo.GetXaxis().SetTitleOffset(0.9)
     histo.GetYaxis().SetTitleOffset(0.92)
     #histo.GetXaxis().SetLabelSize(0.05)
     #histo.GetYaxis().SetLabelSize(0.05)
     histo.GetXaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetNdivisions(504)
     histo.GetXaxis().SetNdivisions(504)
path="../output/"
fileName="hp17MP_77p_225_wocosth.root_MVA.root"
TaggerName="WpH_Tagger_qqbb_17MP_wocosth"

infile=TFile.Open(path+fileName,"READ")

test=infile.GetDirectory("dataset").GetDirectory("Method_BDT").GetDirectory(TaggerName)
#test.ls()
c1=TCanvas("","",900,550)
c1.SetTicks(1,1)
c1.Divide(3,2)

n=4

#names={"pTWmvH":"p_{T}^{W}/m_{Wh}","pTHmvH":"p_{T}^{h}/m_{Wh}","btagjH1":"b-tag score jet 1","btagjH2":"b-tag score jet 2","H_mass":"mass_{higgs}","Phi_HW":"#Delta#Phi(Wh)","cosThetaStar":"cos(#Theta^{*})","Eta_HW":"#Delta#eta(Wh)"}
names={"Wp_pT_D_mass_VH":"p_{T}^{W}/m_{Wh}","H_pT_D_mass_VH":"p_{T}^{h}/m_{Wh}","btagjH1":"b-tag score of jet 1 constituting Higgs","btagjH2":"b-tag score of jet 2 constituting Higgs", "btagjW1":"b-tag score of jet 1 constituting W","btagjW2":"b-tag score of jet 2 constituting W", "H_mass":"mass_{higgs}", "Wp_mass":"mass_{W}", "Phi_HW":"#Delta#Phi(Wh)","Eta_HW":"#Delta#eta(Wh)"}
#Plot the distrbutions of the input variables
#for variable in["Wp_pT_D_mass_VH","H_pT_D_mass_VH","btagjH1","btagjH2","btagjW1","btagjW2","H_mass","Wp_mass","Phi_HW","Eta_HW"]:
#for variable in["Wp_pT_D_mass_VH","H_pT_D_mass_VH","btagjH1","btagjH2","btagjW1","btagjW2"]:
for variable in["H_mass","Wp_mass","Phi_HW","Eta_HW"]:        
    c1.cd(n)
    c1.SetTicks(1,1)
    c1.SetBottomMargin(1)
    signal=test.Get(variable+"__Signal")
    background=test.Get(variable+"__Background")
    NormalizeHisto(signal)
    NormalizeHisto(background)
    signal.SetFillStyle(1001)
    signal.SetFillColor(38)
    signal.SetLineColor(4)
    signal.SetLineWidth(1)
    signal.GetXaxis().SetTitle(names[variable])
    signal.GetYaxis().SetTitle("Event Fraction")
    signal.SetName(names[variable])
    signal.SetTitle("Input variable: "+names[variable])
    
    background.GetXaxis().SetTitle(names[variable])
    background.GetYaxis().SetTitle("Event Fraction")
    
    background.SetName(names[variable])
    background.SetTitle("input variable: "+names[variable])

    background.SetFillStyle(3354)
    background.SetFillColor(2)
    background.SetLineColor(2)
    background.SetLineWidth(1)
    
    leg=TLegend(0.2,0.6,0.4,0.8)
    leg.SetFillStyle(0)
    leg.SetBorderSize(0)
    leg.SetFillColor(0)
    leg.AddEntry(signal,"Signal","f")
    leg.AddEntry(background,"Combinatorial background","f")
    leg.SetTextSize(0.045)
    leg.Draw()
    tbLabel=TLatex(0.5,.5,"H^{+} #rightarrow qqbb")
    tbLabel.Draw()
    #ATLAS_LABEL(0.42,0.82," Internal",1,0.12,0.035);  
    
    
    signal.Draw("HIST")
    background.Draw("HISTSAME")
    leg.Draw()
    if(n==1):
         #ATLAS_LABEL(0.2,0.82,"  Internal",1,0.12,0.045); 
         ATLAS_LABEL(0.2,0.82,"  work-in-progress",1,0.12,0.045); 
    c1.RedrawAxis()
    n-=1
   
    
c1.SaveAs("../Plots/BDT_qqbb_inputvariables_V2_DPG.pdf")
#c1.SaveAs("../Plots/BDT_qqbb_inputvariables.png")
c1.Clear()
leg.Clear()
#Plot the BDT Response
gStyle.SetOptTitle(0)
leg=TLegend(0.2,0.5,0.4,0.7)
leg.SetFillStyle(0)
leg.SetBorderSize(0)
leg.SetFillColor(0)
c1=TCanvas("","",1200,900)
c1.SetTicks(1,1)
signal=test.Get("MVA_"+TaggerName+"_S")
background=test.Get("MVA_"+TaggerName+"_B")
signal.GetXaxis().SetTitle("BDT response")
background.GetXaxis().SetTitle("BDT response")
signal.GetYaxis().SetTitle("(1/N) dN/dx")
signal.SetFillStyle(1001)
signal.SetFillColor(38)
signal.SetLineColor(4)
signal.SetLineWidth(3)
background.SetFillStyle(3354)
background.SetFillColor(2)
background.SetLineColor(2)
background.SetLineWidth(3)
signal.Draw("HIST")
background.Draw("HISTSAME")
leg.AddEntry(signal,"Signal","f")
leg.AddEntry(background,"Combinatorial background","f")
leg.SetTextSize(0.045)
leg.Draw()
tbLabel=TLatex(0.2,4,"H^{+} #rightarrow qqbb")
tbLabel.Draw()
#ATLAS_LABEL(0.2,0.82," Internal",1,0.12,0.05);
ATLAS_LABEL(0.2,0.82," work-in-progress",1,0.12,0.05);
c1.RedrawAxis()
c1.SaveAs("../Plots/BDTResponse_qqbb_DPG.pdf")
#c1.SaveAs("../Plots/BDTResponse_qqbb.png")
c1.Clear()


