# -*- coding: utf-8 -*-
import sys
import glob
import math
import re
from ROOT import *
from array import *
gROOT.SetBatch(True)
gStyle.SetOptStat(0)
gStyle.SetPalette(1)
#gROOT.LoadMacro("../style/AtlasStyle.C")
#gROOT.LoadMacro("../style/AtlasUtils.C")
gROOT.LoadMacro("../style/AtlasStyle.C")
gROOT.LoadMacro("../style/AtlasUtils.C")
gROOT.LoadMacro("../style/AtlasLabels.C")
SetAtlasStyle()
#import ROOT
#import array

#gROOT.SetBatch(True)
#gStyle.SetOptStat(0)
#gStyle.SetPalette(1)
#gROOT.LoadMacro("../AtlasUtils/AtlasStyle.C")
#gROOT.LoadMacro("../AtlasUtils/AtlasUtils.C")
#gROOT.LoadMacro("../AtlasUtils/AtlasLabels.C")
#SetAtlasStyle()

#from array import *
def getTGraphs(): 
    "Function to read mass resolution from text files"
    graphs={}
    #filename_MVA07="../res_qqbb/res_qqbb_bukin_MVA07_ge4b_Bukin.txt"
    #filename_MVA08="../res_qqbb/res_qqbb_bukin_MVA08_ge4b_Bukin.txt"
    #filename_MVA09="../res_qqbb/res_qqbb_bukin_MVA09_ge4b_Bukin.txt"
    #filename_MVA095="../res_qqbb/res_qqbb_fp_225_3b_MVA095.txt"

    filename_MVA07="../res_qqbb/res_qqbb_bukin_200_Bukin.txt"
    filename_MVA08="../res_qqbb/res_qqbb_bukin_225_Bukin.txt"
    filename_MVA09="../res_qqbb/res_qqbb_bukin_250_Bukin.txt"
    filename_MVA095="../res_qqbb/res_qqbb_fp_225_3b_MVA095.txt"

    textfile_MVA07 = open(filename_MVA07, "r")
    textfile_MVA08 = open(filename_MVA08, "r")
    textfile_MVA09 = open(filename_MVA09, "r")
    textfile_MVA095 = open(filename_MVA095, "r")
    v_mass = array('d')
    v_res_MVA07=array('d')
    v_res_MVA08=array('d')
    v_res_MVA09=array('d') 
    v_res_MVA095=array('d') 
    for line in textfile_MVA07:
        tokens=line.rstrip().split(" ")
        v_mass.append(float(tokens[0]))
        v_res_MVA07.append(float(tokens[2]))
    for line in textfile_MVA08:
        tokens=line.rstrip().split(" ")
        v_res_MVA08.append(float(tokens[2]))
    for line in textfile_MVA09:
        tokens=line.rstrip().split(" ")
        v_res_MVA09.append(float(tokens[2])) 
    for line in textfile_MVA095:
        tokens=line.rstrip().split(" ")
        v_res_MVA095.append(float(tokens[2]))           
    graphs["Res_MVA07"]=TGraph(len(v_mass),v_mass, v_res_MVA07)
    graphs["Res_MVA08"]=TGraph(len(v_mass),v_mass, v_res_MVA08)
    graphs["Res_MVA09"]=TGraph(len(v_mass),v_mass, v_res_MVA09)
    graphs["Res_MVA095"]=TGraph(len(v_mass),v_mass, v_res_MVA095)
    return graphs

sample = {}
cmassres = TCanvas("massres","massres",800,600)
frame = TH1F("", "", 35, 250, 3000)
frame.SetMinimum(0.03)
frame.SetMaximum(1.0)
frame.Draw()
frame.GetXaxis().SetTitle("H^{+} mass [GeV]")
frame.GetXaxis().SetTitleOffset(1.1)
#frame.GetYaxis().SetTitle("#sigma(pp #rightarrow tbH^{#pm}) #times BR(H^{#pm}#rightarrow Wh) [pb]")
frame.GetYaxis().SetTitle("H^{+} mass resolution")
frame.GetXaxis().SetRangeUser(250,3000)

#cLimit.SetLogy()

#graphs=getTGraphs(options.inFile)
graphs=getTGraphs()
res_MVA07=graphs["Res_MVA07"]
res_MVA08=graphs["Res_MVA08"]
res_MVA09=graphs["Res_MVA09"]
res_MVA095=graphs["Res_MVA095"]

#leg = TLegend(0.61, 0.7, 0.91, 0.9)
leg = TLegend(0.55, 0.5, 0.86, 0.7)
leg.SetFillColor(kWhite)
leg.SetBorderSize(0)
leg.SetLineColor(kWhite)
leg.SetTextFont(43)
leg.SetTextSize(20)

myText(0.20,0.80,1,"#sqrt{s}=13 TeV, 139 fb^{-1}")
#myText(0.20,0.75,1,"at least 5 jet, 3 b-tags and at least 4 b-tags")
#myText(0.20,0.75,1,"at least 5 jet, 3 b-tags")
#myText(0.20,0.75,1,"at least 5 jet, at least 4 b-tags")
myText(0.20,0.75,1,"at least 5 jet, at least 2 b-tags")

res_MVA07.SetLineColor(kBlack)
res_MVA07.SetLineStyle(1)
res_MVA07.SetLineWidth(2)
#expected_MVA07.SetLineStyle(2)
    #expected.SetMarkerStyle(24)
    #expected.SetMarkerSize(20)
res_MVA07.Draw("L") #CSAME

res_MVA08.SetLineColor(kRed)
res_MVA08.SetLineStyle(1)
res_MVA08.SetLineWidth(2)
#expected_MVA08.SetLineStyle(2)
    #expected.SetMarkerStyle(24)
    #expected.SetMarkerSize(20)
res_MVA08.Draw("SAME L") #CSAME

res_MVA09.SetLineColor(kBlue)
res_MVA09.SetLineStyle(1)
res_MVA09.SetLineWidth(2)
#expected_MVA09.SetLineStyle(2)
    #expected.SetMarkerStyle(24)
    #expected.SetMarkerSize(20)
res_MVA09.Draw("SAME L") #CSAME

res_MVA095.SetLineColor(kGreen)
res_MVA095.SetLineStyle(1)
res_MVA095.SetLineWidth(2)
#expected_MVA09.SetLineStyle(2)
    #expected.SetMarkerStyle(24)
    #expected.SetMarkerSize(20)
#res_MVA095.Draw("SAME L") #CSAME

#observed.GetXaxis().SetRangeUser(200,2000)
#observed.SetLineColor(1)
#observed.SetLineStyle(1)
#observed.SetLineWidth(2)
#observed.SetMarkerStyle(20)
#observed.SetMarkerSize(1.)
    ##observed.Draw("LPSAME")

gPad.RedrawAxis()
    
    #leg.AddEntry(observed,"95% observed CL_{s}", "lp")
#leg.AddEntry(res_MVA07,"maxMVA >= 0.7", "l")
#leg.AddEntry(res_MVA08,"maxMVA >= 0.8","l")
#leg.AddEntry(res_MVA09,"maxMVA >= 0.9","l")
#leg.AddEntry(res_MVA095,"maxMVA >= 0.95","l")

leg.AddEntry(res_MVA07,"Lep Top mass < 200 GeV", "l")
leg.AddEntry(res_MVA08,"Lep Top mass < 225 GeV","l")
leg.AddEntry(res_MVA09,"Lep Top mass < 250 GeV","l")
#leg.AddEntry(res_MVA095,"maxMVA >= 0.95","l")
leg.Draw()

#ATLASLabel(0.2, 0.85, "work-in-progress", kBlack)
#ATLASLabel(0.2, 0.85, "work-in-progress", kBlack)
#ATLAS_LABEL(0.20,0.85,"work-in-progress",1,0.19)
ATLAS_LABEL(0.20,0.85,"Internal",1,0.19)
    #ROOT.ATLASLabel(0.32, 0.85, "Preliminary", ROOT.kBlack)
    #ROOT.ATLASLabel(0.32, 0.85, "", ROOT.kBlack)
         
#pngfilename="Limit.png"
pdffilename="MassRes_LepTop_INT.pdf"
#pdffilename="MassRes_ge5jge2b_Lep_INT.pdf"
    
cmassres.SaveAs(pdffilename)
#cLimit.Print(pdffilename)    

