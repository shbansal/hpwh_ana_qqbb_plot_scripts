#include "TFile.h"
#include "TTree.h"
#include "TSystemDirectory.h"
#include "TSystemFile.h"
#include "TList.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include "TLatex.h"
#include <cmath>
#include <fstream>
#include <iostream>

//void GetSampleN(const char* in_dir)
void GetMVAEff()
{

  Int_t n=17;
//  Double_t x_graph[n], y_graph[n];
 // Double_t x_graph[13]= {0.007, 0.003, 0.002, 0.005, 0.01, 0.05, 0.1, 0.3, 0.5, 0.7, 1, 0.03, 0.001};
 // Double_t y_graph[13]= {0.039, 0.007, 0.005, 0.009, 0.031, 0.104, 0.157, 0.363, 0.565, 0.764, 1.056, 0.083, 0.007};
 // Double_t ey_graph[13]= {0.188,0.079,	0.063,	0.305,	0.246,	0.06,	0.058,	0.058,	0.059,	0.06,	0.06, 0.058, 0.113};

  //Double_t x_graph[10]= {5,10,20,50,100,200,400,600,800,1000};
  //Double_t y_graph[10]= {0.922,0.931,0.942,0.953,0.958,0.960,0.961,0.961,0.961,0.961};
  
  /*
  Double_t x_graph[17]= {250,300,350,400,500,600,700,800,900,1000,1200,1400,1600,1800,2000,2500,3000};
  Double_t y_graph_den[17]= {83916, 91874, 73427, 76022, 53549, 54521, 54952, 54938, 52308, 54565, 51322, 48406, 44622, 42254, 40086, 37606, 36361};
  
  Double_t y_graph_numWhad[17]= {8798, 13319 , 13078, 15894, 13447, 14547, 15484, 15327, 15029, 15143, 14261, 13429, 11975, 11163, 11195, 10344 , 10060};
  
  Double_t y_graph_numWlep[17]= {9562, 14951, 15240, 18416, 16514, 18757, 20616, 22761, 22295, 24588, 24430, 24232, 23278, 22599, 21398, 20958, 21052};
  
  Double_t y_graph_numtrWhad[17]= {29020, 32918, 26724, 28301, 20202, 20090, 20252, 19384, 18299, 18365, 17103, 15632, 14084, 12927, 12932, 11698, 11265 };

  Double_t y_graph_numtrWlep[17]= {47551, 53692,43996, 45902, 32750, 34302, 34888, 35789, 34274, 36478, 34518, 33042, 30755, 29621, 27453, 26125, 25416}; */
  
  //Arrays using normalisation to all the events:
  /*Double_t x_graph[17]= {250,300,350,400,500,600,700,800,900,1000,1200,1400,1600,1800,2000,2500,3000};
  Double_t y_graph_den[17]= {83916, 91874, 73427, 76022, 53549, 54521, 54952, 54938, 52308, 54565, 51322, 48406, 44622, 42254, 40086, 37606, 36361};
  
  Double_t y_graph_numWhad[17]= {9957, 14576, 14246, 17248, 14639, 15758, 16723, 16483, 16090, 16057, 15061, 14301, 12732, 11854, 11829, 10948, 10630};
  
  Double_t y_graph_numWlep[17]= {9832, 15506, 16050, 19518, 17422, 20009, 22041, 24264, 23731, 26279, 26036, 25580, 24743, 23921, 22686, 22146, 22073};
  
  Double_t y_graph_numtrWhad[17]= {36541, 38482, 29703, 30486, 21021, 20499, 20372, 19428, 18337, 18302, 17015, 15538, 14012, 12846, 12805, 11658, 11125};

  Double_t y_graph_numtrWlep[17]= {47349, 53368, 43697, 45518, 32508, 34017, 34555, 35515, 33941, 36259, 34323, 32849, 30602, 29403, 27277, 25952, 25225}; 

  Double_t y_graph_numAcc[17]= {2311, 4879, 6094, 8323, 8197, 9684, 11368, 11552, 11508, 11526, 11157, 10490, 9448, 8889, 8867, 8513, 8369};
  Double_t y_graph_numAcc_2[17]= {1336, 2983, 4184, 5792, 6061, 7577, 9151, 9389, 9284, 9214, 8898, 8283, 7483, 7067, 7175, 7004, 7045};
  Double_t y_graph_numAcc_4[17]= {3252, 6477, 7794, 10360, 9665, 11158, 12841, 12802, 12791, 12877, 12289,11586, 10496, 9824, 9713, 9059, 8832};
  Double_t y_graph_numAcc_5[17]= {4139, 7825, 9296, 12088, 10900, 12224, 13744, 13614, 13534, 13644, 12932, 12149, 10997, 10330, 10148, 9400, 9099}; */
  //Double_t y_graph_numAcc_6[17]= {4071, 7942, 9165, 11665, 10224, 10780, 11867, 11361, 11397, 10874, 10064, 9158, 8335, 7374, 7095, 6342, 6058};

  //Arrays using normalisation to only the qqbb events:
  
  Double_t x_graph[17]= {250,300,350,400,500,600,700,800,900,1000,1200,1400,1600,1800,2000,2500,3000};
  Double_t y_graph_den[17]= {80022, 85147, 64553, 62323, 37567, 33633, 30995, 28072, 25137, 24444, 21188, 18199, 16117, 14606, 13444, 11736, 10744};
  
  Double_t y_graph_numWhad[17]= {8957, 13368, 12913, 15667, 12997, 13598, 14175, 13673, 13442, 13151, 12275, 11273, 10277, 9183, 9040, 8232, 7378};
  
  Double_t y_graph_numWlep[17]= {9737, 14539, 13751, 14351, 9379, 8069, 7598, 6789, 5655, 5480, 4421, 3591, 2934, 2707, 2118, 1709, 1424};
  
  Double_t y_graph_numtrWhad[17]= {33843, 35558, 27122, 27755, 18637, 17707, 17229, 16129, 15297, 14947, 13797, 12263, 11201, 9903, 9714, 8745, 8218};

  Double_t y_graph_numtrWlep[17]= {46153, 49567, 37405, 34550, 18914, 15922, 13736, 11940, 9819, 9487, 7402, 5925, 4906, 4697, 3718, 2995, 2514}; 

  Double_t y_graph_numAcc[17]= {1993, 4388, 5503, 7466, 7211, 8248, 9443, 9466, 9590, 9314, 8995, 8340, 7629, 6989, 6811, 6430, 6235};
  Double_t y_graph_numAcc_2[17]= {1132, 2647, 3741, 5178, 5322, 6395, 7451, 7639, 7676, 7468, 7135, 6615, 6082, 5675, 5605, 5423, 5308};
  Double_t y_graph_numAcc_4[17]= {2846, 5829, 7044, 9326, 8537, 9510, 10717, 10531, 10666, 10431, 9890, 9240, 8396, 7699, 7411, 6786, 6534};
  Double_t y_graph_numAcc_5[17]= {3667, 7051, 8433, 10907, 9656, 10451, 11533, 11215, 11313, 11033, 10409, 9659, 8783, 8051, 7720, 7023, 6716};
  
  //Double_t y_graph_numAcc_6[17]= {4071, 7942, 9165, 11665, 10224, 10780, 11867, 11361, 11397, 10874, 10064, 9158, 8335, 7374, 7095, 6342, 6058}; 

  //Double_t ey_graph[7]= {0, 0, 0.362,	0.393,	0.292,	0.087,	0.089};
  //Double_t ex_graph[13]= {0,0,0,0,0,0,0,0,0,0,0,0,0};
   //int n = 13;
  Double_t eff_numWhad[17];
  Double_t eff_numWlep[17];
  Double_t eff_numtrWhad[17];
  Double_t eff_numtrWlep[17]; 
  Double_t eff_numAcc[17];
  Double_t eff_numAccTr[17];
  Double_t eff_numAcc_2[17];
  Double_t eff_numAccTr_2[17];
  Double_t eff_numAcc_4[17];
  Double_t eff_numAccTr_4[17];
  Double_t eff_numAcc_5[17];
  Double_t eff_numAccTr_5[17];
  Double_t eff_numAcc_6[17];
  Double_t eff_numAccTr_6[17];
  Double_t eff_numWlepTrM[17];
  Double_t eff_numWhadTrM[17];


  for (int i=0; i<17; i++)
  {
   eff_numWhad[i]=y_graph_numWhad[i]/y_graph_den[i];
   eff_numWlep[i]=y_graph_numWlep[i]/y_graph_den[i];
   eff_numWlepTrM[i]=y_graph_numWlep[i]/y_graph_numtrWlep[i];
   eff_numWhadTrM[i]=y_graph_numWhad[i]/y_graph_numtrWhad[i];
   eff_numtrWhad[i]=y_graph_numtrWhad[i]/y_graph_den[i];
   eff_numtrWlep[i]=y_graph_numtrWlep[i]/y_graph_den[i];
   eff_numAcc[i] = y_graph_numAcc[i]/y_graph_den[i];
   eff_numAccTr[i] = y_graph_numAcc[i]/y_graph_numWhad[i];
   eff_numAcc_2[i] = y_graph_numAcc_2[i]/y_graph_den[i];
   eff_numAcc_4[i] = y_graph_numAcc_4[i]/y_graph_den[i];
   eff_numAcc_5[i] = y_graph_numAcc_5[i]/y_graph_den[i];
   //eff_numAcc_6[i] = y_graph_numAcc_6[i]/y_graph_den[i];
   eff_numAccTr_2[i] = y_graph_numAcc_2[i]/y_graph_numWhad[i];
   eff_numAccTr_4[i] = y_graph_numAcc_4[i]/y_graph_numWhad[i];
   eff_numAccTr_5[i] = y_graph_numAcc_5[i]/y_graph_numWhad[i];
   //eff_numAccTr_6[i] = y_graph_numAcc_6[i]/y_graph_numWhad[i];
  }
   
  TGraph *gr1  = new TGraphErrors(n, x_graph, eff_numWhad);
  TGraph *gr2  = new TGraph(n, x_graph, eff_numWlep);
  TGraph *gr3  = new TGraph(n, x_graph,eff_numtrWhad);
  TGraph *gr4  = new TGraph(n, x_graph,eff_numtrWlep);
  TGraph *gr5  = new TGraph(n, x_graph,eff_numAcc);
  TGraph *gr6  = new TGraph(n, x_graph,eff_numAccTr);
  TGraph *gr7  = new TGraph(n, x_graph,eff_numAcc_2);
  TGraph *gr8  = new TGraph(n, x_graph,eff_numAccTr_2);
  TGraph *gr9  = new TGraph(n, x_graph,eff_numAcc_4);
  TGraph *gr10  = new TGraph(n, x_graph,eff_numAccTr_4);
  TGraph *gr11  = new TGraph(n, x_graph,eff_numAcc_5);
  TGraph *gr12  = new TGraph(n, x_graph,eff_numAccTr_5);
  TGraph *gr13  = new TGraph(n, x_graph,eff_numWhadTrM);
  TGraph *gr14  = new TGraph(n, x_graph,eff_numWlepTrM);
  //TGraph *gr13  = new TGraph(n, x_graph,eff_numAcc_6);
  //TGraph *gr14  = new TGraph(n, x_graph,eff_numAccTr_6);
  //TGraph *gr5  = new TGraph(n, x_graph,y_graph_topcon275);
   TCanvas *c1 = new TCanvas("c1","Graph Draw Options",200,10,600,400);

   TLegend *leg=new TLegend(0.40,0.80,0.80,0.70);
  leg->SetBorderSize(0);
  leg->SetFillStyle(0);

   //c1->SetLogy();
   //gr3->SetLineColor(kGreen);
   gr3->SetLineColor(kBlack);
   gr3->SetLineWidth(2);
   gr3->SetMarkerColor(4);
   gr3->SetMarkerStyle(21);
   gr3->GetXaxis()->SetRangeUser(200,3400);
   gr3->GetYaxis()->SetRangeUser(0,1.0);
   gr3->GetXaxis()->SetTitle("#bf{H^{+} Mass Point [GeV]}");
   gr3->GetYaxis()->SetTitle("#bf{Fraction of Events with truth W#rightarrow qq + H#rightarrow bb}");
   //gr3->GetYaxis()->SetTitle("#bf{Fraction of Events with truth particle found}");
   
   gr1->SetLineColor(kBlack);
   gr1->SetLineWidth(2);
   gr1->SetMarkerColor(4);
   gr1->SetMarkerStyle(21);
   gr1->GetXaxis()->SetRangeUser(200,3400);
   gr1->GetYaxis()->SetRangeUser(0,1.0);
   gr1->GetXaxis()->SetTitle("#bf{H^{+} Mass Point [GeV]}");
   gr1->GetYaxis()->SetTitle("#bf{Truth matching efficiency}");

   gr2->SetLineColor(kRed);
   gr2->SetLineWidth(2);
   gr2->SetMarkerColor(4);
   gr2->SetMarkerStyle(21);
   gr2->GetXaxis()->SetRangeUser(200,3400);
   gr2->GetYaxis()->SetRangeUser(0,1.0);

   
   gr4->SetLineColor(kBlue);
   gr4->SetLineWidth(2);
   gr4->SetMarkerColor(4);
   gr4->SetMarkerStyle(21);
   gr4->GetXaxis()->SetRangeUser(200,3400);
   gr4->GetYaxis()->SetRangeUser(0,1.0);

   gr5->SetLineColor(kBlack);
   gr5->SetLineWidth(2);
   gr5->SetMarkerColor(4);
   gr5->SetMarkerStyle(21);
   gr5->GetXaxis()->SetRangeUser(200,3400);
   gr5->GetYaxis()->SetRangeUser(0,1.0);
   gr5->GetXaxis()->SetTitle("#bf{H^{+} Mass Point [GeV]}");
   gr5->GetYaxis()->SetTitle("#bf{Accuracy}");

   gr6->SetLineColor(kBlack);
   gr6->SetLineWidth(2);
   gr6->SetMarkerColor(4);
   gr6->SetMarkerStyle(21);
   gr6->GetXaxis()->SetRangeUser(200,3400);
   gr6->GetYaxis()->SetRangeUser(0,1.0);
   gr6->GetXaxis()->SetTitle("#bf{H^{+} Mass Point [GeV]}");
   gr6->GetYaxis()->SetTitle("#bf{Optimal Accuracy}");

   gr7->SetLineColor(kBlack);
   gr7->SetLineWidth(2);
   gr7->SetMarkerColor(4);
   gr7->SetMarkerStyle(21);
   gr7->GetXaxis()->SetRangeUser(200,3400);
   gr7->GetYaxis()->SetRangeUser(0,1.0);
   gr7->GetXaxis()->SetTitle("#bf{H^{+} Mass Point [GeV]}");
   gr7->GetYaxis()->SetTitle("#bf{Accuracy}");

   gr8->SetLineColor(kBlack);
   gr8->SetLineWidth(2);
   gr8->SetMarkerColor(4);
   gr8->SetMarkerStyle(21);
   gr8->GetXaxis()->SetRangeUser(200,3400);
   gr8->GetYaxis()->SetRangeUser(0,1.0);
   gr8->GetXaxis()->SetTitle("#bf{H^{+} Mass Point [GeV]}");
   gr8->GetYaxis()->SetTitle("#bf{Optimal Accuracy}");

   gr9->SetLineColor(kBlack);
   gr9->SetLineWidth(2);
   gr9->SetMarkerColor(4);
   gr9->SetMarkerStyle(21);
   gr9->GetXaxis()->SetRangeUser(200,3400);
   gr9->GetYaxis()->SetRangeUser(0,1.0);
   gr9->GetXaxis()->SetTitle("#bf{H^{+} Mass Point [GeV]}");
   gr9->GetYaxis()->SetTitle("#bf{Accuracy}");

   gr10->SetLineColor(kBlack);
   gr10->SetLineWidth(2);
   gr10->SetMarkerColor(4);
   gr10->SetMarkerStyle(21);
   gr10->GetXaxis()->SetRangeUser(200,3400);
   gr10->GetYaxis()->SetRangeUser(0,1.0);
   gr10->GetXaxis()->SetTitle("#bf{H^{+} Mass Point [GeV]}");
   gr10->GetYaxis()->SetTitle("#bf{Optimal Accuracy}");

   gr11->SetLineColor(kBlack);
   gr11->SetLineWidth(2);
   gr11->SetMarkerColor(4);
   gr11->SetMarkerStyle(21);
   gr11->GetXaxis()->SetRangeUser(200,3400);
   gr11->GetYaxis()->SetRangeUser(0,1.0);
   gr11->GetXaxis()->SetTitle("#bf{H^{+} Mass Point [GeV]}");
   gr11->GetYaxis()->SetTitle("#bf{Accuracy}");

   gr12->SetLineColor(kBlack);
   gr12->SetLineWidth(2);
   gr12->SetMarkerColor(4);
   gr12->SetMarkerStyle(21);
   gr12->GetXaxis()->SetRangeUser(200,3400);
   gr12->GetYaxis()->SetRangeUser(0,1.0);
   gr12->GetXaxis()->SetTitle("#bf{H^{+} Mass Point [GeV]}");
   gr12->GetYaxis()->SetTitle("#bf{Optimal Accuracy}");

   gr13->SetLineColor(kBlack);
   gr13->SetLineWidth(2);
   gr13->SetMarkerColor(4);
   gr13->SetMarkerStyle(21);
   gr13->GetXaxis()->SetRangeUser(200,3400);
   gr13->GetYaxis()->SetRangeUser(0,1.0);
   gr13->GetXaxis()->SetTitle("#bf{H^{+} Mass Point [GeV]}");
   gr13->GetYaxis()->SetTitle("#bf{Optimal truth matching efficiency}");

   gr14->SetLineColor(kRed);
   gr14->SetLineWidth(2);
   gr14->SetMarkerColor(4);
   gr14->SetMarkerStyle(21);
   gr14->GetXaxis()->SetRangeUser(200,3400);
   gr14->GetYaxis()->SetRangeUser(0,1.0);
   //gr14->GetXaxis()->SetTitle("#bf{H^{+} Mass Point [GeV]}");
   //gr14->GetYaxis()->SetTitle("#bf{Accuracy}");
   
   /*
   gr13->SetLineColor(kBlack);
   gr13->SetLineWidth(2);
   gr13->SetMarkerColor(4);
   gr13->SetMarkerStyle(21);
   gr13->GetXaxis()->SetRangeUser(200,3400);
   gr13->GetYaxis()->SetRangeUser(0,1.0);
   gr13->GetXaxis()->SetTitle("#bf{H^{+} Mass Point [GeV]}");
   gr13->GetYaxis()->SetTitle("#bf{Accuracy}");

   gr14->SetLineColor(kBlack);
   gr14->SetLineWidth(2);
   gr14->SetMarkerColor(4);
   gr14->SetMarkerStyle(21);
   gr14->GetXaxis()->SetRangeUser(200,3400);
   gr14->GetYaxis()->SetRangeUser(0,1.0);
   gr14->GetXaxis()->SetTitle("#bf{H^{+} Mass Point [GeV]}");
   gr14->GetYaxis()->SetTitle("#bf{Accuracy}");
   */



   //gr5->SetLineColor(kOrange);
   //gr5->SetLineWidth(2);
   //gr5->SetMarkerColor(4);
   //gr5->SetMarkerStyle(21);
   
   //gr1->SetLineColor(2);
   //gr1->SetLineWidth(2);
   //gr1->Draw("SAME");

   // gr3->SetLineColor(4);
   //gr3->SetLineWidth(2);
  // gr3->Draw("SAME");

   //leg->AddEntry(gr2,"#bf{Without b-tagging}","l");
   //gr2->Draw("AC");

   /*leg->AddEntry(gr3,"#bf{W#rightarrow qq + H#rightarrow bb}","l");
   leg->AddEntry(gr4,"#bf{W#rightarrow lv + H#rightarrow bb}","l");*/

   /*leg->AddEntry(gr13,"#bf{qqbb decay}","l");
   leg->AddEntry(gr14,"#bf{lvbb decay}","l"); */

   //leg->AddEntry(gr14,"#bf{Truth Matching for #Delta R < 0.6}","l");
   //leg->AddEntry(gr4,"#bf{Truth particle build from truth jets+dR(t1,t2)>0.8}","l");
   ////leg->AddEntry(gr5,"#bf{Pre-Sel+m(t_{l})<=275 GeV}","l");
   //leg->AddEntry(gr11,"#bf{Truth Matching for #Delta R < 0.5}","l");
   //gr1->Draw("AC");
   //gr2->Draw("SAME");
    //gr11->Draw("AC");
   //gr13->Draw("AC");
   //gr14->Draw("SAME");
   //gr14->Draw("AC");
   //gr2->Draw("SAME");
    gr13->Draw("AC");
    

   TLatex *tex0 = new TLatex();
  float lx ; float ly;
  lx=0.11;
  ly=0.85;
  tex0= new TLatex(lx,ly,"");
  tex0->SetNDC();
  tex0->SetTextSize(0.045*1.5);
  tex0->SetTextColor(1);
  tex0->SetTextFont(42);

  TLatex *tex1= new TLatex();
  lx=0.11;
  ly=0.82;
  tex1= new TLatex(lx,ly,""); 
  tex1->SetNDC();
  tex1->SetTextSize(0.025*1.2);
  tex1->SetTextColor(1);
  tex1->SetTextFont(42);

  TLatex *tex2= new TLatex();
  lx=0.30;
  ly=0.85;
  //tex2= new TLatex(lx,ly,"");
  tex2= new TLatex(lx,ly,"#bf{ATLAS Internal}");
  tex2->SetNDC();
  tex2->SetTextSize(0.030*1.5);
  tex2->SetTextColor(1);
  tex2->SetTextFont(42);

  TLatex *tex3= new TLatex();
  lx=0.30;
  ly=0.66 ;
  tex3= new TLatex(lx,ly,"#bf{Number Of Trees: 600, NCuts=40}");
  tex3->SetNDC();
  tex3->SetTextSize(0.030*1.5);
  tex3->SetTextColor(1);
  tex3->SetTextFont(42);

  TLatex *tex4= new TLatex();
  lx=0.30;
  ly=0.71;
  tex4= new TLatex(lx,ly,"#bf{Tree Depth = 5}");
  tex4->SetNDC();
  tex4->SetTextSize(0.030*1.5);
  tex4->SetTextColor(1);
  tex4->SetTextFont(42);

  //pad1->cd();
  //tex0->Draw("same");
  // tex1->Draw("same");
  //tex2->Draw("same");
  //tex3->Draw("same");
  //tex4->Draw("same");

  leg->Draw();

   // draw the graph with axis, continuous line, and put
   // a * at each point
   //gr->Draw("AC");


   //TAxis *axis2 =h89->GetXaxis();
   
   //int bmin = axis2->FindBin(xpt); 
   //int bmax2 =axis2->FindBin(1.5); 
   //sum2 = h89->Integral(bmin, bmax2);
   
   //std::cout<<sum2<<std::endl;
}
