# -*- coding: utf-8 -*-
#python
from ROOT import *
from array import array

gROOT.SetBatch(False)
gROOT.LoadMacro("../style/AtlasStyle.C")
gROOT.LoadMacro("../style/AtlasUtils.C")
gStyle.SetOptStat(0)
gStyle.SetOptTitle(0)
gStyle.SetPalette(1)


def NormalizeHisto(histo):
     n_events=histo.Integral(-1,histo.GetNbinsX()+1)
     if n_events == 0:
         return

     histo.Scale(1./n_events)
     histo.SetLineWidth(1)
     histo.SetStats(0)
     histo.SetFillStyle(3001)
     histo.SetMarkerColor(histo.GetLineColor())
     histo.SetMarkerSize(0.0)
     histo.GetXaxis().SetTitleOffset(0.9)
     histo.GetYaxis().SetTitleOffset(0.92)
     #histo.GetXaxis().SetLabelSize(0.05)
     #histo.GetYaxis().SetLabelSize(0.05)
     histo.GetXaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetNdivisions(504)
     histo.GetXaxis().SetNdivisions(504)
path="../output/"
fileName="TMVAResults_02_11_21_mHiggsCut.root"
TaggerName="WpH_Tagger_m250_m3000_28_10_21"

infile=TFile.Open(path+fileName,"READ")

test=infile.GetDirectory("test").GetDirectory("Method_BDT").GetDirectory(TaggerName)
#test.ls()
c1=TCanvas("","",900,550)
c1.SetTicks(1,1)
c1.Divide(3,2)

n=6

names={"pTWmvH":"p_{T}^{W}/m_{Wh}","pTHmvH":"p_{T}^{h}/m_{Wh}","btagjH1":"b-tag score jet 1","btagjH2":"b-tag score jet 2","H_mass":"mass_{higgs}","Phi_HW":"#Delta#Phi(Wh)","cosThetaStar":"cos(#Theta^{*})","Eta_HW":"#Delta#eta(Wh)"}
#Plot the distrbutions of the input variables
for variable in["pTWmvH","pTHmvH","btagjH1","btagjH2","H_mass","Phi_HW"]:
    c1.cd(n)
    c1.SetTicks(1,1)
    c1.SetBottomMargin(1)
    signal=test.Get(variable+"__Signal")
    background=test.Get(variable+"__Background")
    NormalizeHisto(signal)
    NormalizeHisto(background)
    signal.SetFillStyle(1001)
    signal.SetFillColor(38)
    signal.SetLineColor(4)
    signal.SetLineWidth(1)
    signal.GetXaxis().SetTitle(names[variable])
    signal.GetYaxis().SetTitle("Event Fraction")
    signal.SetName(names[variable])
    signal.SetTitle("Input variable: "+names[variable])
    
    background.GetXaxis().SetTitle(names[variable])
    background.GetYaxis().SetTitle("Event Fraction")
    
    background.SetName(names[variable])
    background.SetTitle("input variable: "+names[variable])

    background.SetFillStyle(3354)
    background.SetFillColor(2)
    background.SetLineColor(2)
    background.SetLineWidth(1)
    
    leg=TLegend(0.2,0.6,0.4,0.8)
    leg.SetFillStyle(0)
    leg.SetBorderSize(0)
    leg.SetFillColor(0)
    leg.AddEntry(signal,"Signal","f")
    leg.AddEntry(background,"Combinatorial background","f")
    leg.SetTextSize(0.045)
    leg.Draw()
    tbLabel=TLatex(0.5,.5,"tbH^{+}")
    tbLabel.Draw()
    #ATLAS_LABEL(0.42,0.82," Internal",1,0.12,0.035);  
    
    
    signal.Draw("HIST")
    background.Draw("HISTSAME")
    leg.Draw()
    if(n==1):
         ATLAS_LABEL(0.2,0.82,"  Internal",1,0.12,0.045); 
    c1.RedrawAxis()
    n-=1
   
    
c1.SaveAs("../Plots/BDT_inputvariables.pdf")
c1.SaveAs("../Plots/BDT_inputvariables.png")
c1.Clear()
leg.Clear()
#Plot the BDT Response
gStyle.SetOptTitle(0)
leg=TLegend(0.2,0.5,0.4,0.7)
leg.SetFillStyle(0)
leg.SetBorderSize(0)
leg.SetFillColor(0)
c1=TCanvas("","",1200,900)
c1.SetTicks(1,1)
signal=test.Get("MVA_"+TaggerName+"_S")
background=test.Get("MVA_"+TaggerName+"_B")
signal.GetXaxis().SetTitle("BDT response")
background.GetXaxis().SetTitle("BDT response")
background.GetYaxis().SetTitle("(1/N) dN/dx")
signal.SetFillStyle(1001)
signal.SetFillColor(38)
signal.SetLineColor(4)
signal.SetLineWidth(3)
background.SetFillStyle(3354)
background.SetFillColor(2)
background.SetLineColor(2)
background.SetLineWidth(3)
background.Draw("HIST")
signal.Draw("HISTSAME")
leg.AddEntry(signal,"Signal","f")
leg.AddEntry(background,"Combinatorial background","f")
leg.SetTextSize(0.045)
leg.Draw()
tbLabel=TLatex(0.5,4,"tbH^{+}")
tbLabel.Draw()
ATLAS_LABEL(0.2,0.82," Internal",1,0.12,0.05);
c1.RedrawAxis()
c1.SaveAs("../Plots/BDTResponse_newStyle.pdf")
c1.SaveAs("../Plots/BDTResponse_newStyle.png")
c1.Clear()

########################
#Draw plots for MultiBDT
n=6
multBDT_file=TFile.Open("TMVAResults_v3.root")
c1=TCanvas("","",1400,900)
c1.Divide(3,2)
leg=TLegend(0.6,0.7,0.8,0.9)
leg.SetFillStyle(0)
leg.SetBorderSize(0)
leg.SetFillColor(0)
var_name={"Lep_pT":"lepton p_{T}","pT_jet1":"leading jet p_{T}","dRbb_HiggsMass_85":"#Delta R(bb) Higgs mass","min_dPhi_vj":"min #Delta#Phi(#nu j)","MET":"E_{T}^{miss}","Mbb_MaxM_85":"m(bb) max m","Mlvj_MinPt":"m(lvj) Min p_{T}"}
maxmimums={"Lep_pT":0.25,"pT_jet1":0.2,"dRbb_HiggsMass_85":0.08,"min_dPhi_vj":0.1,"MET":0.2,"Mbb_MaxM_85":0.2,"Mlvj_MinPt":0.3}
for variable in ["Lep_pT","pT_jet1","dRbb_HiggsMass_85","Mlvj_MinPt","MET","Mbb_MaxM_85"]:
    max_y=-99
    c1.cd(n)
    c1.SetTicks(1,1)
    qqbb=multBDT_file.GetDirectory("dataset").GetDirectory("InputVariables_Id").Get(variable+"__qqbb_Id")
    lvbb=multBDT_file.GetDirectory("dataset").GetDirectory("InputVariables_Id").Get(variable+"__lvbb_Id")
    bkg=multBDT_file.GetDirectory("dataset").GetDirectory("InputVariables_Id").Get(variable+"__Bkg_Id")
    NormalizeHisto(qqbb)
    NormalizeHisto(lvbb)
    NormalizeHisto(bkg)
    if max_y<qqbb.GetMaximum():
            max_=qqbb.GetMaximum()
    if max_y<lvbb.GetMaximum():
            max_=qqbb.GetMaximum()
    if max_y<bkg.GetMaximum():
            max_=bkg.GetMaximum()
    qqbb.GetXaxis().SetTitle(var_name[variable])
    qqbb.GetYaxis().SetTitle("Event Fraction")
    qqbb.SetFillStyle(1001)
    qqbb.SetFillColor(38)
    qqbb.SetLineColor(4)
    qqbb.SetLineWidth(2)
    lvbb.SetFillStyle(3003)
    lvbb.SetFillColor(3)
    lvbb.SetLineColor(3)
    lvbb.SetLineWidth(2)
    bkg.SetFillStyle(3954)
    bkg.SetFillColor(2)
    bkg.SetLineColor(2)
    bkg.SetLineWidth(2)
    qqbb.GetYaxis().SetRangeUser(0,maxmimums[variable])
    qqbb.Draw("HIST")
    bkg.Draw("HISTSAME")
    lvbb.Draw("HISTSAME")
    leg.Clear()
    leg.AddEntry(qqbb,"H^{+}#rightarrow qqbb","f")
    leg.AddEntry(lvbb,"H^{+}#rightarrow l#nu bb","f")
    leg.AddEntry(bkg,"background","f")
    leg.SetTextSize(0.045)
    leg.Draw()
    n-=1
    c1.RedrawAxis()
    c1.SaveAs("plt/test.pdf")
    c1.SaveAs("plt/test.png")
c1.Clear()
####################PLot BDT response
c1=TCanvas("","",1200,900)
c1.SetTicks(1,1)
leg=TLegend(0.6,0.7,0.8,0.9)
leg.SetFillStyle(0)
leg.SetBorderSize(0)
leg.SetFillColor(0)
maxes={"qqbb":14,"lvbb":10,"Bkg":6}
reso_name={"qqbb":"H^{+}#rightarrow qqbb","lvbb":"H^{+}#rightarrow l#nu bb","Bkg":"background"}
for response in ["qqbb","lvbb","Bkg"]:
    lvbb=multBDT_file.GetDirectory("dataset").GetDirectory("Method_BDT").GetDirectory("WpH_Tagger_v2").Get("MVA_WpH_Tagger_v2_Test_"+response+"_prob_for_lvbb")
    qqbb=multBDT_file.GetDirectory("dataset").GetDirectory("Method_BDT").GetDirectory("WpH_Tagger_v2").Get("MVA_WpH_Tagger_v2_Test_"+response+"_prob_for_qqbb")
    bkg=multBDT_file.GetDirectory("dataset").GetDirectory("Method_BDT").GetDirectory("WpH_Tagger_v2").Get("MVA_WpH_Tagger_v2_Test_"+response+"_prob_for_Bkg")
    qqbb.SetFillStyle(1001)
    qqbb.SetFillColor(38)
    qqbb.SetLineColor(4)
    qqbb.SetLineWidth(2)
    lvbb.SetFillStyle(3003)
    lvbb.SetFillColor(3)
    lvbb.SetLineColor(3)
    lvbb.SetLineWidth(2)
    bkg.SetFillStyle(3954)
    bkg.SetFillColor(2)
    bkg.SetLineColor(2)
    bkg.SetLineWidth(2)
    qqbb.GetXaxis().SetTitle("Response for "+reso_name[response])
    qqbb.GetYaxis().SetTitle("(1/N) dN/dx")
    qqbb.GetYaxis().SetRangeUser(0,maxes[response])
    qqbb.Draw("HIST")
    bkg.Draw("HISTSAME")
    lvbb.Draw("HISTSAME")
    leg.AddEntry(qqbb,"H^{+}#rightarrow qqbb","f")
    leg.AddEntry(lvbb,"H^{+}#rightarrow l#nu bb","f")
    leg.AddEntry(bkg,"background","f")
    leg.Draw()
    leg.Clear()
    leg.AddEntry(qqbb,"H^{+}#rightarrow qqbb","f")
    leg.AddEntry(lvbb,"H^{+}#rightarrow l#nu bb","f")
    leg.AddEntry(bkg,"background","f")
   # c1.SaveAs("plt/multiResponse_reponse_for"+response+".pdf")
    #c1.SaveAs("plt/multiResponse_reponse_for"+response+".png")
    c1.Clear()
