# -*- coding: utf-8 -*-
import sys
import glob
import math
import re
from ROOT import *
from array import *
gROOT.SetBatch(True)
gStyle.SetOptStat(0)
gStyle.SetPalette(1)
#gROOT.LoadMacro("../style/AtlasStyle.C")
#gROOT.LoadMacro("../style/AtlasUtils.C")
gROOT.LoadMacro("../style/AtlasStyle.C")
gROOT.LoadMacro("../style/AtlasUtils.C")
#gROOT.LoadMacro("../utilis/AtlasLabels.C")
SetAtlasStyle()
#import ROOT
#import array

#gROOT.SetBatch(True)
#gStyle.SetOptStat(0)
#gStyle.SetPalette(1)
#gROOT.LoadMacro("../AtlasUtils/AtlasStyle.C")
#gROOT.LoadMacro("../AtlasUtils/AtlasUtils.C")
#gROOT.LoadMacro("../AtlasUtils/AtlasLabels.C")
#SetAtlasStyle()

#from array import *
def getTGraphs(): 
    "Function to read mass resolution from text files"
    graphs={}
    v_mass=array('d')
    v_Whadev_6j=array('d')
    v_trWhadev_6j=array('d')
    v_Whadev_7j=array('d')
    v_trWhadev_7j=array('d')
    v_Whadev_8j=array('d')
    v_trWhadev_8j=array('d')
    v_Whadev_9j=array('d')
    v_trWhadev_9j=array('d')
    v_Whadev_10j=array('d')
    v_trWhadev_10j=array('d')
    v_opteff_5j=array('d')
    v_opteff_6j=array('d')
    v_opteff_7j=array('d')
    v_opteff_8j=array('d')
    v_opteff_9j=array('d')
    v_opteff_10j=array('d')
    v_opteff_11j=array('d')
    v_opteff_12j=array('d')
    mass_list = [250.0, 300.0, 350.0, 400.0, 500.0, 600.0, 700.0, 800.0, 900.0, 1000.0, 1200.0, 1400.0, 1600.0, 1800.0, 2000.0, 2500.0, 3000.0]

    #######***********Arrays for qqbb events*****************
    #totev_list = [80022, 85147, 64553, 62323, 37567, 33633, 30995, 28072, 25137, 24444, 21188, 18199, 16117, 14606, 13444, 11736, 10744]
    #Whadev_list = [8957, 13368, 12913, 15667, 12997, 13598, 14175, 13673, 13442, 13151, 12275, 11273, 10277, 9183, 9040, 8232, 7378]
    #Wlepev_list = [9737, 14539, 13751, 14351, 9379, 8069, 7598, 6789, 5655, 5480, 4421, 3591, 2934, 2707, 2118, 1709, 1424]
    #trWhadev_list = [33843, 35558, 27122, 27755, 18637, 17707, 17229, 16129, 15297, 14947, 13797, 12263, 11201, 9903, 9714, 8745, 8218]
    #trWlepev_list = [46153, 49567, 37405, 34550, 18914, 15922, 13736, 11940, 9819, 9487, 7402, 5925, 4906, 4697, 3718, 2995, 2514]
    #acc03_list = [1993, 4388, 5503, 7466, 7211, 8248, 9443, 9466, 9590, 9314, 8995, 8340, 7629, 6989, 6811, 6430, 6235]
    
    #######***********Arrays normalised to total events*****************
    #totev_list = [83916, 91874, 73427, 76022, 53549, 54521, 54952, 54938, 52308, 54565, 51322, 48406, 44622, 42254, 40086, 37606, 36361]
    #Whadev_list = [9957, 14576, 14246, 17248, 14639, 15758, 16723, 16483, 16090, 16057, 15061, 14301, 12732, 11854, 11829, 10948, 10630]
    #Wlepev_list = [9832, 15506, 16050, 19518, 17422, 20009, 22041, 24264, 23731, 26279, 26036, 25580, 24743, 23921, 22686, 22146, 22073]
    #trWhadev_list = [36541, 38482, 29703, 30486, 21021, 20499, 20372, 19428, 18337, 18302, 17015, 15538, 14012, 12846, 12805, 11658, 11125]
    #trWlepev_list = [47349, 53368, 43697, 45518, 32508, 34017, 34555, 35515, 33941, 36259, 34323, 32849, 30602, 29403, 27277, 25952, 25225]
    #acc03_list = [2311, 4879, 6094, 8323, 8197, 9684, 11368, 11552, 11508, 11526, 11157, 10490, 9448, 8889, 8867, 8513, 8369]
    Whadev_list_5j = [2521, 3812, 3564, 4369, 3513, 3832, 4087, 4483, 4257, 4467, 4473, 4598, 4163, 4247, 4218, 4053, 3936]
    Whadev_list_6j = [5447, 8137, 7842, 9632, 7982, 8687, 9519, 9493, 9285, 9614, 9302, 8871, 8153, 7864, 7950, 7635, 7335]
    trWhadev_list_6j = [36541, 38482, 29703, 30486, 21021, 20499, 20372, 19428, 18337, 18302, 17015, 15538, 14012, 12846, 12805, 11658, 11125]
    Whadev_list_7j = [7388, 10892, 10511, 13081, 11000, 11960, 13051, 12946, 12605, 12962, 12173, 11654, 10715, 10001, 10128, 9393, 9273]
    trWhadev_list_7j = [36541, 38482, 29703, 30486, 21021, 20499, 20372, 19428, 18337, 18302, 17015, 15538, 14012, 12846, 12805, 11658, 11125]
    Whadev_list_8j = [8139, 11979, 11667, 14389, 12227, 13501, 14587, 14527, 14303, 14421, 13597, 12947, 11810, 11065, 10996, 10325, 10014]
    trWhadev_list_8j = [36541, 38482, 29703, 30486, 21021, 20499, 20372, 19428, 18337, 18302, 17015, 15538, 14012, 12846, 12805, 11658, 11125]
    Whadev_list_9j = [8324, 12139, 11912, 14815, 12637, 13978, 15006, 14957, 14750, 14920, 14065, 13503, 12113, 11389, 11290, 10624, 10349]
    trWhadev_list_9j = [36541, 38482, 29703, 30486, 21021, 20499, 20372, 19428, 18337, 18302, 17015, 15538, 14012, 12846, 12805, 11658, 11125]
    Whadev_list_10j = [8398, 12162, 11905, 14887, 12671, 14093,  15073, 15057, 14857, 15124, 14186, 13599, 12216, 11481, 11390, 10672, 10465]
    trWhadev_list_10j = [36541, 38482, 29703, 30486, 21021, 20499, 20372, 19428, 18337, 18302, 17015, 15538, 14012, 12846, 12805, 11658, 11125]
    Whadev_list_11j = [8417, 12193, 11889, 14900, 12663, 14099, 15094, 15081, 14872, 15123, 14192, 13605, 12182, 11495, 11408, 10661, 10456]
    Whadev_list_12j = [8419, 12183, 11889, 14903, 12662, 14090, 15063, 15069, 14870, 15137, 14193, 13600, 12159, 11478, 11390, 10687, 10450]

    for i in mass_list:
        v_mass.append(float(i))
    for k in Whadev_list_6j:
        v_Whadev_6j.append(float(k))
    for m in trWhadev_list_6j:
        v_trWhadev_6j.append(float(m))
    for k in Whadev_list_7j:
        v_Whadev_7j.append(float(k))
    for m in trWhadev_list_7j:
        v_trWhadev_7j.append(float(m))
    for k in Whadev_list_8j:
        v_Whadev_8j.append(float(k))
    for m in trWhadev_list_9j:
        v_trWhadev_9j.append(float(m))
    for k in Whadev_list_10j:
        v_Whadev_10j.append(float(k))
    for m in trWhadev_list_10j:
        v_trWhadev_10j.append(float(m))  
    for i,j in zip(trWhadev_list_6j, Whadev_list_5j):
        v_opteff_5j.append(float(j)/float(i))    
    for i,j in zip(trWhadev_list_6j, Whadev_list_6j):
        v_opteff_6j.append(float(j)/float(i))
    for i,j in zip(trWhadev_list_7j, Whadev_list_7j):
        v_opteff_7j.append(float(j)/float(i))
    for i,j in zip(trWhadev_list_8j, Whadev_list_8j):
        v_opteff_8j.append(float(j)/float(i))
    for i,j in zip(trWhadev_list_9j, Whadev_list_9j):
        v_opteff_9j.append(float(j)/float(i)) 
    for i,j in zip(trWhadev_list_10j, Whadev_list_10j):
        v_opteff_10j.append(float(j)/float(i))     
    for i,j in zip(trWhadev_list_6j, Whadev_list_11j):
        v_opteff_11j.append(float(j)/float(i))
    for i,j in zip(trWhadev_list_6j, Whadev_list_12j):
        v_opteff_12j.append(float(j)/float(i))    
    #v_mass= array([250.0, 300.0, 350.0, 400.0, 500.0, 600.0, 700.0, 800.0, 900.0, 1000.0, 1200.0, 1400.0, 1600.0, 1800.0, 2000.0, 2500.0, 3000.0])
    #v_totev=array([80022, 85147, 64553, 62323, 37567, 33633, 30995, 28072, 25137, 24444, 21188, 18199, 16117, 14606, 13444, 11736, 10744])     
    #print v_mass  
    #print v_totev  
    #print v_Whadev
    #print v_Wlepev
    #print v_trWhadev
    #print v_trWlepev
    #print v_acc03
    #print v_eff
    #graphs["Res_MVA07"]=TGraph(len(v_mass),v_mass, v_res_MVA07)
    #graphs["Res_MVA08"]=TGraph(len(v_mass),v_mass, v_res_MVA08)
    #graphs["Res_MVA09"]=TGraph(len(v_mass),v_mass, v_res_MVA09)
    graphs["v_opteff_5j"]=TGraph(len(v_mass),v_mass, v_opteff_5j)
    graphs["v_opteff_6j"]=TGraph(len(v_mass),v_mass, v_opteff_6j)
    graphs["v_opteff_7j"]=TGraph(len(v_mass),v_mass, v_opteff_7j)
    graphs["v_opteff_8j"]=TGraph(len(v_mass),v_mass, v_opteff_8j)
    graphs["v_opteff_9j"]=TGraph(len(v_mass),v_mass, v_opteff_9j)
    graphs["v_opteff_10j"]=TGraph(len(v_mass),v_mass, v_opteff_10j)
    graphs["v_opteff_11j"]=TGraph(len(v_mass),v_mass, v_opteff_11j)
    graphs["v_opteff_12j"]=TGraph(len(v_mass),v_mass, v_opteff_12j)
    return graphs

sample = {}
cmassres = TCanvas("Eff","Eff",800,600)
frame = TH1F("", "", 36, 200, 3200)
frame.SetMinimum(0.0)
frame.SetMaximum(1.6)
frame.Draw()
frame.GetXaxis().SetTitle("H^{+} mass [GeV]")
frame.GetXaxis().SetTitleOffset(1.1)
#frame.GetYaxis().SetTitle("#sigma(pp #rightarrow tbH^{#pm}) #times BR(H^{#pm}#rightarrow Wh) [pb]")
frame.GetYaxis().SetTitle("Optimal truth matching efficiency")
frame.GetXaxis().SetRangeUser(250,3000)

#cLimit.SetLogy()

#graphs=getTGraphs(options.inFile)
graphs=getTGraphs()
opteff_5j = graphs["v_opteff_5j"]
opteff_6j = graphs["v_opteff_6j"]
opteff_7j = graphs["v_opteff_7j"]
opteff_8j = graphs["v_opteff_8j"]
opteff_9j = graphs["v_opteff_9j"]
opteff_10j = graphs["v_opteff_10j"]
opteff_11j = graphs["v_opteff_11j"]
opteff_12j = graphs["v_opteff_12j"]

leg = TLegend(0.55, 0.9, 0.85, 0.65)
leg.SetFillColor(kWhite)
leg.SetBorderSize(0)
leg.SetLineColor(kWhite)
leg.SetTextFont(43)
leg.SetTextSize(20)
myText(0.20,0.80,1,"#sqrt{s}=13 TeV, 139 fb^{-1}")
#myText(0.20,0.75,1,"at least 5 jet, 3 b-tags and at least 4 b-tags")
#myText(0.20,0.75,1,"at least 5 jet, 3 b-tags")

#eff.SetLineColor(kBlack)
#eff.SetLineStyle(1)
#eff.SetLineWidth(3)
#eff.Draw("L")

#opt_eff.SetLineColor(kBlack)
#opt_eff.SetLineStyle(1)
#opt_eff.SetLineWidth(3)
#opt_eff.Draw("L")

#acc.SetLineColor(kBlack)
#acc.SetLineStyle(1)
#acc.SetLineWidth(3)
#acc.Draw("L")

#opt_acc.SetLineColor(kBlack)
#opt_acc.SetLineStyle(1)
#opt_acc.SetLineWidth(3)
#opt_acc.Draw("L")
opteff_5j.SetLineColor(kOrange)
opteff_5j.SetLineStyle(1)
opteff_5j.SetLineWidth(3)
opteff_5j.Draw("L")

opteff_6j.SetLineColor(kBlack)
opteff_6j.SetLineStyle(1)
opteff_6j.SetLineWidth(3)
opteff_6j.Draw("L")

opteff_7j.SetLineColor(kYellow)
opteff_7j.SetLineStyle(1)
opteff_7j.SetLineWidth(3)
opteff_7j.Draw("SAME L")

opteff_8j.SetLineColor(kRed)
opteff_8j.SetLineStyle(1)
opteff_8j.SetLineWidth(3)
opteff_8j.Draw("SAME L")

opteff_9j.SetLineColor(kGreen)
opteff_9j.SetLineStyle(1)
opteff_9j.SetLineWidth(3)
opteff_9j.Draw("SAME L")

opteff_10j.SetLineColor(kBlue)
opteff_10j.SetLineStyle(1)
opteff_10j.SetLineWidth(3)
opteff_10j.Draw("SAME L")

opteff_11j.SetLineColor(kMagenta)
opteff_11j.SetLineStyle(1)
opteff_11j.SetLineWidth(3)
opteff_11j.Draw("SAME L")

opteff_12j.SetLineColor(kCyan)
opteff_12j.SetLineStyle(1)
opteff_12j.SetLineWidth(3)
opteff_12j.Draw("SAME L")

gPad.RedrawAxis()
leg.AddEntry(opteff_5j,"upto five leading jets in p_{T}", "l")
leg.AddEntry(opteff_6j,"upto six leading jets in p_{T}", "l")
leg.AddEntry(opteff_7j,"upto seven leading jets in p_{T}","l")
leg.AddEntry(opteff_8j,"upto eight leading jets in p_{T}", "l")
leg.AddEntry(opteff_9j,"upto nine leading jets in p_{T}","l")
leg.AddEntry(opteff_10j,"upto ten leading jets in p_{T}", "l")
leg.AddEntry(opteff_11j,"upto eleven leading jets in p_{T}","l")
leg.AddEntry(opteff_12j,"upto twelve leading jets in p_{T}", "l")

leg.Draw()
ATLAS_LABEL(0.20,0.85,"Internal",1,0.19)
pdffilename="Training_jets.pdf"
cmassres.SaveAs(pdffilename)
