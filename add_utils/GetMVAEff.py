# -*- coding: utf-8 -*-
import sys
import glob
import math
import re
from ROOT import *
from array import *
gROOT.SetBatch(True)
gStyle.SetOptStat(0)
gStyle.SetPalette(1)
#gROOT.LoadMacro("../style/AtlasStyle.C")
#gROOT.LoadMacro("../style/AtlasUtils.C")
gROOT.LoadMacro("../style/AtlasStyle.C")
gROOT.LoadMacro("../style/AtlasUtils.C")
#gROOT.LoadMacro("../utilis/AtlasLabels.C")
SetAtlasStyle()
#import ROOT
#import array

#gROOT.SetBatch(True)
#gStyle.SetOptStat(0)
#gStyle.SetPalette(1)
#gROOT.LoadMacro("../AtlasUtils/AtlasStyle.C")
#gROOT.LoadMacro("../AtlasUtils/AtlasUtils.C")
#gROOT.LoadMacro("../AtlasUtils/AtlasLabels.C")
#SetAtlasStyle()

#from array import *
def getTGraphs(): 
    "Function to read mass resolution from text files"
    graphs={}
    v_mass = array('d')
    v_totev=array('d')
    v_Whadev=array('d')
    v_Wlepev=array('d') 
    v_trWhadev=array('d')
    v_trWlepev=array('d')
    v_acc03=array('d')
    v_acc03_v2=array('d')
    v_eff=array('d')
    v_opteff=array('d')
    v_opteff_v2=array('d')
    v_opteff_v3=array('d')
    v_acc=array('d')
    v_optacc=array('d')
    v_optacc_v2=array('d')
    v_optacc_v3=array('d')
    v_TrWhad=array('d') #Array for truth W hadronic
    v_TrWlep=array('d') #Array for truth W leptonic
    v_neglep=array('d')
    v_poslep=array('d')
    v_qqbb=array('d')
    v_lvbb=array('d')
    v_lecqqbb=array('d')
    v_leclvbb=array('d')
    v_genqqbb=array('d')
    v_genlvbb=array('d')
    v_efflecqqbb=array('d')
    v_effleclvbb=array('d')
    v_effgenqqbb=array('d')
    v_effgenlvbb=array('d')
    v_effqqbb=array('d')
    v_efflvbb=array('d')
    v_tplvbb=array('d')
    v_tpqqbb=array('d')
    v_eff_MVA07=array('d')
    v_eff_MVA08=array('d')
    v_eff_MVA09=array('d')
    v_eff_MVA095=array('d')
    mass_list = [250.0, 300.0, 350.0, 400.0, 500.0, 600.0, 700.0, 800.0, 900.0, 1000.0, 1200.0, 1400.0, 1600.0, 1800.0, 2000.0, 2500.0, 3000.0]

    #######***********Arrays for qqbb events*****************
    #totev_list = [80022, 85147, 64553, 62323, 37567, 33633, 30995, 28072, 25137, 24444, 21188, 18199, 16117, 14606, 13444, 11736, 10744]
    #Whadev_list = [8957, 13368, 12913, 15667, 12997, 13598, 14175, 13673, 13442, 13151, 12275, 11273, 10277, 9183, 9040, 8232, 7378]
    #Wlepev_list = [9737, 14539, 13751, 14351, 9379, 8069, 7598, 6789, 5655, 5480, 4421, 3591, 2934, 2707, 2118, 1709, 1424]
    #trWhadev_list = [33843, 35558, 27122, 27755, 18637, 17707, 17229, 16129, 15297, 14947, 13797, 12263, 11201, 9903, 9714, 8745, 8218]
    #trWlepev_list = [46153, 49567, 37405, 34550, 18914, 15922, 13736, 11940, 9819, 9487, 7402, 5925, 4906, 4697, 3718, 2995, 2514]
    #acc03_list = [1993, 4388, 5503, 7466, 7211, 8248, 9443, 9466, 9590, 9314, 8995, 8340, 7629, 6989, 6811, 6430, 6235]
    acc03_v2_list = [939, 2608, 3250, 4712, 4665, 5382, 6153, 6258, 6506, 6522, 6204, 5810, 5367, 4909, 4887, 4601, 4226]
    acc03_MVA07_list = [1981, 4165, 5227, 7022, 6743, 7782, 9005, 8902, 8963, 8767, 8150, 7251, 6375, 5645, 5242, 4615, 4283]
    acc03_MVA08_list = [1659, 3749, 4761, 6556, 6394, 7501, 8619, 8497, 8450, 8228, 7490, 6425, 5439, 4661, 4198, 3503, 3117]
    acc03_MVA09_list = [938, 2762, 3830, 5443, 5533, 6587, 7552, 7451, 7233, 6959, 6009, 4683, 3673, 2716,2215,1230,844]
    acc03_MVA095_list = [288, 1418, 2585, 4021, 4286, 5283, 6077, 5821, 5664, 5132, 4194, 3225, 2432, 1684, 1224, 679, 314]
    trWhadev_MVA07_list = [19647, 23567, 19590, 20358, 14752, 14254, 14425, 13389, 12650, 12351, 10873, 9321, 8183, 6919, 6488, 5461, 5064]
    trWhadev_MVA08_list = [14631, 18907, 16486, 17552, 13119, 12855, 13188, 12226, 11447, 11113, 9655, 8027, 6703, 5553, 5033, 4084, 3605]
    trWhadev_MVA09_list = [7282, 11600, 11160, 12626, 10173, 10087, 10532, 9864, 9211, 8805, 7345, 5586, 4316, 3141, 2621, 1426, 998]
    trWhadev_MVA095_list = [2339, 5257, 6303, 8093, 6951, 7322, 7663, 7024, 6684, 6031, 4809, 3622, 2707, 1842, 1394, 742, 379]
    #Whadev_v2_list = [5643, 8476, 7945, 10191, 8536, 9214, 9394, 9311, 9237, 9283, 8706, 8143, 7355, 6554, 6658, 6081, 5752]
    
    #######***********Arrays normalised to total events*****************
    totev_list = [83916, 91874, 73427, 76022, 53549, 54521, 54952, 54938, 52308, 54565, 51322, 48406, 44622, 42254, 40086, 37606, 36361]
    Whadev_list = [9957, 14576, 14246, 17248, 14639, 15758, 16723, 16483, 16090, 16057, 15061, 14301, 12732, 11854, 11829, 10948, 10630]
    Whadev_v2_list = [8428, 12182, 11886, 14900, 12658, 14090, 15047, 15065, 14870, 15132, 14196, 13595, 12148, 11474, 11387, 10695, 10457]
    Whadev_v3_list = [12137, 17444, 16378, 19321, 15761, 16527, 17340, 16887, 16446, 16371, 15306, 14430, 12807, 11900, 11870, 10990, 10634]
    Wlepev_list = [9832, 15506, 16050, 19518, 17422, 20009, 22041, 24264, 23731, 26279, 26036, 25580, 24743, 23921, 22686, 22146, 22073]
    trWhadev_list = [36541, 38482, 29703, 30486, 21021, 20499, 20372, 19428, 18337, 18302, 17015, 15538, 14012, 12846, 12805, 11658, 11125]
    trWlepev_list = [47349, 53368, 43697, 45518, 32508, 34017, 34555, 35515, 33941, 36259, 34323, 32849, 30602, 29403, 27277, 25952, 25225]
    acc03_list = [2311, 4879, 6094, 8323, 8197, 9684, 11368, 11552, 11508, 11526, 11157, 10490, 9448, 8889, 8867, 8513, 8369]
    qqbb_list = [80022.0, 85147.0, 64553.0, 62323.0, 37567.0, 33633.0, 30995.0, 28072.0, 25137.0, 24444.0, 21188.0, 18199.0, 16117.0, 14606.0, 13444.0, 11736.0, 10744.0]
    lvbb_list = [3894.0, 6727.0, 8874.0, 13699.0, 15982.0, 20888.0, 23957.0, 26866.0, 27171.0, 30121.0, 30134.0, 30207.0, 28505.0, 27648.0, 26642.0, 25870.0, 25617.0]
    lecqqbb_list = [37945.0, 39445.0, 30327.0, 30716.0, 20655.0, 19652.0, 19148.0, 17861.0, 17044.0, 16674.0, 15314.0, 13602.0, 12456.0, 11259.0, 10864.0, 9754.0, 9117.0]
    leclvbb_list = [911.0, 3448.0, 5883.0, 10456.0, 13134.0, 17549.0, 20210.0, 22942.0, 23253.0, 25807.0, 26057.0, 25998.0, 24814.0, 23814.0, 22725.0, 22051.0, 21653.0]
    genqqbb_list = [33843.0, 35558.0, 27122.0, 27755.0, 18637.0, 17707.0, 17229.0, 16129.0, 15297.0, 14947.0, 13797.0, 12263.0, 11201.0, 9903.0, 9714.0, 8745.0, 8218.0]
    genlvbb_list = [1196.0, 3801.0, 6292.0, 10968.0, 13594.0, 18095.0, 20819.0, 23575.0, 24122.0, 26772.0, 26921.0, 26924.0, 25696.0, 24706.0, 23559.0, 22957.0, 22711.0]
    neglep_list = [40928.0, 42724.0, 33318.0, 33959.0, 23503.0, 22991.0, 22895.0, 21785.0, 20962.0, 20988.0, 19391.0, 17811.0, 16147.0, 15093.0, 14781.0, 13573.0, 13081.0]
    poslep_list = [42988.0, 49150.0, 40109.0, 42063.0, 30046.0, 31530.0, 32057.0, 33153.0, 31346.0, 33577.0, 31931.0, 30595.0, 28475.0, 27161.0, 25305.0, 24033.0, 23280.0]

    for i in mass_list:
        v_mass.append(float(i))
    for j in totev_list:
        v_totev.append(float(j))
    for k in Whadev_list:
        v_Whadev.append(float(k))
    for l in Wlepev_list:
        v_Wlepev.append(float(l))    
    for m in trWhadev_list:
        v_trWhadev.append(float(m))
    for n in trWlepev_list:
        v_trWlepev.append(float(n))   
    for i in acc03_list:
        v_acc03.append(float(i))  
    for i,j in zip(totev_list, Whadev_list):  
        v_eff.append(float(j)/float(i))
    for i,j in zip(trWhadev_list, Whadev_list):
        v_opteff.append(float(j)/float(i))
    for i,j in zip(trWhadev_list, Whadev_v2_list):
        v_opteff_v2.append(float(j)/float(i)) 
    for i,j in zip(trWhadev_list, Whadev_v3_list):
        v_opteff_v3.append(float(j)/float(i))       
    for i,j in zip(totev_list, acc03_list):  
        v_acc.append(float(j)/float(i))
    for i,j in zip(Whadev_list, acc03_list):
        v_optacc.append(float(j)/float(i))
    for i,j in zip(Whadev_v2_list, acc03_v2_list):    
        v_optacc_v2.append(float(j)/float(i))   
    for i,j in zip(totev_list, trWhadev_list):
        v_TrWhad.append(float(j)/float(i))
    for i,j in zip(totev_list, trWlepev_list):
        v_TrWlep.append(float(j)/float(i))    
    for i,j in zip(poslep_list, leclvbb_list):
        v_effleclvbb.append(float(j)/float(i))     
    for i,j in zip(neglep_list, lecqqbb_list):
        v_efflecqqbb.append(float(j)/float(i))   
    for i,j in zip(trWhadev_list, genqqbb_list):
        v_effgenqqbb.append(float(j)/float(i))  
    for i,j in zip(trWlepev_list, genlvbb_list):
        v_effgenlvbb.append(float(j)/float(i))  
    for i,j in zip(lvbb_list, genlvbb_list):
        v_tplvbb.append(float(j)/float(i))  
    for i,j in zip(qqbb_list, genqqbb_list):
        v_tpqqbb.append(float(j)/float(i))   
    for i,j in zip(trWhadev_MVA07_list, acc03_MVA07_list):   
        v_eff_MVA07.append(float(j)/float(i)) 
    for i,j in zip(trWhadev_MVA08_list, acc03_MVA08_list):   
        v_eff_MVA08.append(float(j)/float(i)) 
    for i,j in zip(trWhadev_MVA09_list, acc03_MVA09_list):   
        v_eff_MVA09.append(float(j)/float(i)) 
    for i,j in zip(trWhadev_MVA095_list, acc03_MVA095_list):   
        v_eff_MVA095.append(float(j)/float(i))                 
    #v_mass= array([250.0, 300.0, 350.0, 400.0, 500.0, 600.0, 700.0, 800.0, 900.0, 1000.0, 1200.0, 1400.0, 1600.0, 1800.0, 2000.0, 2500.0, 3000.0])
    #v_totev=array([80022, 85147, 64553, 62323, 37567, 33633, 30995, 28072, 25137, 24444, 21188, 18199, 16117, 14606, 13444, 11736, 10744])     
    #print v_mass  
    #print v_totev  
    #print v_Whadev
    #print v_Wlepev
    #print v_trWhadev
    #print v_trWlepev
    #print v_acc03
    #print v_eff
    #graphs["Res_MVA07"]=TGraph(len(v_mass),v_mass, v_res_MVA07)
    #graphs["Res_MVA08"]=TGraph(len(v_mass),v_mass, v_res_MVA08)
    #graphs["Res_MVA09"]=TGraph(len(v_mass),v_mass, v_res_MVA09)
    graphs["TrWhad"]=TGraph(len(v_mass),v_mass, v_TrWhad)
    graphs["TrWlep"]=TGraph(len(v_mass),v_mass, v_TrWlep)
    graphs["Eff"]=TGraph(len(v_mass),v_mass, v_eff)
    graphs["Opt_Eff"]=TGraph(len(v_mass),v_mass, v_opteff)
    graphs["Opt_Eff_v2"]=TGraph(len(v_mass),v_mass, v_opteff_v2)
    graphs["Opt_Eff_v3"]=TGraph(len(v_mass),v_mass, v_opteff_v3)
    graphs["Accu"]=TGraph(len(v_mass),v_mass, v_acc)
    graphs["OptAccu"]=TGraph(len(v_mass),v_mass, v_optacc)
    graphs["OptAccu_v2"]=TGraph(len(v_mass),v_mass, v_optacc_v2)
    graphs["lecqqbb"]=TGraph(len(v_mass),v_mass, v_efflecqqbb)
    graphs["leclvbb"]=TGraph(len(v_mass),v_mass, v_effleclvbb)
    graphs["genqqbb"]=TGraph(len(v_mass),v_mass, v_effgenqqbb)
    graphs["genlvbb"]=TGraph(len(v_mass),v_mass, v_effgenlvbb)
    graphs["tplvbb"]=TGraph(len(v_mass),v_mass, v_tplvbb)
    graphs["tpqqbb"]=TGraph(len(v_mass),v_mass, v_tpqqbb)
    graphs["effMVA07"]=TGraph(len(v_mass),v_mass, v_eff_MVA07)
    graphs["effMVA08"]=TGraph(len(v_mass),v_mass, v_eff_MVA08)
    graphs["effMVA09"]=TGraph(len(v_mass),v_mass, v_eff_MVA09)
    graphs["effMVA095"]=TGraph(len(v_mass),v_mass, v_eff_MVA095)
    return graphs

sample = {}
cmassres = TCanvas("Eff","Eff",800,600)
frame = TH1F("", "", 36, 200, 3200)
frame.SetMinimum(0.0)
frame.SetMaximum(1.2)
frame.Draw()
frame.GetXaxis().SetTitle("H^{+} mass [GeV]")
frame.GetXaxis().SetTitleOffset(1.5)
#frame.GetYaxis().SetTitle("#sigma(pp #rightarrow tbH^{#pm}) #times BR(H^{#pm}#rightarrow Wh) [pb]")
#frame.GetYaxis().SetTitle("Leptonic top cut efficiency")
#frame.GetYaxis().SetTitle("Accuracy")
#frame.GetYaxis().SetTitle("Accuracy")
#frame.GetYaxis().SetTitle("Signal selection efficiency")
frame.GetYaxis().SetTitle("Matching efficiency")
frame.GetXaxis().SetRangeUser(250,3000)

#cLimit.SetLogy()

#graphs=getTGraphs(options.inFile)
graphs=getTGraphs()
eff = graphs["Eff"]
opt_eff = graphs["Opt_Eff"]
opt_eff_v2 = graphs["Opt_Eff_v2"]
opt_eff_v3 = graphs["Opt_Eff_v3"]
acc = graphs["Accu"]
opt_acc = graphs["OptAccu"]
opt_acc_v2 = graphs["OptAccu_v2"]
tr_Whad = graphs["TrWhad"]
tr_Wlep = graphs["TrWlep"]
lecqqbb = graphs["lecqqbb"]
leclvbb = graphs["leclvbb"]
genqqbb = graphs["genqqbb"]
genlvbb = graphs["genlvbb"]
tplvbb = graphs["tplvbb"]
tpqqbb = graphs["tpqqbb"]
effMVA07 = graphs["effMVA07"]
effMVA08 = graphs["effMVA08"]
effMVA09 = graphs["effMVA09"]
effMVA095 = graphs["effMVA095"]

leg = TLegend(0.55, 0.5, 0.85, 0.3)
leg.SetFillColor(kWhite)
leg.SetBorderSize(0)
leg.SetLineColor(kWhite)
leg.SetTextFont(43)
leg.SetTextSize(20)
myText(0.20,0.80,1,"H^{+}#rightarrow Wh #rightarrow qqbb")
#myText(0.20,0.75,1,"at least 5 jet, 3 b-tags and at least 4 b-tags")
#myText(0.20,0.75,1,"at least 5 jet, 3 b-tags")

#eff.SetLineColor(kBlack)
#eff.SetLineStyle(1)
#eff.SetLineWidth(3)
#eff.Draw("L")

#opt_eff.SetLineColor(kBlack)
#opt_eff.SetLineStyle(1)
#opt_eff.SetLineWidth(3)
#opt_eff.Draw("L")

#opt_eff_v2.SetLineColor(kBlue)
#opt_eff_v2.SetLineStyle(1)
#opt_eff_v2.SetLineWidth(3)
#opt_eff_v2.Draw("SAME L")

#opt_eff_v3.SetLineColor(kGreen)
#opt_eff_v3.SetLineStyle(1)
#opt_eff_v3.SetLineWidth(3)
#opt_eff_v3.Draw("SAME L")

#opt_acc.SetLineColor(kBlack)
#opt_acc.SetLineStyle(1)
#opt_acc.SetLineWidth(3)
#opt_acc.Draw("L")

#opt_acc_v2.SetLineColor(kBlack)
#opt_acc_v2.SetLineStyle(1)
#opt_acc_v2.SetLineWidth(3)
#opt_acc_v2.Draw("SAME L")

#acc.SetLineColor(kBlack)
#acc.SetLineStyle(1)
#acc.SetLineWidth(3)
#acc.Draw("L")

#opt_acc.SetLineColor(kBlack)
#opt_acc.SetLineStyle(1)
#opt_acc.SetLineWidth(3)
#opt_acc.Draw("L")

#tr_Whad.SetLineColor(kBlack)
#tr_Whad.SetLineStyle(1)
#tr_Whad.SetLineWidth(3)
#tr_Whad.Draw("L")

#tr_Wlep.SetLineColor(kBlue)
#tr_Wlep.SetLineStyle(1)
#tr_Wlep.SetLineWidth(3)
#tr_Wlep.Draw("SAME L")

#genqqbb.SetLineColor(kBlack)
#genqqbb.SetLineStyle(1)
#genqqbb.SetLineWidth(3)
#genqqbb.Draw("L")

#genlvbb.SetLineColor(kBlue)
#genlvbb.SetLineStyle(1)
#genlvbb.SetLineWidth(3)
#genlvbb.Draw("SAME L")

effMVA07.SetLineColor(kBlue)
effMVA07.SetLineStyle(1)
effMVA07.SetLineWidth(3)
effMVA07.Draw("L")

effMVA08.SetLineColor(kOrange)
effMVA08.SetLineStyle(1)
effMVA08.SetLineWidth(3)
effMVA08.Draw("SAME L")

effMVA09.SetLineColor(kRed)
effMVA09.SetLineStyle(1)
effMVA09.SetLineWidth(3)
effMVA09.Draw("SAME L")

effMVA095.SetLineColor(kGreen)
effMVA095.SetLineStyle(1)
effMVA095.SetLineWidth(3)
effMVA095.Draw("SAME L")

#tpqqbb.SetLineColor(kBlack)
#tpqqbb.SetLineStyle(1)
#tpqqbb.SetLineWidth(3)
#tpqqbb.Draw("L")

#tplvbb.SetLineColor(kBlue)
#tplvbb.SetLineStyle(1)
#tplvbb.SetLineWidth(3)
#tplvbb.Draw("SAME L")

gPad.RedrawAxis()

#leg.AddEntry(tpqqbb,"W #rightarrow q#bar{q'} + H #rightarrow b#bar{b}", "l")
#leg.AddEntry(tplvbb,"W #rightarrow l#nu + H #rightarrow b#bar{b}","l")

leg.AddEntry(effMVA07,"maxMVA Response> = 0.7", "l")
leg.AddEntry(effMVA08,"maxMVA Response> = 0.8","l")
leg.AddEntry(effMVA09,"maxMVA Response> = 0.9", "l")
leg.AddEntry(effMVA095,"maxMVA Response> = 0.95","l")

#leg.AddEntry(opt_eff,"Pole H + (quark quark) W", "l")
#leg.AddEntry(opt_eff_v2,"(b b) H + (quark quark) W","l")
#leg.AddEntry(opt_eff_v3,"Pole H + Pole W","l")

leg.Draw()
ATLAS_LABEL(0.20,0.85,"Internal",1,0.19)
#pdffilename="Signal_selection_eff_INT.pdf"
pdffilename="Matching_Efficiency_EB_INT.pdf"
cmassres.SaveAs(pdffilename)
