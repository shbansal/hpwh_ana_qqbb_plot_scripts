# -*- coding: utf-8 -*-
import sys
import glob
import math
import re
from ROOT import *
from array import *
gROOT.SetBatch(True)
gStyle.SetOptStat(0)
gStyle.SetPalette(1)
#gROOT.LoadMacro("../style/AtlasStyle.C")
#gROOT.LoadMacro("../style/AtlasUtils.C")
gROOT.LoadMacro("../style/AtlasStyle.C")
gROOT.LoadMacro("../style/AtlasUtils.C")
#gROOT.LoadMacro("../utilis/AtlasLabels.C")
SetAtlasStyle()
#import ROOT
#import array

#gROOT.SetBatch(True)
#gStyle.SetOptStat(0)
#gStyle.SetPalette(1)
#gROOT.LoadMacro("../AtlasUtils/AtlasStyle.C")
#gROOT.LoadMacro("../AtlasUtils/AtlasUtils.C")
#gROOT.LoadMacro("../AtlasUtils/AtlasLabels.C")
#SetAtlasStyle()

#from array import *
def getTGraphs(): 
    "Function to read mass resolution from text files"
    graphs={}
    v_cosba_tanb05 = array('d')
    v_BRWh_tanb05 = array('d')
    v_BRtb_tanb05 = array('d')
    v_cosba_tanb5 = array('d')
    v_BRWh_tanb5 = array('d')
    v_BRtb_tanb5 = array('d')
    v_cosba_tanb25 = array('d')
    v_BRWh_tanb25 = array('d')
    v_BRtb_tanb25 = array('d')

    cosba_tanb05 = [0.360001, 0.979796, 0.240000, 0.100004, 0.219999, 0.994987, 0.000000, 0.160000, 0.039992, 0.349999, 0.119999, 0.060006, 0.049984, 0.916515, 0.399989, 0.280000, 0.300001, 0.319999, 0.199999, 0.866025, 0.953939, 0.299997, 0.435890, 0.714143, 0.800000, 0.199980, 0.340001, 0.099974, 0.019999,
    0.600000, 0.249984, 0.139996, 0.449997, 0.079998, 0.179998, 0.380001, 0.259999, 0.499992, 0.149974]
    BRtb_tanb05 = [0.931112, 0.650773, 0.967240, 0.992703, 0.972059, 0.643771, 0.998225, 0.984212, 0.997338, 0.934555, 0.990294, 0.996230, 0.996840, 0.680375, 0.916661, 0.956518, 0.950642, 0.944440, 0.976502, 0.704407, 0.662789, 0.950643, 0.902824, 0.777653, 0.736188, 0.976506, 0.937924, 0.992707, 0.998003, 
    0.831705, 0.964697, 0.987462, 0.897185, 0.994685, 0.980556, 0.924018, 0.962056, 0.876379, 0.985892]
    BRWh_tanb05 = [0.067232, 0.348070, 0.031040, 0.005531, 0.026212, 0.355085, 0.000000, 0.014038, 0.000889, 0.063783, 0.007945, 0.001999, 0.001388, 0.318415, 0.081709, 0.041781, 0.047668, 0.053881, 0.021762, 0.294341, 0.336033, 0.047667, 0.095570, 0.220964, 0.262503, 0.021758, 0.060408, 0.005528, 0.000222, 
    0.166816, 0.033588, 0.010783, 0.101220, 0.003547, 0.017700, 0.074339, 0.036233, 0.122063, 0.012354]
    cosba_tanb5 = [0.139996, 0.160000, 0.300001, 0.179998, 0.449997, 0.240000, 0.000000, 0.100004, 0.349999, 0.600000, 0.994987, 0.249984, 0.199980, 0.360001, 0.019999, 0.219999, 0.299997, 0.039992, 0.199999, 0.979796, 0.916515, 0.119999, 0.714143, 0.435890, 0.380001, 0.319999, 0.340001, 0.079998, 0.099974, 
    0.499992, 0.866025, 0.399989, 0.149974, 0.953939, 0.049984, 0.060006, 0.280000, 0.800000, 0.259999]
    BRWh_tanb5 = [0.521532, 0.587416, 0.833483, 0.643098, 0.918447, 0.762099, 0.000000, 0.357409, 0.872005, 0.952430, 0.982162, 0.776561, 0.689842, 0.878164, 0.021760, 0.729127, 0.833480, 0.081683, 0.689884, 0.981614, 0.979043, 0.444706, 0.965944, 0.913546, 0.889269, 0.850634, 0.865395, 0.262495, 0.357272, 
    0.932901, 0.976587, 0.898969, 0.555734, 0.980624, 0.121999, 0.166845, 0.813441, 0.972673, 0.789896]
    BRtb_tanb5 = [0.477619, 0.411851, 0.166221, 0.356268, 0.081408, 0.237479, 0.998225, 0.641450, 0.127767, 0.047486, 0.017807, 0.223042, 0.309608, 0.121620, 0.976504, 0.270392, 0.166224, 0.916687, 0.309566, 0.018353, 0.020920, 0.554308, 0.033995, 0.086300, 0.110534, 0.149101, 0.134366, 0.736196, 0.641587, 
    0.066980, 0.023371, 0.100852, 0.443478, 0.019342, 0.876442, 0.831676, 0.186228, 0.027279, 0.209731]
    cosba_tanb25 = [0.160000, 0.179998, 0.199980, 0.099974, 0.866025, 0.340001, 0.249984, 0.079998, 0.953939, 0.449997, 0.149974, 0.139996, 0.499992, 0.100004, 0.049984, 0.916515, 0.219999, 0.979796, 0.240000, 0.800000, 0.060006, 0.119999, 0.299997, 0.039992, 0.280000, 0.380001, 0.000000, 0.435890, 0.199999, 
    0.019999, 0.319999, 0.399989, 0.360001, 0.300001, 0.259999, 0.714143, 0.600000, 0.349999, 0.994987]
    BRWh_tanb25 = [0.972673, 0.978283, 0.982333, 0.932871, 0.999042, 0.993817, 0.988622, 0.898970, 0.999210, 0.996461, 0.969014, 0.964602, 0.997131, 0.932909, 0.776475, 0.999145, 0.985357, 0.999251, 0.987667, 0.998877, 0.833511, 0.952429, 0.992072, 0.689799, 0.990910, 0.995044, 0.000000, 0.996229, 0.982337, 
    0.357366, 0.993025, 0.995525, 0.994481, 0.992072, 0.989472, 0.998592, 0.998006, 0.994163, 0.999274]
    BRtb_tanb25 = [0.027279, 0.021678, 0.017635, 0.067010, 0.000956, 0.006172, 0.011358, 0.100851, 0.000788, 0.003533, 0.030931, 0.035335, 0.002864, 0.066972, 0.223128, 0.000854, 0.014617, 0.000747, 0.012311, 0.001121, 0.166193, 0.047487, 0.007914, 0.309650, 0.009074, 0.004947, 0.998225, 0.003764, 0.017632, 
    0.641493, 0.006962, 0.004467, 0.005509, 0.007914, 0.010509, 0.001406, 0.001990, 0.005827, 0.000725]
    
    cosba_tanb05.sort()
    cosba_tanb5.sort()
    cosba_tanb25.sort()
    BRtb_tanb05.sort()
    BRtb_tanb5.sort()
    BRtb_tanb25.sort()
    BRWh_tanb05.sort()
    BRWh_tanb5.sort()
    BRWh_tanb25.sort()

    for i in cosba_tanb05:
        v_cosba_tanb05.append(float(i))
    for j in BRWh_tanb05:
        v_BRtb_tanb05.append(1-float(j))
    for k in BRWh_tanb05:
        v_BRWh_tanb05.append(float(k))
    for i in cosba_tanb5:
        v_cosba_tanb5.append(float(i))
    for j in BRWh_tanb5:
        v_BRtb_tanb5.append(1-float(j))
    for k in BRWh_tanb5:
        v_BRWh_tanb5.append(float(k))  
    for i in cosba_tanb25:
        v_cosba_tanb25.append(float(i))
    for j in BRWh_tanb25:
        v_BRtb_tanb25.append(1-float(j))
    for k in BRWh_tanb25:
        v_BRWh_tanb25.append(float(k))        
              
    #v_mass= array([250.0, 300.0, 350.0, 400.0, 500.0, 600.0, 700.0, 800.0, 900.0, 1000.0, 1200.0, 1400.0, 1600.0, 1800.0, 2000.0, 2500.0, 3000.0])
    #v_totev=array([80022, 85147, 64553, 62323, 37567, 33633, 30995, 28072, 25137, 24444, 21188, 18199, 16117, 14606, 13444, 11736, 10744])     
    #print v_mass  
    #print v_totev  
    #print v_Whadev
    #print v_Wlepev
    #print v_trWhadev
    #print v_trWlepev
    #print v_acc03
    #print v_eff
    #graphs["Res_MVA07"]=TGraph(len(v_mass),v_mass, v_res_MVA07)
    #graphs["Res_MVA08"]=TGraph(len(v_mass),v_mass, v_res_MVA08)
    #graphs["Res_MVA09"]=TGraph(len(v_mass),v_mass, v_res_MVA09)
    graphs["BR_Wh_tanb05"]=TGraph(len(v_cosba_tanb05),v_cosba_tanb05, v_BRWh_tanb05)
    graphs["BR_Wh_tanb5"]=TGraph(len(v_cosba_tanb5),v_cosba_tanb5, v_BRWh_tanb5)
    graphs["BR_Wh_tanb25"]=TGraph(len(v_cosba_tanb25),v_cosba_tanb25, v_BRWh_tanb25)

    graphs["BR_tb_tanb05"]=TGraph(len(v_cosba_tanb05),v_cosba_tanb05, v_BRtb_tanb05)
    graphs["BR_tb_tanb5"]=TGraph(len(v_cosba_tanb5),v_cosba_tanb5, v_BRtb_tanb5)
    graphs["BR_tb_tanb25"]=TGraph(len(v_cosba_tanb25),v_cosba_tanb25, v_BRtb_tanb25)

    return graphs

sample = {}
cmassres = TCanvas("Eff","Eff",800,600)
frame = TH1F("", "", 36, 0, 1.1)
frame.SetMinimum(0.0)
frame.SetMaximum(1.7)
frame.Draw()
frame.GetXaxis().SetTitle("cos(#beta-#alpha)")
frame.GetXaxis().SetTitleOffset(1.5)
#frame.GetYaxis().SetTitle("#sigma(pp #rightarrow tbH^{#pm}) #times BR(H^{#pm}#rightarrow Wh) [pb]")
#frame.GetYaxis().SetTitle("Leptonic top cut efficiency")
#frame.GetYaxis().SetTitle("Accuracy")
#frame.GetYaxis().SetTitle("Accuracy")
frame.GetYaxis().SetTitle("BR(H^{#pm} #rightarrow W^{#pm}h)")
#frame.GetYaxis().SetTitle("BR(H^{#pm} #rightarrow tb)")
frame.GetXaxis().SetRangeUser(0,1.1)

#cLimit.SetLogy()

#graphs=getTGraphs(options.inFile)
graphs=getTGraphs()
br_Wh_tanb05 = graphs["BR_Wh_tanb05"]
br_Wh_tanb5 = graphs["BR_Wh_tanb5"]
br_Wh_tanb25 = graphs["BR_Wh_tanb25"]

br_tb_tanb05 = graphs["BR_tb_tanb05"]
br_tb_tanb5 = graphs["BR_tb_tanb5"]
br_tb_tanb25 = graphs["BR_tb_tanb25"]



leg = TLegend(0.70, 0.65, 0.90, 0.85)
leg.SetFillColor(kWhite)
leg.SetBorderSize(0)
leg.SetLineColor(kWhite)
leg.SetTextFont(43)
leg.SetTextSize(20)
#myText(0.20,0.80,1,"#sqrt{s}=13 TeV, 139 fb^{-1}")
#myText(0.20,0.75,1,"at least 5 jet, 3 b-tags and at least 4 b-tags")
#myText(0.20,0.75,1,"at least 5 jet, 3 b-tags")
myText(0.20,0.75,1,"m(H^{#pm}) = 600 GeV")
myText(0.20,0.70,1,"2HDM, Type I")
myText(0.20,0.80,1,"work-in-progress")

br_Wh_tanb05.SetLineColor(kBlack)
br_Wh_tanb05.SetLineStyle(1)
br_Wh_tanb05.SetLineWidth(3)
br_Wh_tanb05.Draw("L")

br_Wh_tanb5.SetLineColor(kBlue)
br_Wh_tanb5.SetLineStyle(1)
br_Wh_tanb5.SetLineWidth(3)
br_Wh_tanb5.Draw("SAME L")

br_Wh_tanb25.SetLineColor(kGreen)
br_Wh_tanb25.SetLineStyle(1)
br_Wh_tanb25.SetLineWidth(3)
br_Wh_tanb25.Draw("SAME L")

#br_tb_tanb05.SetLineColor(kBlack)
#br_tb_tanb05.SetLineStyle(1)
#br_tb_tanb05.SetLineWidth(3)
#br_tb_tanb05.Draw("L")

#br_tb_tanb5.SetLineColor(kBlue)
#br_tb_tanb5.SetLineStyle(1)
#br_tb_tanb5.SetLineWidth(3)
#br_tb_tanb5.Draw("SAME L")

#br_tb_tanb25.SetLineColor(kGreen)
#br_tb_tanb25.SetLineStyle(1)
#br_tb_tanb25.SetLineWidth(3)
#br_tb_tanb25.Draw("SAME L")

#genlvbb.SetLineColor(kBlue)
#genlvbb.SetLineStyle(1)
#genlvbb.SetLineWidth(3)
#genlvbb.Draw("SAME L")

gPad.RedrawAxis()

#leg.AddEntry(lecqqbb,"W #rightarrow q#bar{q'} + H #rightarrow b#bar{b}", "l")
#leg.AddEntry(leclvbb,"W #rightarrow l#nu + H #rightarrow b#bar{b}","l")

leg.AddEntry(br_Wh_tanb05,"tan#beta = 0.5", "l")
leg.AddEntry(br_Wh_tanb5,"tan#beta = 5","l")
leg.AddEntry(br_Wh_tanb25,"tan#beta = 25","l")

leg.Draw()
#ATLAS_LABEL(0.20,0.85,"work-in-progress",1,0.19)
ATLAS_LABEL(0.20,0.85,"Simulation",1,0.19)
pdffilename="BR_HpWh_tanb.pdf"
cmassres.SaveAs(pdffilename)

