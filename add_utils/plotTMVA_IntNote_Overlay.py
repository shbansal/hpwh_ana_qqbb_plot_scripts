# -*- coding: utf-8 -*-
#python
from ROOT import *
from array import array

gROOT.SetBatch(False)
gROOT.LoadMacro("../style/AtlasStyle.C")
gROOT.LoadMacro("../style/AtlasUtils.C")
gStyle.SetOptStat(0)
gStyle.SetOptTitle(0)
gStyle.SetPalette(1)


def NormalizeHisto(histo):
     n_events=histo.Integral(-1,histo.GetNbinsX()+1)
     if n_events == 0:
         return

     histo.Scale(1./n_events)
     histo.SetLineWidth(1)
     histo.SetStats(0)
     histo.SetFillStyle(3001)
     histo.SetMarkerColor(histo.GetLineColor())
     histo.SetMarkerSize(0.0)
     histo.GetXaxis().SetTitleOffset(0.9)
     histo.GetYaxis().SetTitleOffset(0.92)
     #histo.GetXaxis().SetLabelSize(0.05)
     #histo.GetYaxis().SetLabelSize(0.05)
     histo.GetXaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetNdivisions(504)
     histo.GetXaxis().SetNdivisions(504)

path="../output/"
fileName_Sch1="hp3MP_77p_225_MVA.root"
fileName_Sch2="hp3MP_77p_225_Pole_MVA.root"
#TaggerName="WpH_Tagger_qqbb_17MP_wocosth"

infile_Sch1=TFile.Open(path+fileName_Sch1,"READ")
infile_Sch2=TFile.Open(path+fileName_Sch2,"READ")

TaggerName_Sch1="WpH_Tagger_qqbb_hp3MP_77p_225"
TaggerName_Sch2="WpH_Tagger_qqbb_hp3MP_77p_225_Pole"

test_Sch1=infile_Sch1.GetDirectory("dataset").GetDirectory("Method_BDT").GetDirectory(TaggerName_Sch1)
test_Sch2=infile_Sch2.GetDirectory("dataset").GetDirectory("Method_BDT").GetDirectory(TaggerName_Sch2)
#test.ls()
c1=TCanvas("","",500,500)
#c1.SetTicks(1,1)
#c1.Divide(3,2)

n=6

#names={"pTWmvH":"p_{T}^{W}/m_{Wh}","pTHmvH":"p_{T}^{h}/m_{Wh}","btagjH1":"b-tag score jet 1","btagjH2":"b-tag score jet 2","H_mass":"mass_{higgs}","Phi_HW":"#Delta#Phi(Wh)","cosThetaStar":"cos(#Theta^{*})","Eta_HW":"#Delta#eta(Wh)"}
names={"Wp_pT_D_mass_VH":"p_{T}^{W}/m_{Wh}","H_pT_D_mass_VH":"p_{T}^{h}/m_{Wh}","btagjH1":"b-tag score of jet 1 constituting Higgs","btagjH2":"b-tag score of jet 2 constituting Higgs", "btagjW1":"b-tag score of jet 1 constituting W","btagjW2":"b-tag score of jet 2 constituting W", "H_mass":"mass_{higgs}", "Wp_mass":"mass_{W}", "Phi_HW":"#Delta#Phi(Wh)","Eta_HW":"#Delta#eta(Wh)"}
#Plot the distrbutions of the input variables
for variable in["Wp_pT_D_mass_VH","H_pT_D_mass_VH","btagjH1","btagjH2","btagjW1","btagjW2","H_mass","Wp_mass","Phi_HW","Eta_HW"]:
#for variable in["Wp_pT_D_mass_VH","H_pT_D_mass_VH","btagjH1","btagjH2","btagjW1","btagjW2"]:
#for variable in["H_mass","Wp_mass","Phi_HW","Eta_HW"]:        
    c1.cd(n)
    c1.SetTicks(1,1)
    c1.SetBottomMargin(1)
    signal_Sch1=test_Sch1.Get(variable+"__Signal")
    signal_Sch2=test_Sch2.Get(variable+"__Signal")
    NormalizeHisto(signal_Sch1)
    NormalizeHisto(signal_Sch2)
    #signal.SetFillStyle(1001)
    #signal.SetFillColor(38)
    signal_Sch1.SetLineColor(kBlue)
    signal_Sch1.SetLineWidth(2)
    signal_Sch2.SetLineColor(kRed)
    signal_Sch2.SetLineWidth(2)
    signal_Sch1.GetXaxis().SetTitle(names[variable])
    signal_Sch1.GetYaxis().SetTitle("Event Fraction")
    signal_Sch1.SetName(names[variable])
    signal_Sch1.SetTitle("Input variable: "+names[variable])
    
    #background.GetXaxis().SetTitle(names[variable])
    #background.GetYaxis().SetTitle("Event Fraction")
    
    #background.SetName(names[variable])
    #background.SetTitle("input variable: "+names[variable])
    #background.SetLineColor(2)
    #background.SetLineWidth(1)
    
    leg=TLegend(0.55,0.65,0.955,0.855)
    leg.SetFillStyle(0)
    leg.SetBorderSize(0)
    leg.SetFillColor(0)
    leg.AddEntry(signal_Sch1,"(b b) H + (quark quark) W","L")
    leg.AddEntry(signal_Sch2,"Pole H + Pole W","L")
    leg.SetTextSize(0.0250)
    leg.Draw()
    tbLabel=TLatex(0.5,.5,"H^{+} #rightarrow qqbb")
    tbLabel.Draw()
    #ATLAS_LABEL(0.42,0.82," Internal",1,0.12,0.035);  
     
    signal_Sch1.GetYaxis().SetRangeUser(0.000, signal_Sch1.GetMaximum()*1.2)
    signal_Sch1.Draw("HIST")
    signal_Sch2.Draw("HISTSAME")
    leg.Draw()
    ATLAS_LABEL(0.1,0.825,"Simulation",1,0.20); 
    myText(0.1,0.790,1,"Internal")
    c1.RedrawAxis()
    c1.Update()
    c1.RedrawAxis()
    c1.SaveAs("../Plots/Train_Var_Overlay/ShapePlot_%s_qqbb_Kinem_PolewoPole_3MP.pdf" % (variable))

    
